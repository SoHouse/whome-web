(function(window, $) {
	Array.prototype.clean = function(deleteValue) {
		for (var i = 0; i < this.length; i++) {
			if (this[i] == deleteValue) {
				this.splice(i, 1);
				i--;
			}
		}
		return this;
	};
	Array.prototype.insert = function(index, item) {
		this.splice(index, 0, item);
		return this;
	};

	function removeElementFromArray(arr, item) {
		for (var i = 0; i < arr.length; i++) {
			if (item == arr[i]) {
				arr.splice(i, 1);
				return;
			}
		}
	}
	//Array.forEach implementation for IE support..
	//https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/forEach
	if (!Array.prototype.forEach) {
		Array.prototype.forEach = function(callback, thisArg) {
			var T, k;
			if (this == null) {
				throw new TypeError(" this is null or not defined");
			}
			var O = Object(this);
			var len = O.length >>> 0; // Hack to convert O.length to a UInt32
			if ({}.toString.call(callback) != "[object Function]") {
				throw new TypeError(callback + " is not a function");
			}
			if (thisArg) {
				T = thisArg;
			}
			k = 0;
			while (k < len) {
				var kValue;
				if (k in O) {
					kValue = O[k];
					callback.call(T, kValue, k, O);
				}
				k++;
			}
		};
	}
	
	var bucket = "lookin";
	var rootFolder = "ad";
	var domain = "http://7xokz9.com1.z0.glb.clouddn.com/";
	var fileMap = {};
	var inited = false;
	var dirinfo = function(bucket, prefix) {
		this.baseUrl += "?bucket=" + bucket;
		if (typeof prefix != "undefined") {
			this.prefix = prefix;
		}
		this.reset();
		//this.dir = this.prefix;
	};
	getuploadtoken();
	var uploadtoken;
	function getuploadtoken(){
		parent.window.hzh.api.request(0x03070201, "getUploadToken", {},function(res){
			if(res.response.code == 200){
				uploadtoken=res.response.data.result;
				//console.log(uploadtoken);
				//hzh.api.user.uploadtoken=uploadtoken;
			}
		});
	}
	
	var sdk = function(token, api, mbucket, folder, mdomain) {
		//this.qiniu=qiniu;
		this.token = token;

		this.api = api || window.parent.hzh.api;
		if (mbucket)
			bucket = mbucket;
		if (folder)
			rootFolder = folder;
		if (mdomain)
			domain = mdomain;
		this.currentDir = new dirinfo(bucket, rootFolder);
	};
	$.extend(sdk.prototype, {
		token: null,
		currentDir: null,
		api: null,
		root: null,
		lastDir: null,
		send: function(args) {
			console.log(args);
			switch (args.data.cmd) {
				case "open":

					return open(args, this);
				case "tree":

					return openTree(args, this);
				case "parents":
					return openParents(args, this);
				case "rename":
					return rename(args, this);
				case "paste":
					return paste(args, this);
				case "rm":
					return remove(args, this);
					/*				case "upload":
										return upload(args,this);*/
				case "mkdir":
					return mkdir(args, this);
			}
		},
		abort: function() {

		},
		upload: function(files, obj) {
			console.log(files);
			var dfrd = $.Deferred();
			var folder = fileMap[files.target];//当前文件夹
			if (!folder) {
				folder = this.lastDir;
			}
			if (folder == null || folder.isdir == false) {
				return dfrd.reject();
			}
			var file = files.input.files[0];//找到上传的文件
			var name = file.name
			var filename = folder.name + name;
			if(uploadtoken){
				doupload(files.input, filename, uploadtoken, function(data) {
					var nf = new File(filename, name, name2filehash(filename), folder, file.type, new Date().getTime(), file.size, false);
					folder.subs.push(nf);
					dfrd.resolve({
						added: [createElFinderFile(nf)]
					});
					obj.sync();//刷新
				});
			}else{
				getuploadtoken();
			}
			
			/*			this.uploader.addFile(files.input,folder.name+files.input.value.split("\\").reverse()[0]);
						this.uploader.start();*/

			return dfrd;
		}
	});
	
	
	function init() {
		if (inited) return;
		inited = true;
		uploader.setOption("browse_button", ['hzhuploadbtn', 'hzhuploadpop','newsuploadbtn'], false);
	};

	function doupload(fileinput, filename, token, cb) {//七牛上传
		var form = $('<form id="picForm" method="post" action="http://upload.qiniu.com/" enctype="multipart/form-data"></form>');
		$('<input name="key" type="hidden" />').val(filename).appendTo(form);
		$(fileinput).attr("name", "file").appendTo(form);
		$('<input name="token" type="hidden" />').val(token).appendTo(form);
		form.ajaxSubmit({
			success: function(data) { // data 保存提交后返回的数据，一般为 json 数据
				cb(data);
			}

		});
	};

	function mkdir(args, sdk) {//encode64  decode64
		var dfrd = $.Deferred();
		var folder = findFile(sdk.root.subs, args.data.target);
		/*var name = sdk.currentDir.dblclick(args.data.name);
		console.log(name);*/
//		var fakeDir = folder.name + name + "_";
		var fakeDir = folder.name + args.data.name + "_";
		var fakeFile = fakeDir + "tmp.fkdir";
//		console.log(fakeFile);
		if(uploadtoken){
			doupload($("<input type='file' />"), fakeFile, uploadtoken, function(data) {
			//if succeed
				var nf = new File(fakeDir, args.data.name, "l1_" + fakeDir, folder, "directory", 0, 0, true);
				fileMap[nf.hash] = nf;
				folder.subs.push(nf);
				var ret = {
					added: [createElFinderFolder("l1_" + fakeDir, args.data.name, folder.hash)]
				};
				console.log(ret);
				dfrd.resolve(ret);
			});
		}else{
			getuploadtoken();
			mkdir(args, sdk);
		}
		return dfrd;
	};

	/*
	 * 粘贴操作
	 * 
	 * */

	function paste(args, sdk) {
		var dfrd = $.Deferred();
		var copyCmd = args.data.targets;
		var renameCmd = args.data.renames;
		var processed = 0;
		var srcFd = findFile(sdk.root.subs, args.data.src);
		var dstFd = findFile(sdk.root.subs, args.data.dst);
		//var rename= srcFd == dstFd;
		var cut = args.data.cut;
		var copys = [];
		var moves = [];
		var deletes = [];
		var added = [];
		var removedHash = [];
		//粘贴操作
		copyCmd.forEach(
			function(target) {
				var file = findFile(sdk.root.subs, target); //找到粘贴的文件
				if (file) {
					var src = file.name; //找到文件的名称(文件原来的地址)
					var dest = dstFd.name + file.display; //目标地址（新文件的名称）
					if (cut) { //剪切操作，要删除原文件
						moves.push({
							src: src,
							dst: dest
						});
						removedHash.push(file.hash); //就删除原来的地址（hash）
					} else { //复制操作
						copys.push({
							src: src,
							dst: dest
						});
					}
					var newFile = {};
					$.extend(true, newFile, file);
					newFile.name = dest;
					newFile.hash = name2filehash(dest);
					added.push(newFile);
				}
			});
		//重命名操作,将同名的原文件的名字改成另外的
		renameCmd.forEach(function(target) {

			var src = dstFd.name + target; //src为新的文件的name，dstFd为文件夹
			var dst = dstFd.name + args.data.suffix + target; //重复文件，dst为重复文件的名称
			var srcHash = name2filehash(src);
			var srcFile = findFile(sdk.root.subs, srcHash); //找到重名的文件

			if (srcFile) {
				moves.push({
					src: src,
					dst: dst
				});
				removedHash.push(srcHash);
				srcFile.hash = name2filehash(dst); //将重名的文件的hash
				srcFile.name = dst;
				srcFile.display = args.data.suffix + target;
				added.push(srcFile);
			}
		});
		batchFileOperation(copys, moves, deletes, sdk.api, function(res) {
			//console.log(res);
			if (res.response.data.code == "200") {
				removedHash.forEach(function(e) {
					var rf = findFile(sdk.root.subs, e);
					if (e.parent) {
						removeElementFromArray(e.parent.subs, rf);
					}
				});
				var addedEl = [];
				added.forEach(function(e) {
					var parr = e.name.split("_");
					parr.splice(parr.length - 1, 1, "");
					var phash = "l1_" + parr.join("_");
					var pf = findFile(sdk.root.subs, phash);
					if (pf == null) {
						pf = sdk.root;
					}
					if (pf) {
						pf.subs.push(e);
						e.parent = pf;
					}
					addedEl.push(createElFinderFile(e));
				});
				var ret = {
					added: addedEl,
					removed: removedHash
				};
				console.log(ret);
				dfrd.resolve(ret);
			}else{
				window.parent.hzh.alert(res.response.data);
			}
		});
		return dfrd;
	};



	function name2filehash(name) {
		return ("l1_" + name.replace(".", "-_-").replace("~", "_-_"));
	};

	function filehash2name(hash) {
		return hash.replace("l1_", "").replace("-_-", ".").replace("_-_", "~");
	};


	/*
	 *删除操作
	 * 1、根据传入的target找到要删除的文件
	 * 2、拼出地址
	 * 3、移除文件操作
	 * */
	function removeFile(sdk, hash, cb) {
		var deletes = [];
		var removedHash = [];
		//var hash=name2filehash(name);
		var file = findFile(sdk.root.subs, hash);//找到要删除的文件或文件夹
		/*删除文件时的操作*/
		if (file && file.isdir == false) {
			batchFileOperation(null, null, [{src: file.name}], sdk.api, function(res) {
				if (file.parent) {
					removeElementFromArray(file.parent.subs, file);//删除文件
				}
				delete fileMap[file.hash];//删除hash
				cb(res.response.data.code == "200", hash);//成功就将hash扔给ret
			});
			return;
		}
		//删除文件夹的操作
		var name = file ? file.name : hash;
		sdk.currentDir.openDir(name, true);//设置当前目录
		sdk.api.request(0x03070201, "qiniuHttp", {
			method: "post",
			url: sdk.currentDir.generateUrl()
		}, function(res) {
			if (res.response.code == 200) {
				var data = res.response.data;
				if (data.code == "200") { //succeed
					var body = $.parseJSON(data.body);
					//console.log("open response:" + data.body);
					if (body.items.length == 0) {//文件已经删除完时
						if (body.commonPrefixes) {//文件夹下的处理
							removeFiles(sdk, body.commonPrefixes, function(dirs) {
								$.each(dirs, function(i, e) {//遍历
									var hash = name2filehash(e);
									delete fileMap[hash];//从全局fileMap中删除dirs
								});
								cb(true, name);
							});
						} else {
							cb(true, name)
						}
					} else {//先删除文件
						//items存在的情况处理
						$.each(body.items, function(i, d) {
							deletes.push({
								src: d.key
							});
							/*  if(d.key.indexOf("tmp.fkdir")>=0){
	                            removedHash.push("l1_" +d.key.replace("tmp.fkdir",""));
							                        }else{*/
							removedHash.push(name2filehash(d.key));//将删除文件的hashpush到removeHash中
							/*}*/
						});
						batchFileOperation(null, null, deletes, sdk.api, function(res) {//批量删除文件操作
							//console.log("batch remove items:" + res.response.data)
							if (res.response.data.code == "200") {
								removedHash.forEach(function(e) {
									var rf = findFile(sdk.root.subs, e); //根据hash找到要删除的文件
									if (rf) {
										delete fileMap[e];
									}
									if (rf && rf.parent) {
										removeElementFromArray(rf.parent.subs, rf);
									}
								});
							}
							removeFile(sdk, name, cb);
						});
					}
				}else{
                    window.parent.hzh.alert("删除失败!");
				}
			}
		});
	};

	function removeFiles(sdk, files, cb) {//对移除文件的操作
		var finished = 0;
		var count = files.length;
		$.each(files, function(i, e) {
			removeFile(sdk, e, function(success, file) {
				finished++;
				if (finished == count) {
					cb(files);
				}
			});
		});
	};

	function remove(args, sdk) {
		var dfrd = $.Deferred();
		removeFiles(sdk, args.data.targets, function(files) {
			var ret = {
				removed: files
			};
			dfrd.resolve(ret);
		});
		return dfrd;
	};

	/*
	 * 批量操作文件
	 * */
	function batchFileOperation(copys, moves, deletes, api, cb) {//bG9va2luOmFkX+i/h+WIhl90bXAuZmtkaXI=
		function concact(operation, src, dst) { //将三个参数连接起来
			var source64 = urlsafebase64_encode(utf16to8(bucket + ":" + src)); //文件原来的地址，转换成七牛所需的格式
			
			var ret = "op=/" + operation + "/" + source64; //
			if (dst) { //删除操作没有dst
				var dest64 = urlsafebase64_encode(utf16to8(bucket + ":" + dst)); //dst文件的目标地址，转换成七牛所需的格式
				ret = ret + "/" + dest64;
			}  
			return ret;
		};
		var url = "http://rs.qiniu.com/batch"; //批量操作请求的地址
		var operations = [];
		if (deletes && deletes.length > 0) { //删除操作
			deletes.forEach(function(e) {
				operations.push(concact("delete", e.src));
			});
		}
		if (moves && moves.length > 0) { //移动操作
			moves.forEach(function(e) {
				operations.push(concact("move", e.src, e.dst));
			});
		}
		if (copys && copys.length > 0) { //复制
			copys.forEach(function(e) {
				operations.push(concact("copy", e.src, e.dst));
			});
		}
		var str = operations.join("&");
		api.request(0x03070201, "qiniuHttp", {
			method: "post",
			url: url,
			body: str
		}, function(res) {
			console.log(res);
			cb(res);
		});
	};
		
		
	function renameFile(sdk, hash,newName,root, cb) {
		//var deletes = [];
		//var removedHash = [];
		//var hash=name2filehash(name);
		var file = findFile(sdk.root.subs, hash);//找到要删除的文件或文件夹
		root=root||file.getPname();
		/*删除文件时的操作*/
		if (file && file.isdir == false) {
			var fileNameSplits=file.name.split("_");
			var rootSplits=root?root.split("_"):[];
			fileNameSplits[rootSplits.length-1]=newName;
			var newFileName=fileNameSplits.join("_");
			batchFileOperation(null,[{src: file.name,dst:newFileName}], null, sdk.api, function(res) {
				if (file.parent) {
					removeElementFromArray(file.parent.subs, file);//删除文件
				}
				
				delete fileMap[file.hash];//删除hash
				file.hash=name2filehash(newFileName);
				fileMap[file.hash]=file;
				cb(res.response.data.code == "200", hash,file);//成功就将hash扔给ret
			});
			return;
		}
		//删除文件夹的操作
		var name = file ? file.name : hash;
		sdk.currentDir.openDir(name, true);//设置当前目录
		sdk.api.request(0x03070201, "qiniuHttp", {
			method: "post",
			url: sdk.currentDir.generateUrl()
		}, function(res) {
			if (res.response.code == 200) {
				var data = res.response.data;
				if (data.code == "200") { //succeed
					var body = $.parseJSON(data.body);
					//console.log("open response:" + data.body);
					if (body.items.length == 0) {//文件已经删除完时
						if (body.commonPrefixes) {//文件夹下的处理
							renameFiles(sdk, body.commonPrefixes, newName,root,function(dirs) {
								$.each(dirs, function(i, e) {//遍历
									var hash = name2filehash(e);
									delete fileMap[hash];//从全局fileMap中删除dirs
								});
								cb(true, hash,file);
							});
						} else {
							cb(true, hash,file)
						}
					} else {//先删除文件
						//items存在的情况处理
						var renames=[];
						$.each(body.items, function(i, d) {
							var fileNameSplits=d.key.split("_");
							var rootSplits=root?root.split("_"):[];
							fileNameSplits[rootSplits.length-1]=newName;
							var newFileName=fileNameSplits.join("_");
							renames.push({
								src: d.key,
								dst:newFileName
							});
							/*  if(d.key.indexOf("tmp.fkdir")>=0){
	                            removedHash.push("l1_" +d.key.replace("tmp.fkdir",""));
							                        }else{*/
							//removedHash.push(name2filehash(d.key));//将删除文件的hashpush到removeHash中
							/*}*/
						});
						batchFileOperation(null, renames, null, sdk.api, function(res) {//批量删除文件操作
							//console.log("batch remove items:" + res.response.data)
							if (res.response.data.code == "200") {
								renames.forEach(function(e) {
									var rf = findFile(sdk.root.subs, e.src); //根据hash找到要删除的文件
								if (rf) {
										delete fileMap[e];
										if (rf.parent) {
											removeElementFromArray(rf.parent.subs, rf);
										}
									}
								});
							}
							renameFile(sdk, hash,newName,root, cb);
						});
					}
				}else{
                    window.parent.hzh.alert("删除失败!");
				}
			}
		});
	};

	
	function renameFiles(sdk, files, newName,root,cb) {//对移除文件的操作
		var finished = 0;
		var count = files.length;
		$.each(files, function(i, e) {
			renameFile(sdk, e, newName,root,function(success, hash,file) {
				finished++;
				if (finished == count) {
					cb(files);
				}
			});
		});
	};
	function rename(args, sdk){
		var dfrd = $.Deferred();
		var name=args.data.name;
		var fileHash=args.data.target;
		renameFile(sdk,fileHash,name , null,function(success,hash,file) {
/*			var df = findFile(sdk.root.subs, file);
			df.display=name;
			df.name=newName;*/
			var fileNameSplits=filehash2name(fileHash).split("_");
			var rootSplits=file.getPname().split("_");
			fileNameSplits[rootSplits.length-1]=name;
			var newFileName=fileNameSplits.join("_");
			file.hash=name2filehash(newFileName);
			file.display=name;
			file.name=newFileName;
			var ret = {
				added:[file.isdir?createElFinderFolder(file.hash,name,file.getPhash()):createElFinderFile(file)],
				removed: [hash]
			};
			console.log(ret);
			dfrd.resolve(ret);
		});
		return dfrd;
	};
	
	/*
	 * 重命名操作
	 * */
	/*function renameFile(file, hash, sdk, count, newname, cb) {
    	var moves=[];
        var itemnewFile;
        var filelength=0;
        var ret={};
        var name = file ? file.name:hash;
        sdk.currentDir.openDir(name, true);//设置当前目录
        sdk.api.request(0x03070201, "qiniuHttp", {
            method: "post",
            url: sdk.currentDir.generateUrl()
        }, function (res) {
            if (res.response.code == 200) {
                var data = res.response.data;
                if (data.code == "200") { //succeed
                    var body = $.parseJSON(data.body);
                    console.log(body);
//                  if (body.items.length != filelength) {
                    	$.each(body.items, function (i, d) {
                    		filelength++;
                            var itemsArr = d.key.split("_"); //将原来的名字按照_拆分
                            var nameArr = name.split("_");
                            var n = nameArr.length - count - 1;
                            itemnewFile = itemsArr.clean(nameArr[n]).insert(n,newname).join("_");
                            console.log(itemnewFile);
                            moves.push({
                                src: d.key,
                                dst: itemnewFile
                            });
                        });
                        batchFileOperation(null, moves, null, sdk.api, function (res) {//批量删除文件操作
	                        if (res.response.data.code == "200") {
	                        	var add=[];
	                        	for(var i in moves){
	                        		var newfile=moves[i].dst;
	                        		console.log(newfile);
	                        		if(newfile){
	                        			if(newfile.indexOf("tmp.fkdir") >= 0){
	                        			//file.name = newfile; //将新的文件name覆盖原来文件的name
				                            var newfolder = newfile.split("_");
				                            var newfoldname = newfolder[newfolder.length - 2];
				                            //file.display=newfoldname;
				                            var newhash= name2filehash(newfolder.clean("tmp.fkdir").join("_"))+"_";
				                            file.hash=newhash;
				                            var newphash = newhash.split("_").clean(newfoldname).join("_");
				                            if(newphash == "l1_ad_") newphash="l1_XA";
				                            console.log(file);
				                            add.push(createElFinderFolder(newhash,newfoldname,newphash));
		                        		}else{
		                        			file.name = newfile; //将新的文件name覆盖原来文件的name
				                            var newfolder = newfile.split("_");
				                            var newfoldname = newfolder[newfolder.length - 1];
				                            file.display=newfoldname;
				                            var newhash= name2filehash(newfolder.join("_"))+"_";
				                            file.hash=newhash;
				                            var newphash = newhash.split("_").clean(newfoldname).join("_");
				                            if(newphash == "l1_ad_") newphash="l1_XA";
				                            console.log(file);
		                        		}
	                        		}
	                        	}
	                            ret = {
	                                added: add,
	                                removed: [hash]
	                            };
//	                            renameFile(file, hash, sdk, count, newname,cb)
	                        }
	                    });
//                  }else{
                    	if (body.commonPrefixes) {//文件夹下的处理
                        	count++;
                            $.each(body.commonPrefixes, function (i, k) {
                                renameFiles(sdk,"l1_"+k, newname,count, function(files){
                                	console.log(files);
                                });
                            });
                            cb(ret);
                        }
//                  	else{
//                      	cb(ret);
//                      }
//                  }
                }

            }
        });
    };
	
	function renameFiles(sdk,hash,newName,count, cb) {//对移除文件的操作
		var file = findFile(sdk.root.subs, hash);
		if (file && file.isdir == false) {
			 //新的显示名字display
			var moves=[];
			var arr = file.name.split("_"); //将原来的名字按照_拆分
			arr.splice(arr.length - 1, 1, newName); //将原来的名字删除，将新的名字放进数组
			var newFile = arr.join("_"); //   将数组中的东西用_连接起来（新的名字name）
            moves.push({
                src: file.name,
                dst:newFile
            });
			batchFileOperation(null, moves, null, sdk.api, function(res) {
				if(res.response.code == 200){
					file.name = newFile; //将新的文件name覆盖原来文件的name
					file.display = newName; //显示name用新的newName覆盖
					var ret = {
						added: [createElFinderFile(file)],
						removed: [hash]
					};
					cb(ret);
					//alert("修改成功！");
				}
			});
			return;
		}
		renameFile(file, hash, sdk, count, newName, cb);
	};
	
	function rename(args,sdk){
		var dfrd = $.Deferred();
		var newName = args.data.name;
		var count=1;
		var filelength=0;
		renameFiles(sdk,args.data.target,newName,count,function(ret){
			console.log(ret);
			dfrd.resolve(ret);
		});
		return dfrd;
	};*/
	/*function rename(args, sdk) {
		var dfrd = $.Deferred();
		var file = findFile(sdk.root.subs, args.data.target); //找到未修改名字之前的文件
		if (file && file.isdir) {
			var newName = args.data.name; //新的显示名字display
			var arr = file.name.split("_"); //将原来的名字按照_拆分
            arr = arr.slice(0,arr.length-2);
            arr.push(newName);
			var newFile = arr.join("_");
			moveFile(sdk.api, file.name+"tmp.fkdir", newFile+"_tmp.fkdir", function(res) {//转换成七牛所需格式
				if (res.response.data.code == "200") {
					file.name = newFile; //将新的文件name覆盖原来文件的name
					file.display = newName; //显示name用新的newName覆盖
					var ret = {
						added: [createElFinderFolder("l1_"+newFile+"_", newName, file.parent.hash)],//createElFinderFolder("l1_" + fakeDir, args.data.name, folder.hash)
						removed: [args.data.target]
					};
					dfrd.resolve(ret);
					//alert("修改成功！");
				} else {
                    window.parent.hzh.alert("修改失败！");
				}
			});
		}else{
			var newName = args.data.name; //新的显示名字display
			var arr = file.name.split("_"); //将原来的名字按照_拆分
			arr.splice(arr.length - 1, 1, newName); //将原来的名字删除，将新的名字放进数组
			var newFile = arr.join("_"); //   将数组中的东西用_连接起来（新的名字name）
			moveFile(sdk.api, file.name, newFile, function(res) {//转换成七牛所需格式
				if (res.response.data.code == "200") {
					file.name = newFile; //将新的文件name覆盖原来文件的name
					file.display = newName; //显示name用新的newName覆盖
					var ret = {
						added: [createElFinderFile(file)],
						removed: [args.data.target]
					};
					dfrd.resolve(ret);
					//alert("修改成功！");
				} else {
                    window.parent.hzh.alert("修改失败！");
				}
			});
		}
		return dfrd;
	};*/

	/*
	 * 移动文件
	 */

	function moveFile(api, source, dest, cb) {
		var source64 = urlsafebase64_encode(utf16to8(bucket + ":" + source)); //文件原来的地址并转换成七牛所需要的格式（source为原文件的name）
		var dest64 = urlsafebase64_encode(utf16to8(bucket + ":" + dest)); //文件的目标地址
		api.request(0x03070201, "qiniuHttp", {
			method: "post",
			url: "http://rs.qiniu.com/move/" + source64 + "/" + dest64 //
		}, function(res) {
			console.log(res);
			cb(res);
		});
	};

	/*function copyFile(api, source, dest, cb) {
		var source64 = urlsafebase64_encode(bucket + ":" + source);
		var dest64 = urlsafebase64_encode(bucket + ":" + dest);
		api.request(0x03070201, "qiniuHttp", {
			method: "post",
			url: "http://rs.qiniu.com/copy/" + source64 + "/" + dest64
		}, function(res) {
			cb(res);
		});
	};*/

	function processQiniuFiles(ret, dirs, items, dirinfo) {
		ret.files = [];
		ret.cwd.hash = dirinfo.hash;
		ret.cwd.name = dirinfo.display;
//		dirinfo.display=encode64(dirinfo.display);
		if (dirinfo.parent == null) {//parent为Null，就置为根目录
			delete ret.cwd.phash;
			ret.options.path = "files";
			//dirinfo.display=encode64(dirinfo.display);
			var dir = createElFinderFolder(dirinfo.hash, dirinfo.display);
			ret.files.push(dir);
		} else {
			delete ret.cwd.root;
			var phash = dirinfo.parent ? dirinfo.parent.hash : "";
			ret.cwd.phash = phash;
			ret.options.path = dirinfo.name.split("_").clean("").insert(0, "files").join("\\");
		}
		dirinfo.subs = [];
		if (dirs && dirs.length > 0) {
			dirs.forEach(function(d) {
				var f = new File(d, processFileName(d, dirinfo.name), "l1_" + d, dirinfo, "directory", 0, 0, true);//创建文件夹
				fileMap[f.hash] = f;
				dirinfo.subs.push(f);
				var dir = createElFinderFolder(f.hash, f.display, dirinfo.hash);//创建elfinder文件夹格式

				ret.files.push(dir);
			});
		}
		items.forEach(function(item) {
			if (item.key.indexOf("fkdir") >= 0) {
				return;
			}
			var file = new File(item.key, processFileName(item.key, dirinfo.name), name2filehash(item.key), dirinfo, item.mimeType, item.putTime, item.fsize, false);
			fileMap[file.hash] = file;
			dirinfo.subs.push(file);
			ret.files.push(createElFinderFile(file));
		});
		return ret;
	};

	function processFileName(name, parentName) {
		if (name) {
			return name.replace(parentName, "").replace("_", "");
		}
		return null;
	};

	function createElFinderFolder(hash, name, phash) {
		return {
			"isowner": false,
			"mime": "directory",
			"ts": 0,
			"read": 1,
			"write": 1,
			"size": 0,
			"hash": hash,
			"name": name,
			"phash": phash,
			"dirs": 1
		};
	};

	function createElFinderFile(file) {
		return {
			"isowner": false,
			"mime": file.mimeType,
			"ts": Math.floor(file.ts / 10000000),
			"read": 1,
			"write": 1,
			"size": file.size,
			"hash": file.hash,
			"name": file.display,
			"phash": file.getPhash(),
			"url": domain + file.name,
			"tmb": file.name + "?imageView2/2/w/100/h/100"
		};
	};

	function findFile(files, hash) {
		if (fileMap[hash]) {
			return fileMap[hash];
		}
		for (var n in files) {
			var d = files[n];
			if (d.hash == hash) {
				return d;
			}
			if (d.isdir) {
				var r = findFile(d.subs, hash);
				if (r) {
					return r;
				}
			}
		}
		return null;
	};

	function openHttpAPI(api, dr, cdir, cb) {
		api.request(0x03070201, "qiniuHttp", {
			method: "post",
			url: dr.generateUrl()
		}, function(res) {
			if (res.response.code == 200) {
				var data = res.response.data;
				if (data.code == "200") { //succeed
					//process body
					var body = $.parseJSON(data.body);
					if (body.marker) {
						dr.setMarker(body.marker);
					}
					var ret = {};
					$.extend(true, ret, baseElFinderResult);
					ret = processQiniuFiles(ret, body.commonPrefixes, body.items, cdir);
					cb(ret);
				}
                /*else{
                    window.parent.hzh.alert(res.response.data);
                }*/
			}
			/*else{
				
			}*/
		});
	};

	function open(args, sdk) {
		var dfrd = $.Deferred();
		var dirToOpen = args.data.target ? args.data.target.replace("l1_", "") : null;
		var api = sdk.api;
		var dr = sdk.currentDir;
		var cdir;
		if (args.data.init == 1) {
			sdk.root = new File(dr.dir, "files", "l1_XA", null, "directory", 0, 0, true);
			cdir = sdk.root;
			fileMap["l1_XA"] = sdk.root;
		} else {
			if (dirToOpen)
				dr.openDir(dirToOpen, true); //当前文件夹为dirToOpen

			cdir = findFile(sdk.root.subs, args.data.target); //找到改文件
		}
		if (cdir == null || cdir == sdk.root) { //如果cdir是null或者是root

			dr.reset(); //重置当前dr的信息
			cdir = sdk.root;
		}
		sdk.lastDir = cdir;
		openHttpAPI(api, dr, cdir, function(ret) {
			dfrd.resolve(ret); //回调处理
		});
		return dfrd;
	};

	function openParents(args, sdk) {
		var dfrd = $.Deferred();
		var dirToOpen = args.data.target.replace("l1_", "");
		var api = sdk.api;
		var dr = sdk.currentDir;
		var cdir;
		cdir = findFile([sdk.root], args.data.target); //找根下的文件夹
		setTimeout(function() {
			var d = cdir.parent; //cdir的父级
			var ret = [];
			if (d == null) {
				var dir = createElFinderFolder(cdir.hash, cdir.display, null); //hash, name, phash
				ret.push(dir);
			}
			while (d != null) {
				d.subs.forEach(function(e) { //遍历d下的subs
					if (!e.isdir)
						return;
					var dir = createElFinderFolder(e.hash, e.display, d.hash); //创建文件夹
					ret.push(dir);
				});
				var tmp = d;
				d = d.parent;
				if (d == null) {
					var dir = createElFinderFolder(tmp.hash, tmp.display); //d为空时
					ret.push(dir);
				}
			}
			ret.reverse();
			ret = {
				tree: ret
			};
			dfrd.resolve(ret);
		}, 1);
		return dfrd;
	};


	function openTree(args, sdk) {
		var dfrd = $.Deferred();
		var dirToOpen = args.data.target.replace("l1_", "");

		var api = sdk.api;
		var dr = sdk.currentDir;
		var cdir;
		dr.openDir(dirToOpen, true); //dr为当前目录
		cdir = findFile(sdk.root.subs, args.data.target); //找根下的子文件夹
		openHttpAPI(api, dr, cdir, function(ret) {
			dfrd.resolve({
				tree: ret.files
			});
		});
		return dfrd;
	};

	/*创建文件或文件夹*/
	var File = function(name, display, hash, parent, mimeType, ts, size, isdir) {
		this.display = display;
		this.name = name;
		this.hash = hash;
		this.parent = parent;
		this.subs = [];
		this.mimeType = mimeType;
		this.ts = ts;
		this.size = size;
		this.isdir = isdir || false;
		this.url = domain + name;
	};
	$.extend(File.prototype, {
		display: null,
		name: null,
		hash: null,
		parent: null,
		isdir: false,
		mimeType: "",
		ts: 0,
		size: 0,
		subs: [],
		url: null,
		getPhash: function() {
			if (this.parent) {
				return this.parent.hash;
			}
			return null;
		},
		getPname:function(){
			if (this.parent) {
				return this.parent.name;
			}
			return null;
		}
	});




	$.extend(dirinfo.prototype, {
		marker: null,
		dir: null,
		limit: 1000,
		delimiter: "_",
		prefix: "ad",
		dirHash: "l1_XA",
		baseUrl: "http://rsf.qbox.me/list",
		//updateUrl: "http://rs.qiniu.com/chgm",
		openDir: function(name, absolute) {
			if (absolute) { //绝对路径，dir就为name
				this.dir = name;
			} else {
				if (name == "..") { //返回上一级
					this.dir = this.dir.split(delimiter).reverse().slice(1).reverse().join(delimiter); //对dir文件夹的处理
				} else {
					this.dir += delimiter + name;
				}
			}
			this.marker = null;
		},
		reset: function() { //重置dir
			this.dir = this.prefix + this.delimiter;
			this.marker = null;
		},
		setLimit: function(val) { //设置每页显示条数
			this.limit = val;
		},
		setMarker: function(val) { //设置分页参数
			this.marker = val;
		},
		setDirHash: function(val) { //这是dir的hash
			this.dirHash = val;
		},
		generateUrl: function() { //设置请求的url
			//"http://rsf.qbox.me/list?bucket=lookin&prefix=ad_1_&limit=50"
			var ret = this.baseUrl + "&delimiter=" + encodeURIComponent(this.delimiter) + "&prefix=" + encodeURIComponent(this.dir) + "&limit=" + this.limit;
			//			var ret = this.baseUrl+"?bucket=lookin&prefix=ad_1_&limit=50";
			if (this.marker) {
				ret += "&marker=" + this.marker;
			}
			return ret;
		}

	});
	var baseElFinderResult = {
		"api": "2.0",
		"uplMaxSize": "64M",
		"uplMaxFile": 100,
		"netDrivers": [],
		"cwd": {
			"isowner": false,
			"ts": 1449019421,
			"mime": "directory",
			"read": 1,
			"write": 1,
			"size": 0,
			"hash": "l1_XA",
			"name": "files",
			"csscls": "elfinder-navbar-root-local",
			"volumeid": "l1_",
			"root": "l1_XA",
			"locked": 1,
			"uiCmdMap": [],
			"disabled": ["chmod", "dim", "duplicate"]
		},
		"options": {
			"path": "files",
			"url": domain,
			"tmbUrl": domain,
			"disabled": ["chmod"],
			"separator": "\\",
			"copyOverwrite": 1,
			"uploadOverwrite": 1,
			"uploadMaxSize": 2147483647,
			"archivers": {
				"create": [],
				"extract": [],
				"createext": {}
			},
			"uiCmdMap": []
		},
	};
	
	window.QiniuSDK = sdk;
	window.elFinder.prototype.commands.download = function(hashes) {
		var self = this,
			fm = this.fm,
			filter = function(hashes) {
				return $.map(self.files(hashes), function(f) {
					return f.mime == 'directory' ? null : f;
				});
			};

		this.shortcuts = [{
			pattern: 'shift+enter'
		}];

		this.getstate = function() {
			var sel = this.fm.selected(),
				cnt = sel.length;

			return !this._disabled && cnt && ((!fm.UA.IE && !fm.UA.Mobile) || cnt == 1) && cnt == filter(sel).length ? 0 : -1;
		};

		this.exec = function(hashes) { //
			hashes.forEach(function(e) {
				var file = fileMap[e];
				if (file) {
					window.open(domain + file.name);
				}

			});

		};
	};
	window.elFinder.prototype.commands.open = function() {
		this.alwaysEnabled = true;

		this._handlers = {
			dblclick: function(e) {
				e.preventDefault();
				this.exec()
			},
			'select enable disable reload': function(e) {
				this.update(e.type == 'disable' ? -1 : void(0));
			}
		}

		this.shortcuts = [{
			pattern: 'ctrl+down numpad_enter' + (this.fm.OS != 'mac' && ' enter')
		}];

		this.getstate = function(sel) {
			var sel = this.files(sel),
				cnt = sel.length;

			return cnt == 1 ? 0 : (cnt && !this.fm.UA.Mobile) ? ($.map(sel, function(file) {
				return file.mime == 'directory' ? null : file
			}).length == cnt ? 0 : -1) : -1
		}

		this.exec = function(hashes, opts) {
			var fm = this.fm,
				dfrd = $.Deferred().fail(function(error) {
					error && fm.error(error);
				}),
				files = this.files(hashes),
				cnt = files.length,
				thash = (typeof opts == 'object') ? opts.thash : false,
				file, url, s, w, imgW, imgH, winW, winH;

			if (!cnt && !thash) {
				{
					return dfrd.reject();
				}
			}

			// open folder
			if (thash || (cnt == 1 && (file = files[0]) && file.mime == 'directory')) {
				return !thash && file && !file.read ? dfrd.reject(['errOpen', file.name, 'errPerm']) : fm.request({
					data: {
						cmd: 'open',
						target: thash || file.hash
					},
					notify: {
						type: 'open',
						cnt: 1,
						hideCnt: true
					},
					syncOnFail: true
				});
			}

			files = $.map(files, function(file) {
				return file.mime != 'directory' ? file : null
			});

			// nothing to open or files and folders selected - do nothing
			if (cnt != files.length) {
				return dfrd.reject();
			}
			var tfiles = [];
			files.forEach(function(e) {
				var f = fileMap[e.hash];
				if (f) {
					tfiles.push(f);
				}
			});
			onFileSelected(tfiles);
			return dfrd.resolve(hashes);
		}
	};

	function onFileSelected(files) {
		if (window.parent && window.parent.onFileSelected) {
			window.parent.onFileSelected(files);
		}
	};
	window.selectFile=function(){
		//$("#elfinder").elfinder("instance").comands.open.exec();
		$(".elfinder-button-icon-open").parent().click();
	};
	

	window.elFinder.prototype.commands.upload = function() {
		var hover = this.fm.res('class', 'hover');

		this.disableOnSearch = true;
		this.updateOnSelect = false;

		// Shortcut opens dialog
		this.shortcuts = [{
			pattern: 'ctrl+u'
		}];

		/**
		 * Return command state
		 *
		 * @return Number
		 **/
		this.getstate = function(sel) {
			var fm = this.fm,
				f,
				sel = fm.directUploadTarget ? [fm.directUploadTarget] : (sel || [fm.cwd().hash]);
			if (!this._disabled && sel.length == 1) {
				f = fm.file(sel[0]);
			}
			return (f && f.mime == 'directory' && f.write) ? 0 : -1;
		};


		this.exec = function(data) {
			var fm = this.fm,
				targets = data && (data instanceof Array) ? data : null,
				check = !targets && data && data.target ? [data.target] : targets,
				fmUpload = function(data) {
					fm.upload(data)
						.fail(function(error) {
							dfrd.reject(error);
						})
						.done(function(data) {
							var cwd = fm.getUI('cwd');
							dfrd.resolve(data);
							if (data && data.added && data.added[0]) {
								var newItem = cwd.find('#' + data.added[0].hash);
								if (newItem.length) {
									newItem.trigger('scrolltoview');
								}
							}
						});
				},
				upload = function(data) {
					dialog.elfinderdialog('close');
					if (targets) {
						data.target = targets[0];
					}
					fmUpload(data);
				},
				dfrd = $.Deferred(),
				dialog, input, button, dropbox, pastebox, dropUpload, paste;

			if (this.getstate(check) < 0) {
				return dfrd.reject();
			}

			dropUpload = function(e) {
				e.stopPropagation();
				e.preventDefault();
				var file = false,
					type = '',
					elfFrom = null,
					mycwd = '',
					data = null,
					target = e._target || null;
				try {
					elfFrom = e.dataTransfer.getData('elfinderfrom');
				} catch (e) {}
				if (elfFrom) {
					mycwd = window.location.href + fm.cwd().hash;
					if ((!target && elfFrom === mycwd) || target === mycwd) {
						dfrd.reject();
						return;
					}
				}
				try {
					data = e.dataTransfer.getData('text/html');
				} catch (e) {}
				if (data) {
					file = [data];
					type = 'html';
				} else if (e.dataTransfer && e.dataTransfer.items && e.dataTransfer.items.length) {
					file = e.dataTransfer;
					type = 'data';
				} else if (e.dataTransfer && e.dataTransfer.files && e.dataTransfer.files.length) {
					file = e.dataTransfer.files;
					type = 'files';
				} else if (data = e.dataTransfer.getData('text')) {
					file = [data];
					type = 'text';
				}
				if (file) {
					fmUpload({
						files: file,
						type: type,
						target: target
					});
				} else {
					dfrd.reject();
				}
			};

			if (!targets && data) {
				if (data.input || data.files) {
					data.type = 'files';
					fmUpload(data);
				} else if (data.dropEvt) {
					dropUpload(data.dropEvt);
				}
				return dfrd;
			}

			paste = function(e) {
				var e = e.originalEvent || e;
				var files = [],
					items = [];
				var file;
				if (e.clipboardData) {
					if (e.clipboardData.items && e.clipboardData.items.length) {
						items = e.clipboardData.items;
						for (var i = 0; i < items.length; i++) {
							if (e.clipboardData.items[i].kind == 'file') {
								file = e.clipboardData.items[i].getAsFile();
								files.push(file);
							}
						}
					} else if (e.clipboardData.files && e.clipboardData.files.length) {
						files = e.clipboardData.files;
					}
					if (files.length) {
						upload({
							files: files,
							type: 'files'
						});
						return;
					}
				}
				var my = e.target || e.srcElement;
				setTimeout(function() {
					if (my.innerHTML) {
						$(my).find('img').each(function(i, v) {
							if (v.src.match(/^webkit-fake-url:\/\//)) {
								// For Safari's bug.
								// ref. https://bugs.webkit.org/show_bug.cgi?id=49141
								//      https://dev.ckeditor.com/ticket/13029
								$(v).remove();
							}
						});
						var src = my.innerHTML.replace(/<br[^>]*>/gi, ' ');
						var type = src.match(/<[^>]+>/) ? 'html' : 'text';
						my.innerHTML = '';
						upload({
							files: [src],
							type: type
						});
					}
				}, 1);
			};

			input = $('<input id="hzhuploadpop" class="hzhupload hzhuploadpop" type="file" />')
				.change(function() {
					upload({
						input: input[0],
						type: 'files'
					});
				})
				.on('dragover', function(e) {
					e.originalEvent.dataTransfer.dropEffect = 'copy';
				});

			button = $('<div class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="ui-button-text">' + fm.i18n('selectForUpload') + '</span></div>')
				.append($('<form/>').append(input))
				.hover(function() {
					button.toggleClass(hover)
				});

			dialog = $('<div class="elfinder-upload-dialog-wrapper"/>')
				.append(button);

			pastebox = $('<div id="hzhdropcontainer" class="ui-corner-all elfinder-upload-dropbox hzhdropcontainer" contenteditable="true">' + fm.i18n('dropFilesBrowser') + '</div>')
				.on('paste drop', function(e) {
					paste(e);
				})
				.on('mousedown click', function() {
					$(this).focus();
				})
				.on('focus', function() {
					this.innerHTML = '';
				})
				.on('dragenter mouseover', function() {
					pastebox.addClass(hover);
				})
				.on('dragleave mouseout', function() {
					pastebox.removeClass(hover);
				});

			if (fm.dragUpload) {
				dropbox = $('<div class="ui-corner-all elfinder-upload-dropbox" contenteditable="true">' + fm.i18n('dropPasteFiles') + '</div>')
					.on('paste', function(e) {
						paste(e);
					})
					.on('mousedown click', function() {
						$(this).focus();
					})
					.on('focus', function() {
						this.innerHTML = '';
					})
					.on('mouseover', function() {
						$(this).addClass(hover);
					})
					.on('mouseout', function() {
						$(this).removeClass(hover);
					})
					.prependTo(dialog)
					.after('<div class="elfinder-upload-dialog-or">' + fm.i18n('or') + '</div>')[0];

				dropbox.addEventListener('dragenter', function(e) {
					e.stopPropagation();
					e.preventDefault();
					$(dropbox).addClass(hover);
				}, false);

				dropbox.addEventListener('dragleave', function(e) {
					e.stopPropagation();
					e.preventDefault();
					$(dropbox).removeClass(hover);
				}, false);

				dropbox.addEventListener('dragover', function(e) {
					e.stopPropagation();
					e.preventDefault();
					e.dataTransfer.dropEffect = 'copy';
					$(dropbox).addClass(hover);
				}, false);

				dropbox.addEventListener('drop', function(e) {
					dialog.elfinderdialog('close');
					targets && (e._target = targets[0]);
					dropUpload(e);
				}, false);

			} else {
				pastebox
					.prependTo(dialog)
					.after('<div class="elfinder-upload-dialog-or">' + fm.i18n('or') + '</div>')[0];

			}

			fm.dialog(dialog, {
				title: this.title + (targets ? ' - ' + fm.escape(fm.file(targets[0]).name) : ''),
				modal: true,
				resizable: false,
				destroyOnClose: true
			});

			return dfrd;
		};

	};
	
	function utf16to8(str) {
    var out, i, len, c;

    out = "";
    len = str.length;
    for(i = 0; i < len; i++) {
    c = str.charCodeAt(i);
    if ((c >= 0x0001) && (c <= 0x007F)) {
        out += str.charAt(i);
    } else if (c > 0x07FF) {
        out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));
        out += String.fromCharCode(0x80 | ((c >>  6) & 0x3F));
        out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));
    } else {
        out += String.fromCharCode(0xC0 | ((c >>  6) & 0x1F));
        out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));
    }
    }
    return out;
};
	function base64encode(str) {
    var out, i, len;
    var c1, c2, c3;
    var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    len = str.length;
    i = 0;
    out = "";
    while(i < len) {
    c1 = str.charCodeAt(i++) & 0xff;
    if(i == len)
    {
        out += base64EncodeChars.charAt(c1 >> 2);
        out += base64EncodeChars.charAt((c1 & 0x3) << 4);
        out += "==";
        break;
    }
    c2 = str.charCodeAt(i++);
    if(i == len)
    {
        out += base64EncodeChars.charAt(c1 >> 2);
        out += base64EncodeChars.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4));
        out += base64EncodeChars.charAt((c2 & 0xF) << 2);
        out += "=";
        break;
    }
    c3 = str.charCodeAt(i++);
    out += base64EncodeChars.charAt(c1 >> 2);
    out += base64EncodeChars.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4));
    out += base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >>6));
    out += base64EncodeChars.charAt(c3 & 0x3F);
    }
    return out;
}
	function urlsafebase64_encode(str) {
		var c1, c2, c3;
		var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		var i = 0,
			len = str.length,
			string = '';

		while (i < len) {
			c1 = str.charCodeAt(i++) & 0xff;
			if (i == len) {
				string += base64EncodeChars.charAt(c1 >> 2);
				string += base64EncodeChars.charAt((c1 & 0x3) << 4);
				string += "==";
				break;
			}
			c2 = str.charCodeAt(i++);
			if (i == len) {
				string += base64EncodeChars.charAt(c1 >> 2);
				string += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
				string += base64EncodeChars.charAt((c2 & 0xF) << 2);
				string += "=";
				break;
			}
			c3 = str.charCodeAt(i++);
			string += base64EncodeChars.charAt(c1 >> 2);
			string += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
			string += base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
			string += base64EncodeChars.charAt(c3 & 0x3F)
		}
		/*var arr = string.split("");
		
		$.each(arr, function(i,e) {
			if(e == "+"){
				arr[i]="-";
			}
		});
		string=arr.join("");
		string=arr.replace("+", "-");
		string=string.replace("/", "_");*/
		string = string.split("+").join('-').split("/").join("_");
		return string;//
	};
	
})(window, $);