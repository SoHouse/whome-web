$(function($){
	//$.fn===$.prototype
	var PageSwitch = (function(){
		function PageSwitch(element,options){
			this.settings = $.extend(true,$.fn.PageSwitch.default,options || {});
			this.element = element;//存放element
			this.init();//调用init()初始化插件
		}
		PageSwitch.prototype={
			init:function(){//定义inti方法
				
			}
		}
		return PageSwitch;
	})();
	$.fn.PageSwidth = function(options){
		return this.each(function(){
			var me = $(this),
				instance = me.data("PageSwitch");//存放插件的实例
			if(!instance){//判断实例是否为空
				instance = new PageSwitch(me, options);//PageSwitch对象
				me.data("PageSwitch",instance);//存放实例到PageSwitch上
			}
			if($.type(options) === "string") return instance[options]();
			
		});
	}
	//配置参数
	$.fn.PageSwitch.default = {
		selectors:{//配置参数，可以随意命名
			sections:".secitons",
			section:".section",
			page:".pages",//分页
			active:".active"//分页被选中时的class
		},
		index:0,//对应页面的索引值
		easing:"ease",//动画效果，css3动画
		duration:500,//动画执行的时间ms
		loop:false,//是否循环播放
		pagination:true,//分页处理
		keyboard:true,
		direction:"vertical",//竖屏滑动
		callback:""//回调函数
	}
})(jQuery);
