(function(window, $) {
	
	
	hzh.test=function(input){
		var form=$("<form method='post' action='https://mapi.alipay.com/gateway.do'/>");
		form.appendTo($(document.body));
		var arr=input.split("&");
		for(var i in arr){
			var pair=arr[i].split("=");
			$("<input type='hidden' />").attr("name",pair[0]).val(pair[1]).appendTo(form);
		}
		$("<input type=submit value='submit'></input>").appendTo(form);
		
	}
	
	
	hzh.pages.js["Payment"] = {
		init: function() {

			$(".toPay").click(toPay);

			showPayment();
			$("#choosePayment").change(function() {
				showPayment();
			})

			$('.toPay').click(function() {
				var btn = $(this).button('loading');
			    setTimeout(function () {
			        btn.button('reset');
			    }, 3000);
			});

			$('#jfTable').DataTable({
				ordering: false,
	bAutoWidth: false,
	bProcessing: true,
	bFilter: false,
	bPaginate: false,
	responsive: true,
				"oLanguage": {
					"sLengthMenu": "每页显示 _MENU_ 条记录",
					"sZeroRecords": "抱歉， 没有找到",
					"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
					"sInfoEmpty": "没有数据",
					"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
					"oPaginate": {
						"sFirst": "首页",
						"sPrevious": "前一页",
						"sNext": "后一页",
						"sLast": "尾页"
					},
					"sZeroRecords": "没有检索到数据",
					"sProcessing": "<img src='./loading.gif' />",
					"sSearch": "搜索"
				},
				columns: [{
					data: "name"
				}, {
					data: "amount"
				}, {
					data: "id"
				}, {
					data: "id",
					render: function(data, type, row) {
						if (type == 'display') {
							return '<input type="checkbox" class="jfxz" checked=true sId="' + data + '">';
						}
						return data;
					}
				}]
			});
		},
		destroy: function() {}
	};

	function showPayment() {
		hzh.api.request(0x03010201, "queryPaymentStatus", {
			loginName: hzh.api.user.loginName,
		}, function(res) {
			var thisItem = $("#choosePayment option:selected").attr("name");
			if (thisItem != "sy") {
				hzh.RefreshTable("#jfTable", res.response.data[thisItem].items);
			} else {
				var all = [];
				for (var i in res.response.data) {
					var n = res.response.data[i]
					if (typeof(n) == "object") {
						all = all.concat(n["items"]);

					}
				}
				hzh.RefreshTable("#jfTable", all);
			}
		});
	};

	function toPay() {
		var bSelected = $("#jfTable").find("input:checked")
		var aId = "";
		for (var i = 0; i < bSelected.length; i++) {
			aId = aId + bSelected.eq(i).attr("sId") + ","
		}
		aId = aId.substring(0, aId.length - 1);
		hzh.api.request(0x03080001, "createOrder", {
			id: aId,
			source: "web-test",
			paymentAccount: hzh.api.user.paymentAccount,
		}, function(res) {
			var oData ={};// res.response.data;
			oData={};
			console.log(res.response.data);
			console.log(oData);
			oData["amount"]=res.response.data.amount;
			oData["app_key"]="sis";
			oData["client"]="Linux,3.0.8-g5e86763-dirty";
			oData["extra_info"]="";
			oData["format"]="json";
			oData["method"]="api.bill.create";
			oData["order_id"]=res.response.data.order_id;
			oData["order_name"]=res.response.data.order_name;
			oData["order_thumbnail"]="";
			oData["payee_id"]=res.response.data.payee_id;
			oData["payer_id"]=res.response.data.payer_id;
			oData["remark"]="";
			oData["sdk"]="0.9a4java";
			oData["sign_type"]="rsa";
			oData["source"] = "sis";
			oData["timestamp"] = CurentTime();
			oData["trade_type"]=res.response.data.trade_type;
			oData["v"]="1.0";
			getSign(oData);
//			oData["sign"]=getSign(oData);
//				var bData=create(oData);
//				console.log(bData);
//				oData["bill_id"]=bData["id"];
//				oData["payment_method"]=2;
//				oData["payByAccount"]=bData["payerId"];
//				oData["version"]=bData["version"];
//				oData["method"]="api.bill.prepay";
//				oData["sign"]=getSign(oData);
//				console.log(oData);
//				var fData=prepay(oData);
		});
	};
	
	function getSign(data){
		var get1;
		/*hzh.api.request(0x03090002, "", data, function(sign) {
			get1=sign.response.data.result;
			data["sign"]=get1;
			console.log(get1);*/
			create(data);
		//});
		
	};
	
	function getSign2(data11){
		var get11;
//		hzh.api.request(0x03090002, "", data11, function(sign2) {
//			get11=sign2.response.data.result;
//			data11["sign"]=get11;
			prepay(data11);
//		});
		
	};
	
	function create(order){
		var get2;
		hzh.api.request(0x03090001, "api.bill.create", order, function(bill) {
			get2=bill.response.data;
			order["bill_id"]=get2["id"];
			order["payment_method"]=2;
			order["payByAccount"]=get2["payerId"];
			order["version"]=get2["version"];
			order["method"]="api.bill.prepay";
			getSign2(order);
		});
		
	};
	
	function prepay (bill){
		var get3
		hzh.api.request(0x03090001, "api.bill.prepay", bill, function(lastRes){
			get3=lastRes.response.data;
			//get3.info.replace("\"","");
			hzh.test(get3.info.replace(/"/g,""));
			console.log(get3);
		});
		
	};
	
	function CurentTime() {
		var now = new Date();
		var year = now.getFullYear(); //年
		var month = now.getMonth() + 1; //月
		var day = now.getDate(); //日
		var hh = now.getHours(); //时
		var mm = now.getMinutes(); //分
		var ss = now.getSeconds(); //秒
		var clock = year + "-";
		
		if (month < 10)
			clock += "0";
		clock += month + "-";
		if (day < 10)
			clock += "0";
		clock += day + " ";
		if (hh < 10)
			clock += "0";
		clock += hh + ":";
		if (mm < 10) clock += '0';
		clock += mm + ":";
		if (ss < 10) clock += '0';
		clock += ss;
		return (clock);
	}
})(window, $);