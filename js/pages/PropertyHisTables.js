(function(window, $) 
{
	var thisName;
	var page;
	var managmentpagination;
	hzh.pages.js["PropertyHisTables"]={
		init:function()
		{	
			queryOrder(0,10);
			//查询初始化调用及分页
			managmentpagination=new hzh.Pagination($(".pagination"),10,function(page){
				queryOrder(page*10,thisName);
			});
			//点击查询
			$("#wypaymentHistoryBtn").click(function(){
				var start=$(".dp").val();
				var end=$(".dpe").val();
				var account=$("#wylsNameInput").val();
				thisName=account;
				queryOrder(0,account,start,end);
			})
			
			//日期插件设置
			$(".dp, .dpe").datetimepicker(
			{
				todayHighlight: true,
				format: 'yyyy-mm-dd',
				autoclose: true,
				todayBtn: true,
				language: "zh-CN",
				minView: "month",
			});
			
			$(".dp").datetimepicker().on('changeDate', function(){
				$(".dpe").datetimepicker('setStartDate',$(".dp").val());
			});
			
			$(".icon-arrow-left").attr("class","glyphicon glyphicon-arrow-left");
			$(".icon-arrow-right").attr("class","glyphicon glyphicon-arrow-right");
			
			//表格数据初始化
			$('#orderTable').DataTable({
				ordering: false,
				bAutoWidth: false,
				bProcessing: true,
				bFilter: false,
				bPaginate: false,
				responsive: true,
                "oLanguage": {
                    "sLengthMenu": "每页显示 _MENU_ 条记录",
                    "sZeroRecords": "抱歉， 没有找到",
                    "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                    "sInfoEmpty": "没有数据",
                    "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "前一页",
                        "sNext": "后一页",
                        "sLast": "尾页"
                    },
                    "sZeroRecords": "没有检索到数据",
                    "sProcessing": "<img src='./loading.gif' />",
                    "sSearch":"搜索"
                },
                columns: 
                [{
                	data: "orderId" ,
                	className:"text-center"
                },{
                	data: "headOfHouseHoldName",
                	className:"text-center",
                	render:function(data,type,row){
                		if(data == undefined){
                			return "";
                		}
                		return data;
                	}
                },{
                	data: "money",
                	className:"text-center" 
                },{
                	data: "name",
                	className:"text-center"
                },{ 
                	data: "paymentTime",
                	render:function ( data, type, row ){
                		if ( data === undefined ){
                			return '未支付';
                		}
	                  	return data;
                	},
	                className:"text-center"
	             },{ 
	             	data: "status",
	             	className:"text-center" 
	             }]
            });

		},
		destroy:function(){}
	};


	function queryOrder(startNumber,name){
		var start = $("#startTime").val();
		var end = $("#endTime").val();
		var username = hzh.api.user.loginName;
		console.log(username);
//		(sd==undefined?sd="":sd);
//		(ed==undefined?ed="":ed);
//		(name==undefined?name="":ed);
		 hzh.api.request(0x03010203,"queryOrder",{
		 	limitStart:startNumber,
		 	limitEnd:10,
		 	startDate:start,
		 	endDate:end,
		 	loginName:username
		 },function(res){
            if(res.response.code==200){
                hzh.RefreshTable("#orderTable",res.response.data.result);
                console.log(res.response.data.result);
                var allPage=res.response.data.pageCount;
				page=(startNumber+10)/10-1;
				managmentpagination.refresh(allPage, page);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class","");
            }
            else{
                alert("查询失败");
            }
        });
	}







})(window,$);