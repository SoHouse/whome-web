(function(window, $) {
	var page;
	var choose;
	var moreVal;
	var data;
	var dataAll;
	var more = {};
	hzh.pages.js["PropertyTables"] = {
		init: function() {
			//日期插件设置	
			$(".dp, .dpe").datebox();
			queryAdd();
			//点击查询
			$("#queryButton").click(queryPayment);
			//缴费跳转页面
			$(".goToPay").click(function() {
				//	    	window.location.href ='Payment.html';
				hzh.pages.load("Payment.html", false, "Payment");

			});
//			$(".").bind("click",updatepayment);
			$("#paymentOtherTable").delegate(".updatePay","click",updatepayment);
			$("#paymentOtherTable").delegate(".ensureBtn","click",ensurePay);
			//表格初始化
			$('#paymentDetailTable').DataTable({
				ordering: false,
				bAutoWidth: false,
				bProcessing: true,
				bFilter: false,
				bPaginate: false,
				responsive: true,
				"oLanguage": {
					"sLengthMenu": "每页显示 _MENU_ 条记录",
					"sZeroRecords": "抱歉， 没有找到",
					"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
					"sInfoEmpty": "没有数据",
					"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
					"oPaginate": {
						"sFirst": "首页",
						"sPrevious": "前一页",
						"sNext": "后一页",
						"sLast": "尾页"
					},
					"sZeroRecords": "没有检索到数据",
					"sProcessing": "<img src='./loading.gif' />",
					"sSearch": "搜索"
				},
				columns: [{
					data: "id",
					className: "hide idCol"
				}, {
					data: "name",
					className: "text-center",
					sWidth:"6em"
				}, {
					data: "merchCount",
					className: "text-center",
					render:function(data,type,row){
						if(data==undefined){
							return "";
						}
						return data;
					}
				}, {
					data: "merchPrice",
					className: "text-center",
					render:function(data,type,row){
						if(data==undefined){
							return "";
						}
						return data;
					}
				}, {
					data: "amount",
					className: "text-center amountCol"
				}]
			});
			
			$('#paymentOtherTable').DataTable({
				ordering: false,
				bAutoWidth: false,
				bProcessing: true,
				bFilter: false,
				bPaginate: false,
				responsive: true,
				"oLanguage": {
					"sLengthMenu": "每页显示 _MENU_ 条记录",
					"sZeroRecords": "抱歉， 没有找到",
					"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
					"sInfoEmpty": "没有数据",
					"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
					"oPaginate": {
						"sFirst": "首页",
						"sPrevious": "前一页",
						"sNext": "后一页",
						"sLast": "尾页"
					},
					"sZeroRecords": "没有检索到数据",
					"sProcessing": "<img src='./loading.gif' />",
					"sSearch": "搜索"
				},
				columns: [{
					data: "id",
					className: "hide idCol"
				}, {
					data: "name",
					className: "text-center",
					sWidth:"6em"
				}, {
					data: "merchCount",
					className: "text-center hzheditable",
					render:function(data,type,row){
						if(data==undefined){
							return "";
						}
						return data;
					}
				}, {
					data: "merchPrice",
					className: "text-center hzheditable",
					render:function(data,type,row){
						if(data==undefined){
							return "";
						}
						return data;
					}
				}, {
					data: "amount",
					className: "text-center amountCol hzheditable"
				},{
					render: function(data, type, row) {
						if (type === 'display') {//<input type="button" value="取消" style="display:none;" class="btn btn-default cancleBtn">
							return '<input type="button" value="修改" class="btn btn-default updatePay"><input type="button" value="确定" style="display:none;" class="btn btn-default ensureBtn">';
						}
						return data;
					},
					className: "text-center",
					sWidth:"3em"
				}]
			});
			
			$('#paymentTable').DataTable({
				ordering: false,
				bAutoWidth: false,
				bProcessing: true,
				bFilter: false,
				bPaginate: false,
				responsive: true,
				"oLanguage": {
					"sLengthMenu": "每页显示 _MENU_ 条记录",
					"sZeroRecords": "抱歉， 没有找到",
					"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
					"sInfoEmpty": "没有数据",
					"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
					"oPaginate": {
						"sFirst": "首页",
						"sPrevious": "前一页",
						"sNext": "后一页",
						"sLast": "尾页"
					},
					"sZeroRecords": "没有检索到数据",
					"sProcessing": "<img src='./loading.gif' />",
					"sSearch": "搜索"
				},
				columns: [{
					data: "name",
					className: "nameCol text-center"
				}, {
					data: "status",
					render: function(data, type, row) {
						if (data == "1") {
							return "欠费";
						}else{
							return "未欠费";
						}
						return data;
					},
					className: "text-center"
				}, {
					data: "balance",
					className: "text-center"
				}, {
					render: function(data, type, row) {
						if (row.name === '附加费') {
							return '<input type="button" value="详情" class="btn btn-default detail" data-toggle="modal" data-target="#payotherModal">';
						}else {
							return '<input type="button" value="详情" class="btn btn-default detail" data-toggle="modal" data-target="#payModal">';
						}
						return data;
					},
					className: "dt-body-center text-center"
				}]
			});
						
			//点击详情
			$("#paymentTable").delegate(".detail", "click", function() {
				var feiyongName=$(this).parents("tr").find(".nameCol").html();
                if(choose==''){
                	for (var j in data){
                		if(data[j].name==feiyongName){ 
                			if(data[j].name=='附加费'){
                				hzh.RefreshTable("#paymentOtherTable",data[j].items);
                			}else{
                				hzh.RefreshTable("#paymentDetailTable",data[j].items);
                			}
                		}
                	}
                }else{
                	hzh.RefreshTable("#paymentDetailTable", moreVal);
                }
            });
			
			$(".icon-arrow-left").attr("class","glyphicon glyphicon-arrow-left");
			$(".icon-arrow-right").attr("class","glyphicon glyphicon-arrow-right");
			
		},
		destroy: function() {}
	};


	function queryPayment() {
		var name = $("#wyjfNameInput").val();
		choose = $("#paymentSelect").val();
		
		hzh.api.request(0x03080005, "queryPaymentStatus", {
			loginName:name,
			typeId:choose,
			communityId:hzh.api.user.communityId
		}, function(res) {
			if (res.response.code == 200) {
				data = res.response.data;
				var nameVal = $("#paymentSelect  option:selected").text();
				for(var i=0;i<data.length;i++){
					data[i].balance=(data[i].balance)/100;
					if(choose=="1" && data[i].name=="水费") {
						moreVal = data[i].items;
						$(moreVal).each(function(i,e){
							e.amount=e.amount/100;
						});
						hzh.RefreshTable("#paymentTable", data);
	                	
					}else if(choose=="2"  && data[i].name=="电费"){	
						moreVal = data[i].items;
						$(moreVal).each(function(i,e){
							e.amount=e.amount/100;
						});
						hzh.RefreshTable("#paymentTable", data);
	                	
					}else if(choose=="3" && data[i].name=="燃气费"){
						moreVal = data[i].items;
						$(moreVal).each(function(i,e){
							e.amount=e.amount/100;
						});
						hzh.RefreshTable("#paymentTable", data);
					}else {
						$(data[i].items).each(function(j,e){
            				e.amount=e.amount/100;
            			});
						hzh.RefreshTable("#paymentTable", data); 						
					}
				}
			}
		});
	};
	
	function queryAdd() {
		hzh.api.request(0x03020407, "query", {
//			limitStart: startNumber,
			isInternal:1
		}, function(res) {
			var data=res.response.data.result;
			console.log(data);
			var selectVal=$("#paymentSelect");
			$(data).each(function(i,e){
				$("<option value="+e.id+">"+e.utilitiesName+"</option>").appendTo(selectVal);
			});
		});
	};
	
//	function updatepayment(){
//		hzh.api.request(0x03080013,"updatepayment",{},function(res){
//			if(res.response.data == 200){
//				hzh.alert("修改成功！");
//			}else{
//				hzh.alert("修改失败！");
//			}
//		});
//	};
	
	function updatepayment() {
		var hTr = $(this).parents("tr");
		var trSibNode = hTr.siblings(); //找到hTr的兄弟节点
		$(trSibNode).find(".ensureBtn:visible").parents("tr").each(function(i, e) { ///对兄弟行有处于编辑的进行遍历
			console.log($(e));
			change(0, $(e)); ///取消这些行的编辑状态
		});
		change(1, hTr); //其他行处于编辑状态
	};


	function ensurePay() {
		var hTr = $(this).parents("tr");
		var pId = hTr.find("td.idCol").html();
		var data = {
			id: pId,
		};
		$(hTr).find(".hzheditable").each(function(i, e) {
			var td = $(e); //对有类hzheditable的列遍历
			var txtInput = td.find("input"); //对有类hzheditable的列找有input元素
			var id = txtInput.attr("id"); //获取id的属性值，并把取得的属性值赋给id
			data[id] = txtInput.val(); //取得txtInput中的值，放入data中
		});
		data.amount=(data.amount)*100;
		var amount = data.amount;
//		if(phonetest&&data.telephone.length==11){
	 	hzh.api.request(0x03080013, "ensurePay", data, function(res) {
			if (res.response.code == 200) {
				hzh.alert("修改成功！");
				change(0, hTr);
				queryPayment();
			} else {
				hzh.alert("修改失败！");
			}
		});
//		 }else{
//		 	hzh.alert("您输入的信息有误！");
//		 }
	};

	function change(status, tr) {
		$(".ensureBtn", tr).toggle(status == 1);
		$(".updatePay", tr).toggle(status == 0);
		$(".cancleBtn", tr).toggle(status == 1);

		var table = $('#paymentOtherTable').DataTable();
		$(tr).find(".hzheditable").each(function(i, e) {
			var td = $(e); //找到有.hzheditable的列
			var idx = table.cell(td).index().column; //找到要修改的列
			var id = table.column(idx).dataSrc(); //要修改列的属性名
			
			if (status) {//status为1时执行IF中的语句
				td.html("<input style='width:100%' class='htableinput' id='" + id + "' type='text' value='" + td.html() + "' />"); //如果该行处于编辑状态，则将输入的值赋给该列
			} else {
				td.html(td.find("input").val()); //如果不是处于编辑状态，则将其字段原来的值赋他本身
			}

		});
	};

//	function cancle() {
//		change(0, $(this).parents("tr"));
//		findHouseHold(0, 10);
//	};
	

})(window, $);