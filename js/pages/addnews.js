(function(window, $) {
	var editor;
	hzh.pages.js["addnews"]={
		init:function(){
			
			findSection();
			
			//imgsLable
			$("#selectType").change(function(){
				imgsLable();
			});
			$('#sectionSelect').combogrid({
//				toolbar:"#updateTb",
				panelWidth: 500,
				panelHeight:370,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					
					{field: 'name',title: '栏目名称',width: 180,center:true},
					{field: 'createTime',title: '创建时间',width: 225},
					{field: 'id',title: 'ID',width:100,hidden: true}
				]],	
			});
			fillCommunities();
			KindEditor.basePath = '/whome-web/editor/';
//			KindEditor.basePath = '/whome/editor/';
			var element=$("#editor_id");
			var ngModel=true;
			var inited=false;
                editor = KindEditor.create('#editor_id',{
                    height:500,
                    allowPreviewEmoticons : false,
                    allowImageUpload : false,
                    /*sinastorage_bucket:"whome.southindustry.com",
                    sinastorage_ak:"q6z75g1Z2bOlfew1JBw8",
                    sinastorage_sk:"a9e7f67ead0df2be242283d3dc895b84718ebff5",  */                 	
                    afterChange:function(){
                       KindEditor.sync(element); 
                       if(ngModel){
                           // ngModel.$setViewValue(element.val());
                           if(inited){
                                //第一次加载不执行change ，否则 formname.fieldname.$dirty 判断会有误。 
                                element.trigger('change');
                                hzh.hideLoading();
                           }
                       }
                       inited=true;
                    },
                    items : [
                        'source','|','fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
                        'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
                        'insertunorderedlist', '|', 'emoticons', 'qiniuupload',  'link','more','fullscreen','|','media','about']//newsuploadbtn  sinaupload
                });
                function fillCommunities(){
                	var carr=hzh.api.user.communities;
                	if(carr && carr.length > 0){
                		var ele=$("#communitySelect");
                		$.each(carr,function(i,e){
                			$('<label class="checkbox-inline"><input type="checkbox" name="scopeId" value="'+e.id+'">'+e.name+'</label>').appendTo(ele);
                		});
                	}
                	$("#selectAll").change(function(){
                		var ck=this.checked;
                		$("input","#communitySelect").each(function(i,e){
                			e.checked=ck;
                		});
                	});
                };
        	$('.formular').validationEngine({onValidationComplete:function(form,valid){
		  		if(valid){
		  			$(form).hzhsubmit();
		  		}else{
				hzh.alert("请将信息填写完整！");
				}
			}});
		},
		destroy:function(){
			KindEditor.remove("#editor_id");
			$('.combo-p').remove();
		}
		,
		processForm:function(data){
			data.content=hzh.BASE64.encoder(data.content);
			var scopeId="";
			$("input:checked","#communitySelect").each(function(i,e){
                   scopeId+=e.value+",";
            });
            if(scopeId){
            	data.scopeId=scopeId.substr(0,scopeId.length-1);
            }
            hzh.showLoading();
			return data;
		},
		processResponse:function(res){
			hzh.hideLoading();
			if(res.response.code == 200){
				hzh.alert("添加成功");
				editor.html("");
				$("#selectType").find("textarea").val("");
				$("#selectType").find("textarea").hide();
//				$("#newscontent").find(".ke-content").val("");
//				$("#editor_id").val("");
				$("#addnews").find("input").val("");
				$("#selectbox").find("input[type='checkbox']").prop('checked', false);
				
				//$("#dataTables-example").find("input[type='checkbox']").prop('checked', false);
			}
			else{
				hzh.alert(res.response.data);
			}			
		}
	};
	
	function findSection(start,num){
		hzh.api.request(0x03060006 , "querySection", {
			limitStart: start,
			limitEnd: num
		},function(res){
			var data = res.response.data.result;
			$("#sectionSelect").combogrid("grid").datagrid("loadData", data);
			var sectionPager = $("#sectionSelect").combogrid("grid").datagrid("getPager");
            sectionPager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03060006, "querySection", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#sectionSelect").combogrid("grid").datagrid("loadData", res.response.data.result);
						sectionPager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
		});
	};
	
	function imgsLable(){
		var selected = $("#selectType").find("option:selected").val();
		if(selected=="I"){
			$("#selectType").find("textarea").show();
		}else{
			$("#selectType").find("textarea").hide();
		}
	};
	
	
	
})(window,$);
