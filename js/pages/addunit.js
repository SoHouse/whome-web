(function(window, $) {
	var comId;
	hzh.pages.js["addunit"] = {
		
		init: function() {
		  	$('.formular').validationEngine({onValidationComplete:function(form,valid){
		  		if(valid){
		  			$(form).hzhsubmit();
		  		}else{
					hzh.alert("请讲信息填写完整！");
				}
			}});
		  	bindCampany();
			
            queryBuilding(0,10);
			$('#buildingNum').combogrid({
				toolbar:"#tb",
				panelWidth: 480,
				panelHeight:420,
				textField:"buildingNum",
			    idField:'buildingNum',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
//              pageList: [10,20],//可以设置每页记录条数的列表 
				columns:[[
//					{field: 'id',title: 'id',width: 200},
//					{field: 'showName',title: '楼宇名称',width: 70},
					{field: 'buildingNum',title: '楼宇及对应小区',width: 100},
					{field: 'storeyCount',title: '楼层数',width: 80},
					{field: 'communityName',title: '小区名称',width:100},					
					{field: 'communityId',title: '小区id',width: 80}
				]],	
			});
			//$('#buildingNum').combogrid('grid').datagrid('selectRecord',idValue)
			$("#btn").bind("click",function(){
				queryBuilding(0,10);
			});
			$(".datagrid-view").click(getSelected);
		},
		destroy: function() {
			$('.combo-p').remove();
		},
		processForm: function(data) {
			data["communityId"] = comId;
//			var errorlength = $('#signupForm').find(".form-group>.formError:visible").length;
			var build=data.buildingNum;
			data.buildingNum = build.split("-").reverse().slice(2).join();
			return data;
		},
		processResponse: function(res) {
			if (res.response.code == 200) {

				hzh.hideLoading();
				hzh.alert("添加成功！");
				$(".formular").find("input").val("");
			} else {
				hzh.alert(res.response.data);
			}
		},

		batchAddunitFrom: function(data) {
			var oInput = document.getElementById("exampleInputFile");
			if (oInput.files.length == 0) { //判断有没有上传一个excel文档
				hzh.alert("请上传一个文件");
			} else {
				var fileSize = oInput.files[0].size;
				if (fileSize > 3145728) {
					hzh.alert("上传文件过大，请小于3M!");
				} else if (fileSize) {
					hzh.showLoading();
					oInput.outerHTML = oInput.outerHTML;
					console.log(data);
					return data;
				};
				oInput.outerHTML = oInput.outerHTML;
			}
		},
		batchAddunitResponse: function(res) {
			if (res.response.code == 200) {
				var html="";
				hzh.hideLoading();
				hzh.alert("添加成功！");
				var data = res.response.data;
				var aa = $(".bootbox").find(".modal-body");
				//var errors = data.paramErrCount;//identificationExistCount;existCount;successCount;errorCount
				$("<span>成功添加条数：</span>").appendTo($(aa));
				$("<span class='bootbox-body'>"+data.successCount+"</span>").appendTo($(aa));
				$("<br/>").appendTo($(aa));
				$("<span>重复添加条数：</span>").appendTo($(aa));
				$("<span class='bootbox-body'>"+data.existCount+"</span>").appendTo($(aa));
				$("<br/>").appendTo($(aa));
				$("<span>所填信息有错条数：</span>").appendTo($(aa));
				$("<span class='bootbox-body'>"+data.paramErrCount+"</span>").appendTo($(aa));
				$("<br/>").appendTo($(aa));
				$("<span>硬件号重复条数：</span>").appendTo($(aa));
				$("<span class='bootbox-body'>"+data.identificationExistCount+"</span>").appendTo($(aa));
				
//				$("<span>添加失败条数：</span>").appendTo($(aa));
//				$("<span class='bootbox-body'>"+data.errorCount+"</span>").appendTo($(aa));
//				$(aa).html(html);
			} else {
				hzh.alert(res.response.data);
			}
		}
	};
	
	function bindCampany() {
		hzh.api.request(0x03020122,"selectCampany",{
//			name:
		},function(res){
			var data = res.response.data.result;
			var html = "";
			$(data).each(function(i,e){
				var name = e.companyName;
				var id = e.id;
				html+="$(<option value="+id+">"+name+"</option>)";
			});
			$(".company").html(html);
//			$("#communityId").combogrid("grid").datagrid("loadData", data.result);
			
		});
	};
	
	/*function getSelected(){
		var row = $("#buildingNum").combogrid("grid").datagrid("getSelected");
		if (row){
			console.log(row);
			comId=row;
		}
	};*/
	
	function queryBuilding(start,num){
		var buildName = $("#inputval").val();
		hzh.api.request(0x03060015,"queryBuilding",{
			limitStart: start,
			limitEnd: num,
			buildingNum:buildName
		},function(res){
			if(res.response.code == 200){
				var data = res.response.data.result;
				$(data).each(function(i,e){
					e.buildingNum+="-栋-"+e.communityName;
				});
				
				$("#buildingNum").combogrid("grid").datagrid("loadData", data);
				//分页
				var pager = $("#buildingNum").combogrid("grid").datagrid("getPager");
	            pager.pagination({
	                total:res.response.data.dataCount,
	                onSelectPage:function (pageNo, pageSize) {
	                	var st = (pageNo - 1) * pageSize;
	                	hzh.api.request(0x03060015, "queryBuilding", {
							limitStart: st,
							limitEnd: pageSize
						}, function(res) {
							$("#buildingNum").combogrid("grid").datagrid("loadData", res.response.data.result);
							pager.pagination('refresh', {  
		                        total:res.response.data.dataCount,  
		                        pageNumber:pageNo  
		                    }); 
						});
					}
	            });
			}
		});
	};
	
	function getSelected(){
		var row = $("#buildingNum").combogrid("grid").datagrid("getSelected");
		if (row){
			comId=row.communityId;
		}
	};
	

})(window, $);