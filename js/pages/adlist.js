(function(window, $) {

	var adPage;
	var page;
	var adid;
	var sourceLink;
	var keyobj;
	hzh.pages.js["adlist"] = {
		init: function() {
			adListTable();
			queryAdlist(0, 10);
			$("#adListBtn").bind("click", function() {
				queryAdlist(0, 10);
			});
			$("#adListTable").delegate(".adlistDelBtn", "click", delAdlist);
			adPage = new hzh.Pagination($(".pagination"), 10, function(page) {
				queryAdlist(page * 10, 10);
			});
			//日期插件设置
			$(".startDate,.endDate").datebox();
			$(".startTime,.endTime").timespinner();
			$("#adAddBtn").bind("click",function(){
				adforzone(0,10);
			});
			$("#adAddBtn").bind("click",function(){
				adforcustomer(0,10);
			});
//			adforcustomer();
//			$("#adAddBtn").bind("click",adforcustomer);
			
			//批量删除
			$("#adcheckBox").bind("click",selectAll);
			$("#delAlladBtn").bind("click",delAlladlist);
			//验证
			$('.formular').validationEngine({
				onValidationComplete:function(form,valid){
					if(valid){
						$(form).hzhsubmit();
					}else{
						hzh.alert("请将信息填写完整！");
					}
				}
			});
			
			//返回
			$("#beback").bind("click",function(){
				$("#home").find("input[type=text]").val("");
				queryAdlist(0,10);
			});
			
			$("#adListTable").delegate(".adlistUpdateBtn", "click", function() {
				var adlistTr = $(this).parents("tr");
				adid = adlistTr.find("td.idCol input").val();

				$("#resourceName").val(adlistTr.find("td.nameCol").html());
				var srcurl = adlistTr.find("td.urlCol > img").attr("title");
				$("#updateUrl").val(srcurl);

				$("#altText").val(adlistTr.find("td.textcol").html());
				$("#weight").val(adlistTr.find("td.weightcol ").html());
				$("#totalDisplay").val(adlistTr.find("td.totalcol").html());
				$("#playTime").val(adlistTr.find("td.playTimecol").html());
				
				$("#startDisplayDate").datebox("setValue",adlistTr.find("td.startDatecol").html());
				$("#endDisplayDate").datebox("setValue",adlistTr.find("td.endDatecol").html());
				$("#startTime").timespinner("setValue",adlistTr.find("td.startTimecol").html());
				$("#endTime").timespinner("setValue",adlistTr.find("td.endTimecol").html());
				
			});
			
//			$("#adselect").combobox({valueField:'id',textField:'name',panelHeight:"auto"});
			$('#adselect').combogrid({
//				toolbar:"#updateTb",
				panelWidth: 520,
				panelHeight:400,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'name',title: '版位名称',width:150},
					{field: 'remark',title: '版位描述',width: 80},
					{field: 'id',title: 'ID',width: 240}
				]],	
			});
			$('#customerIds').combogrid({
//				toolbar:"#updateTb",
				panelWidth: 520,
				panelHeight:400,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'name',title: '客户名称',width:150},
					{field: 'linkman',title: '客户联系人',width: 80},
					{field: 'id',title: 'ID',width: 240}
				]],	
			});
			$(".filemanagerBtn").click(function(){
				window.hzh.openFileManager(function(files){
					var urlArr=[];
					if(files){
						$.each(files,function(i,e){
							urlArr.push(e.url);
						});
					}
					console.log(urlArr);
					$("#destinationUrl").val(urlArr.join());
					if(files.length == 0){
						window.hzh.alert("no file selected");
					}
					else{
						//hzh.alert("添加成功！");
						return true;
					}
				});
			});
		},
		destroy: function() {
			$('.combo-p').remove();
		},
		processForm: function(data) {
			var id = $("#zonediv").find("input[type=hidden]");
			var ids = [];
			$(id).each(function(i,e){
				ids.push($(e).val());
			});
			var idString = '';
			idString = ids.join(",");
			data.zoneIds = idString;
			/*var errorlength = $('#myModal').find(".form-group>.formError:visible").length;
			if(errorlength == 0){*/
				hzh.showLoading();
				return data;
			/*}else{
				return ;
			}*/
		},
		processResponse: function(res) {
			hzh.hideLoading();
			if (res.response.code == 200) {
				hzh.alert("添加成功！");
				queryAdlist(0, 10);
				$("#myModal").modal("hide");
				$("#myModal").find("input").val("");
			} else {
				hzh.alert(res.response.data);
			}
		},
		processUpdate: function(data) {
			data["id"] = adid;
			return data;
		},
		processBackUpdate: function(res) {
			if (res.response.code == 200) {
				hzh.alert("修改成功！");
				queryAdlist(page, 10);
				$("#updateAdModal").modal("hide");
			} else {
				hzh.alert(res.response.data);
			}
		}
	};	
	
	/*function uploadAddress(){
		hzh.api.request(0x03070201,"postUrl",{
			url:sourceLink,
			postMap:keyobj
		},function(res){
			console.log(res);
		});
	};*/
	
	function queryAdlist(start, num) {
		var pageName = $("#page").val();
		var zoneName = $("#zone").val();
		var adName = $("#adName").val();
		var customerName = $("#customer").val();
		var campaignName = $("#campaign").val();
		
		hzh.api.request(0x03040101, "selectADResource", {
			zoneName: zoneName,
			pageName: pageName,
			campaignName: campaignName,
			customerName: customerName,
			resourceName: adName,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			if (res.response.code == 200) {
				var data = res.response.data.result;
//				console.log(data.destinationUrl);
				page=start;
				hzh.RefreshTable("#adListTable", data);
				var totalpage = res.response.data.pageCount;
				adPage.refresh(totalpage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var dataCount = res.response.data.dataCount;
				var li = $(".pagination").find("li.active").nextAll();
//				console.log(li.length);
				if(li.length == 1){
					$("#adListTable_info").html("从 "+(start+1)+" 到 " +dataCount+ " 条 / 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#adListTable_info").html("从 "+(0)+" 到 " +0+ " 条 / 共 "+(0)+" 条数据");
				}else{
					$("#adListTable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条 / 共 "+(dataCount)+" 条数据");
				}
			}
		});
	};


	function delAdlist() {

		var adlistid = $(this).parents("tr").find("td.idCol input").val();
//		console.log(adlistid);
		var r = confirm("确认删除？");
		if(r==true){
			hzh.api.request(0x03040104, "delADResource", {
				id: adlistid
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功！");
					queryAdlist(page * 10, 10);
				} else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}
		
	};
	
	function selectAll() {
		if(this.checked == true){
			$('#adListTable').find("input[type='checkbox']").prop("checked",true);
		}else{
			$("#adListTable").find("input[type='checkbox']").prop("checked",false);
		}
	};
	
	function delAlladlist() {
		var ids=[];
		var idString="";
		$('#adListTable').find("input[type='checkbox']:checked").each(function(i,e){
			ids.push($(e).val());
		});
		if (ids.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		idString=ids.join(",");
		var r = confirm("确定删除？");
		if(r==true){
			hzh.api.request(0x03040104, "delADResource", {
				id: idString
			}, function(res) {
				if(res.response.code == 200){
					hzh.alert("删除成功！");
					queryAdlist(0, 10);
				}else{
					hzh.alert("删除失败！");
				}
			});
		}
	};

	function adListTable() {
		$('#adListTable').DataTable({

			ordering: false,
			bAutoWidth: true,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					//					"sFirst": "首页",
					//					"sPrevious": "前一页",
					//					"sNext": "后一页",
					//					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" value="' + data + '">';
					}
					return data;
				},
				className: "idCol text-center",
				sWidth: "3em"
			}, {
				data: "resourceName",
				className: "nameCol editcol text-center",
				render: function(data, type, row) {
					if (data == undefined) {
						return '';
					}
					return data;
				}
			}, {
				data: "destinationUrl",
				className: "urlCol editcol text-center",
				"sWidth":"3em",
				render: function(data, type, row) {
					var dataArr=data.split(",");
					dataArr=dataArr.slice(0,1).join("");
					var extStart = data.lastIndexOf(".");
					var ext = data.substring(extStart, data.length).toUpperCase();
					if (ext == ".PNG" || ext == ".GIF" || ext == ".JPG" || ext == ".JPEG") {
						return "<img class='imgsrc' src='" + dataArr + "' title='"+data+"'>";
					} else {//<img src='../images/iconfont-video.png' style='height:3%;width:5%' value='data'>
						return "<img src='../images/iconfont-video.png' title='"+data+"'>";
					}
					return data;
				}
			}, {
				data: "altText",
				className: "textcol editcol hide text-center"
			}, {
				data: "zoneName",
				className: "zonecol text-center",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				}
			}, {
				data: "pageName",
				className: "pagecol hide text-center",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				}
			}, {
				data: "campaignName",
				className: "campaigncol hide text-center",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				}
			}, {
				data: "customerName",
				className: "customercol text-center",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				}
			}, {
				data: "weight",
				className: "weightcol hide editcol text-center"
			}, {
				data: "startDisplayDate",
				className: "startDatecol hide text-center editcol"

			}, {
				data: "endDisplayDate",
				className: "endDatecol hide text-center editcol"
			}, {
				data: "startTime",
				className: "startTimecol hide text-center editcol"
			}, {
				data: "endTime",
				className: "endTimecol hide text-center editcol"
			}, {
				data: "totalDisplay",
				className: "totalcol editcol hide text-center"
			}, {
				data: "displayCount",
				className: "displayCountcol hide editcol text-center"
			}, {
				data: "playTime",
				className: "playTimecol editcol hide text-center"
			}, {
				render: function(data, type, row) {
					if (data === undefined) {
						return "<a href='#' data-toggle='modal' data-target='#updateAdModal' style='margin-right:10px' class='btn btn-default adlistUpdateBtn'>修改</a><a href='javascript://'  class='btn btn-default adlistDelBtn'>删除</a><a href='javascript://' style='display:none;margin-right:10px;' class='btn btn-default adlistEnsureBtn'>确定</a><a href='javascript://' style='display:none;' class='btn btn-default adlistCancleBtn'>取消</a>";
					}
					return data;
				},
				sWidth: "7em",
				className: "text-center"
			}]
		});
	};
	
	function adforzone(start,num) {
		hzh.api.request(0x03040119, "selectADZone", {
			limitStart: start,
			limitEnd: num
		}, function(res) {
			var data = res.response.data.result;
			$("#adselect").combogrid("grid").datagrid("loadData", data);
			var zonePager = $("#adselect").combogrid("grid").datagrid("getPager");
            zonePager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03040119, "selectADZone", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#adselect").combogrid("grid").datagrid("loadData", res.response.data.result);
						zonePager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
		});
	};
	
	function adforcustomer(start,num) {
		hzh.api.request(0x03040106, "selectADCustomer", {
			limitStart: start,
			limitEnd: num
		}, function(res) {
			var data = res.response.data.result;
			$("#customerIds").combogrid("grid").datagrid("loadData", data);
			var cusPager = $("#customerIds").combogrid("grid").datagrid("getPager");
            cusPager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03040106, "selectADCustomer", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#customerIds").combogrid("grid").datagrid("loadData", res.response.data.result);
						cusPager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
			
		});
	};
	
	

})(window, $)