(function(window, $) {

	var adPage;
	var page;
	var pageid;
	hzh.pages.js["adpage"] = {
		init: function() {
			adListTable();
			queryAdPage(0, 10);
			$("#adListBtn").bind("click", function() {
				queryAdPage(0, 10);
			});
			$("#adListTable").delegate(".adpageDelBtn", "click", delAdPage);
			adPage = new hzh.Pagination($(".pagination"), 10, function(page) {
				queryAdPage(page * 10, 10);
			});
			//添加验证
			$('.formular').validationEngine({
				onValidationComplete:function(form,valid){
				if(valid){
					$(form).hzhsubmit();
				}else{
					hzh.alert("请将信息填写完整！");
				}
			}});
			//返回
			$("#beback").bind("click",function(){
				$("#home").find("input[type=text]").val("");
				queryAdPage(0,10);
			});
			$("#pagecheckBox").bind("click",selectAll);
			$("#delAllpageBtn").bind("click",delAllpage);
			$("#adListTable").delegate(".adpageUpdateBtn", "click", function(){
				updatezone(0,10);
			});
			
			$("#adListTable").delegate(".adpageUpdateBtn", "click", function(){
				var pageTr = $(this).parents("tr");
				pageid = pageTr.find(".idCol input").val();
				var td = pageTr.find("td.exitcol");
				var table = $("#updatedapage").find("input");
				for(var i=0;i<td.length;i++){
					var tdval = $(td).eq(i).html();
					$(table).eq(i).val(tdval);
				}
				$("#zoneids").val(pageTr.find(".zoneIdCol").html());
				$("#zoneId").combogrid("setValue",pageTr.find(".zoneNamecol").html());
			});
			
			$('#zoneId').combogrid({
//				toolbar:"#updateTb",
				panelWidth: 520,
				panelHeight:400,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'name',title: '版位名称',width:150},
					{field: 'remark',title: '版位描述',width: 80},
					{field: 'id',title: 'ID',width: 240}
				]],	
			});
			
			var text = $("#updatedapage").find(".textbox-text");
			$(text).keydown(function(event){
				var content = $(this).val();
				var zoneidval = $("#zoneids").val();
				if(event.keyCode == 8){
					var arr=content.split(",");
					var newVal=arr.slice(0,arr.length-1).join(",");
					$(this).val(newVal);
					var arrids=zoneidval.split(",");
					var newidval = arrids.slice(0,arrids.length-1).join(",");
//					console.log(newidval);
					$("#zoneids").val(newidval);
				}
				return false;				
			});
			
			
		},
		destroy: function() {
			$(".combo-p").remove()
		},
		processForm: function(data) {
			/*var errorlength = $('#myModal').find(".form-group>.formError:visible").length;
			console.log(errorlength);
			if(errorlength == 0){*/
				//console.log(data);
				return data;
			/*} else {
				hzh.alert("请将信息填写完整！");
			}*/
		},
		processResponse: function(res) {
			if (res.response.code == 200) {
				hzh.alert("添加成功！");
				queryAdPage(0, 10);
				$("#myModal").modal("hide");
				$("#myModal").find("input").val("");
			} else {
				hzh.alert(res.response.data);
			}
		},
		
		updateForm:function(data){
			data["id"] = pageid;
			var id = $("#updatedapage").find("input[type=hidden]");
			var ids = [];
			$(id).each(function(i,e){
				ids.push($(e).val());
			});
			var idString = '';
			idString = ids.join(",");
			data.zoneId = idString;
//			console.log(data);
			return data;
		},
		updateResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("修改成功！");
				queryAdPage(page, 10);
				$("#updatedapage").modal("hide");
			}else{
				hzh.alert("修改失败！");
			}
		}
	};
	
	

	function queryAdPage(start, num) {
		
		var pageName = $("#page").val();
		var zoneName = $("#zone").val();
		var campaignName = $("#campaign").val();
		var customerName = $("#customer").val();
		var adName = $("#adName").val();

		hzh.api.request(0x03040115, "selectADPage", {
			zoneName: zoneName,
			name: pageName,
			campaignName: campaignName,
			customerName: customerName,
			resourceName: adName,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			if (res.response.code == 200) {
				var data = res.response.data.result;
				hzh.RefreshTable("#adListTable",data);
				var totalpage = res.response.data.pageCount;
				page = start;
				adPage.refresh(totalpage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var dataCount = res.response.data.dataCount;
				var li = $(".pagination").find("li.active").nextAll();
				if(li.length == 1){
					$("#adListTable_info").html("从 "+(start+1)+" 到 " +(dataCount)+ " 条/ 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#adListTable_info").html("从 "+(0)+" 到 " +(0)+ " 条/ 共 "+(0)+" 条数据");
				}else{
					$("#adListTable_info").html("从 "+(start+1)+" 到 " +(start+10)+ " 条/ 共 "+(dataCount)+" 条数据");
				}				
			}
		});
	};
	
	function updatezone(start,num){
		hzh.api.request(0x03040119, "selectADZone", {
			limitStart: start,
			limitEnd: num
		},function(res){
			var data = res.response.data.result;
			$("#zoneId").combogrid("grid").datagrid("loadData", data);
			var zonePager = $("#zoneId").combogrid("grid").datagrid("getPager");
            zonePager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03040119, "selectADZone", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#zoneId").combogrid("grid").datagrid("loadData", res.response.data.result);
						zonePager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
			
//			var cusforcamp = $("#zoneId");
//			$(data).each(function(i,e){
//			    var	zoneName = e.name;
//				var zoneid = e.id;
//				$('<option value='+zoneid+'>'+zoneName+'</option>').appendTo(cusforcamp);
//			});
//			$("#zoneId").combobox("loadData",data);
		});
	};
	
	function delAdPage() {
		var adid = $(this).parents("tr").find(".idCol input").val();
		var r = confirm("确认删除？");
		if(r==true){
			hzh.api.request(0x03040114, "deleADPage", {
				id: adid
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功！");
					queryAdPage(page * 10, 10);
				} else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}
		
	};
	
	function selectAll(){
		if(this.checked == true){
			$('#adListTable').find("input[type='checkbox']").prop("checked",true);
		}else{
			$('#adListTable').find("input[type='checkbox']").prop("checked",false);
		}
	};
	
	function delAllpage(){
		var ids=[];
		$('#adListTable').find("input[type='checkbox']:checked").each(function(i,e){
//			console.log($(e).val());
			ids.push($(e).val());
		});
		if (ids.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		var idString=ids.toString();
		console.log(idString);
		var r = confirm("确定删除？");
		if(r==true){
			hzh.api.request(0x03040114, "deleADPage", {
				id: idString
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功！");
					queryAdPage(0, 10);
				} else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}
		
	};
	
	function adListTable() {
		$('#adListTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					//					"sFirst": "首页",
					//					"sPrevious": "前一页",
					//					"sNext": "后一页",
					//					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				className: "idCol text-center",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" value="' + data + '">';
					}
					return data;
				},
				sWidth: "3em"
			},{
				data: "zoneId",
				className: "zoneIdCol hide"
			},{
				data: "name",
				className: "namecol exitcol text-center"
			}, {
				data: "linkman",
				className: "linkcol exitcol text-center"
			}, {
				data: "email",
				className: "emailcol exitcol text-center"
			}, {
				data: "url",
				className: "urlcol exitcol text-center"
			}, {
				data: "zoneName",
				className: "zoneNamecol hide text-center"
			},{
				data: "campaignName",
				className: "text-center hide"
			}, {
				data: "customerName",
				className: "text-center hide"
			}, {
				data: "resourceName",
				className: "text-center hide"
			},  {
				render: function(data, type, row) {
					if (data === undefined) {
						return "<a href='#' data-toggle='modal' data-target='#updatedapage' style='margin-right:10px;' class='btn btn-default adpageUpdateBtn'>修改</a><a href='javascript://'  class='btn btn-default adpageDelBtn'>删除</a>";
					}
					return data;
				},
				sWidth: "8em",
				className: "text-center"
			}]
		});
	};

})(window, $)