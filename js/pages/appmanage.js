(function(window, $) {
	hzh.pages.js["appmanage"] = {
		
//		var page;
//		var appPage;
		init: function() {
			appListTable();
			queryApp(0,10);
			$("#appListBtn").bind("click", function(){
				queryApp(0,10);
			});
			$("#appListTable").delegate(".appUpdateBtn", "click", updateApp);
			$("#appListTable").delegate(".appEnsureBtn", "click", ensure);
			$("#appListTable").delegate(".appDelBtn", "click", appDel);
			$("#appListTable").delegate(".appCancleBtn", "click", cancle);
			appPage = new hzh.Pagination($(".pagination"), 10 , function(page){
				queryApp(page*10 , 10);
			});
			//批量删除
			$("#delAllappBtn").bind("click",delAllapp);
			$("#appcheckBox").bind("click",selectAll);
			//添加的验证
			$('.formular').validationEngine();
		},
		
		destory: function() {},
		
		
		processForm: function(data) {
			var errorlength = $('#myModal').find(".form-group>.formError:visible").length;
			console.log(errorlength);
			if(errorlength == 0){
				return data;
			}else{
				return ;
			}
		},
		processResponse: function(res) {
			if (res.response.code == 200) {
				hzh.alert("添加成功!");
				queryApp(0,10)
				$("#myModal").modal("hide");
			} else {
				hzh.alert(res.response.data);
			}
		}
	};
	
	function queryApp(start,num) {
//		console.log(num);
		hzh.api.request("0x03040125", "selectApp", {
			limitStart:start,
			limitEnd:num
		}, function(res) {
			if (res.response.code == 200) {
				hzh.RefreshTable("#appListTable", res.response.data.result);
				var totalPage = res.response.data.pageCount;
				appPage.refresh(totalPage,start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class","");
				var dataCount = res.response.data.dataCount;
				var li = $(".pagination").find("li.active").nextAll();
				console.log(li.length);
				if(li.length == 1){
					$("#appListTable_info").html("从 "+(start+1)+" 到 " +dataCount+ " 条 / 共 "+(dataCount)+" 条数据");
				}else{
					$("#appListTable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条 / 共 "+(dataCount)+" 条数据");
				}
			} 
//			else {
//				hzh.alert("查询失败！");
//			}
		});
	};
	

	function updateApp() {
		var uTr = $(this).parents("tr");
		var trSibNode = uTr.siblings();//找到hTr的兄弟节点
		
		$(trSibNode).find(".appEnsureBtn:visible").parents("tr").each(function(i, e) {//找到其他未处于编辑行的，对其遍历
			change(0, $(e));//取消当前行的编辑状态
		});
		change(1, uTr);//其他行处于编辑状态
	};
	
	function ensure(){
		
		var uTr = $(this).parents("tr");
		var appid = uTr.find("td.idCol input").val();
//		var name = uTr.find(".nameCol").html();
//		var del = uTr.find(".delCol").html();
//		var uremark = uTr.find(".remarkCol").html();
		var data={id:appid};
		
		$(uTr).find(".exitCol").each(function(i,e){
			var td=$(e);//对有类exitCol的列遍历
			var txtInput=td.find("input");//对有类exitCol的列找有input元素
			var id=txtInput.attr("id");//获取id的属性值，并把取得的属性值赋给id
			data[id]=txtInput.val();	//取得txtInput中的值，放入data中
		});
		
		hzh.api.request(0x03040126, "updateApp", data , function(res) {
			if(res.response.code == 200){
				hzh.alert("修改成功！");
				change(0,uTr);
			}else {
				hzh.alert("修改失败！");
			}
		});
	};
	
	function cancle() {
		change(0,$(this).parents("tr"));
		queryApp(0,10);
	};
	
	function change(status,tr){
		
		$(".appUpdateBtn",tr).toggle(status == 0);
		$(".appDelBtn",tr).toggle(status == 0);
		$(".appEnsureBtn",tr).toggle(status == 1);
		$(".appCancleBtn",tr).toggle(status == 1);
		
		var table = $("#appListTable").DataTable();
		$(tr).find(".exitCol").each(function(i,e){
			var td = $(e);
			var idx =table.cell(td).index().column;
			
			var id=table.column(idx).dataSrc();
//			console.log(id);
			if(status){
				td.html("<input class='htableinput' id='"+id+"' type='text' value='" + td.html() + "' />");
			}else {
				td.html(td.find("input").val());
			}
		});
	};
	
	function appDel() {
		
		var appid = $(this).parents("tr").find(".idCol input").val();		
		var r = confirm("确认删除？");
		if(r == true){
			hzh.api.request(0x03040124,"delApp",{id:appid},function(res){
				if(res.response.code == 200){
					hzh.alert("删除成功！");
					queryApp(0,10);
				}else {
					hzh.alert("删除失败！");
				}
			});
		}
	};
	
	function selectAll(){
		if(this.checked == true){
			$('#appListTable').find("input[type='checkbox']").prop("checked",true);
		}else{
			$("#appListTable").find("input[type='checkbox']").prop("checked",false);
		}
	};
	
	function delAllapp(){
		var ids=[];
		$('#appListTable').find("input[type='checkbox']:checked").each(function(i,e){
			ids.push($(e).val());
		});
		if (ids.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		var idString=ids.toString();
		console.log(idString);
		var r = confirm("确定删除？");
		if(r==true){
			hzh.api.request(0x03040124, "delApp", {
				id: idString
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功！");
					queryApp();
					$("#appcheckBox").prop("checked",false);
				} else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}		
	};
	
	function appListTable() {
		$('#appListTable').DataTable({
			ordering: false,
	        bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
									//					"sFirst": "首页",
									//					"sPrevious": "前一页",
									//					"sNext": "后一页",
									//					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				className: "idCol text-center",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" value="' + data + '">';
					}
					return data;
				},
				sWidth: "3em"
				}, {
					data: "appName",
					render: function(data, type, row) {
						if (data == undefined) {
							return "";
						}
						return data;
					},
					className: "nameCol exitCol text-center"
				}, {
					data: "remark",
					render: function(data, type, row) {
						if (data == undefined) {
							return "";
						}
						return data;
					},
					className: "remarkCol exitCol text-center"
				},
//				{
//					data: "isDel",
//					className: "delCol exitCol text-center"
//				}, 
				{
				render: function(data, type, row) {
					if (data === undefined) {
						return "<a href='javascript://' style='margin-right:10px' class='btn btn-default appUpdateBtn'>修改</a><a href='javascript://'  class='btn btn-default appDelBtn'>删除</a><a href='javascript://' style='display:none;margin-right:10px' class='btn btn-default appEnsureBtn'>确定</a><a href='javascript://' style='display:none;' class='btn btn-default appCancleBtn'>取消</a>";
					}
					return data;
				},
				className: "text-center",
//				sWidth: "9em"
			}]
		});
	};


})(window, $)
