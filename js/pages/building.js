(function(window, $) {
	var buildingPage;
	var buildId;
	var commName;
	var contents;
//	var comIdInput;
	var comInput;
	hzh.pages.js["building"] = {
		init: function() {
			initBuildingListTable();
			queryBuilding(0,10);
			$("#queryBuildBtn").bind("click",function(){
				queryBuilding(0,10);
			});
			buildingPage = new hzh.Pagination($("#buildingPage"), 10, function(page) {
				queryBuilding(page * 10, 10);
			});
			selectCommunity(0,10);
			updateCommunity(0,10);
			$("#buildingListTable").delegate(".delBuildBtn", "click", delBuilding);
			
			
			$("#beback").bind("click",function(){
				$("#home").find("input[type=text]").val("");
				queryBuilding(0, 10);
			});			
			//验证
			$('.formular').validationEngine();
			$("#buildingListTable").delegate(".updateBuild", "click", function(){
				var  tr= $(this).parents("tr");
				buildId = $(tr).find("td.idCol").html();
				contents = $(tr).find("td.editCol");
				var td = $("#updateBuildModal").find("input[type=text]");
				for(var i=0;i<contents.length;i++){
					var tdVal = $(contents).eq(i).html();
					$(td).eq(i).val(tdVal);
				}
				comInput=$(tr).find("td.communityNameCol");
				commName = $(comInput).html();
				$("#updateCommunityId").combogrid("setValue",commName);
				var comIdInput = $(tr).find("td.communityIdCol");
				$("#updateId").val($(comIdInput).html());
			});
			
			$('#communityId').combogrid({
//				toolbar:"#tb",
				panelWidth: 500,
				panelHeight:400,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'id',title: 'ID',width: 70}, 
					{field: 'name',title: '小区名称',width: 90},
					{field: 'householdCount',title: '住户数量',width: 80},
					{field: 'cityName',title: '小区地址',width: 100},
					{field: 'cityId',title: '小区编号',width: 60}
				]],	
			});
			
			
			$('#updateCommunityId').combogrid({
//				toolbar:"#updateTb",
				panelWidth: 500,
				panelHeight:400,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'id',title: 'ID',width: 70}, 
					{field: 'name',title: '小区名称',width: 90},
					{field: 'householdCount',title: '住户数量',width: 80},
					{field: 'cityName',title: '小区地址',width: 100},
					{field: 'cityId',title: '小区编号',width: 60}
				]],	
			});
			
			$('#communityContent').change(function(){
				queryBuilding(0,10);
			});
			
		},
		destroy: function() {
			$('.combo-p').remove();
		},
		processForm:function(data){
			if(data.buildingNum !="" && data.storeyCount !="" && data.communityId != ""){
				return data;
			}
			else{
				hzh.alert("请将信息填写完整！");	
			}
			
		},
		processResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("添加成功！");
				$("#myModal").modal("hide");
				$("#myModal").find("input").val("");
				queryBuilding(0,10);
			}else{
				hzh.alert(res.response.data);
			}
		},
		updateForm:function(data){
			data["id"] = buildId;
			var newName = $("#updateCommunitydiv").find("span input.textbox-value").val();
			if(newName != commName){
				return data;
			}else{
				data.communityId = $("#updateId").val();
				return data;
			}
		},
		updateResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("修改成功！");
				$("#updateBuildModal").modal("hide");
				//queryBuilding(0,10);
				var updateinput = $("#updateBuildModal").find("input.buildupdateVal");
				$(updateinput).each(function(i,e){
					$(contents[i]).html($(e).val());
				});
				var communityName=$("#updateCommunitydiv").find("input.textbox-text").val();
				$(comInput).html(communityName);
				//$(comIdInput).html($("#updateCommunitydiv").find("input[type=hidden]").val());
				//$("#updateCommunityId").combogrid("setValue",communityName);
				//$("#updateId").val($(tr).find("td.communityIdCol").html());
				
			}else{
				hzh.alert(res.response.data);
			}
		}
		
	};
	
	function queryBuilding(start,num){
		var buildName = $("#showName").val();
		//var commName = $("#communityName").val();
		var commName = $('#communityContent').find("option:selected").text();
		console.log(commName);
		hzh.api.request(0x03060015,"queryBuilding",{
			buildingNum:buildName,
			communityName:commName,
			limitStart: start,
			limitEnd: num
		},function(res){
			if(res.response.code == 200){
				hzh.RefreshTable("#buildingListTable", res.response.data.result);
				var totalPage = res.response.data.pageCount;
				buildingPage.refresh(totalPage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var dataCount = res.response.data.dataCount;
				var li = $("#buildingPage").find("li.active").nextAll();
				if(li.length == 1){
					$("#buildingListTable_info").html("从 "+(start+1)+" 到 " +dataCount+ " 条 / 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#buildingListTable_info").html("从 "+(0)+" 到 " +0+ " 条 / 共 "+(0)+" 条数据");
				}else{
					$("#buildingListTable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条 / 共 "+(dataCount)+" 条数据");
				}
			}
		});
	};
	
	function delBuilding(){
		var buildId  = $(this).parents("tr").find("td.idCol").html();
		var r = confirm("确定删除？");
		if(r == true){
			hzh.api.request(0x03060014,"delBuilding",{id:buildId},function(res){
				if(res.response.code == 200){
					hzh.alert("删除成功！");
					queryBuilding(0,10);
				}else{
					hzh.alert("删除失败");
				}
			});
		}
		
	};
	
	function selectCommunity(start,num){
		hzh.api.request(0x03020121,"selectCommunity",{//0x03020121
			userId:hzh.api.user.userId,
			limitStart: start,
			limitEnd: num
		},function(res){
			if(res.response.code == 200){
				var data = res.response.data.result;
				$("#communityId").combogrid("grid").datagrid("loadData", data);
				
				var communityPager = $("#communityId").combogrid("grid").datagrid("getPager");
	            communityPager.pagination({ 
	                total:res.response.data.dataCount,
	                onSelectPage:function (pageNo, pageSize) {
	                	var st = (pageNo - 1) * pageSize;
	                	hzh.api.request(0x03020121, "selectCommunity", {
							limitStart: st,
							limitEnd: pageSize
						}, function(res) {
							$("#communityId").combogrid("grid").datagrid("loadData", res.response.data.result);
							communityPager.pagination('refresh', {  
		                        total:res.response.data.dataCount,  
		                        pageNumber:pageNo  
		                    }); 
						});
					}
	            });
			}
		});
	};
	
	function updateCommunity(start,num){
		hzh.api.request(0x03020121,"selectCommunity",{//0x03020121
			userId:hzh.api.user.userId,
			limitStart: start,
			limitEnd: num
		},function(res){
			if(res.response.code == 200){
				var data = res.response.data.result;
				$("#updateCommunityId").combogrid("grid").datagrid("loadData", data);
				
				var updatePager = $("#updateCommunityId").combogrid("grid").datagrid("getPager");
	            updatePager.pagination({ 
	                total:res.response.data.dataCount,
	                onSelectPage:function (pageNo, pageSize) {
	                	hzh.api.request(0x03020121, "selectCommunity", {
							limitStart: (pageNo - 1) * pageSize,
							limitEnd: pageSize
						}, function(res) {
							$("#updateCommunityId").combogrid("grid").datagrid("loadData", res.response.data.result);
							updatePager.pagination('refresh', {  
		                        total:res.response.data.dataCount,  
		                        pageNumber:pageNo  
		                    }); 
						});
					}
				});
			}
		});
	};
	
	function initBuildingListTable() {
		$('#buildingListTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bPaginate: false,
			responsive: true,
			bFilter: false,
			//			AllowSorting:false,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",

				"oPaginate": {
					//				"sFirst": "首页",
					//				"sPrevious": "前一页",
					//				"sNext": "后一页",
					//				"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				className: "text-center idCol hide",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			},{
				data: "buildingNum",
				className: "text-center editCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			},{
				data: "storeyCount",
				className: "text-center editCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			},{
				data: "communityName",
				className: "text-center communityNameCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			}, {
				data: "communityId",
				className: "text-center hide communityIdCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			},{
				render: function(data, type, row) {
					if (data === undefined) {
						return "<a href='#' data-toggle='modal' style='margin-right:10px;' data-target='#updateBuildModal' class='btn btn-default updateBuild'>修改</a><a href='javascript://' class='btn btn-default delBuildBtn'>删除</a>";
					}
					return data;
				},
				className: "dt-body-center text-center",
				sWidth: "8em"
			}]
		});
	};
	

})(window, $);