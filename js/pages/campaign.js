(function(window, $) {

	var camPage;
	var page;
	var campId;
	var cusName;
	//	var modalheight;
	hzh.pages.js["campaign"] = {
		init: function() {
			camListTable();
			camZoneTable();
			queryCampaign(0, 10);
            customerforcam(0,10);
            $("#addcampaign").bind("click",function(){
            	zoneforcam(0,10);
            });
            
			$("#camListBtn").bind("click", function() {
				queryCampaign(0, 10);
			});
			camPage = new hzh.Pagination($(".pagination"), 10, function(page) {
				queryCampaign(page * 10, 10);
			});
			
			//验证
			$(".formular").validationEngine({
				onValidationComplete:function(form,valid){
					if(valid){
						$(form).hzhsubmit();
					}else{
						hzh.alert("请将信息填写完整！")
					}
				}
			});
			
			//删除
			$("#campaignListTable").delegate(".campDelBtn", "click", delCampaign);
			//批量删除
			$("#campcheckBox").bind("click",selectAll);
			$("#delAllcampBtn").bind("click",delAllcamp);
            
            //返回
			$("#beback").bind("click",function(){
				$("#home").find("input[type=text]").val("");
				queryCampaign(0,10);
			});
			
			var table = $("#updateModal").find("input");
			$("#campaignListTable").delegate(".campUpdateBtn", "click", function(){
				var campaignTr = $(this).parents("tr");
				campId = campaignTr.find("td.idCol input").val();
				var td = campaignTr.find("td.exitcol");
				for(var i=0;i<td.length;i++){
					var textval = $(td).eq(i).html();
					$(table).eq(i).val(textval);
				}
				cusName = $(campaignTr).find("td.customercol").html();
				$("#customerid").combogrid("setValue",cusName);
				$("#updateId").val($(campaignTr).find("td.customerIdCol").html());
			});
			
            $('#customerId').combogrid({
//				toolbar:"#updateTb",
				panelWidth: 520,
				panelHeight:400,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'name',title: '客户名称',width:150},
					{field: 'linkman',title: '客户联系人',width: 80},
					{field: 'id',title: 'ID',width: 240}
				]],	
			});
			
			$("#customerid").combogrid({
//				toolbar:"#updateTb",
				panelWidth: 520,
				panelHeight:400,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
				{field: 'name',title: '客户名称',width:150},
					{field: 'linkman',title: '客户联系人',width: 80},
					{field: 'id',title: 'ID',width: 240}
				]],	
			});
			
			$('#zoneselect').combogrid({
//				toolbar:"#updateTb",
				panelWidth: 520,
				panelHeight:400,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
				{field: 'name',title: '版位名称',width:110},
					{field: 'remark',title: '版位描述',width: 80},
					{field: 'id',title: 'ID',width: 240}
				]],	
			});
		},
		destroy: function() {
			$('.combo-p').remove();
		},
		processForm: function(data) {
			
			console.log(data);
			var errorlength = $('#myModal').find(".form-group>.formError:visible").length;
			console.log(errorlength);
			if(errorlength == 0){
				return data;
			}else{
				return ;
			}
			
//			var email = data.email;
//			var isemail = /\w+[@]{1}\w+[.]\w+/;
//			if (isemail.test(email)) {
//				email;
//			} else {
//				hzh.alert("您输入的邮箱地址有误！");
//				return;
//			}
//			console.log(data);
//			return data;
		},
		processResponse: function(res) {
			if (res.response.code == 200) {
				hzh.alert("添加成功！");
				queryCampaign(0, 10);
				$("#myModal").modal("hide");
				$("#myModal").find("input").val("");
			} else {
				hzh.alert(res.response.data);
			}
		},
		updateForm:function(data){
			data["id"] = campId;
			var newName = $("#updateCusDiv").find("span input.textbox-value").val();
			if(newName != cusName){
				return data;
			}else{
				
				data.customerId = $("#updateId").val();
				return data;
			}
		},
		updateResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("修改成功！");
				$("#updateModal").modal("hide");
				queryCampaign(page, 10);
			}
		}
	};
	//	 

	function queryCampaign(start, num) {

		var pageName = $("#page").val();
		var zoneName = $("#zone").val();
		var adName = $("#adName").val();
		var customerName = $("#customer").val();
		var campaignName = $("#campaign").val();

		hzh.api.request(0x03040110, "selectADCampaign", {
			zoneName: zoneName,
			pageName: pageName,
			name: campaignName,
			customerName: customerName,
			resourceName: adName,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			if (res.response.code == 200) {
				var data = res.response.data.result;
				hzh.RefreshTable("#campaignListTable", data);
				page = start;
				var totalpage = res.response.data.pageCount;
				camPage.refresh(totalpage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var dataCount = res.response.data.dataCount;
				var li = $("#camPage").find("li.active").nextAll();
				if(li.length == 1){
					$("#campaignListTable_info").html("从 "+(start+1)+" 到 " +dataCount+ " 条 / 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#campaignListTable_info").html("从 "+(0)+" 到 " +(0)+ " 条 / 共 "+(0)+" 条数据");
				}else{
					$("#campaignListTable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条 / 共 "+(dataCount)+" 条数据");
				}
			} 
//			else {
//				hzh.alert("查找失败，请稍后再试！");
//			}
		});
	}



	function delCampaign() {
		var camptid = $(this).parents("tr").find(".idCol input").val();
		console.log(camptid);
		var r = confirm("确认删除？");
		if(r==true){
			hzh.api.request(0x03040112, "delADCampaign", {
				id: camptid
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功！");
					queryCampaign(0, 10);
				} else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}		
	};
	
	function selectAll(){
		if(this.checked == true){
			$('#campaignListTable').find("input[type='checkbox']").prop("checked",true);
		}else{
			$("#campaignListTable").find("input[type='checkbox']").prop("checked",false);
		}
	};
	
	function delAllcamp(){
		var ids=[];
		$('#campaignListTable').find("input[type='checkbox']:checked").each(function(i,e){
//			console.log($(e).val());
			ids.push($(e).val());
		});
		if (ids.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		var idString=ids.toString();
		console.log(idString);
		var r = confirm("确定删除？");
		if(r==true){
			hzh.api.request(0x03040112, "delADCampaign", {
				id: idString
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功！");
					queryCampaign(0, 10);
				} else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}		
	};
	
//	function querycampZone() {
//
//		var tr = $(this).parents("tr");
//		var campid = tr.find(".idCol input").val();
//		console.log(campid);
//		var zonename = tr.find(".zoneCol");
//		$("#updateCampZoneBtn").data("zonename", zonename);
//		$("#updateCampZoneBtn").data("campid", campid);
//		hzh.api.request(0x03040128, "queryZoneWithCampaign", {
//			id: campid
//		}, function(res) {
//			if (res.response.code == 200) {
//				hzh.RefreshTable("#campZoneTable", res.response.data.result);
//			} else {
//				hzh.alert("查找失败，请稍后再试！");
//			}
//		});
//	};

//	function updateCampZone() {
//		var upCampid = $(this).data("campid");
//		var zonename = $(this).data("zonename");
//		var zoneNames = [];
//		var campIds = [];
//		var campIdString = "";
//		var zoneNameString = '';
//		var checkedbox = $("#campZoneTable").find("input:checkbox[name='checkbox']:checked");
//		checkedbox.each(function(i, e) {
//			var campId = $(e).parents("tr").find(".zoneIdCol").html();
//			var zonenameval = $(e).parents("tr").find(".zonenamecol").html();
//			campIds.push(campId);
//			zoneNames.push(zonenameval);
//		});
//		campIdString = campIds.join(",");
//		zoneNameString = zoneNames.join(",");
//		hzh.api.request(0x03040129, "updateZoneWithCampaign", {
//			zoneIds: campIdString,
//			campaignId: upCampid
//		}, function(res) {
//			if (res.response.code == 200) {
//				hzh.alert("修改成功！");
//				$("#campZoneBtn").modal('hide');
//				$(zonename).html(zoneNameString);
//			} else {
//				hzh.alert("修改失败，请稍后再试！");
//			}
//		});
//	};

	function camListTable() {
		$('#campaignListTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					//					"sFirst": "首页",
					//					"sPrevious": "前一页",
					//					"sNext": "后一页",
					//					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				className: "idCol text-center",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" value="' + data + '">';
					}
					return data;
				},
				sWidth: "3em"
			},{
				data: "customerId",
				className: "customerIdCol hide"
			}, {
				data: "name",
				className: "namecol exitcol text-center"
			}, {
				data: "contact",
				className: "contactcol exitcol text-center"
			}, {
				data: "email",
				className: "emailcol exitcol hide text-center"
			}, {
				data: "customerName",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
				className: "text-center customercol"
			}, {
				data: "zoneName",
				className: "text-center zoneCol",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				}
			}, {
				data: "pageName",
				className: "text-center hide",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				}
			}, {
				data: "resourceName",
				className: "text-center hide",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				}
			}, {
				render: function(data, type, row) {
					if (data === undefined) {
						//<a href='#' data-toggle='modal' data-target='#campZoneBtn' class='btn btn-default campZoneBtn'>修改版位</a>
						return "<a href='#' data-toggle='modal' data-target='#updateModal' style='margin-right:10px;' class='btn btn-default campUpdateBtn'>修改</a><a href='javascript://' class='btn btn-default campDelBtn'>删除</a>";
					}

					return data;
				},
				sWidth: "8em",
				className: "text-center"
			}]
		});
	};

	function camZoneTable() {
		$('#campZoneTable').DataTable({
			ordering: false,
			"bLengthChange": false,
			bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					"sFirst": "首页",
					"sPrevious": "前一页",
					"sNext": "后一页",
					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "zoneId",
				className: "zoneIdCol text-center"
			}, {
				data: "zoneName",
				className: "text-center zonenamecol"
			}, {
				data: "selected",
				render: function(data, type, row) {
					if (type === 'display') {
						var checkedString = data == 1 ? 'checked = "checked" ' : "";
						return '<input type="checkbox" name="checkbox" ' + checkedString + '  class="editor-active row-cbx" value="' + data + '" data-row="' + data + '">';
					}
					return data;
				},
				className: "text-center",
				"sWidth": "8em"
			}]
		});
	};
	
	
	
    function customerforcam(start,num) {
		hzh.api.request(0x03040106, "selectADCustomer", {
			limitStart: start,
			limitEnd: num
		}, function(res) {
			
			var data = res.response.data.result;
			$("#customerId").combogrid("grid").datagrid("loadData", data);
			var cusPager = $("#customerId").combogrid("grid").datagrid("getPager");
            cusPager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03040106, "selectADCustomer", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#customerId").combogrid("grid").datagrid("loadData", res.response.data.result);
						cusPager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });

            $("#customerid").combogrid("grid").datagrid("loadData",data);
			var updateCusPager = $("#customerid").combogrid("grid").datagrid("getPager");
            updateCusPager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03040106, "selectADCustomer", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#customerid").combogrid("grid").datagrid("loadData", res.response.data.result);
						updateCusPager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
            
//			var html='';
//			var customercontent = $(".customerId");
//			var customerids = $("#customerid");
//			$(data).each(function(i,e){
//				var cusname = e.name;
//				var cusid = e.id;
//				html+="$(<option value="+cusid+">"+cusname+"</option>)";
//				
//			});
			
			
			
			
		});
//		zoneforcam();	
	};
	
	
	function zoneforcam(start,num) {
		hzh.api.request(0x03040119, "selectADZone", {
			limitStart: start,
			limitEnd: num
		}, function(res) {
//			console.log(res);
			var data = res.response.data.result;
			console.log(data);
			$("#zoneselect").combogrid("grid").datagrid("loadData",data);
			var updateZonePager = $("#zoneselect").combogrid("grid").datagrid("getPager");
            updateZonePager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03040106, "selectADCustomer", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#zoneselect").combogrid("grid").datagrid("loadData", res.response.data.result);
						updateZonePager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
//			var html='';
//			$(data).each(function(i,e){
//				var zonename = e.name;
//				var zoneid = e.id;
//				html+="$(<option value="+zoneid+">"+zonename+"</option>)";
//			});
			
			
		});
	};
	

})(window, $)