(function(window, $) {
	var commpage;
	var communityid;
	var cityid;
	var hideval;
	var form1Valid=false;
	hzh.pages.js["community"] = {
		init: function() {
			initCommunitytable();
			selectCommunity(0,10);
			findCity();
			bindCity();
			$("#comcheckBox").bind("click",uselectAll);
			$("#delAllcomBtn").bind("click",delAllCommunity);
			$(".buildDate").datebox({
				required: true
			});
//			$("#myModal").ready(function(){
			  	$('.formular').validationEngine({onValidationComplete:function(form,valid){
			  		if(valid){
			  			$(form).hzhsubmit();
			  		}else{
					hzh.alert("请讲信息填写完整！");
				}
			  	}});
//			});
			
			commpage = new hzh.Pagination($(".pagination"), 10, function(page) {
				var start = page * 10;
				selectCommunity(start, 10);
			});
			
//			$('.validate').validatebox({
//			    required: true,
//			    validType: '不能为空'
//			});
			
			$("#cummunityListTable").delegate(".delbtn", "click", delcommunity);
			$('#cityId').combogrid({
				toolbar:"#tb",
				panelWidth: 500,
				panelHeight:430,
				textField:"cityName",
			    idField:'id',
			    required:true,
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
//              pageList: [10,20],//可以设置每页记录条数的列表 
				columns:[[
					{field: 'id',title: 'id',width: 150}, 
					{field: 'cityName',title: '城市名称',width: 255},
//					{field: 'type',title: '小区类型',width: 70},
//					{field: 'buildDate',title: '投入使用时间',width: 150},
//					{field: 'householdCount',title: '数量',width: 60},
//					{field: 'address',title: '小区地址',width: 150},
//					{field: 'realEstateDeveloper',title: '开发商',width: 70}
				]]
			});
			
			$('#updatecityId').combogrid({
				toolbar:"#updateTb",
				panelWidth: 500,
				panelHeight:430,
				textField:"cityName",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
//              pageList: [10,20],//可以设置每页记录条数的列表 
				columns:[[
					{field: 'id',title: 'id',width: 150}, 
					{field: 'cityName',title: '城市名称',width: 255},
//					{field: 'type',title: '小区类型',width: 70},
//					{field: 'buildDate',title: '投入使用时间',width: 150},
//					{field: 'householdCount',title: '数量',width: 60},
//					{field: 'address',title: '小区地址',width: 150},
//					{field: 'realEstateDeveloper',title: '开发商',width: 70}
				]],	
			});
			
            $("#addComBtn").bind("click", findCity);
			$("#updateComBtn").bind("click", bindCity);
			
			
			//修改
			$("#cummunityListTable").delegate(".updatebtn","click",function(){
				var utr = $(this).parents("tr");
				communityid = utr.find(".idCol").html();
				cityid = utr.find(".cityIdCol").html();
				$("#updateBtn").data("id",cityid);
				var utd = utr.find(".exitcol");
				var table = $("#updateModal").find("input");
				for(var i=0;i<utd.length;i++){
					var tdval = utd.eq(i).html();
					$(table).eq(i).val(tdval);
				}
				var typename = $(utr).find(".typeCol").html();
				if(typename=="居住"){
					$("#type").val("false");
				}else{
					$("#type").val("true");
				}
				$("#updateDate").datebox("setValue",$(utr).find(".dateColumn").html());
				hideval = $(utr).find(".addressColumn").html();
				$("#updatecityId").combogrid("setValue",$(utr).find(".addressColumn").html());
			});
		},
		destroy: function() {
			$('.combo-p').remove();
		},
		processForm: function(data) {
			/*var all = $("#myModal").find("input.validate");
			var allString="";
			$(all).each(function(i,e){
				if($(e).val() =="")
					return;
			});
			var errorlength = $('#myModal').find(".form-group>.formError:visible").length;
			console.log(errorlength);
			if(errorlength == 0){
				return data;
			}*/
/*			if($("#id").val() != "" && $("#name").val() != "" && $("#householdCount").val() != "" && $("#developer").val != ""){
				return data;
			}else{
				hzh.alert("请讲信息填写完整！");
			}*/
			/*if(form1Valid){
				return data;
			}*/
			return data;
			
		},
		processResponse: function(res) {
			if (res.response.code == 200) {
				hzh.alert("添加成功!");
				selectCommunity(0,10);
				$("#myModal").modal("hide");
				$("#myModal").find("input").val("");
			} else {
				hzh.alert(res.response.data);
			}
		},
		processUpdate:function(data) {
			data["id"]=communityid;
			var newhideval = $("#updateModal").find("input[type=hidden]").val();
//			console.log(newhideval == hideval);
			if(hideval == newhideval){
				data.cityId=cityid;
			}else{
				data.cityId = newhideval;
			}
			var errorlength = $('#updateModal').find(".form-group>.formError:visible").length;
			if(errorlength == 0){
				return data;
			}else{
				return ;
			}
		},
		updateResponse:function(res) {
			if (res.response.code == 200) {
				hzh.alert("修改成功！");
				selectCommunity(0,10);
				$("#updateModal").modal("hide");
				
			} else {
				hzh.alert(res.response.data);
			}
		}
		
	};
	
	function selectCommunity(start,num){
		hzh.api.request(0x03020121,"selectCommunity",{//0x03020121
			userId:hzh.api.user.userId,
			limitStart: start,
			limitEnd: num
		},function(res){
			if(res.response.code == 200){
				hzh.RefreshTable("#cummunityListTable", res.response.data.result);
				
				var totalPage = res.response.data.pageCount;
				commpage.refresh(totalPage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var dataCount = res.response.data.dataCount;
				var li = $(".pagination").find("li.active").nextAll();
				if(li.length == 1){
					$("#cummunityListTable_info").html("从 "+(start+1)+" 到 " +dataCount+ " 条 / 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#cummunityListTable_info").html("从 "+(0)+" 到 " +dataCount+ " 条 / 共 "+(0)+" 条数据");
				}else{
					$("#cummunityListTable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条 / 共 "+(dataCount)+" 条数据");
				}
			}
//			else{
//				hzh.alert("查询失败！");
//			}
		});
	};
	
	//删除
	function delcommunity(){
		var id = $(this).parents("tr").find(".idCol").html();
		console.log(id);
		var r = confirm("确认删除？"); //确定删除
		if (r == true) {
			hzh.api.request(0x03020118, "delCommunity", {id:id},function(res){
				if(res.response.code == 200){
					selectCommunity(0,10);
					hzh.alert("删除成功!");
					
				}else{
					hzh.alert("删除失败！");
				}
			});
		}
	};
	
	//选中所有
	function uselectAll() {
		if (this.checked == true) {
			$("#cummunityListTable").find("input[type='checkbox']").prop('checked', true);
		} else {
			$("#cummunityListTable").find("input[type='checkbox']").prop('checked', false);
		}
	};


	//删除所有

	function delAllCommunity() {
//		var userDelId = "";
		var communityIds = [];
		$("#cummunityListTable").find(".row-cbx:checked").each(function(i, e) { //each()方法遍历选中的checked
			communityIds.push($(e).data("row")); 
		});
		if (communityIds.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		var idString = communityIds.toString();
		var r = confirm("确认删除？"); //确定删除
		if (r == true) {
			hzh.api.request(0x03020118, "delCommunity", {
				id: idString
			}, function(res) {
				if (res.response.code == 200) {
					selectCommunity(0,10);
					hzh.alert("删除成功");
					
				}else{
					hzh.alert("删除失败！");
				}
			});
		}
	};
	
	function findCity(){
		var inputNewVal=$("#addComval").val();
		hzh.api.request(0x03050005,"findCity",{
			name:inputNewVal
		},function(res){
			
			var data = res.response.data.result;
			$("#cityId").combogrid("grid").datagrid("loadData", data);
			//分页
			var pager = $("#cityId").combogrid("grid").datagrid("getPager");
            pager.pagination({
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03050005, "queryHousehold", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#cityId").combogrid("grid").datagrid("loadData", res.response.data.result);
						pager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
			
//			$('#cityId').combogrid('grid').datagrid('selectRecord',data.id);
			//其中id为combogrid的id值，idValue对应combogrid定义的idValue属性
		});
	};
	
	function bindCity(){
		var aa=$("#updateComVal").val();
		hzh.api.request(0x03050005,"findCity",{
			name:aa
		},function(res){
			var data = res.response.data.result;
			//分页
			var pager = $("#updatecityId").combogrid("grid").datagrid("getPager");
            pager.pagination({
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03050005, "queryHousehold", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#updatecityId").combogrid("grid").datagrid("loadData", res.response.data.result);
						pager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
			$("#updatecityId").combogrid("grid").datagrid("loadData", data);
		});
	};
	
	function initCommunitytable() {
		$('#cummunityListTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bPaginate: false,
			responsive: true,
			bFilter: false,
			//			AllowSorting:false,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",

				"oPaginate": {
					//				"sFirst": "首页",
					//				"sPrevious": "前一页",
					//				"sNext": "后一页",
					//				"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				className: "text-center",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" data-row="' + data + '">';
					}
					return data;
				},				
				sWidth: "3em"
			},{
				data: "cityId",
				className: "hide cityIdCol text-center"
			},{
				data: "id",
				className: "idCol text-center"
			},{
				data: "name",
				className: "nameCol exitcol text-center"
			}, {
				data: "cityName",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
				className: "addressColumn text-center"
			}, {
				data: "type",
				className: "text-center typeCol",
				render:function(data,type,row){
					if(data == "false"){
						return "居住";
					}else{
						return "商用"
					}
//					return data;
				},
			}, {
				data: "buildDate",
				className: "text-center dateColumn"
			}, {
				data: "householdCount",
				className: "exitcol text-center"
			},{
				data: "realEstateDeveloper",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
				className: "roleColumn exitcol text-center"
			},{
				render: function(data, type, row) {
					if (data === undefined) {
						return "<a href='#' data-toggle='modal' data-target='#updateModal' style='margin-right: 10px;' class='btn btn-default updatebtn'>修改</a><a href='javascript://' class='btn btn-default delbtn'>删除</a>";

					}
					return data;
				},
				className: "dt-body-center text-center",
				sWidth: "8em"
			}]
		});
	};
	

})(window, $);