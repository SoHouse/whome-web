(function(window, $) {
	var configPage;
	//hzh.pages.js["listnews"] = {
	hzh.pages.js["config"] = {

		init: function() {
			queryConfig(0, 10);
			configListTable();
			$("#configListBtn").bind("click", function() {
				queryConfig("0", 10);
				
			});
			
			//返回
			$("#beback").bind("click",function(){
				var queryterms=$("#home").find("input[type=text]").val("");
				queryConfig(0, 10);
			});
			
			$("#configListTable").delegate(".delConfigBtn", "click", delConfig);
			$("#configListTable").delegate(".updateConfigBtn", "click", updateConfig);
			$("#configListTable").delegate(".ensureConfigBtn", "click", ensureConfig);
			$("#configListTable").delegate(".cancleConfigBtn", "click", configCancle);
			//批量删除
			$("#appcheckBox").bind("click",selectAll);
			$("#delAllappBtn").bind("click",delAllconfig);
			configPage = new hzh.Pagination($(".pagination"), 10, function(page) {
				var limitStart = page * 10 + "";
				queryConfig(limitStart, 10);
			});
			//验证
			$('.formular').validationEngine();
			queryUser();
//			$("#addconfigBtn").bind("click",function(){
//				var userid = $('#userName').find("option:selected").val();
//				console.log(userid);
//			});
		},
		destory: function() {

		},
		processForm: function(data) {
			var errorlength = $('#myModal').find(".form-group>.formError:visible").length;
			console.log(errorlength);
			if(errorlength == 0){
				console.log(data);
				return data;
			}else{
				return ;
			}
		},
		processResponse: function(res) {
			if (res.response.code == 200) {
				hzh.alert("添加成功！");
				$("#upApk").find("input").each(function(i,e){
					$(e).val("");
				});
                $("#myModal").modal("hide");
				queryConfig(0,10);
			} else {
				hzh.alert("添加失败，请稍后再试！");
			}
		}
	};

	//查询
	function queryConfig(start, num) {
		
		var confName = $("#name").val();
		var confVersion = $("#version").val();
		var confLoginName = $("#loginName").val();

		hzh.api.request(0x03050001, "queryConf", {
			name: confName,
			version: confVersion,
			loginName: confLoginName,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			if (res.response.code == 200) {
				hzh.RefreshTable("#configListTable", res.response.data.result);
				var totalPage = res.response.data.pageCount;
				configPage.refresh(totalPage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var dataCount = res.response.data.dataCount;
				var li = $(".pagination").find("li.active").nextAll();
				if(li.length == 1){
					$("#configListTable_info").html("从 "+(start+1)+" 到 " +dataCount+ " 条 / 共 "+(dataCount)+" 条数据");
				}else{
					$("#configListTable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条 / 共 "+(dataCount)+" 条数据");
				}
			} 
//			else {
//				hzh.alert("查询失败，请稍后再试！");
//			}
		});
	};

	function delConfig() {
		var configId = $(this).parents("tr").find(".idCol input").val();
		
		var r = confirm("确认删除？");
		if(r==true){
			hzh.api.request(0x03050004, "delConfig", {
				id: configId
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功！");
					queryConfig(0, 10);
					
				} else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}
	};
	
	function selectAll(){
		if(this.checked == true){
			$('#configListTable').find("input[type='checkbox']").prop("checked",true);
		}else {
			$("#configListTable").find("input[type='checkbox']").prop("checked",false);
		}
	};
	
	function delAllconfig(){
		var ids=[];
		$('#configListTable').find("input[type='checkbox']:checked").each(function(i,e){
			ids.push($(e).val());
		});
		if (ids.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		var idString=ids.toString();
		console.log(idString);
		var r = confirm("确定删除？");
		if(r==true){
			hzh.api.request(0x03050004, "delConfig", {
				id: idString
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功！");
					queryConfig(0, 10);
					$("#appcheckBox").prop("checked",false);
				} else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}		
	};
	
	function updateConfig() {
		var utd = $(this).parents("tr");
		var trSibNode = utd.siblings(); //找打该行的所有兄弟行
		$(trSibNode).find(".ensureConfigBtn:visible").parents("tr").each(function(i, e) { //对兄弟行有处于编辑的进行遍历
			change(0, $(e)); //取消这些行的编辑状态
		});
		change(1, utd); //将该行处于编辑状态
	};

	function ensureConfig() {
		var confTr = $(this).parents("tr");
		var adId = confTr.find("td.idCol input").val();
		var data = {
			id: adId
		};
		$(confTr).find(".editcol").each(function(i, e) {
			var td = $(e);
			var txtInput = td.find("input");
			var id = txtInput.attr("id");
			data[id] = txtInput.val();
		});
		hzh.api.request(0x03050003, "updateConf", data, function(res) {
			if (res.response.code == 200) {
				hzh.alert("修改成功！");
				change(0, confTr);
			} else {
				hzh.alert("修改失败，请稍后再试！");
			}
		});
	};

	function change(status, tr) {
		$(".updateConfigBtn", tr).toggle(status == 0);
		$(".delConfigBtn", tr).toggle(status == 0);
		$(".ensureConfigBtn", tr).toggle(status == 1);
		$(".cancleConfigBtn", tr).toggle(status == 1);

		var table = $("#configListTable").DataTable();
		$(tr).find(".editcol").each(function(i, e) {
			var td = $(e); //找到有.editcol的列
			var idx = table.cell(td).index().column; //找到要修改的列
			var id = table.column(idx).dataSrc(); //要修改列的属性名
			if (status) {
				td.html('<input style="width:100%" class="htableinput" id="' + id + '" type="text" value="' + td.html() + '" />'); //如果该行处于编辑状态，则将输入的值赋给该列
			} else {
				td.html(td.find("input").val()); //如果不是处于编辑状态，则将其字段原来的值赋他本身
			}
		});
	};

	function configCancle() {
		change(0, $(this).parents("tr"));
		queryConfig(0, 10);
	};

	function configListTable() {
		$('#configListTable').DataTable({
			ordering: false,
		bAutoWidth: false,
		bPaginate: false,
		responsive: true,
		bFilter: false,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					//					"sFirst": "首页",
					//					"sPrevious": "前一页",
					//					"sNext": "后一页",
					//					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				className: "idCol text-center",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" value="' + data + '">';
					}
					return data;
				},
				sWidth: "3em"
			}, {
				data: "name",
				className: "editcol text-center",
				render:function(data,type,row) {
					if(data == undefined){
						return null;
					}
					return data;
				}
			},
			{
				data: "group",
				"bVisible": false, //隐藏列表
				render:function(data,type,row) {
					if(data == undefined){
						return null;
					}
					return data;
				}
			}, {
				data: "param",
				"bVisible": false,
				render:function(data,type,row) {
					if(data == undefined){
						return null;
					}
					return data;
				}
			}, 
			{
				data: "version",
				className: "editcol text-center" 
			}, {
				data: "confContent",
				className: "editcol text-center",
				render:function(data,type,row) {
					if(data == undefined){
						return null;
					}
					return data;
				}
			}, {
				data: "loginName",
				className: "text-center",
				render:function(data,type,row) {
					if(data == undefined){
						return null;
					}
					return data;
				}
			}, {
				render: function(data, type, row) {
					if (data === undefined) {
						return "<a href='javascript://' style='margin-right:10px;' class='btn btn-default updateConfigBtn'>修改</a><a href='javascript://'  class='btn btn-default delConfigBtn'>删除</a><a href='javascript://' style='display:none; margin-right:10px;' class='btn btn-default ensureConfigBtn'>确定</a><a href='javascript://' style='display:none'  class='btn btn-default cancleConfigBtn'>取消</a>";
					}
					return data;

				},
				className: "text-center"
			}]

		});

	};

    function queryUser() {
    	hzh.api.request(0x03020123, "queryUser", {},function(res){
    		var data = res.response.data.result;
    		var nameval = $("#userName");
    		$(data).each(function(i,e){
				var userName = e.loginName;
				var userid = e.userId;
				$('<option value='+userid+'>'+userName+'</option>').appendTo(nameval);
			});
    	});
    };
    

})(window, $)