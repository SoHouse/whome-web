(function(window,$){
	
	var cusPage;
	var cusid;
	var campid;
//	var campName;
	hzh.pages.js["customer"] = {
		
		init:function() {
			customerListTable();
			queryCustomer(0,10);
			$("#customerListBtn").bind("click",function(){
				queryCustomer(0,10);
			});
			
			//返回
			$("#beback").bind("click",function(){
				$("#home").find("input[type=text]").val("");
				queryCustomer(0,10);
			});

			
			$("#customerListTable").delegate(".cusDelBtn", "click", delCustomer);
			cusPage = new hzh.Pagination($(".pagination"), 10, function(page) {
				queryCustomer(page*10, 10);
			});
			$('#addcustomerForm').validationEngine({
				onValidationComplete:function(form,valid){
					if(valid){
			  			$(form).hzhsubmit();
			  		}else{
					hzh.alert("请讲信息填写完整！");
				}
				}
			});
			//批量删除
			$("#cuscheckBox").bind("click",selectAll);
			$("#delAllcusBtn").bind("click",delAllcus);
			updatecampaign(0,10);
//			$("#customerListTable").delegate(".cusUpdateBtn", "click", );
			
			
			
//			$("#campaignName").combogrid({valueField:'id',textField:'name',panelHeight:"auto"});
			
			$('#campaignName').combogrid({
//				toolbar:"#updateTb",
				panelWidth: 520,
				panelHeight:400,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
				{field: 'name',title: '项目名称',width:110},
					{field: 'contact',title: '项目联系人',width: 80},
					{field: 'id',title: 'ID',width: 240}
				]],	
			});
			
			$("#customerListTable").delegate(".cusUpdateBtn", "click", function(){
				var that = $(this);
				var cusTr = that.parents("tr");
				cusid = cusTr.find("td.idCol input").val();
				campid = cusTr.find("td.campaignIdCol").html();
				custd = cusTr.find(".exitcol");
				var formContent = $("#updateModal .hzhform").find("input");
				for (var i = 0; i < custd.length; i++) {
					var io = custd.eq(i).html();
					formContent.eq(i).val(io);
				}
				$("#campaignId").val(cusTr.find("td.campaignIdCol").html());	
				var campName = cusTr.find("td.campaignNamecol").html();
				$("#campaignName").combogrid("setValue",campName);
//				$("#campaignId").val(cusTr.find("td.campaignIdCol").html());					
			});
			
			var textkey = $("#updateModal").find(".textbox-text");
			$(textkey).keydown(function(event){
				var content = $(this).val();
				var idval = $("#campaignId").val();
				if(event.keyCode == 8){
					var arr=content.split(",");
					var newVal=arr.slice(0,arr.length-1).join(",");
					$(this).val(newVal);
					var idarr = idval.split(",");
					var idnewval = idarr.slice(0,arr.length-1).join(",");
					console.log(idnewval);
					$("#campaignId").val(idnewval);
				}
				return false;			
			});
		},
		destroy:function() {
			$('.combo-p').remove();
		},
		
		processForm:function(data){
			/*console.log(data);
			var errorlength = $('#addcustomerForm').find(".form-group>.formError:visible").length;
			console.log(errorlength);
			if(errorlength == 0){*/
				return data;
			/*}else{
				return ;
			}*/
		},
		processResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("添加成功！");
				queryCustomer(0,10);
				$("#myModal").modal("hide");
				$("#myModal").find("input").val("");
			}else {
				hzh.alert(res.response.data);
			}
		},
		
		updateForm:function(data){
			data["id"] = cusid;
			var id = $("#updateModal").find("input[type=hidden]");
			var ids = [];
			$(id).each(function(i,e){
				ids.push($(e).val());
			});
			var idString = '';
			idString = ids.join(",");
			data.campaignId = idString;
			return data;
					
		},
		updateBack:function(res){
			if(res.response.code == 200){
				hzh.alert("修改成功！");
				$("#updateModal").modal("hide");
				queryCustomer(0,10);
			}else {
				hzh.alert(res.response.data);
			}
		}		
	};
	
	function queryCustomer(start,num) {
		var pageName = $("#page").val();
		var zoneName = $("#zone").val();
		var adName = $("#adName").val();
		var customerName = $("#customer").val();
		var campaignName = $("#campaign").val();
		
		hzh.api.request(0x03040106, "selectADCustomer", {
			zoneName:zoneName,
			pageName:pageName,
			campaignName:campaignName,
			name:customerName,
			resourceName:adName,
			limitSatrt:start,
			limitEnd:num
		},function(res){
			if(res.response.code == 200){
				console.log(res.response.data);
				hzh.RefreshTable("#customerListTable",res.response.data.result);	
				var totalpage = res.response.data.pageCount;
				cusPage.refresh(totalpage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var dataCount = res.response.data.dataCount;
				var li = $(".pagination").find("li.active").nextAll();
				if(li.length == 1){
					$("#customerListTable_info").html("从 "+(start+1)+" 到 " +(dataCount)+ " 条/ 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#customerListTable_info").html("从 "+(0)+" 到 " +(0)+ " 条/ 共 "+(0)+" 条数据");
				}else{
					$("#customerListTable_info").html("从 "+(start+1)+" 到 " +(start+10)+ " 条/ 共 "+(dataCount)+" 条数据");
				}
			}
//			else {
//				hzh.alert("查找失败！");
//			}
		});
	};
	
	function delCustomer() {
		var cusid = $(this).parents("tr").find(".idCol input").val();
		var r = confirm("确认删除？");
		if(r==true){
			hzh.api.request(0x03040108, "delADCustomer",{id:cusid},function(res){
				if(res.response.code == 200){
					hzh.alert("删除成功！");
					queryCustomer(0,10);
				}else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}
		
	};
	
	function selectAll(){
		if(this.checked == true){
			$('#customerListTable').find("input[type='checkbox']").prop("checked",true);
		}else{
			$("#customerListTable").find("input[type='checkbox']").prop("checked",false);
		}
	};
	
	function delAllcus(){
		var ids=[];
		$('#customerListTable').find("input[type='checkbox']:checked").each(function(i,e){
//			console.log($(e).val());
			ids.push($(e).val());
		});
		if (ids.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		var idString=ids.toString();
		console.log(idString);
		var r = confirm("确定删除？");
		if(r==true){
			hzh.api.request(0x03040108, "delADCustomer", {
				id: idString
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功！");
					queryCustomer(page * 10, 10);
				} else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}
	};
	
	function updatecampaign(start,num){
		hzh.api.request(0x03040110, "selectADCampaign", {
			limitSatrt:start,
			limitEnd:num
		},function(res){
			var data = res.response.data.result;
//			$("#campaignName").combobox("loadData",data);
			$("#campaignName").combogrid("grid").datagrid("loadData", data);
			var campPager = $("#campaignName").combogrid("grid").datagrid("getPager");
            campPager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03040110, "selectADCampaign", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#campaignName").combogrid("grid").datagrid("loadData", res.response.data.result);
						campPager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
//			var cusforcamp = $("#campaignName");
//			$(data).each(function(i,e){
//			    var	campName = e.name;
//				var campid = e.id;
//				$('<option value='+campid+'>'+campName+'</option>').appendTo(cusforcamp);
//			});
			
		});
	};
	
	function customerListTable() {
		$('#customerListTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					//					"sFirst": "首页",
					//					"sPrevious": "前一页",
					//					"sNext": "后一页",
					//					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [
			{
				data: "id",
				className:"idCol text-center",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" value="' + data + '">';
					}
					return data;
				},
				sWidth: "3em"
			},{
				data: "campaignId",
				className:"campaignIdCol hide"
			},{
				data: "name",
				className: "namecol exitcol text-center"
			}, {
				data: "linkman",
				className: "linkmancol exitcol text-center"
			}, {
				data: "email",
				className: "emailcol exitcol text-center"
			}, {
				data: "remark",
				className: "remarkcol exitcol text-center",
				render:function(data,type,row){
					if(data==undefined){
						return "";
					}
					return data;
				}
			},{
				data: "sendReport",
				className: "sendReportcol hide text-center",
				render:function(data,type,row) {
					if(data === "false"){
						return "否";
					}else {
						return "是";
					}
				}
			},{
				data: "sendCycle",
				className: "sendCyclecol hide exitcol text-center"
			},{
				data: "campaignName",
				className: "campaignNamecol hide exitcol text-center"
			}, {
				data: "pageName",
				className: "text-center hide"
			}, {
				data: "resourceName",
				className: "text-center hide"
			}, {
				data: "zoneName",
				className: "text-center hide"
			},{
				render: function(data, type, row) {
					if (data === undefined) {
						return "<a href='#' data-toggle='modal' style='margin-right:10px;'data-target='#updateModal' class='btn btn-default cusUpdateBtn'>修改</a><a href='javascript://'  class='btn btn-default cusDelBtn'>删除</a>";
					}
					return data;
				},
				sWidth:"8em",
				className:"text-center"
			}]
		});
	};
		
	
})(window,$)
