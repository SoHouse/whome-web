(function(window, $) {
	var page;
	var addpagination;
	var checkedId;
	var addcompanyhtml;
	hzh.pages.js["editSurcharge"] = {
		init: function() {
			$('.formular').validationEngine();
			//查询初始化调用及分页
			addpagination = new hzh.Pagination($(".pagination"), 10, function(page) {
				queryAdd(page * 10);
			});
			queryAdd(0);
			feecompany();
			$("#expensename").change(function(){
				paycompany();
			});
			$("#addpaycompanydiv").css("display","none");
			$("#additionalfee").css("display","block");
			/*$("#days").focus(function(){
				$("#month").find("input[type=text]").attr("disabled","disabled");
			});
			$(".monthinput").focus(function(){
				$("#days").attr("disabled","disabled");
			});*/
			
			$(".modal").delegate(".dropdown", "click", function(){
				var menu = $(this).find(".menu");
				menu.fadeToggle("slow");
				$(menu).delegate("li", "click", function(event){
					var that = $(this);
					that.parents(".dropdown").find("p > span").html(that.find("span").html());
		         	$(menu).hide();
		         	event.stopPropagation();
				});
			});
			
			//时间插件
			$(".putDate").datetimebox();
			$(".icon-arrow-left").attr("class", "glyphicon glyphicon-arrow-left");
			$(".icon-arrow-right").attr("class", "glyphicon glyphicon-arrow-right");
			//计算方式
			var select='<div style="float: left;"><select><option value="">--选择--</option>';
			select+='<option value="payable">应缴费用</option><option value="overdue">逾期时间</option><option value="floor">楼层</option>';
			select+='<option value="area">面积</option><option value="count">人数</option><option value="dosage">用量</option><option value="rate">费率</option>';
			select+='</select></div>';
			var html='<div class="addProject"><div class="dropdown"><p style="font-size: 14px;" class="projectP">请选择<span style="padding: 0 5px;"></span>';
			html+='<ul class="menu menuSelect"><li>加<span>+</span></li><li>减<span>-</span></li><li>乘<span>*</span></li><li>除<span>/</span></li></ul></p></div>';
			html+=select+'</div>';
			//项目
			var selectfix = '<div class="addProject fixselect">'+select+'</div>';
			$("#addProjectBtn").bind("click",function(){
				$(html).appendTo($("#add"));
			});
			//事件绑定
			$(".addfjf").click(function(){
				$("#add").html("");
				add();
				$(selectfix).appendTo($("#add"));
			});
			$("#delButton").click(delAdd);
//			$(".fjfUpdateBtn").click(modifyAdd);
            $("#updateProjectBtn").bind("click",function(){
            	$(html).appendTo("#updatefjf");
            });
			$("#fjfTable").delegate(".fjfUpdateBtn", "click", function(){
				$("#updatefjf").html("");
				//modifyAdd();
				var table = $(this).parents("tr");
				checkedId = table.find(".idcol").html();
				console.log(checkedId);
				var tableContent = table.find("td.editcol");
				$("#utilitiesName").val(table.find(".namecol").html());
				/*$("#utilitiesCode").val(table.find(".codecol").html());
				$("#charOffDate").datetimebox("setValue",table.find("td.chargeCol").html());
				$("#standard").val(table.find(".standardcol").html());
				
				$("#cardinal").val(table.find(".cardinalcol").html());*/
				$("#rate").val(table.find(".ratecol").html());
				$("#remark").val(table.find(".remarkcol").html());
				$(selectfix).appendTo($("#updatefjf"));
			});

			//表格初始化
			fjftable();
			//自动切换小区
			$("#communityContent").click(function(){
				queryAdd(page * 10);
			});
		},
		processForm: function(data) {
			
			var thatDiv = $(".addProject");
			var selectVal = thatDiv.find("option:selected");
			var fuhao = thatDiv.find("p>span");
			var fuhaoArr=[];
			
			var selectString = '';
			var selectArr = [];
			$(selectVal).each(function(i,e){
				selectArr.push($(e).parents(".addProject").find("p>span").html());
				selectArr.push($(e).val());
			});
			selectString = selectArr.join("");
			data["lateFeeScript"] = selectString;
			data["communityId"] = hzh.api.user.communityId;
			var inputs=$("#fjfModal").find("input.form-control");
			var check = $("#deadline").find("input[type=radio]:checked");
			var timestr = "";
			var timeArr=[];
			if(check){
				var inputdays=$(check).parent().find("input[type=text]");
				$(inputdays).each(function(i,e){
					if(e.length==1){
						data.lateFee = $(e).val();
					}else{
						timeArr.push($(e).val());
						timestr = timeArr.join("-");
						data.lateFee =timestr;
					}
				});
			}
			var name = $("#expensename").find("option:checked").text();
			console.log(name);
			if(name != "附加费")
				data.utilitiesName = name;
			return data;
		},
		processResponse: function(res) {
			if (res.response.code == 200) {         
				$("#fjfModal").modal("hide");
				hzh.alert("添加成功！");
				queryAdd(page * 10);
			} else {
				hzh.alert(res.response.data);
			};
		},
		
		processResquest: function(data) {
			
			data["id"] = checkedId;
			var thatDiv = $(".addProject");
			var selectVal = thatDiv.find("option:selected");
			var fuhao = thatDiv.find("p>span");
			var fuhaoArr=[];
			
			var selectString = '';
			var selectArr = [];
			$(selectVal).each(function(i,e){
				selectArr.push($(e).parents(".addProject").find("p>span").html());
				selectArr.push($(e).val());
			});
			selectString = selectArr.join("");
			data["lateFeeScript"] = selectString;
			console.log(data);
			return data;
		},
		processBack: function(res) {
			if (res.response.code == 200) {         
				$("#fjfupdatemodal").modal("hide");
				hzh.alert("修改成功！");
				queryAdd(page * 10);
			} else {
				hzh.alert(res.response.data);
			};
		},
		
		processEnter: function(data) {
			var oInput = document.getElementById("upAddition");
			if (oInput.files.length == 0) {//判断有没有上传一个excel文档
				hzh.alert("请上传一个文件");
			} else {
				var fileSize = oInput.files[0].size;
				if (fileSize > 3145728) {
					hzh.alert("上传文件过大，请小于3M");
				} else if (fileSize) {
					hzh.showLoading();
					oInput.outerHTML = oInput.outerHTML;
					return data;
				};
				oInput.outerHTML = oInput.outerHTML;
			}
		},
		processEnterResponse: function(res) {
			if (res.response.code == 200) {
				hzh.hideLoading();
				hzh.alert("提交成功");
			};
		},
		
		destroy: function() {
			$('.combo-p').remove();
		}
	};

	function queryAdd(startNumber) {
		hzh.api.request(0x03020407, "query", {
			limitStart: startNumber,
			communityId:hzh.api.user.communityId
		}, function(res) {
			var data=res.response.data.result;
			//console.log(data);
			var dataCount = res.response.data.dataCount;
			hzh.RefreshTable("#fjfTable", res.response.data.result);
			var allPage = res.response.data.pageCount;
			page = (startNumber + 10) / 10 - 1;
			addpagination.refresh(allPage, page);
			$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
			var li = $(".pagination").find("li.active").nextAll();
			if(li.length == 1){
				$("#fjfTable_info").html("从 "+(startNumber+1)+" 到 " +(dataCount)+ " 条/ 共 "+(dataCount)+" 条数据");
			}else if(li.length == 0){
				$("#fjfTable_info").html("从 "+(0)+" 到 " +(0)+ " 条/ 共 "+(0)+" 条数据");
			}else{
				$("#fjfTable_info").html("从 "+(startNumber+1)+" 到 " +(startNumber+10)+ " 条/ 共 "+(dataCount)+" 条数据");
			}
			
//			var data = res.response.data.result;
//			$("#utilitiesName").val(data[0].utilitiesName);
		});
	};

	function delAdd() {
		var delId = "";
		$("#fjfTable").find("input:checked").each(function() {
			delId = delId + $(this).attr("data-row") + ",";
		});
		delId = delId.substring(0, delId.length - 1);
		if (delId != ""){
			var r=confirm("确认删除？");
			if (r == true) {
				hzh.api.request(0x03020309, "del", {
					id: delId
				}, function(res) {
					if (res.response.code == 200) {
						hzh.alert("删除成功");
						queryAdd(page * 10);
					}
				});
			}
		} else {
			hzh.alert("选择至少一项");
		}
	};

	function modifyAdd() {
		
//		$("#fjfModal .hzhform").attr("data-event", "0x03020301");
//		var checked = $("#fjfTable").find("input:checked");
//		if (checked.length == 1) {
//			checkedId = checked.attr("data-row");
//			$("#fjfupdatemodal").modal('show');
			var table = $(this).parents("tr");
			checkedId = table.find(".idcol").html();
			console.log(checkedId);
			var tableContent = table.find("td.editcol");
			$("#utilitiesName").val(table.find(".namecol").html());
			/*$("#utilitiesCode").val(table.find(".codecol").html());
			$("#charOffDate").datetimebox("setValue",table.find("td.chargeCol").html());
			$("#standard").val(table.find(".standardcol").html());
			
			$("#cardinal").val(table.find(".cardinalcol").html());*/
			$("#rate").val(table.find(".ratecol").html());
			$("#remark").val(table.find(".remarkcol").html());
			/*var formContent = $("#fjfupdatemodal .hzhform").find("input");
			for (var i = 0; i < tableContent.length; i++) {
				var io = tableContent.eq(i).html();
				formContent.eq(i).val(io);
			}*/
			
	};

    

	function add() {
		$("#fjfModal .hzhform").attr("data-event", "0x03020303");
		$("#fjfModal .hzhform input").val("");
	};
	
	function feecompany(){
		addcompanyhtml="<option value='2'>物业代收</option>";
		hzh.api.request(0x03070115, "queryOrderInterface", {
			communityId:hzh.api.user.communityId
		}, function(res) {
			var data = res.response.data.result;
			console.log(data);
			$.each(data, function(i,e) {
				addcompanyhtml+="<option value='1'>"+e.displayName+"</option>";                                                          
			});
			//$("#addpaycompany").html(html);
			/*$("#additionalfee").css("display","none");
			$("#addpaycompanydiv").css("display","block");*/
			
		});
	};
	
	function paycompany(){
		var val = $("#expensename").find("option:selected").val();
		if(val == 1){
			$("#addpaycompany").html(addcompanyhtml);
			$("#additionalfee").css("display","none");
			$("#addpaycompanydiv").css("display","block");
			$("#addProjectBtn").hide();			
		}
		else{
			$("#addpaycompanydiv").css("display","none");
			$("#addProjectBtn").show();
			$("#additionalfee").css("display","block");
			//$("#addpaycompany").html(html);
		}
	};
	
	function fjftable() {
		$('#fjfTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
//						"sFirst": "首页",
//						"sPrevious": "前一页",
//						"sNext": "后一页",
//						"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" value="'+data+'" class="editor-active row-cbx" data-row="' + data + '">';
					}
					return data;
				},
				className:"text-center"
	        },{
				data: "communityName",
				className:"text-center",
				render: function(data, type, row) {
					if (data === undefined) {
						return '';
					}
					return data;
				},
			},{
				data: "id",
				className:"text-center idcol"
			},{
				data: "utilitiesName",
				render: function(data, type, row) {
					if (data === undefined) {
						return '';
					}
					return data;
				},
				className:"text-center namecol"
			}, {
				data: "utilitiesCode",
				className:"text-center hide",
				render: function(data, type, row) {
					if (data === undefined) {
						return '';
					}
					return data;
				},
			}, {
				data: "lateFeeScript",
				render: function(data, type, row) {
					if (data === undefined) {
						return '';
					}
					return data;
				},
				className:"text-center"
			}, {
				data: "lateFee",
				render: function(data, type, row) {
					if (data === undefined) {
						return '';
					}
					return data;
				},
				className:"text-center standardcol"
			}, {
				data: "rate",
				render: function(data, type, row) {
					if (data === undefined) {
						return '';
					}
					return data;
				},
				className:"text-center ratecol"
			},{
				data: "remark",
				render: function(data, type, row) {
					if (data === undefined) {
						return '无数据';
					}
					return data;
				},
				className:"text-center remarkcol"
			},{
				data: "isDel",
				render: function(data, type, row) {
					if (data === "true") {
						return '是';
					}else{
						return "否"
					}
//					return data;
				},
				className:"text-center hide"
			},{
			render: function(data, type, row) {
				if (data === undefined) {
					return "<a href='#' class='btn btn-default fjfUpdateBtn' data-toggle='modal' data-target='#fjfupdatemodal'>修改</a>";
				}
				return data;
			},
			sWidth: "3em",
			className:"text-center"
		}]
	});
};

})(window, $);