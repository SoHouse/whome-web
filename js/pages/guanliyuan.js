(function(window, $) {

	var userPage;
	var userResourceId;
//	var rolePage;
	var arr=[];
	var roleNameArr=[];
	hzh.pages.js["guanliyuan"] = {
		init: function() {
			queryUser(0, 10);
//			bindhousehold(0,10);
//			bindhouseHold();
			initDatable();
			initRoleTable();
			currentPage = 0;
			$("#userbtn").bind("click", function() {
				queryUser(0, 10); //初始化用户/列表
			});
			$("#addUserBtn").bind("click", addUser);
			//返回
			$("#beback").bind("click",function(){
				$("#querybody").find("input[type=text]").val("");
				queryUser(0,10);
			});
			$("#dataTables-example").delegate(".delbtn", "click", delUser); //删除用户
			$("#dataTables-example").delegate(".updatebtn", "click", updateUser); //修改用户
			$("#dataTables-example").delegate(".ensurebtn", "click", ensure);
			$("#userRoleBox").delegate(".role-select","change",onRoleSelectChange);
//			$("#dataTables-example").keydown(function(event) {
//				var uTr = $("#dataTables-example").find(".ensurebtn:visible").parents("tr");
//				var upId = uTr.find("td.userIdColumn").html();
//				var telVal = $("#telval").val();
//				if (event.keyCode == 13) {
//					hzh.api.request(0x03020105, "updateUser", {
//						id: upId,
//						telephone: telVal
//					}, function(res) {
//						if (res.response.code == 200) {
//							hzh.alert("修改成功！");
//							change(0, uTr);
//							userPage = new hzh.Pagination($(".pagination"), 10, function(page) {
//								queryUser(page * 10, 10);
//							});
//						} else {
//							alert("修改失败！");
//						}
//					});
//				}
//			});
//			$("#querybody").keydown(function(event) {
//				var un = $("#userLoginName").val(); //获取查询条件中登录名的值
//				var tel = $("#userTel").val();
//				var roomnum = $("#useremail").val();
//				if (event.keyCode == 13) {
//					hzh.api.request(0x03020123, "queryUser", { //请求接口
//						loginName: un,
//						telephone: tel,
//						email: roomnum,
//						limitStart: 0,
//						limitEnd: 10
//					}, function(res) {
//						if (res.response.code == 200) {
//							hzh.RefreshTable("#dataTables-example", res.response.data.result); //刷新table表格，并将数据显示到页面上
//							var totalPage = res.response.data.pageCount;
//							userPage.refresh(totalPage, 10);
//							$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
//
//						} else {
//							alert("未找到该用户！");
//						}
//					});
//				}
//			});
			//确定修改用户
			$("#dataTables-example").delegate(".canclebtn", "click", cancle); //取消

			$("#ucheckBox").bind("click", uselectAll); //选中所有
			$("#checkAllBtn").bind("click", allUserDel); //删除所有

			$("#dataTables-example").delegate(".userRoleSetBtn", "click", function(){
				var uTr = $(this).parents("tr"); //找到this的祖先节点
				userResourceId = uTr.find("td.userIdColumn").html(); //取到该行的值
				$("#roleIdInput").val(userResourceId);
				var userRoleName = uTr.find("td.roleColumn");
		
				$("#saveUserRole").data("userid", userResourceId);
				$("#saveUserRole").data("roleName", userRoleName); //.data()取数据
				arr=[];
				selectedRoleArrary=[];
				selectedRoleNameArr=[];
				roleNameArr=[];
				selectUserRole();
				
			});
			$("#dataTables-example").delegate(".queryAdminBtn", "click", function(){
				 var uid = $(this).parents("tr").find(".userIdColumn").html();
				$("#uid").val(uid);
				$("#updateAdminCom").data("uid", uid);
				queryAdminCommunity("0",10);
			});

			userPage = new hzh.Pagination($("#userPage"), 10, function(page) {
				var limitStart = page * 10;
				queryUser(limitStart, 10);
			});
			
//			rolePage = new hzh.Pagination($("#rolePage"), 10, function(page) {	
//				selectUserRole(page * 10, 10);
//			});
			
			
			$("#updateAdminCom").bind("click", updateAdminCommunity);
			$("#saveUserRole").bind("click", updateUserRole);
			//加载treeTable
			$.getScript('../script/treeTable/jquery.treeTable.js', function() {
				console.log("treeTable loaded")
			});

//			$(document).ready(function() {
				$('#adduser').validationEngine();
//			});
						
			$("#btn").bind("click",function(){
				findhousehold(0,10);
			});
			
		},
		destroy: function() {
			arr=[];
			roleNameArr=[];
		}
	};
	function removeFromArray(arr, item) {
		arr.splice($.inArray(item+"", arr), 1);
		/*roleNameArr.splice($.inArray(item+"", roleNameArr), 1);	
		console.log($.inArray(item, roleNameArr), 1);*/
	};
	function onRoleSelectChange(){
		var role=$(this).data("value");
		var roleName = $(this).parents("tr").find(".uRoleCol").html();
		//console.log(roleName);
		if($(this).is(":checked")) {
			arr.push(role);
			roleNameArr.push(roleName);
		}else{
			removeFromArray(arr,role);
			removeFromArray(roleNameArr,roleName);
		}
	};
	
	//查询用户
	function queryUser(limitStart, limitEnd) {
		var un = $("#userLoginName").val(); //获取查询条件中登录名的值
		var tel = $("#userTel").val();
		var roomnum = $("#useremail").val();
		hzh.api.request(0x03020103, "queryAdmin", { //请求接口
//			hzh.api.request(0x03010101, "queryUser", { //请求接口
			loginName: un,
			telephone: tel,
			email: roomnum,
			limitStart: limitStart,
			limitEnd: limitEnd
		}, function(res) {
			if (res.response.code == 200) {
				hzh.RefreshTable("#dataTables-example", res.response.data.result); //刷新table表格，并将数据显示到页面上
				var totalPage = res.response.data.pageCount;
				userPage.refresh(totalPage, limitStart / limitEnd);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				$("#dataTables-example_info").html("第 "+(limitStart/10+1)+" 页 / 共 "+totalPage+" 页");  
				var li = $("#userPage").find("li.active").nextAll();
				if(li.length == 1){
					$("#dataTables-example_info").html("从 "+(limitStart+1)+" 到 " +(res.response.data.dataCount)+ " 条/ 共 "+(res.response.data.dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#dataTables-example_info").html("从 "+(0)+" 到 " +(0)+ " 条/ 共 "+(0)+" 条数据");
				}else{
					$("#dataTables-example_info").html("从 "+(limitStart+1)+" 到 " +(limitStart+limitEnd)+ " 条/ 共 "+(res.response.data.dataCount)+" 条数据");
				}
			} 
//			else {
//				hzh.alert("未找到该用户！");
//			}
		});
	};
	
	
	//用户绑定户，查找户
	function findhousehold(start,num){
		var inputval = $("#inputval").val();
		hzh.api.request(0x03020101, "queryHousehold", {
//			buildingNum: inputval,
//			unitNum: inputval,
			headOfHousehold: inputval,
			limitStart: start,
			limitEnd: num
//			telephone: inputval
		}, function(res) {
			var data = res.response.data.result;
			$("#householdName").combogrid("grid").datagrid("loadData", data);
			
		});
	};
		
	//添加用户
	function addUser() {
		console.log(hzh.api.user.communityId);
//		var communityId = hzh.api.user.communityId;
//		console.log(communityId);
		var addname = $("#addLoginName").val();
		var tel = $("#addTel").val();
		var addpwd = $("#addpwd").val();
		var addremark = $("#addRemark").val();
		var addmail = $("#addEmail").val();
		var bindhousehold = $("#householdId").find("input.textbox-value").val();
        var errorlength = $('#adduser').find(".form-group>.formError:visible").length;
//		console.log(errorlength);
		if(errorlength==0 && tel.length==11){
			hzh.api.request(0x03020104, "addUser", {
				loginName: addname,
				telephone: tel,
				pwd: hex_md5(addpwd),
				remark: addremark,
				email: addmail,
				householdId:bindhousehold,
				isAdmin:1
	//			communityId:communityId
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("添加成功");
					queryUser(0, 10);
					$("#profile").find("input").each(function(i, e) {
						$(e).val("");
					});
				} else if (res.response.code == 525) {
					alert("已存在！");
				} else {
					alert("添加失败！");
				}
			});
		}else{
			hzh.alert("您输入的信息有误！");
		}
		
	};

	//删除用户
	function delUser() {
		var uTr = $(this).parents("tr");
		var delId = uTr.find('td.userIdColumn').html();
		var r = confirm("确认删除？");
		if(r == true){
			hzh.api.request(0x03020106, "delUser", {
				id: delId
			}, function(res) {
				if (res.response.code == 200) {
					uTr.remove();
					hzh.alert("删除成功！");
					queryUser(0, 10);
				} else {
					alert("删除失败！");
				}
			});
		}		
	};

	//选中所有
	function uselectAll() {
		if (this.checked == true) {
			$("#dataTables-example").find("input[type='checkbox']").prop('checked', true);
		} else {
			$("#dataTables-example").find("input[type='checkbox']").prop('checked', false);
		}
	};


	//删除所有

	function allUserDel() {
		var userDelId = "";
		var userIds = [];
		$("#dataTables-example").find(".row-cbx:checked").each(function(i, e) { //each()方法遍历选中的checked
			userIds.push($(e).data("row")); //将所有checked选中的userid放入数组中
		});

		if (userIds.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		userDelId = userIds.join(","); //join()把数组中的元素用，隔开放入一个字符串
		var r = confirm("确认删除？"); //确定删除
		if (r == true) {
			hzh.api.request(0x03020106, "allUserDel", {
				id: userDelId
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功");
					$("#ucheckBox").prop('checked', false);
				}else{
					hzh.alert("删除失败！");
				}
			});
		}

	};


	function updateUser() {
		//alert("aa");
		var uTr = $(this).parents("tr"); //找到该节点的祖先节点
		var trSibNode = uTr.siblings(); //找到uTr的所有兄弟节点
		$(trSibNode).find(".ensurebtn:visible").parents("tr").each(function(i, e) {
			change(0, $(e));
		});
		change(1, uTr);
	};


	function ensure() {

		var uTr = $(this).parents("tr");
		var upId = uTr.find("td.userIdColumn").html();
		var telVal = $("#telval").val();
		var phone = /^(1\d{10})|(0\d{2,3}-?\d{7,8})$/;
		if(phone.test(telVal)&&telVal.length==11){
			hzh.api.request(0x03020105, "updateUser", {
				id: upId,
				telephone: telVal
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("修改成功！");
					change(0, uTr);
					userPage = new hzh.Pagination($(".pagination"), 10, function(page) {
						queryUser(page * 10, 10);
					});
				} else {
					alert("修改失败！");
				}
			});
		}else{
			hzh.alert("您输入的电话有误！");
		}
		
	};

	function change(status, tr) { //status为0，1，1表示按钮为显示的状态，0表示按钮为隐藏的状态；tr表示选中的行

		$(".ensurebtn", tr).toggle(status == 1); //确定按钮显示
		$(".updatebtn", tr).toggle(status == 0); //修改按钮隐藏
		$(".delbtn", tr).toggle(status == 0); //删除按钮隐藏
		$(".canclebtn", tr).toggle(status == 1); //取消按钮显示

		$(tr).find(".telColumn").each(function(i, e) { //对修改的电话字段遍历
			var td = $(e);
			if (status) {
				td.html("<input class='telColumn' id='telval' type='text' value='" + td.html() + "' />"); //如果该行处于编辑状态，则将输入的值赋给该列
			} else {
				td.html(td.find("input").val()); //如果不是处于编辑状态，则将其字段原来的值赋他本身
			}

		});
	};

	function cancle() {
		change(0, $(this).parents("tr"));
		queryUser(0, 10);
	};

	//查询用户角色
	function selectUserRole() {
		userResourceId = $("#roleIdInput").val();
		hzh.api.request(0x03020310, "selectUserRole", {
			userId: userResourceId
//			limitStart: start,
//			limitEnd: num
		}, function(res) {
			if (res.response.code == 200) {
				hzh.RefreshTable("#userRoleBox", res.response.data.result);
				//var aa=$("#userRoleBox").find("input[type=checkbox]:checked");
				//var bb = $("#userRoleBox").find("td.roleIdcol");
				//var roleIds = [];
//				console.log(arr);
//				$("#userRoleBox").find(".role-select").each(function(i,e){
//					if(arr.indexOf($(e).data("value"))>=0){
//						$(e).prop('checked', true);
//					}
//				});
				
				/*$.each(aa, function(i,e) {
					arr.push($(e).val()); 
					console.log(arr);
					$.each(bb, function(j,k) { 
						roleIds.push($(k).html());
						if($(e).val() == roleIds[j]){
							console.log($(e));
							$(e).prop('checked', true);
						}
					});
				});*/
//				var total = res.response.data.pageCount;
//				rolePage.refresh(total, start / num);
//				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				
			} else {
				alert("查找失败");
			}
		});
	};


	//修改用户角色
	function updateUserRole() {
		var uid = $(this).data("userid"); //.data()取回数据
		var uroleName = $(this).data("roleName"); //.data()取回数据
		console.log(uroleName);
//		$("#userRoleBox").find("input:checkbox[name='checkbox']:checked").each(function(i, e) { //each()方法遍历选中的checked
//			arr.push($(e).val()); //将已选中的checkbox的值放入数组中
//			roleNameVal.push($(e).parents("tr").find("td.uRoleCol").html()); //将角色名称取出并放入roleNameVal数组中
//		});
		
		var roleIds = arr.join(","); //将数组用逗号给开并传给字符串
		var roleNames = roleNameArr.join(",");
		var r = confirm("确认修改");
		if (r == true) {
			hzh.api.request(0x03020408, "updateUserRole", {
				userId: uid,
				roleIds: roleIds
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("修改成功！");
					$('#userRoleModal').modal('hide'); //隐藏弹窗
					//queryUser(0,10);
					$(uroleName).html(roleNames); //将取得的角色名赋给用户列表中的角色列中
//					roleNameArr=[];
				} else {
					hzh.alert("修改失败！");
				}
			});
		}
	};



	function treeSelector() {
		var that = this;
		var thisId = $(this).parents("tr").attr("id");
		checkTree(thisId, that.checked);
	};

	function checkTree(pid, checked) {
		$("#queryAdminTable").find("tr[pid='" + pid + "']").each(function() {
			$(this).find("input").prop("checked", checked);
			checkTree($(this).attr("id"), checked);
		});
	};

	function loadTreeDatatable(html) {
		$('#queryAdminTable').empty();
		var tableHtml = $('#queryAdminTable')[0].outerHTML;
		var treeTable = $(tableHtml);
		$('#queryAdminTable').replaceWith(treeTable);
		treeTable.html(html);
		var option = {
			theme: 'vsStyle',
			expandLevel: 4,
			beforeExpand: function($treeTable, id) {
				//判断id是否已经有了孩子节点，如果有了就不再加载，这样就可以起到缓存的作用
				if ($('.' + id, $treeTable).length) {
					return;
				}
			},
			nSelect: function($treeTable, id) {
				window.console && console.log('onSelect:' + id);
			}
		};
		treeTable.treeTable(option);
		$("#queryAdminTable").find("input").click(treeSelector);

	};

	function queryAdminCommunity(start,num) {
		var id = $("#uid").val();
		hzh.api.request(0x03020411, "queryAdminCommunity", {//0x03020412
			userId: id,
			limitStart: start,
			limitEnd: num
		}, function(res) {
//			console.log(res.response.data);
			var data = res.response.data.regionArray;
			var comData = res.response.data.communityArray;
			var ptree = buildPermissionTree(data, comData, 0);
			var html = '<tr hasChild="true" id="-1" controller="true"><td>地区</td><td>选择</td></tr>';
			html += buildPermissionTable(ptree);
			loadTreeDatatable(html);
		});
	};

	function updateAdminCommunity() {
		//		$("#updateAdminCom").bind("click", function() {
		var uid = $(this).data("uid");
		console.log(uid);
		var cId = "";
		$("#queryAdminTable").find("input").each(function() {
			if (this.checked == true) {
				var checkenVal = $(this).val();
				if (checkenVal != "undefined") {
					cId = cId + checkenVal + ",";
				}
			}
		})
		cId = cId.substring(0, cId.length - 1);

		hzh.api.request(0x03020307, "updateAdminCommunity", {
				userId: uid,
				communityIds: cId
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("修改成功");
					$('#queryCellModal').modal('hide');
				} else {
					hzh.alert("修改失败");
				}

				//				$("#updateAdminCom").unbind("click");
			})
			//		})
	};


	function buildPermissionTable(list) {
		var html = "";
		for (var i = 0; i < list.length; i++) {
			var data = list[i];
			var pid = data.parentId;
			var ckd = data.selected == 1 ? "checked='checked'" : "";
			html = html + '<tr id="' + data.id + '" pId="' + (pid == 0 ? "-1" : pid) + '"><td>' + data.cityName + '</td><td><input type="checkbox" name="communityId" value=' + data.communityId + ' data-tid=' + data.id + ' ' + ckd + '></td></tr>';
			if (data.children && data.children.length > 0){
				
				html = html + buildPermissionTable(data.children);
			}
				

		}
		return html;
	};

	function buildPermissionTree(regions, communities, parentId) {
		var ret = [];
		$.each(regions, function(i, e) {
			if (e.parentId == parentId) {
				var cld = buildPermissionTree(regions, communities, e.id);
				if (cld.length > 0)
					e.children = cld;
				else { //try finding communities in this region
					e.children = buildCommunityTree(communities, e.id);
					
				}
				ret.push(e);
			}
		});
		return ret;
	};

	function buildCommunityTree(communities, regionId) {
		var ret = [];
		$.each(communities, function(i, e) {
			if (e.cityId == regionId) {
	//			console.log(e.communityName);
				e.parentId = regionId;
				e.id = Math.random();
				e.cityName = e.communityName;
				ret.push(e);
			}
		});
		return ret;
	};
		//用户查询列表

	function initDatable() {
		$('#dataTables-example').DataTable({
			ordering: false,
			bAutoWidth: false,
			bPaginate: false,
			responsive: true,
			bFilter: false,
			//			AllowSorting:false,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",

				"oPaginate": {
					//				"sFirst": "首页",
					//				"sPrevious": "前一页",
					//				"sNext": "后一页",
					//				"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "userId",
				//              sClass: "text-center",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" data-row="' + data + '">';
					}
					return data;
				},
				className: "dt-body-center text-center",
				sWidth: "3em"
			}, {
				data: "userId",
				className: "userIdColumn text-center hide"

			}, {
				data: "telephone",
				className: "telColumn text-center"
			}, {
				data: "loginName",
				className: "text-center"
			}, {
				data: "email",
				className: "text-center"
			}, {
				data: "roleNames",
				className: "roleColumn text-center"
					//              sClass: "text-center"
			}, {
				render: function(data, type, row) {
					if (data === undefined) {
						return "<a href='javascript://' style='margin-right: 10px;' class='btn btn-default updatebtn'>修改</a><a href='javascript://' style='display:none;margin-right: 10px;'  class='btn btn-default ensurebtn'>确定</a><a href='javascript://' style='display:none;margin-right: 10px;'  class='btn btn-default canclebtn'>取消</a><a href='javascript://' style='margin-right: 10px;' class='btn btn-default delbtn'>删除</a><a href='#' data-toggle='modal' style='margin-right: 10px;' data-target='#userRoleModal' class='btn btn-default userRoleSetBtn'>角色设置</a><a href='#' data-toggle='modal' data-target='#queryCellModal' class='btn btn-default queryAdminBtn'>查询小区</a>";

					}
					return data;
				},
				className: "dt-body-center text-center ",
				sWidth: "21em"
			}]
		});
	};

	//查询角色列表
	function initRoleTable() {
		$('#userRoleBox').DataTable({
			ordering: false,
			bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
//			bPaginate: true,
			responsive: true,
			bLengthChange: false,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",

				"oPaginate": {
					"sFirst": "首页",
					"sPrevious": "前一页",
					"sNext": "后一页",
					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "roleId",
				className: "text-center roleIdcol"
			}, {
				data: "roleName",
				className: "uRoleCol text-center"
			}, {
				data: "selected",
				className: "text-center",
				render: function(data, type, row) {
					if (type === 'display') {
						if(data == 1 && $.inArray(row.roleId,selectedRoleArrary) < 0 && $.inArray(row.roleName,selectedRoleNameArr) < 0){
							arr.push(row.roleId);
							selectedRoleArrary.push(row.roleId);
							roleNameArr.push(row.roleName);
							selectedRoleNameArr.push(row.roleName);
							
						}
//						data=0;
						var checkedString = data == 1 ? 'checked = "checked" ' : "";
						return '<input type="checkbox" name="checkbox" ' + checkedString + '  class="editor-active row-cbx role-select" value="' + row.roleId + '" data-value="' + row.roleId + '" data-row="' + data + '">';
					}
					return data;
				}
			}]
		});
	};
	var selectedRoleArrary=[];
    var selectedRoleNameArr=[];


})(window, $);