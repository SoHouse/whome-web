(function(window, $) {
	var page;
	var newsPagination;
	hzh.pages.js["historyNews"] = {
		init: function() {
			//查询初始化调用及分页
			queryNews(0,10);
			newsPagination=new hzh.Pagination($(".pagination"),10,function(page){
				queryNews(page*10,10);
			});
			$("#hnTable").delegate("a","click",newsContent);
			$('#hnTable').DataTable({
				ordering: false,
				bAutoWidth: false,
				bProcessing: true,
				bFilter: false,
				bPaginate: false,
				responsive: true,
				"oLanguage": {
					"sLengthMenu": "每页显示 _MENU_ 条记录",
					"sZeroRecords": "抱歉， 没有找到",
					"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
					"sInfoEmpty": "没有数据",
					"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
					"oPaginate": {
						"sFirst": "首页",
						"sPrevious": "前一页",
						"sNext": "后一页",
						"sLast": "尾页"
					},
					"sZeroRecords": "没有检索到数据",
					"sProcessing": "<img src='./loading.gif' />",
					"sSearch": "搜索"
				},
				columns: [{
					data: "title",
					render: function(data, type, row) {
						if (data === undefined) {
							return '<a href="#" data-toggle="modal" data-target="#newsModal">无题</a>';
						}
						return '<a href="#" data-toggle="modal" data-target="#newsModal">' + data + '</a>';
					},
					className:"text-center"
				}, {
					data: "createTime",
					className:"text-center"
				}, {
					data: "dept",
					className:"text-center"
				}, {
					data: "id",
					className:"text-center"
				}]
			});
		},
		destroy: function() {},
	};

	function queryNews(start, pages) {
		hzh.api.request(0x03010304, "queryInformation", {
			loginName: hzh.api.user.loginName,
			limitStart:start,
			limitEnd:pages,
			type: "article"
		}, function(res) {
//			console.log(res);
        hzh.RefreshTable("#hnTable", res.response.data.result);
        var allPage=res.response.data.pageCount;
        page=(start+10)/10-1;
        newsPagination.refresh(allPage, page);
        $(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class","");
    });
	};

	function newsContent() {
		var newsId=$(this).parents("tr").find("td").eq(3).html();
		var newsTitle=$(this).text();
		hzh.api.request(0x03010302, "", {
			id: newsId,
			loginName: hzh.api.user.loginName,
		}, function(res) {
			var contentval =hzh.BASE64.decoder(res.response.data.content);
			$("#newsModal .modal-title").text(newsTitle);
			$("#newsModal .modal-body").html(contentval);
			$("#newsModal .modal-body img").css({"max-width":"800px"});
		});
	};
})(window, $);