(function(window, $) {
	var page;
	var noticePagination;
	var unread;
	hzh.pages.js["historyNotice"] = {
		init: function() {
			//查询初始化调用及分页
			queryNotice(0,10);
			noticePagination=new hzh.Pagination($(".pagination"),10,function(page){
				queryNotice(page*10,10);
			});
			$("#noticeTable").delegate("a","click",noticeContent);
			$('#noticeTable').DataTable({
				ordering: false,
	bAutoWidth: false,
	bProcessing: true,
	bFilter: false,
	bPaginate: false,
	responsive: true,
				"oLanguage": {
					"sLengthMenu": "每页显示 _MENU_ 条记录",
					"sZeroRecords": "抱歉， 没有找到",
					"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
					"sInfoEmpty": "没有数据",
					"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
					"oPaginate": {
						"sFirst": "首页",
						"sPrevious": "前一页",
						"sNext": "后一页",
						"sLast": "尾页"
					},
					"sZeroRecords": "没有检索到数据",
					"sProcessing": "<img src='./loading.gif' />",
					"sSearch": "搜索"
				},
				columns: [{
					data: "title",
					render: function(data, type, row) {
						if (data === undefined) {
							return '<a href="#" data-toggle="modal" data-target="#noticeModal">无题</a>';
						}
						return '<a href="#" data-toggle="modal" data-target="#noticeModal">' + data + '</a>';
					},
					className:"text-center"
				}, {
					data: "createTime",
					className:"text-center"
				}, {
					data: "dept",
					className:"text-center"
				}, {
					data: "id",
					className:"text-center"
				}],
				fnDrawCallback: function(){
					if($("#noticeTable a").length){
//						console.log(unread);
						for(var i in unread){
							var news=$("#noticeTable a").eq(unread[i]-1).html();
							$("#noticeTable a").eq(unread[i]-1).html("<strong>"+news+"</strong>");
						}
					}
				},
			});
		},
		destroy: function() {},
	};

	function queryNotice(start, pages) {
		hzh.api.request(0x03010304, "queryInformation", {
			loginName:hzh.api.user.loginName,
			limitStart:start,
			limitEnd:pages,
			type: "notice"
		}, function(res) {
//			console.log(res);
			unread=isRead(res.response.data.result, Array);
			hzh.RefreshTable("#noticeTable", res.response.data.result);
			var allPage=res.response.data.pageCount;
			page=(start+10)/10-1;
			noticePagination.refresh(allPage, page);
			$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class","");
		});
	};

	function noticeContent() {
		var newsId=$(this).parents("tr").find("td").eq(3).html();
		var newsTitle=$(this).text();
		hzh.api.request(0x03010302, "", {
			id: newsId,
			loginName: hzh.api.user.loginName
		}, function(res) {
//			console.log(res);
            var contentval =hzh.BASE64.decoder(res.response.data.content);
			$("#noticeModal .modal-title").text(newsTitle);
			$("#noticeModal .modal-body").html(contentval);
			$("#noticeModal .modal-body img").css({"max-width":"800px"});
		});
	};
	
	function isRead(data, type){
		var unRead=[];
		var unReadNum=0;
		if (data.length){
			for(var i=0; i<data.length; i++){
				if(data[i].isRead=="0"){
					unRead.push(i);
					unReadNum++;
				}
			};
			if (type == Array){
				return unRead;
			} else if (type == Number){
				return unReadNum;
			};
		}
	};
	
})(window, $);