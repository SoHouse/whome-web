$(function () {
    showUnread();
    showAmount();
    showUser();
    showNews();
    newsDetail();
    $(".container a[role='button']").click(goTo);
    $(".unread").delegate("a", "click", function () {

        var thisId = $(this).attr("for");
        unreadDetail(thisId, $(this));
    });


});

function CurentDate() {
    var now = new Date();
    var year = now.getFullYear();       //年
    var month = now.getMonth() + 1;     //月
    var day = now.getDate();            //日
    var clock = year + "-";
    if (month < 10)
        clock += "0";
    clock += month + "-";
    if (day < 10)
        clock += "0";
    clock += day;
    return (clock);
};


function isRead(data, type) {
    var unRead = [];
    var unReadNum = 0;
    if (data.length) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].isRead == "0") {
                unRead.push(i);
                unReadNum++;
            }
        }
        ;
    }
    if (type == Array) {
        return unRead;
    } else if (type == Number) {
        return unReadNum;
    }
    ;
};

function showUnread() {
    hzh.api.request(0x03010304, "queryInformation", {
        loginName: hzh.api.user.loginName,
        type: "notice",
        limitEnd: 10000
    }, function (res) {
        var unreadNum = isRead(res.response.data.result, Array);        
        if (unreadNum.length == 0) {
            $(".unread strong").append("现在没有消息");
            return;
        }
        for (var i = 0; i < Math.min(10, unreadNum.length); i++) {
        	var resultVal = res.response.data.result[unreadNum[i]];
            $(".unread strong").append('<a href="#" data-toggle="modal" data-target="#noticeModal" for="' + resultVal.id + '">' + resultVal.title + '</a><br>');
        }
    });
};

function showAmount() {
    hzh.api.request(0x03010201, "queryPaymentStatus", {
        loginName: hzh.api.user.loginName,
        limitEnd: 10000
    }, function (res) {
        var amount = 0;
        for (var i in res.response.data) {
            if (res.response.data[i].balance) {
                amount += res.response.data[i].balance;
            }
        }
        if (amount == 0) {
            $(".amount strong").html('当前没有欠费');
        } else {
            $(".amount strong").append('当前欠费总额' + amount + '元');
        }
    });
};

function showUser() {
    var name = hzh.api.user.headOfHousehoName;
    $(".userName").html(name + ':');
};

function goTo() {
    var oUrl = $(this).attr("for");
    hzh.pages.load(oUrl + ".html", false, oUrl);
};

function showNews() {
    hzh.api.request(0x03010304, "queryInformation", {
        loginName: hzh.api.user.loginName,
        limitStart: 0,
        limitEnd: 10000,
        type: "article"
    }, function (res) {
        if (res.response.data.result.length == 0) {
            $("#todayList").append('现在没有新闻');
            return;
        }
        for (var i = 0; i < Math.min(10, res.response.data.result.length); i++) {
            $("#todayList").append('<a class="newsTitle list-group-item" href="#" for="' + res.response.data.result[i].id + '"><h4>' + res.response.data.result[i].title + '</h4></a>');
        }
    });
};

function newsDetail() {
    $("#todayList").delegate("a", "click", function () {
        var thisId = $(this).attr("for");
        hzh.api.request(0x03010302, "", {
            id: thisId,
            loginName: hzh.api.user.loginName
        }, function (res) {
            if (res.response.code == 200) {
                var dataObject = res.response.data;
                var contentval =hzh.BASE64.decoder(dataObject.content);
                $("#page-wrapper").html('<div class="row"><div class="col-lg-12"><h1 class="page-header text-center">' + dataObject.title + '</h1><h5 class="text-right">' + dataObject.createAuthor + '<br>' + dataObject.createTime + '</h5></div></div>');
                $("#page-wrapper").append('<div class="panel-body text-center">' + contentval + '</div>')
            }
        });

    });
};

function unreadDetail(id, ev) {
    var newsId = id;
    var newsTitle = ev.text();
    hzh.api.request(0x03010302, "", {
        id: newsId,
        loginName: hzh.api.user.loginName
    }, function (res) {
        $("#noticeModal .modal-title").text(newsTitle);
        var result = res.response.data;
        var resultcontent = hzh.BASE64.decoder(result.content);
        $("#noticeModal .modal-body").html(resultcontent);
        $("#noticeModal .modal-body img").css({"max-width": "800px"});
    });
};

upImg: function(id, cb) {
				var fd = new FormData();  
				fd.append(id, document.getElementById(id).files[0]);  
				var xhr = new XMLHttpRequest();  
//				xhr.upload.addEventListener("progress", uploadProgress, false);  
				xhr.addEventListener("load", uploadComplete, false);  
				xhr.addEventListener("error", uploadFailed, false);  
				xhr.addEventListener("abort", uploadCanceled, false);  
				xhr.open("POST", "promotion/image/add", true);  
				xhr.send(fd);
				xhr.onreadystatechange = function(a, b, c) {
					if (a.currentTarget.readyState == 4)
					console.log(a)
				}
				function uploadProgress(evt) {  
					if (evt.lengthComputable) {  
						var percentComplete = Math.round(evt.loaded * 100 / evt.total);  
						document.getElementById('progressNumber').innerHTML = percentComplete.toString() + '%';  
					}
					else {  
						document.getElementById('progressNumber').innerHTML = 'unable to compute';  
					}  
				}  
				  
				function uploadComplete(evt) {  
					/* This event is raised when the server send back a response */  
					var ret = $.parseJSON(evt.target.responseText);
					console.log(ret);
					if(cb) {
						cb(ret);
					}
				}  
				  
				function uploadFailed(evt) {  
					console.log("There was an error attempting to upload the file.");  
				}  
				  
				function uploadCanceled(evt) {  
					console.log("The upload has been canceled by the user or the browser dropped the connection.");  
				}
			}



