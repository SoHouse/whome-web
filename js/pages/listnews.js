﻿(function(window, $) {

	var newslistPage;
	var page;
	//	var cid;
	hzh.pages.js["listnews"] = {
		init: function() {
			queryInformation(0, 10);
			//初始化列表
			queryInforTable();
			findSection(0,10);
			//查询新闻函数绑定
			$("#infroBtn").bind("click", function() {
				queryInformation(0, 10);
			});
			
			$('#sectionSelect').combogrid({
//				toolbar:"#updateTb",
				panelWidth: 500,
				panelHeight:370,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					
					{field: 'name',title: '栏目名称',width: 180},
					{field: 'createTime',title: '创建时间',width: 225},
					{field: 'id',title: 'ID',width:100,hidden: true}
				]],	
			});
			//新闻发布newsrelease(){releaseBtn  releaseBtn
			$("#infroTable").delegate(".releaseBtn", "click", newsrelease);
			$("#infroTable").delegate(".unreleaseBtn", "click", newsrelease);
			//新闻内容查看绑定
			$("#infroTable").delegate(".listBtn", "click", newslist);
			
			$("#infroTable").delegate(".delInforBtn", "click", delInformation);
			
			//返回
			$("#beback").bind("click",function(){
				$("#home").find("input[type=text]").val("");
				queryInformation(0,10);
			});
			
			//查询新闻分页
			newslistPage = new hzh.Pagination($(".pagination"), 10, function(page) {
				//var limitStart = page * 10;
				queryInformation(page * 10, 10);
			});
			
			$("#infroTable").delegate(".listBtn", "click", function(){
//				newslist();
				
				var tr = $(this).parents("tr");
				var communityIds = $(tr).find(".communityIdCol").html();
				var secId = $(tr).find(".sectionIdCol").html();
				$("#sectionSelect").combogrid("setValue",secId);
				$("#communityId").val(communityIds);
			});
			
			
			$("#newscheckBox").bind("click",selectAll);
			$("#delAllBtn").bind("click",delAllNews);
//			fillCommunities();
			KindEditor.basePath = '/whome-web/editor/';
//			KindEditor.basePath = '/whome/editor/';
			var element = $("#contentSelect");
			var ngModel = true;
			var inited = false;

			editor = KindEditor.create('#contentSelect', {
				width: "100%",
				height: 300,
				allowPreviewEmoticons: false,
				allowImageUpload: false,
				sinastorage_bucket: "whome.southindustry.com",
				sinastorage_ak: "q6z75g1Z2bOlfew1JBw8",
				sinastorage_sk: "a9e7f67ead0df2be242283d3dc895b84718ebff5",
				afterChange: function() {
					KindEditor.sync(element);
					if (ngModel) {
						// ngModel.$setViewValue(element.val());
						if (inited) {
							//第一次加载不执行change ，否则 formname.fieldname.$dirty 判断会有误。 
							element.trigger('change');
						}
					}
					inited = true;
				},
				items: [
					'source', '|', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
					'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
					'insertunorderedlist', '|', 'emoticons', 'qiniuupload', 'link', 'more','insertvideo', 'fullscreen','|','media'
				]
			});

		},
		destroy: function() {
			KindEditor.remove("#contentSelect");
			$('.combo-p').remove();
		},
		processForm: function(data) {
			
			data.content=hzh.BASE64.encoder(data.content);
			data.scopeId=$("#communityId").val();
			hzh.showLoading();
			return data;
		},
		processResponse: function(res) {
			hzh.hideLoading();
			if (res.response.code == 200) {
				$('#newslistModal').modal('hide');
				hzh.alert("修改成功");
				queryInformation(page, 10);
			} else {
				hzh.alert("修改失败，请稍后再试");
			}
		}
	};

	//查询新闻内容
	function queryInformation(start, num) {
		var type = $("#type").val();
		var inforName = $("#loginName").val();
		var infroTitle = $("#title").val();
		hzh.api.request(0x03010304, "queryInformation", {
			type: type,
			loginName: hzh.api.user.loginName,
			title: infroTitle,
			limitStart: start,
			limitEnd: num
//			householeId:hzh.api.user.householdId
		}, function(res) {
			if (res.response.code == 200) {
				var data = res.response.data.result;
				hzh.RefreshTable("#infroTable", data);
				var totalPage = res.response.data.pageCount;
				page = start;
				newslistPage.refresh(totalPage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var dataCount = res.response.data.dataCount;
				var li = $(".pagination").find("li.active").nextAll();
				if(li.length == 1){
					$("#infroTable_info").html("从 "+(start+1)+" 到 " +(dataCount)+ " 条/ 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#infroTable_info").html("从 "+(0)+" 到 " +(0)+ " 条/ 共 "+(0)+" 条数据");
				}else{
					$("#infroTable_info").html("从 "+(start+1)+" 到 " +(start+10)+ " 条/ 共 "+(dataCount)+" 条数据");
				}
			}
		});
	};

	//查询表单内容，并返回值给texterea中
	function newslist() {
		var nTr = $(this).parents("tr");
		var newsid = nTr.find(".idCol input").val();
		var loginName = hzh.api.user.loginName;
		hzh.api.request(0x03010302, "newslist", {
			id: newsid,
			loginName: loginName
		}, function(res) {
			if (res.response.code == 200) {
				var newsdata = res.response.data;
				var contentval = hzh.BASE64.decoder(newsdata.content);
				//编辑新闻
				editor.html(contentval); //将返回的content赋给editor
				$("#description").val(newsdata.description);
				$("#idInput").val(newsdata.id);
				$("#titleInput").val(newsdata.title);
				$("#summary").val(newsdata.summary);
				$("#executiveEditor").val(newsdata.executiveEditor);
				$("#source").val(newsdata.source);
				
				$("#createAuthor").val(newsdata.createAuthor);
				$("#deptSelect").val(newsdata.dept);
				$(".type").val(newsdata.type);
				
				$("#communitySelect").combogrid("setValue",newsdata.communityId);
//				var cid = newsdata.communityId;
//				cid = "," + cid + ",";
//				var checkboxes = $("#communitySelect").find("input");
//				checkboxes.each(function(i, e) {
//					var value = $(e).val();
//					if (cid.indexOf("," + value + ",") >= 0) {
//						$(this).prop("checked", true);
//					} else {
//						$(this).prop("checked", false);
//					}
//				});
			} else {
				hzh.alert(res.response.data);
			}
		});
	};
	
	
	function delInformation() {
		var nTr = $(this).parents("tr");
		var newsid = nTr.find(".idCol input").val();
		var r = confirm("确认删除？");
		if (r == true) {
			hzh.api.request(0x03020304, "delInformation", {
				id: newsid
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功");
					queryInformation(0, 10);
					
				} else {
					hzh.alert("删除失败！");
				}
			});
		}

	};
	
	function selectAll() {		
		if(this.checked == true){
			$("#infroTable").find("input[type='checkbox']").prop('checked', true);
		}else{
			$("#infroTable").find("input[type='checkbox']").prop('checked', false);
		}
	};
	
	function delAllNews() {
		var ids = [];
		var idString = "";
		$("#infroTable").find(".row-cbx:checked").each(function(i,e){
			ids.push($(e).val());
		});
		if (ids.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		idString = ids.join(",");
		var r = confirm("确认删除？");
		if(r==true){
			hzh.api.request(0x03020304, "delInformation", {
				id: idString
			}, function(res) {
				console.log(res.response.code);
				if(res.response.code == 200){
					hzh.alert("删除成功！");
					queryInformation(0, 10);
					$("#newscheckBox").prop('checked', false);
				}else{
					hzh.alert("删除失败！");
				}
			});
		}
		
	};
	
	function queryInforTable() {
		$('#infroTable').DataTable({
			orderable: true,
			ordering: false,
			bAutoWidth: false,
			bPaginate: false,
			responsive: true,
			bFilter: false,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					//					"sFirst": "首页",
					//					"sPrevious": "前一页",
					//					"sNext": "后一页",
					//					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [ {
				data: "id",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" value="' + data + '">';
					}
					return data;
				},
				sWidth: "3em",
				className: "idCol text-center"
			},{
				data: "createTime",
				className: "text-center"
			}, {
				data: "dept",
				className: "text-center",
				render: function(data, type, row) {
					if (data == "article") {
						return "新闻";
					} else {
						return "紧急通知";
					}
				}
			}, {
				data: "title",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
//					return "<a href='http://192.168.1.55:80/"+row.fileName+"' target='_blank'>"+data+"</a>";
//					return "<a href='http://192.168.1.114:80/"+row.fileName+"' target='_blank'>"+data+"</a>";
					return "<a href='http://112.124.114.224:90/"+row.fileName+"' target='_blank'>"+data+"</a>";
				},
				className: "titleCol text-center"
			}, {
				data: "type",
				render: function(data, type, row) {
					if (data == "T") {
						return "图文";
					}else if(data =="I"){
						return "图集";
					}else if(data=="V"){
						return "视频";
					}else{
						return "";
					}
					return data;
				},

				className: "text-center hide"
			},{
				data: "executiveEditor",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
				className: "text-center hide"
			},{
				data: "communityId",
				className: "text-center hide communityIdCol",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
			}, {
				data: "summary",
				className: "text-center hide summaryCol",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
			}, {
				data: "sectionId",
				className: "text-center hide sectionIdCol",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
			}, {
				data: "publish",
				className: "text-center statusCol",
				render: function(data, type, row) {
					if (data == "1") {
						return "已发布";
					}else{
						return "<div style='color:red;'>未发布</div>";
					}
					return data;
				},
			},{
				render: function(data, type, row) {
					if (row.publish == "1") {//data-toggle='modal' data-target='#newsContentModal'
						return "<a href='#' style='margin-right:10px;' class='btn btn-default listBtn' data-toggle='modal' data-target='#newslistModal'>编辑</a><a href='javascript://' style='margin-right:10px;' class='btn btn-default delInforBtn'>删除</a><a href='javascript://' class='btn btn-default unreleaseBtn'>取消发布</a>";
					}else{
						return "<a href='#' style='margin-right:10px;' class='btn btn-default listBtn' data-toggle='modal' data-target='#newslistModal'>编辑</a><a href='javascript://' style='margin-right:10px;' class='btn btn-default delInforBtn'>删除</a><a href='javascript://' class='btn btn-default releaseBtn'>发布</a>";
					}
					return data;
				},
				sWidth: "14em",
				className: "text-center"
			}]

		});

	};
	
	function findSection(start,num){
		hzh.api.request(0x03060006 , "querySection", {
			
			limitStart: start,
			limitEnd: num
		},function(res){
			var data = res.response.data.result;
			$("#sectionSelect").combogrid("grid").datagrid("loadData", data);
			var sectionPager = $("#sectionSelect").combogrid("grid").datagrid("getPager");
            sectionPager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03060006, "querySection", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#sectionSelect").combogrid("grid").datagrid("loadData", res.response.data.result);
						sectionPager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
		});
	};
	
	//releaseBtn   0x03010310
	function newsrelease(){
		var newsId = $(this).parents("tr").find(".idCol input").val();
		var r = confirm("确认发布？")
		if(r == true){
			hzh.api.request(0x03010310,"newsrelease",{
				informationId:newsId
			},function(res){
				if(res.response.code == 200){
					hzh.alert("发布成功！");
					queryInformation(0,10);
				}else{
					hzh.alert("发布失败！");
				}
			});
		}
		
	}
	
})(window, $);