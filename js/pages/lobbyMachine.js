(function(window, $) {

	var lobbyMachinePage;
	var machineId;
//	var buildName;
//	var updatebuildName;
	hzh.pages.js["lobbyMachine"] = {
		init: function() {
//			
			initLobbyMachineTable();
			queryLobbyMachine(0, 10);
			updateBuildNum(0,10);
			$("#queryBuildBtn").bind("click",function(){
				queryLobbyMachine(0, 10);
			});
			$("#beback").bind("click",function(){
				$("#querybody").find("input[type=text]").val("");
				queryLobbyMachine(0, 10);
			});
			bindBuilding(0,10);
			addPage(0,10);
			//分页
			lobbyMachinePage = new hzh.Pagination($("#lobbyMachinePage"), 10, function(page) {
				queryLobbyMachine(page * 10, 10);
			});
			$("#mUpdateBtn").bind("click",function(){
//				updatebuildName = $("#mBuildNum").val();mUpdateBtn
				updateBuildNum(0,10);
			});
			
			 //删除所有
			$("#lcheckBox").bind("click",lselectAll); 
			$("#checkAllBtn").bind("click",allUserDel);
			$("#lobbyMachineTable").delegate(".delLobbyMachineBtn","click",delLobbyMachine);
			$("#lobbyMachineTable").delegate(".mUpdateBtn","click",function(){
				var tr = $(this).parents("tr");
				var buildId = tr.find(".buildIdCol").html();
				var pageId = tr.find(".pageIdCol").html()
				machineId = tr.find('td.idCol>input').val();
				$("#updatemBuildNum").combogrid("setValue",buildId);
				$("#updatePage").combogrid("setValue",pageId);
			});
			//定义列表
			$('#addBuildNum').combogrid({
				toolbar:"#machineTb",
				panelWidth: 475,
				panelHeight:420,
				textField:"buildingNum",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'id',title: 'id',width: 70,hidden: true}, 
					{field: 'buildingNum',title: '楼宇号',width: 120,align:'center'},
					{field: 'storeyCount',title: '楼层数',width: 100,align:'center'},
					{field: 'communityName',title: '所属小区',width: 150,align:'center'}
				]],	
			});
			
			$('#updatemBuildNum').combogrid({
				toolbar:"#updateMtb",
				panelWidth: 475,
				panelHeight:420,
				textField:"buildingNum",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'id',title: 'id',width: 70,hidden: true}, 
					{field: 'buildingNum',title: '楼宇号',width: 120,align:'center'},
					{field: 'storeyCount',title: '楼层数',width: 100,align:'center'},
					{field: 'communityName',title: '所属小区',width: 150,align:'center'}
				]],	
			});
			
			$('#updatePage').combogrid({
				toolbar:"#pageTb",
				panelWidth: 475,
				panelHeight:420,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'id',title: 'id',width: 70,hidden: true}, 
					{field: 'name',title: '页面名称',width: 150,align:'center'},
					{field: 'resourceName',title: '页面对应广告',width: 220,align:'center'},
					//{field: 'communityName',title: '所属小区',width: 150,align:'center'}
				]],	
			});
			/*$(".datagrid-view").click(getSelected);*/
			
			$("#mSearchBtn").bind("click",function(){
//				buildName = $("#mBuildNum").val();
				bindBuilding(0,10);
			});
			
			$("#searchPageBtn").bind("click",function(){
				addPage(0,10);
			});
			
			
		},
		destroy: function() {
			$('.combo-p').remove();
		},
		processForm:function(data){
			return data;
			
		},
		processResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("添加成功！");
				queryLobbyMachine(0,10);
				$("#addlobbyMachineModal").modal("hide");
				
			}else{
				hzh.alert(res.response.data);
			}
		},
		updateForm:function(data){
			data["id"] = machineId;
			return data;
		},
		updateResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("修改成功！");
				$("#updatelobbyMachineModal").modal("hide");
				$("#updatelobbyMachineModal").find("input").val("");
				queryLobbyMachine(0,10);
			}else{
				hzh.alert("修改失败！");
			}
		}
	};
	
	/*0x03070109  addLobbyMachine
0x03070110  delLobbyMachine
0x03070111 queryLobbyMachine
0x03070112 updateLobbyMachine  mUpdateBtn*/

	//查询大堂机
	function queryLobbyMachine(start, num) {
		var buildnum = $("#buildingNum").val(); //获取查询条件中登录名的值
		var name = $("#loginName").val();
		hzh.api.request(0x03070111, "queryLobbyMachine", { 
			buildingNum:buildnum,
			loginName:name,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			if (res.response.code == 200) {
				hzh.RefreshTable("#lobbyMachineTable", res.response.data.result); //刷新table表格，并将数据显示到页面上
				var totalPage = res.response.data.pageCount;
				lobbyMachinePage.refresh(totalPage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var li = $("#lobbyMachinePage").find("li.active").nextAll();
				if(li.length == 1){
					$("#lobbyMachineTable_info").html("从 "+(start+1)+" 到 " +(res.response.data.dataCount)+ " 条/ 共 "+(res.response.data.dataCount)+" 条数据");
				}else if(li.length==0){
					$("#lobbyMachineTable_info").html("从 "+(0)+" 到 " +(0)+ " 条/ 共 "+(0)+" 条数据");
				}else{
					$("#lobbyMachineTable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条/ 共 "+(res.response.data.dataCount)+" 条数据");
				}
			} 
//			else {
//				hzh.alert("未找到该用户！");
//			}
		});
	};
	//绑定 楼宇
	function bindBuilding(start,num) {
		var buildName = $("#mBuildNum").val();
		//var commName = $("#communityName").val();
		hzh.api.request(0x03060015,"queryBuilding",{
			buildingNum:buildName,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			var data = res.response.data.result;
            $("#addBuildNum").combogrid("grid").datagrid("loadData", data);
            var buildPage=$("#addBuildNum").combogrid("grid").datagrid("getPager");
            buildPage.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03060015, "queryHousehold", {
                		//userId:hzh.api.user.userId,
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#addBuildNum").combogrid("grid").datagrid("loadData", res.response.data.result);
						buildPage.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
		});
	};
	
	function updateBuildNum(start,num) {
		var updatebuildName = $("#updateBuildNum").val();
		hzh.api.request(0x03060015,"queryBuilding",{
			buildingNum:updatebuildName,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			var data = res.response.data.result;
            $("#updatemBuildNum").combogrid("grid").datagrid("loadData", data);
            var buildPage=$("#updatemBuildNum").combogrid("grid").datagrid("getPager");
            buildPage.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03060015, "queryHousehold", {
                		//userId:hzh.api.user.userId,
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#updatemBuildNum").combogrid("grid").datagrid("loadData", res.response.data.result);
						buildPage.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
		});
	};
	
	function addPage(start,num) {
		var pagename = $("#pageName").val();
		hzh.api.request(0x03040115, "selectADPage", {
			name:pagename,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			var data = res.response.data.result;
            $("#updatePage").combogrid("grid").datagrid("loadData", data);
            var adPage=$("#updatePage").combogrid("grid").datagrid("getPager");
            adPage.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03040115, "selectADPage", {
                		//userId:hzh.api.user.userId,
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#updatePage").combogrid("grid").datagrid("loadData", res.response.data.result);
						adPage.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
		});
	};
	
	/*function add() {
		$("#addlobbyMachineModal .hzhform").attr("data-event", "0x03070109");
		$("#fjfModal .hzhform input").val("");
	};*/
	
	function updateLobbyMachine(){
		var mbuildId = $(this).parents("tr").find(".buildIdCol").html();
		
	}
	
	function delLobbyMachine() {
		var uTr = $(this).parents("tr");
		var delId = uTr.find('td.idCol>input').val();
		var r = confirm("确认删除？");
		if(r == true){
			hzh.api.request(0x03070110, "delLobbyMachine", {
				id: delId
			}, function(res) {
				if (res.response.code == 200) {
					uTr.remove();
					hzh.alert("删除成功！");
					queryLobbyMachine(0, 10);
				} else {
					alert("删除失败！");
				}
			});
		}		
	};

	//选中所有
	function lselectAll() {
		if (this.checked == true) {
			$("#lobbyMachineTable").find("input[type='checkbox']").prop('checked', true);
		} else {
			$("#lobbyMachineTable").find("input[type='checkbox']").prop('checked', false);
		}
	};

	//删除所有
	function allUserDel() { 
		var lDelId = "";
		var lIds = [];
		$("#lobbyMachineTable").find(".row-cbx:checked").each(function(i, e) { //each()方法遍历选中的checked
			lIds.push($(e).val()); //将所有checked选中的userid放入数组中
		});

		if (lIds.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		lDelId = lIds.join(","); //join()把数组中的元素用，隔开放入一个字符串
		console.log(lDelId);
		var r = confirm("确认删除？"); //确定删除
		if (r == true) {
			hzh.api.request(0x03070110, "delLobbyMachine", {
				id: lDelId
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功");
					$("#lcheckBox").prop('checked', false);
					queryLobbyMachine(0, 10);
				}else{
					hzh.alert("删除失败！");
				}
			});
		}

	};

	function initLobbyMachineTable() {
		$('#lobbyMachineTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bPaginate: false,
			responsive: true,
			bFilter: false,
			//			AllowSorting:false,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_/共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",

				"oPaginate": {
					//				"sFirst": "首页",
					//				"sPrevious": "前一页",
					//				"sNext": "后一页",
					//				"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" value="' + data + '">';
					}
					return data;
				},
				className: "text-center idCol",
//				sWidth: "3em"
			}, {
				data: "loginName",
				className: "text-center"

			}, {
				data: "createTime",
				className: "text-center",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}

			}, {
				data: "isLobbyMachine",
				render:function(data,type,row){
					if(data == "0"){
						return "否";
					}else{
						return "是";
					}
				},
				className: "text-center"

			}, {
				data: "buildingId",
				className: "text-center buildIdCol hide",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}

			}, {
				data: "buildingNum",
				className: "text-center",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}

			}, {
				data: "communityName",
				className: "text-center",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			}, {
				data: "pageName",
				className: "text-center",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			},{
				data: "pageId",
				className: "text-center pageIdCol hide",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
				}
			},{
				render: function(data, type, row) {
					if (data === undefined) {
						//<a href='#' data-toggle='modal' style='margin-right: 10px;' data-target='#userRoleModal' class='btn btn-default userRoleSetBtn'>角色设置</a>
						//<a href='javascript://' style='display:none;margin-right: 10px;'  class='btn btn-default ensurebtn'>确定</a>
						//<a href='javascript://' style='display:none;margin-right: 10px;'  class='btn btn-default canclebtn'>取消</a>
						return "<a href='#' data-target='#updatelobbyMachineModal' data-toggle='modal' style='margin-right: 10px;'  class='btn btn-default mUpdateBtn'>修改</a><a href='javascript://' class='btn btn-default delLobbyMachineBtn'>删除</a>";

					}
					return data;
				},
				className: "text-center",
				sWidth: "8em"
			}]
		});
	};


})(window, $);