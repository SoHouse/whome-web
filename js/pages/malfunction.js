(function(window, $) {

	var malfunctionPage;
//	var buildName;
//	var updatebuildName;
	hzh.pages.js["malfunction"] = {
		init: function() {
//			
			initRepairTable();
			queryRepair(0, 10);
			//updateBuildNum(0,10);
			/*$("#queryBuildBtn").bind("click",function(){
				queryLobbyMachine(0, 10);
			});
			$("#beback").bind("click",function(){
				$("#buildingNum").val("");
				queryLobbyMachine(0, 10);
			});*/
			//分页
			malfunctionPage = new hzh.Pagination($("#malPage"), 10, function(page) {
				queryRepair(page * 10, 10);
			});
			/*$("#mUpdateBtn").bind("click",function(){
				updatebuildName = $("#mBuildNum").val();mUpdateBtn
				updateBuildNum(0,10);
			});*/
			
			 
			$("#checkBox").bind("click",uselectAll);
			$("#delAllBtn").bind("click",allMalDel);
			 
			$("#repairTable").delegate(".updateBtn","click",updateRepair);
			$("#repairTable").delegate(".delBtn","click",delRepair);
			
			
			/*$("#mSearchBtn").bind("click",function(){
				bindBuilding(0,10);
			});*/
			
			
		},
		destroy: function() {
		}
		/*processForm:function(data){
			return data;
			
		},
		processResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("添加成功！");
				queryLobbyMachine(0,10);
				$("#addlobbyMachineModal").modal("hide");
				
			}else{
				hzh.alert(res.response.data);
			}
		},
		updateForm:function(data){
			return data;
		},
		lobbyMachine:function(res){
			if(res.response.code == 200){
				hzh.alert("修改成功！");
				$("#updatelobbyMachineModal").modal("hide");
			}else{
				hzh.alert("修改失败！");
			}
		}*/
	};
	
	/*0x03070107 queryRepair
 0x03070108 updateRepair
0x03070106 delRepair*/

	//查询
	function queryRepair(start, num) {
		//var buildnum = $("#buildingNum").val(); //获取查询条件中登录名的值
		hzh.api.request(0x03070107, "queryRepair", { 
			//buildingNum:buildnum,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			if (res.response.code == 200) {
				hzh.RefreshTable("#repairTable", res.response.data.result); //刷新table表格，并将数据显示到页面上
				var totalPage = res.response.data.pageCount;
				malfunctionPage.refresh(totalPage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var li = $("#malPage").find("li.active").nextAll();
				if(li.length == 1){
					$("#repairTable_info").html("从 "+(start+1)+" 到 " +(res.response.data.dataCount)+ " 条/ 共 "+(res.response.data.dataCount)+" 条数据");
				}else if(li.length==0){
					$("#repairTable_info").html("从 "+(0)+" 到 " +(0)+ " 条/ 共 "+(0)+" 条数据");
				}else{
					$("#repairTable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条/ 共 "+(res.response.data.dataCount)+" 条数据");
				}
			} 
//			else {
//				hzh.alert("未找到该用户！");
//			}
		});
	};
	
	function updateRepair(){
		var tr = $(this).parents("tr");
		var id = tr.find('td.idCol > input').data("value");
		var r = confirm("确认修改");
		if(r == true){
			hzh.api.request(0x03070108,"updateRepair",{id:id},function(res){
				if(res.response.code == 200){
					hzh.alert("修改成功！");
					queryRepair(0,10);
				}else{
					hzh.alert("修改失败");
				}
			});
		}
	}
	
	//删除
	function delRepair() {
		var uTr = $(this).parents("tr").find('td.idCol > input');
		var delId = uTr.data("value");
		var r = confirm("确认删除？");
		if(r == true){
			hzh.api.request(0x03070106, "delRepair", {
				id: delId
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功！");
					queryRepair(0,10);
				} else {
					alert("删除失败！");
				}
			});
		}		
	};

	//选中所有
	function uselectAll() { 
		if (this.checked == true) {
			$("#repairTable").find("input[type='checkbox']").prop('checked', true);
		} else {
			$("#repairTable").find("input[type='checkbox']").prop('checked', false);
		}
	};

	//删除所有
	function allMalDel() {
		var idString = "";
		var ids = [];
		$("#repairTable").find(".row-cbx:checked").each(function(i, e) { //each()方法遍历选中的checked
			ids.push($(e).data("value")); //将所有checked选中的userid放入数组中
		});

		if (ids.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		idString = ids.join(","); //join()把数组中的元素用，隔开放入一个字符串
		console.log(idString);
		var r = confirm("确认删除？"); //确定删除
		if (r == true) {
			hzh.api.request(0x03020106, "allMalDel", {
				id: idString
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功!");
					$("#checkBox").prop('checked', false);
					queryRepair(0, 10);
				}else{
					hzh.alert("删除失败！");
				}
			});
		}

	};

	function initRepairTable() {
		$('#repairTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bPaginate: false,
			responsive: true,
			bFilter: false,
			//			AllowSorting:false,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_/共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",

				"oPaginate": {
					//				"sFirst": "首页",
					//				"sPrevious": "前一页",
					//				"sNext": "后一页",
					//				"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" data-value="' + data + '">';
					}
					return data;
				},
				className: "text-center idCol"
			},{
				data: "headOfHousehold",
				render:function(data,type,row){
					
					if(data == undefined){
						return "";
					}
					return data;
				},
				className: "text-center"

			}, {
				data: "createTime",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
				className: "text-center"

			}, {
				data: "telephone",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
				className: "text-center"

			}, {
				data: "address",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
				className: "text-center"

			},{
				data: "timeOfAppointment",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
				className: "text-center"

			},{
				data: "status",
				render:function(data,type,row){
					if(data == "1"){
						return "已修";
					}else{
						return "未修"
					}
					return data;
				},
				className: "text-center"

			},{
				data: "remark",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
				className: "text-center hide"

			},{
				render: function(data, type, row) {
					if (data === undefined) {
						//<a href='#' data-toggle='modal' style='margin-right: 10px;' data-target='#userRoleModal' class='btn btn-default userRoleSetBtn'>角色设置</a>
						//<a href='javascript://' style='display:none;margin-right: 10px;'  class='btn btn-default ensurebtn'>确定</a>
						//<a href='javascript://' style='display:none;margin-right: 10px;'  class='btn btn-default canclebtn'>取消</a>
						return "<a href='javascript://' style='margin-right: 10px;'  class='btn btn-default updateBtn'>修改</a><a href='javascript://' class='btn btn-default delBtn'>删除</a>";
					}
					return data;
				},
				className: "text-center",
				sWidth: "7em"
			}]
		});
	};


})(window, $);