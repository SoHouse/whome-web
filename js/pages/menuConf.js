(function(window,$){
	var menupages;
	hzh.pages.js["menuConf"] = {
		init:function(){
			menuListTable();
			resourceMenuTable();
			menulist(0,10);
			$("#menuListBtn").bind("click",function(){
				menulist(0,10);
			});
			
			menupages = new hzh.Pagination($(".pagination"), 10, function(page) {
				var limitStart = page * 10 + "";
				menulist(limitStart, 10);
			});
			$("#menuListTable").delegate(".delbtn", "click", delmenu);
			$("#menuListTable").delegate(".updatebtn", "click", updatamenu);
			$("#saveMenuBtn").bind("click",saveMenu);
			$("#menuListTable").delegate(".resourceMenuBtn", "click",queryResourceMenu);
			$("#saveResourceMenu").bind("click",updateResourceMenu);
		},
		destory:function() {
			
		},
		processForm: function(data){
			console.log(data);
			return data;
		},
		processResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("添加成功！");
				$("#myModal").modal("hide");
			}else {
				hzh.alert("添加失败，请稍后再试！");
			}
		}
	};
	
	
	function menulist(start,num){
		var menuname = $("#menuname").val();
//		var uid = hzh.api.user.userId;
		hzh.api.request(0x03010110, "queryMenu",{
//			userId:uid,
			name:menuname,
			limitStart:start,
			limitEnd:num
		},function(res) {
			if(res.response.code == 200){
				hzh.RefreshTable("#menuListTable",res.response.data.result);
//				console.log(res.response.data.result);
				var totalPage = res.response.data.pageCount;
				menupages.refresh(totalPage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
			}
//			else {
//				hzh.alert("查找失败，请稍后再试！");
//			}
		});
	};
	function delmenu(){
		var menuid = $(this).parents("tr").find(".idCol").html();
//		console.log(menuid);
		hzh.api.request(0x03010108, "delMenu",{id:menuid},function(res){
			if(res.response.code == 200){
				hzh.alert("删除成功！");
				menulist(0,10);
			}else {
				hzh.alert("删除失败！");
			}
		});
	};
	
	function updatamenu() {
		var uTr = $(this).parents("tr"); //找到该节点的祖先节点
		var id = $(uTr).find(".idCol").html();
		console.log(id);
		$("#saveMenuBtn").data("menuid",id);
		var nameval = $(uTr).find(".nameCol").html();
		var urlval = $(uTr).find(".urlCol").html();
		var iconval = $(uTr).find(".iconCol").html();
		$("#name").val(nameval);
		$("#url").val(urlval);
		$("#icon").val(iconval);
	};
	
	function saveMenu() {
		var menuid = $(this).data("menuid");
		console.log(menuid);
		var nameval = $("#name").val();
		var urlval = $("#url").val();
		var iconval = $("#icon").val();
		var remarkval = $("#remark").val();
		hzh.api.request(0x03010109, "updateMenu",{
			id:menuid,
			name:nameval,
			url:urlval,
			icon:iconval,
			remark:remarkval
		},function(res){
			if(res.response.code == 200){
				hzh.alert("修改成功！");
				$("#updateMenuModal").modal("hide");
				menulist(0,10);
			}else {
				hzh.alert("修改失败，请稍后再试！");
			}
		});
	};

	function queryResourceMenu(){
		var menuid = $(this).parents("tr").find("td.idCol").html();
		$("#saveResourceMenu").data("menuId",menuid);
		hzh.api.request(0x03010111, "queryResourceWithMenu",{
			menuId:menuid
		},function(res){
			if(res.response.code == 200){
				console.log(res);
				hzh.RefreshTable("#resourceMenuTable",res.response.data.result);
			}else {
				hzh.alert("查询失败！");
			}
		});
	}
	
	function updateResourceMenu(){
		var menuid = $(this).data("menuId");
		var resourceIds = [];
		var resourceIdString = "";		
		
		$("#resourceMenuTable").find("input:checkbox[name='checkbox']:checked").each(function(i, e) { //each()方法遍历选中的checked
			resourceIds.push($(e).val()); //将已选中的checkbox的值放入数组中
//			roleNameVal.push($(e).parents("tr").find("td.uRoleCol").html()); //将角色名称取出并放入roleNameVal数组中
		});
		resourceIdString = resourceIds.join(",");
		console.log(resourceIdString);
		hzh.api.request(0x03010112, "updateResourceWithMenuRequest",{
			menuId:menuid,
			resourceIds:resourceIdString
		},function(res){
			if(res.response.code == 200){
				hzh.alert("修改成功！");
				$("#queryResourceMenu").modal("hide");
			}else {
				hzh.alert("修改失败，请稍后再试！");
			}
		});
	}
	
	function menuListTable() {
		$('#menuListTable').DataTable({
			ordering: false,
	bAutoWidth: false,
	bProcessing: true,
	bFilter: false,
	bPaginate: false,
	responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",

				"oPaginate": {
					//				"sFirst": "首页",
					//				"sPrevious": "前一页",
					//				"sNext": "后一页",
					//				"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				className: "text-center idCol",
				sWidth: "3em"
			}, {
				data: "name",
				className: "text-center nameCol"

			}, {
				data: "icon",
				className: "text-center iconCol"
			}, {
				data: "url",
				className: "text-center urlCol"
			}, {
				data: "parentId",
				className: "text-center",
				render: function(data,type,row) {
					if(data == undefined){
						return "";
					}
					return data;
				}
			}, {
				render: function(data, type, row) {
					if (data === undefined) {
						return "<a href='#' data-toggle='modal' data-target='#updateMenuModal' style='margin-right: 10px;' class='btn btn-default updatebtn'>修改</a><a href='javascript://' style='display:none;margin-right: 10px;'  class='btn btn-default canclebtn'>取消</a><a href='javascript://' style='margin-right: 10px;' class='btn btn-default delbtn'>删除</a><a href='#' data-toggle='modal' data-target='#queryResourceMenu' style='margin-right: 10px;' class='btn btn-default resourceMenuBtn'>菜单权限</a>";

					}
					return data;
				},
				className: "dt-body-center text-center ",
				"sWidth": "22em"
			}]
		});
	};
	
	
	function resourceMenuTable() {
		$('#resourceMenuTable').DataTable({
			bAutoWidth: true,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",

				"oPaginate": {
					//				"sFirst": "首页",
					//				"sPrevious": "前一页",
					//				"sNext": "后一页",
					//				"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "resourceId",
				className: "text-center"
			}, {
				data: "resourceName",
				className: "text-center "

			}, {
				data: "selected",
				className: "text-center",
				render: function(data,type,row){
					if (type === 'display') {
						var checkedString = data == 1 ? 'checked = "checked" ' : "";
						return '<input type="checkbox" name="checkbox" ' + checkedString + '  class="editor-active row-cbx" value="' + row.resourceId + '" data-row="' + data + '">';
					}
					return data;
				}
			}]
		});
	};
	
	
	
	
	
	
	
	
})(window, $)