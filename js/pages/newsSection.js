(function(window, $) {

	var sectionPage;
	hzh.pages.js["newsSection"] = {
		init: function() {
			querySectionTable();
			querySection(0,10);
			sectionPage = new hzh.Pagination($(".pagination"), 10, function(page) {
				var limitStart = page * 10;
				querySection(limitStart, 10);
			});
			$("#infroTable").delegate(".delSectionBtn","click",delSection);
			$("#sectioncheckBox").bind("click",selectAll);
			$("#delAllBtn").bind("click",delAllSection);
			
			$("#infroTable").delegate(".updateBtn","click",updateSection);
			$("#infroTable").delegate(".ensureBtn","click",ensure);
			$("#infroTable").delegate(".cancelBtn","click",cancle);
		},
		destroy: function() {
		},
		processForm: function(data) {
			return data;
		},
		
		processResponse:function(res) {
			if (res.response.code == 200) {
				console.log(res);
				hzh.alert("添加成功！");
				$('#addSectionModal').modal('hide');
				querySection(0, 10);
				$('#addSectionModal').find("input").val("");
			} else {
				hzh.alert(res.response.data);
			}
		}
	};

	//查询新闻内容
	function querySection(start, num) {
		
		hzh.api.request(0x03060006 , "querySection", {
			limitStart: start,
			limitEnd: num
		}, function(res) {
			if (res.response.code == 200) {
				hzh.RefreshTable("#infroTable", res.response.data.result);
				var totalPage = res.response.data.pageCount;
				sectionPage.refresh(totalPage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var dataCount = res.response.data.dataCount;
				var li = $(".pagination").find("li.active").nextAll();
				if(li.length == 1){
					$("#infroTable_info").html("从 "+(start+1)+" 到 " +(dataCount)+ " 条/ 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#infroTable_info").html("从 "+(start+1)+" 到 " +(dataCount)+ " 条/ 共 "+(dataCount)+" 条数据");
				}else{
					$("#infroTable_info").html("从 "+(start+1)+" 到 " +(start+10)+ " 条/ 共 "+(dataCount)+" 条数据");
				}
			} 
//			else {
//				hzh.alert("未找到！");
//			}
		});
	};


	function delSection() {
		var nTr = $(this).parents("tr");
		var sectionId = nTr.find(".idCol input").val();
		var r = confirm("确认删除？");
		if (r == true) {
			hzh.api.request(0x03060008 , "delInformation", {
				id: sectionId
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功");
					querySection(0, 10);
					
				} else {
					hzh.alert("删除失败！");
				}
			});
		}

	};
	
	function selectAll() {		
		if(this.checked == true){
			$("#infroTable").find("input[type='checkbox']").prop('checked', true);
		}else{
			$("#infroTable").find("input[type='checkbox']").prop('checked', false);
		}
	};
	
	function delAllSection() {
		var ids = [];
		var idString = "";
		$("#infroTable").find(".row-cbx:checked").each(function(i,e){
			ids.push($(e).val());
		});
		if (ids.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		idString = ids.join(",");
		var r = confirm("确认删除？");
		if(r==true){
			hzh.api.request(0x03060008, "delInformation", {
				id: idString
			}, function(res) {
				console.log(res.response.code);
				if(res.response.code == 200){
					hzh.alert("删除成功！");
					querySection(0, 10);
					$("#sectioncheckBox").prop('checked', false);
				}else{
					hzh.alert("删除失败！");
				}
			});
		}
		
	};
	
	function updateSection() {
		//alert("aa");
		var uTr = $(this).parents("tr"); //找到该节点的祖先节点
		var trSibNode = uTr.siblings(); //找到uTr的所有兄弟节点
		$(trSibNode).find(".ensureBtn:visible").parents("tr").each(function(i, e) {
			change(0, $(e));
		});
		change(1, uTr);
	};

	function ensure() {
		var uTr = $(this).parents("tr");
		var upId = uTr.find(".idCol input").val();
		var telVal = $("#nameval").val();
		var phone = /^(1\d{10})|(0\d{2,3}-?\d{7,8})$/;
//		if(phone.test(telVal)&&telVal.length==11){
			hzh.api.request(0x03060009 , "updateSection", {
				id: upId,
				name: telVal
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("修改成功！");
					change(0, uTr);
					userPage = new hzh.Pagination($(".pagination"), 10, function(page) {
						queryUser(page * 10, 10);
					});
				} else {
					alert("修改失败！");
				}
			});
//		}else{
//			hzh.alert("您输入的电话有误！");
//		}
		
	};

	function change(status, tr) { //status为0，1，1表示按钮为显示的状态，0表示按钮为隐藏的状态；tr表示选中的行

		$(".ensureBtn", tr).toggle(status == 1); //确定按钮显示
		$(".updateBtn", tr).toggle(status == 0); //修改按钮隐藏
		$(".delSectionBtn", tr).toggle(status == 0); //删除按钮隐藏
		$(".cancelBtn", tr).toggle(status == 1); //取消按钮显示

		$(tr).find(".nameCol").each(function(i, e) { //对修改的电话字段遍历
			var td = $(e);
			if (status) {
				td.html("<input class='nameCol' id='nameval' type='text' value='" + td.html() + "' />"); //如果该行处于编辑状态，则将输入的值赋给该列
			} else {
				td.html(td.find("input").val()); //如果不是处于编辑状态，则将其字段原来的值赋他本身
			}

		});
	};

	function cancle() {
		change(0, $(this).parents("tr"));
		querySection(0, 10);
	};
	
	function querySectionTable() {
		$('#infroTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bProcessing: false,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					//					"sFirst": "首页",
					//					"sPrevious": "前一页",
					//					"sNext": "后一页",
					//					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [ {
				data: "id",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" value="' + data + '">';
					}
					return data;
				},
				sWidth: "3em",
				className: "idCol text-center"
			},{
				data: "name",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
				className: "text-center nameCol"
			}, {
				data: "createTime",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
				className: "text-center"
			},  {
				render: function(data, type, row) {  
					if (data === undefined) {
						return "<a href='javascript://' style='margin-right:10px;' class='btn btn-default updateBtn'>修改</a><a href='javascript://' style='display:none;margin-right:10px;' class='btn btn-default ensureBtn'>确定</a><a href='javascript://' style='display:none;' class='btn btn-default cancelBtn'>取消</a><a href='javascript://' class='btn btn-default delSectionBtn'>删除</a>";
					}
					return data;
				},
				sWidth: "16em",
				className: "text-center"
			}]

		});

	};


})(window, $);