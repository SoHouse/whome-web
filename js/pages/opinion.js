(function(window, $) {
	var opinionPage;
	var id;
	hzh.pages.js["opinion"] = {
		init: function() {
			initOpinionListTable();
			selectFeedback(0,10);
			opinionPage = new hzh.Pagination($("#opinionPage"), 10, function(page) {
				selectFeedback(page * 10, 10);
			});
			
			$("#opinionListTable").delegate(".reply", "click", function(){
				var html="";
				var  tr= $(this).parents("tr");
				id = tr.find(".idCol").html();
				var household = tr.find(".householdCol").html();
				console.log(household);
				$("#opinionId").val(id);
				var contents = tr.find(".contentCol").html();
				var contentArr = contents.split("^&amp;");
				for(var i=0;i<contentArr.length-1;i++){
					if(contentArr[i].indexOf("q:")>=0){
						contentArr[i]=contentArr[i].split("q:").slice(1,2).toString();
						html+="<div class='replayContent'>"+household+"："+contentArr[i]+"</div>";
					}else{
						contentArr[i]=contentArr[i].split("a:").slice(1,2).toString();
						//"+"&nbsp;&nbsp;&nbsp;&nbsp;"+"
						html+="<div class='replayContent'>物管："+contentArr[i]+"</div>";
					}
				}
				$("#replay").html(html);
			});
		},
		destroy: function() {
			
		},
		processForm:function(data){
			data["id"]=id;
			if(data.content==""){
				hzh.alert("请输入内容！");
			}else{
				data.content="a:"+data.content+"^&";
				return data;
			}
		},
		processResponse:function(res){
			if(res.response.code == 200){
				var newContent = $("#myModal").find("textarea").val();
				//newContent = newContent.split("a:").slice(1,2).toString();
				$("<div class='replayContent'>物管："+newContent+"</div>").appendTo($("#replay"));
				$("#myModal").find("textarea").val("");
				selectFeedback(0,10);
			}
		}
		
	};
	
	function selectFeedback(start,num){
		hzh.api.request(0x03030106,"selectFeedback",{
			userId:hzh.api.user.userId,
			limitStart: start,
			limitEnd: num
		},function(res){
			if(res.response.code == 200){
				var data = res.response.data.result;
				var str;
				$.each(data, function(i,e) {
					var contents = e.title;
					str = contents.split("^&");					
					str=str.slice(0,1).toString();
					var contentStr = str.split("q:").slice(1,2);
					e.title = contentStr;
				});
				hzh.RefreshTable("#opinionListTable", data);
				
				var totalPage = res.response.data.pageCount;
				opinionPage.refresh(totalPage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var dataCount = res.response.data.dataCount;
				var li = $("#opinionPage").find("li.active").nextAll();
				if(li.length == 1){
					$("#opinionListTable_info").html("从 "+(start+1)+" 到 " +dataCount+ " 条 / 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#opinionListTable_info").html("从 "+(0)+" 到 " +0+ " 条 / 共 "+(0)+" 条数据");
				}else{
					$("#opinionListTable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条 / 共 "+(dataCount)+" 条数据");
				}
			}
		});
	};
	
	
	function initOpinionListTable() {
		$('#opinionListTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bPaginate: false,
			responsive: true,
			bFilter: false,
			//			AllowSorting:false,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",

				"oPaginate": {
					//				"sFirst": "首页",
					//				"sPrevious": "前一页",
					//				"sNext": "后一页",
					//				"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				className: "text-center idCol hide",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}, 
			},{
				data: "creator",
				className: "text-center",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			},{
				data: "headOfHousehold",
				className: "text-center householdCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
				sWidth: "9em"
			},{
				data: "createTime",
				className: "text-center",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
				sWidth: "9em" 
			},{
				data: "content",
				className: "text-center contentCol hide",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			}, {
				data: "title",
				className: "text-center contentCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			}, {
				data: "updateTime",
				className: "text-center ",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			}, {
				render: function(data, type, row) {
					if (data === undefined) {
						return "<a href='#' data-toggle='modal' data-target='#myModal' class='btn btn-default reply'>查看内容</a>";
					}
					return data;
				},
				className: "dt-body-center text-center",
				sWidth: "3em"
			}]
		});
	};
	

})(window, $);