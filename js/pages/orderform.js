(function(window, $) {
	var orderPage;
	var orderDetailPage;
	hzh.pages.js["orderform"] = {
		init: function() {
			orderListTable();
			orderDetailTable();
			queryOrder(0,10);
//			querylessOrder(0,10);
			$("#moreCondition").click(function(){
				$("#searchMore").fadeToggle("slow");
			});
			$(".startDate,.endDate").datebox();
			//查询订单条件
			$("#searchOrderBtn").bind("click",function(){
				querylessOrder(0,10);
			});
			$("#searchMoreBtn").bind("click",function(){
				queryOrder(0,10);
			});
			orderPage=new hzh.Pagination($(".pagination"),10,function(page){
				var start = page*10;
				queryOrder(start,10);
			});
			orderDetailPage=new hzh.Pagination($(".pagination"),10,function(page){
				var start = page*10;
				queryOrder(start,10);
			});  
			$("#orderListTable").delegate(".cancleOrderBtn", "click", orderCancle);
			$("#orderListTable").delegate(".orderDetailBtn", "click", orderDetail);
			
			//返回
			$("#beback").bind("click",function(){
				$(".dataTable_wrapper").find("input[type=text]").val("");
				$(".dataTable_wrapper").find("input[type=hidden]").val("");
//				$(".dataTable_wrapper").find("select option:seleted").val("");
				queryOrder(0,10);
			});

		},
		
		destroy: function() {
			$('.combo-p').remove();
		},
		
	};
	
	function queryOrder(start,num) {
		var searchval = $("#searchOrderVal").val();
		var type = $("#type").val();
		var orderNum = $("#orderNum").val();
		var userName = $("#userName").val();
		var roomNum = $("#roomNum").val();
		var orderState = $("#orderState").val();
		var startDate = $("#searchMore").find("#startDate input[type=hidden]").val();
		var endDate = $("#searchMore").find("#endDate input[type=hidden]").val();
		hzh.api.request("0x03080006", "queryOrder", {
			limitStart:start,
			limitEnd:num,
			type:type,
			orderId:orderNum,
			headOfHousehold :userName,
			householdId :roomNum,
			status:orderState,
			startDate :startDate,
			endDate :endDate
		}, function(res) {
			if (res.response.code == 200) {
				var data=res.response.data.result;
				var moneyVal;
				$(data).each(function(i,e){
					moneyVal=(e.money)/100;
					e.money=moneyVal;
				});
				hzh.RefreshTable("#orderListTable", data);
				var totalPage = res.response.data.pageCount;
				orderPage.refresh(totalPage,start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class","");
				var dataCount = res.response.data.dataCount;
				var li = $("#orderPage").find("li.active").nextAll("li");
				if(li.length == 1){
					$("#orderListTable_info").html("从 "+(start+1)+" 到 " +dataCount+ " 条 / 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#orderListTable_info").html("从 "+(0)+" 到 " +0+ " 条 / 共 "+(0)+" 条数据");
				}else{
					$("#orderListTable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条 / 共 "+(dataCount)+" 条数据");
				}
			}
//			else {
//				hzh.alert("查询失败！");
//			}
		});
	};
	
	function querylessOrder(start,num) {
		var searchval = $("#searchOrderVal").val();
		
		hzh.api.request("0x03080006", "queryOrder", {
			limitStart:start,
			limitEnd:num,
			orderId:searchval
		}, function(res) {
			if (res.response.code == 200) {
				var data=res.response.data.result;
				var moneyVal;
				$(data).each(function(i,e){
					moneyVal=(e.money)/100;
					e.money=moneyVal;
				});
				hzh.RefreshTable("#orderListTable", data);
				var totalPage = res.response.data.pageCount;
				orderPage.refresh(totalPage,start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class","");
				
			}
		});
	};
	
	
	function orderDetail(start,num) {
		var id = $(this).parents("tr").find(".orderIdCol").html();
		hzh.api.request("0x03080008", "queryOrder", {
			orderId:id
		}, function(res) {
			if (res.response.code == 200) {
				var data=res.response.data.result
				var moneyVal;
				$(data).each(function(i,e){
					moneyVal=(e.amount)/100;
					e.amount=moneyVal;
				});
				hzh.RefreshTable("#orderDetailTable", data);
				
//				var totalPage = res.response.data.pageCount;
//				orderDetailPage.refresh(totalPage,start / num);
//				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class","");
			} else {
				hzh.alert("查询失败！");
			}
		});
	};
	
	function orderCancle() {
		
		var orderId = $(this).parents("tr").find(".orderIdCol").html();	
		var r = confirm("确认取消订单？");
		if(r == true){
			hzh.api.request(0x03080012,"cancelOrder",{orderId:orderId},function(res){
				if(res.response.code == 200){
					hzh.alert("取消成功成功！");
					queryOrder(0,10);
				}
//				else {
//					hzh.alert("取消失败！");
//				}
			});
		}
	};
		
	function orderListTable() {
		$('#orderListTable').DataTable({
			ordering: false,
	        bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
									//					"sFirst": "首页",
									//					"sPrevious": "前一页",
									//					"sNext": "后一页",
									//					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
					data: "orderId",
					render: function(data, type, row) {
						if (data == undefined) {
							return "";
						}
						return data;
					},
					className: "text-center orderIdCol"
				},
				{
					data: "name",
					render: function(data, type, row) {
						if (data == undefined) {
							return "";
						}
						return data;
					},
					className: "text-center"
				},{
					data: "headOfHousehold",
					render: function(data, type, row) {
						if (data == undefined) {
							return "";
						}
						return data;
					},
					className: "text-center"
				}, {
					data: "creator",
					render: function(data, type, row) {
						if (data == undefined) {
							return "";
						}
						return data;
					},
					className: "text-center hide"
				},{
					data: "money",
					render: function(data, type, row) {
						if (data == undefined) {
							return "";
						}
						return data;
					},
					className: "text-center"
				},{
					data: "paymentType",
					render: function(data, type, row) {
//						console.log(data);
//						if (data == "0") {
//							return "";
//						}else
						if(data == "1"){
							return "余额支付";
						}else if(data == "2"){
							return "支付宝支付";
						}else if(data == "3"){
							return "微信支付";
						}else if(data == "4"){
							return "银联支付";
						}else if(data == "5"){
							return "公交卡支付";
						}
						return data;
					},
					className: "text-center hide"
				}, {
					data: "completeTime",
					render: function(data, type, row) {
						if (data == undefined) {
							return "";
						}
						return data;
					},
					className: "text-center"
				},{
					data: "status",
					render: function(data, type, row) {
						if (data == 5) {
							return "订单已取消";
						}else if(data == 1){
							return "扣款成功";
						}else if(data == 2){
							return "订单创建";
						}else if(data == 3){
							return "支付宝已扣款";
						}else if(data == 4){
							return "缴费成功";
						}else if(data == 6){
							return "订单删除";
						}
						return data;
					},
					className: "text-center"
				},{
					render: function(data, type, row) {
						if (data === undefined) {
							//<a href='javascript://' data-toggle='modal' data-target='#updateOrderModal'  class='btn btn-default updateOrderBtn'>修改订单</a><a href='javascript://'  class='btn btn-default DelOrderBtn'>删除订单</a>
							return "<a href='#' data-toggle='modal' data-target='#queryOrderModal' style='margin-right:10px;'  class='btn btn-default orderDetailBtn'>详情</a><a href='javascript://' class='btn btn-default cancleOrderBtn'>取消</a>";
						}
						return data;
					},
					className: "text-center",
					"sWidth":"8em"
			}]
		});
	};
	
	function orderDetailTable() {
		$('#orderDetailTable').DataTable({
			ordering: false,
	        bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: true,
			responsive: true,
			bLengthChange: false,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					"sFirst": "首页",
					"sPrevious": "前一页",
					"sNext": "后一页",
					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
				className: "text-center hide"
			}, {
				data: "utilitiesName",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
				className: "text-center"
			},{
				data: "month",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
				className: "text-center"
			},{
				data: "createTime",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
				className: "text-center"
			}, {
				data: "amount",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
				className: "text-center"
			},{
				data: "isInternal",
				render: function(data, type, row) {
					if (data == undefined) {
						return "";
					}
					return data;
				},
				className: "text-center"
			}]
		});
	};


})(window, $)
