(function(window, $) {
	var page;
	var rolemanagmentpagination;
	hzh.pages.js["role"] = {
		init: function() {
			roleTable();
			//queryRole初始化角色列表数据
			queryRole(0, 10);
			//分页
			rolemanagmentpagination = new hzh.Pagination($(".pagination"), 10, function(page) {
				queryRole(page * 10, 10);
			});
			//全选
			$(".checkboxall").click(selectAll);
			//增删改
			$("#delRole").click(delSelect);
			$("#addRole").click(sureAdd);
			$("#rolebox").delegate(".modifyRole", "click", modifyRole);
			$("#rolebox").delegate(".delOneRole", "click", delOneRole);
			//弹出模态框
			$("#rolebox").delegate('.updPermission', 'click', updRolePermission);
			//搜索
			$("#searchRoleBtn").click(function() {
				var sVal = $("#searchRoleInput").val();
				queryRole(0, 10, sVal);
			})
				//加载treeTable
			$.getScript('../script/treeTable/jquery.treeTable.js', function() {
				console.log("treeTable loaded")
			});
//					$("#searchRoleBtn").bind("");
			$("#saveModal").bind("click",updaterole);

			//绑定回车键，按下回车键修改成功
//			$("#rolebox").keydown(function(event) {
//				var uTr = $("#rolebox").find(".sureModify:visible").parents("tr");
//				var uproleid = uTr.find('td.idCol').html();
//				var uproleName = uTr.find('input.nameCol').val();
//				if (event.keyCode == 13) {
//					hzh.api.request(0x03020405, "updateRole", {
//						userId: hzh.api.user.userId,
//						name: uproleName,
//						id: uproleid
//					}, function(res) {
//						if (res.response.code == 200) {
//							hzh.alert('修改成功');
//							queryRole(page * 10, 10);
//						} else if (res.response.code == 520) {
//							hzh.alert("角色已存在");
//						} else {
//							hzh.alert('修改失败');
//							queryRole(page * 10, 10);
//						}
//					});
//				}
//			});
			
//			$('#roleList').keydown(function(event){
//				
//				var roleName = $("#rolebox").find(".nameCol").html();
////							if (roleName == undefined) {
////								roleName = "";
////							}
//				var keycode = (event.keyCode ? event.keyCode : event.which);
//				if(keycode == '13'){
//					hzh.api.request(0x03020409, "selectAllRole", {
//						limitStart: 0,
//						limitEnd: 10,
//						name: roleName
//					}, function(res) {
//						if (res.response.code == 200) {
//							hzh.RefreshTable("#rolebox", res.response.data.result);
//							var allPage = res.response.data.pageCount;
////									page = (startNumber + showingNumber) / showingNumber - 1;
//							rolemanagmentpagination.refresh(0, 10);
//							
//						}
//					});
//				}
//			});
			
			$("#beback").bind("click",function(){
				var roleName = $("#searchRoleInput").val();
				var queryterms=$("#roleList").find("input[type=search]").val("");
				queryRole(0, 10,"");
			});
		},
	destroy: function() {}
};

	function treeSelector() {
		var that = this;
		var thisId = $(this).parents("tr").attr("id");
		checkTree(thisId, that.checked)
	};

	function checkTree(pid, checked) {
		$("#resourceTable").find("tr[pid='" + pid + "']").each(function() {
			$(this).find("input").prop("checked", checked);
			checkTree($(this).attr("id"), checked);
		})
	};

	function loadTreeDatatable(html) {
		$('#resourceTable').empty();
		var tableHtml = $('#resourceTable')[0].outerHTML;
		var treeTable = $(tableHtml);
		$('#resourceTable').replaceWith(treeTable);
		treeTable.html(html);
		var option = {
			theme: 'vsStyle',
			expandLevel: 2,
			beforeExpand: function($treeTable, id) {
				//判断id是否已经有了孩子节点，如果有了就不再加载，这样就可以起到缓存的作用
				if ($('.' + id, $treeTable).length) {
					return;
				}
			},
			nSelect: function($treeTable, id) {
				window.console && console.log('onSelect:' + id);
			}
		};

		treeTable.treeTable(option);
		$("#resourceTable").find("input").click(treeSelector);

	};

	function updRolePermission() {
		var oRoleId = $(this).parent().parent().find("td").eq(1).html();
		$("#saveModal").data("roleId",oRoleId);
		hzh.api.request(0x03020306, "queryResourceWithRole", {
			roleId: oRoleId
		}, function(res) {
			var data = res.response.data.result;
			console.log(data);
			var ptree = buildPermissionTree(data, 0);
			var html = '<tr hasChild="true" id="-1" controller="true"><td>权限</td><td>选择</td></tr>';
			html += buildPermissionTable(ptree);
			loadTreeDatatable(html);
			$("#resourceTable tr[pid='-1']").attr("style", "display: table-row;");
			$("#resourceTable tr[pid='1']").attr("style", "display: table-row;");
		});
		
	};
	
	function updaterole(){
		var roleId = $(this).data("roleId");	  
			var updId = "";
			$("#resourceTable").find("input").each(function() {
				if (this.checked == true) {
					updId = updId + ($(this).attr("data-tid")) + ",";
				}
			})
			updId = updId.substring(0, updId.length - 1);
			hzh.api.request(0x03020305, "updateUserRole", {
				resourceIds: updId,
				roleId: roleId
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("修改成功");
					$("#roleModal").modal("hide");
				} else {
					hzh.alert("修改失败");
				}
				$('#myModal').modal('hide');
			});
	};
	
	function buildPermissionTable(list) {
		var html = "";
		for (var i = 0; i < list.length; i++) {
			var data = list[i];
			var pid = data.parentId;
			var ckd;
			(data.selected == 1 ? ckd = "checked='checked'" : ckd = null);
			html = html + '<tr id="' + data.resourceId + '" pId="' + (pid == 0 ? "-1" : pid) + '"><td>' + data.resourceName + '</td><td><input type="checkbox"  data-tid=' + data.resourceId + ' ' + ckd + '></td></tr>';
			if (data.children && data.children.length > 0)
				html = html + buildPermissionTable(data.children);
		}
		return html;
	};

	function buildPermissionTree(permissions, parentId) {
		var ret = [];
		$.each(permissions, function(i, e) {
			if (e.parentId == parentId) {
				var cld = buildPermissionTree(permissions, e.resourceId);
				if (cld.length > 0)
					e.children = cld;
				ret.push(e);
			}
		});
		return ret;
	};

	function queryRole(startNumber, showingNumber, roleName) {
//		alert("aa");
		if (roleName == undefined) {
			roleName = "";
		}
		hzh.api.request(0x03020409, "selectAllRole", {
			limitStart: startNumber,
			limitEnd: showingNumber,
			name: roleName
		}, function(res) {
			if (res.response.code == 200) {
				hzh.RefreshTable("#rolebox", res.response.data.result);
				var allPage = res.response.data.pageCount;
				page = (startNumber + showingNumber) / showingNumber - 1;
				rolemanagmentpagination.refresh(allPage, page);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var li = $("#rolePage").find("li.active").nextAll();
				if(li.length == 1){
					$("#rolebox_info").html("从 "+(startNumber+1)+" 到 " +(res.response.data.dataCount)+ " 条/ 共 "+(res.response.data.dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#rolebox_info").html("从 "+(0)+" 到 " +(0)+ " 条/ 共 "+(0)+" 条数据");
				}else{
					$("#rolebox_info").html("从 "+(startNumber+1)+" 到 " +(startNumber+showingNumber)+ " 条/ 共 "+(res.response.data.dataCount)+" 条数据");
				}
			}
		});
	};

	function selectAll() {
		if (this.checked == true) {
			$("#rolebox").find("input[type='checkbox']").prop('checked', true);
		} else {
			$("#rolebox").find("input[type='checkbox']").prop('checked', false);
		}
	};

	function delSelect() {
		var delID = "";
		for (var i = 0; i < $("#rolebox").find(".row-cbx:checked").length; i++) {
			delID = delID + $("#rolebox").find(".row-cbx:checked").eq(i).parent().parent().find('td').eq(1).html() + ",";

		}
		if (delID == "") {
			alert("请选择至少一项");
		} else {
			delID = delID.substring(0, delID.length - 1);
			var r = confirm("确认删除？");
			if (r == true) {
				hzh.api.request(0x03020406, "delRole", {
					userId: hzh.api.user.userId,
					id: delID
				}, function(res) {
					if (res.response.code == 200) {
						hzh.alert("删除成功");
						queryRole(page * 10, 10);
					}
				});
			}
			queryRole(page * 10, 10);
		}
	};

	function modifyRole() {
		var aTd = $(this).parents("tr");
		var oTdRole = aTd.find("td.nameCol");

		var oThisTd = $(this).parent();
		var oVal = oTdRole.html();
		//点击确定
		oTdRole.html('').append($('<input class="nameCol" value=' + oVal + '>'));
		oThisTd.find('.delOneRole').hide();
		oThisTd.parent().find('.modifyRole').hide();
		oThisTd.find('.updPermission').hide();
		oThisTd.find('.sureModify').show().click(function() {
			var upd = oTdRole.find('input').val();
			var roleId = aTd.find("td.idCol").html();
			if (upd != oVal) {
				var r = confirm("确认修改？")
				if (r == true) {
					hzh.api.request(0x03020405, "updateRole", {
						userId: hzh.api.user.userId,
						name: upd,
						id: roleId
					}, function(res) {
						if (res.response.code == 200) {
							hzh.alert('修改成功');
							queryRole(page * 10, 10);
						} else if (res.response.code == 520) {
							hzh.alert("角色已存在");
						} else {
							hzh.alert(res.response.data);
							queryRole(page * 10, 10);
						}
					});
				} else {
					queryRole(page * 10, 10);
				};
			} else {
				queryRole(page * 10, 10);
			};
		});
		//点击取消
		oThisTd.find('.cancelModify').show().click(function() {
			oThisTd.find('.sureModify').hide();
			oThisTd.parent().find('.cancelModify').hide();
			oThisTd.find('.delOneRole').show();
			oThisTd.find('.updPermission').show();
			oThisTd.parents().find('.modifyRole').show();
			oTdRole.html(oVal);
		});
	};


	function delOneRole() {
		var delID = $(this).parent().parent().find('td').eq(1).html();
		var r = confirm("确认删除？")
		if (r == true) {
			hzh.api.request(0x03020406, "delRole", {
				userId: hzh.api.user.userId,
				id: delID
			}, function(res) {

				if (res.response.code == 200) {
					hzh.alert('删除成功');
					queryRole(page * 10, 10);
				}
			})
		}
	};

	function addRole() {
		var addName = $("#roleNameInput").val();
		console.log(addName);
		hzh.api.request(0x03020404, "addRole", {
			userId: hzh.api.user.userId,
			name: addName
		}, function(res) {

			if (res.response.code == 200) {
				$('#roleNameInput').val('');
				hzh.alert("添加成功");
				queryRole(page * 10, 10);
			} else if (res.response.code == 520) {
				hzh.alert("角色已存在");
			} else {
				hzh.alert("添加失败");
			}
		});
	};

	function sureAdd() {
		var adName = $("#roleNameInput").val()
		if (adName == "") {
			hzh.alert("请输入一个角色名！")
		} else {
			var r = confirm("是否添加？")
			if (r == true) {
				addRole()
			}
		};
	};

	function roleTable() {
		$('#rolebox').DataTable({
			ordering: false,
			bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sErrMode": "throw",
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					//	"sFirst": "首页",
					//	"sPrevious": "前一页",
					//	"sNext": "后一页",
					//	"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				//	"sProcessing": "<img src='./loading.gif' />",
				//	"sSearch":"搜索"
			},
			columns: [{
					data: "checkbox",
					render: function(data, type, row) {
						if (data === undefined) {
							return '<input type="checkbox" class="editor-active row-cbx" data-row="' + data + '">';
						}
						return data;
					},
					sortable: false,
					className: "text-center"
				}, {
					data: "id",
					className: "idCol text-center"
				}, {
					data: "name",
					className: "nameCol text-center"
				}, {
					data: "createUser",
					render: function(data, type, row) {
						if (data === undefined) {
							return '未知';
						}
						return data;
					},
					className: "text-center"
				}, {
					data: "createTime",
					className: "text-center",
					"sWidth": "13em"
				},
				//			{
				//				data:"resourceName"
				//			},
				{
					data: "edit",
					render: function(data, type, row) {
						if (data === undefined) {
							return "<input type='button' style='margin-right:10px;' class='btn btn-default delOneRole' value='删除'><input type='button' style='margin-right:10px;' class='btn btn-default modifyRole' value='修改角色'><input type='button'style='margin-right:10px;' class='btn btn-default updPermission' data-toggle='modal' data-target='#roleModal' value='修改权限'><input type='button' style='margin-right:10px;display:none;' class='btn btn-default sureModify' value='确定' style='display:none'><input type='button' class='btn btn-default cancelModify' value='取消' style='display:none'>";
						}
						return data;
					},
					sortable: false,
					className: "text-center",
					"sWidth": "18em"
				},
			],
		});
	};


})(window, $);