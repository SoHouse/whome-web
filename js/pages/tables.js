(function(window, $) {
	hzh.pages.js["tables"]={
		init:function(){
		//日期插件设置	
		$(".dp,.dpe").datebox();
	    //点击查询
	    $("#queryButton").click(queryPayment);
	    //缴费跳转页面
	    $(".goToPay").click(function(){
//	    	window.location.href ='Payment.html';
	    	hzh.pages.load("Payment.html", false, "Payment");
	    	
	    });
	    
	    //表格初始化
	    $('#paymentDetailTable').DataTable({
            rordering: false,
			bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前一页",
                    "sNext": "后一页",
                    "sLast": "尾页"
                },
                "sZeroRecords": "没有检索到数据",
                "sProcessing": "<img src='./loading.gif' />",
                "sSearch":"搜索"
            },
            columns: [
            { data: "id",className:"text-center"  },
                { data: "name",className:"text-center" },
                { data: "amount",className:"text-center"}                
	            ]
	        });	
	    
	    
		$('#paymentTable').DataTable({
            ordering: false,
			bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前一页",
                    "sNext": "后一页",
                    "sLast": "尾页"
                },
                "sZeroRecords": "没有检索到数据",
                "sProcessing": "<img src='./loading.gif' />",
                "sSearch":"搜索"
            },
            columns: [	
                { data: "name",className:"text-center"  },
                { data: "status" ,className:"text-center" },
                { data: "balance",className:"text-center"  },
                {
                    data:   "balance",
                    render: function ( data, type, row ) {
                        if ( type === 'display' ) {
                            return '<input type="button" value="详情" class="btn btn-default detail" data-toggle="modal" data-target="#payModal">';
                        }
                        return data;
                    },
                    className: "dt-body-center text-center"
                }
	            ]
	        });	
	        //历史缴费列表
	        //查询初始化调用及分页
//			queryOrder(0);
			payPagination=new hzh.Pagination($(".pagination"),10,function(page){
				queryOrder(page*10);
			});
			//点击查询
			$("#paymentHistoryBtn").click(function(){
				var start=$(".dp").val();
				var end=$(".dpe").val();
				queryOrder(0,start,end);
			})
			
			//日期插件设置
//			$(".dp, .dpe").datetimepicker({
//				todayHighlight: true,
//				format: 'yyyy-mm-dd',
//				autoclose: true,
//				todayBtn: true,
//				language: "zh-CN",
//				minView: "month",
//				
//			});
//			$(".dp").datetimepicker().on('changeDate', function(){
//				$(".dpe").datetimepicker('setStartDate',$(".dp").val());
//			});
			
			$(".icon-arrow-left").attr("class","glyphicon glyphicon-arrow-left");
			$(".icon-arrow-right").attr("class","glyphicon glyphicon-arrow-right");
			
			//表格数据初始化
			$('#orderTable').DataTable({
				ordering: false,
				bAutoWidth: false,
				bProcessing: true,
				bFilter: false,
				bPaginate: false,
				responsive: true,
                "oLanguage": {
                    "sLengthMenu": "每页显示 _MENU_ 条记录",
                    "sZeroRecords": "抱歉， 没有找到",
                    "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                    "sInfoEmpty": "没有数据",
                    "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "前一页",
                        "sNext": "后一页",
                        "sLast": "尾页"
                    },
                    "sZeroRecords": "没有检索到数据",
                    "sProcessing": "<img src='./loading.gif' />",
                    "sSearch":"搜索"
                },
                columns: [
                    { data: "headOfHouseHoldName",className:"text-center" },
                    { data: "money",className:"text-center"},
                    { data: "name",className:"text-center"},
                    { data: "orderId",className:"text-center" },
                    { data: "paymentTime",
	                  render:function ( data, type, row ){
	                  	if ( data === undefined ){
	                  		return '未支付';
	                  	}
	                  	return data;
	                  },
	                  className:"text-center"						
                    },
                    { data: "status",className:"text-center"},

                ]
            });
	        
	        
		},
		destroy:function()
		{
		}
	};
	
    
    function queryPayment(){
    	$("#paymentTable").undelegate(".detail","click");
    	var choose=$("#paymentSelect option:selected").val();
    	console.log(choose);//欠费信息
	    hzh.api.request(0x03010201,"queryPaymentStatus",{loginName:hzh.api.user.loginName},function(res){
	    	console.log(res.response.code);
	        if(res.response.code==200){
	        	var more={};
	            var information=[];
	            if (choose=="all"){
					for (var i in res.response.data){
						var n=res.response.data[i]
						if (typeof(n)=="object"){
							more[i]=n["items"];
							information.push({name:i,status:n["status"],balance:n["balance"]}); //name项为缴费项目
						}
					}
		            //将得到的数据填入表格中
		            hzh.RefreshTable("#paymentTable",information);
		            console.log(information);
	            }else {
	            	information.push({name:choose,status:res.response.data[choose]["status"],balance:res.response.data[choose]["balance"]});
	            	hzh.RefreshTable("#paymentTable",information);
	            	more[choose]=res.response.data[choose].items;
	            }
	            //点击详情
	    		$("#paymentTable").delegate(".detail","click",function(){
	    			var thisItem=$(this).parents("tr").find("td").eq(0).html();  //点击详情的缴费项目
	    			hzh.RefreshTable("#paymentDetailTable",more[thisItem]);
	    			
	    		});
	        }
	    });
	};  
	
	function queryOrder(startNumber,sd,ed){
		(sd==undefined?sd="":sd);
		(ed==undefined?ed="":ed);
		
		 hzh.api.request(0x03010203,"queryOrder",{//历史缴费信息
		 	limitStart:startNumber,
		 	limitEnd:10,
		 	startDate:sd,
		 	endDate:ed,
		 	loginName:hzh.api.user.loginName
		 },function(lsjf){
		 	console.log(lsjf.response.code);
            if(lsjf.response.code==200){
            	
                hzh.RefreshTable("#orderTable",lsjf.response.data.result);
                var allPage=lsjf.response.data.pageCount;
				page=(startNumber+10)/10-1;
				payPagination.refresh(allPage, page);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class","");
            }
            else{
                alert(lsjf.response.data);
            }
        });
	}
    
    
    
    
})(window,$);
