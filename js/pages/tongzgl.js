(function(window, $) {

	hzh.pages.js["tongzgl"] = {
		init: function() {
			housePayTable();
			$("#queryPayBtn").bind("click",queryPay);

		},
		destroy: function() {

		}


	}

	function queryPay() {
		
		var hId = $("#uId").val();
		var typeId = $("#typeId").val();
		var enddate = $("#endTime").val();
		var comId = $("#comId").val();
		var build = $("#buildName").val();
		var unit = $("#unitName").val();
		var houseId = $("#houseId").val();
		var household = $("#householdName").val();
		var hTel = $("#tel").val();

		hzh.api.request(0x03020201, "queryStatus", {
			uId: hId,
			utilitiesTypeId: typeId,
			endDate: enddate,
			communityId: comId,
			build: build,
			unit: unit,
			householdId: houseId,
			headOfHousehold: household,
			telephone: hTel,

		}, function(res) {
			if (res.response.code == 200) {
				alert("res.response.code");
				hzh.RefreshTable("#householdPayTable", res.response.data.result);
			} else {
				hzh.alert("查询失败!");
			}

		});
	};



	function housePayTable() {
		$('#householdPayTable').DataTable({
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					"sFirst": "首页",
					"sPrevious": "前一页",
					"sNext": "后一页",
					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
					data: "extn",
					render: function(data, type, row) {
						if (type === 'display') {
							return '<input type="checkbox" class="editor-active row-cbx" data-row="' + data + '">';
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: "type"
				},
				{
					data: "total"
				},
				{
					data: "month"
				},
				{
					data: "headOfHousehold"
				},
				{
					data: "telephone"
				}, 
				{
					data: "remark"
				}, 
				{
					data: "breach"
				}
			]
		});
		$("#abutton").click(onaclick);
	};

	function onaclick() {
		$(".row-cbx:checked").each(function(i, e) {
			alert($(e).data("row"));
		});
	};






})(window, $);