(function(window, $) {
	hzh.pages.js["upSurcharge"] = {
		init: function() {},
		destroy: function() {},
		processForm: function(data) {
//			return data;
			var oInput = document.getElementById("upAddition");
			if (oInput.files.length == 0) {//判断有没有上传一个excel文档
				hzh.alert("请上传一个文件");
			} else {
				var fileSize = oInput.files[0].size;
				if (fileSize > 30000) {
					hzh.alert("上传文件过大，请小于xx");
				} else if (fileSize) {
					hzh.showLoading();
					oInput.outerHTML = oInput.outerHTML;
					return data;
				};
				oInput.outerHTML = oInput.outerHTML;
			}
		},
		processResponse: function(res) {
			console.log(res);
			if (res.response.code == 200) {
				hzh.hideLoading();
				hzh.alert("提交成功");
			};
		}
	};


})(window, $);