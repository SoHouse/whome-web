(function(window, $) {
    
    var verPages;
    var page;
	hzh.pages.js["versionList"] = {
		init: function() {
			queryVerList(0,10);
			versionListTable();
			findApp();
			$("#versionListBtn").bind("click",function(){
				queryVerList(0,10);
			});
			
			verPages = new hzh.Pagination($(".pagination"),10,function(page){
				queryVerList(page*10,10);
			});
			
			//时间插件
//			$(".time").datebox();.datetimepicker()
//			$(".time").datetimepicker();
			$('.formular').validationEngine();
			//返回
			$("#beback").bind("click",function(){
				var queryterms=$("#home").find("input[type=text]").val("");
				queryVerList(0, 10);
			});
			
		},
		destroy: function() {
			$('.combo-p').remove();
		},
		processForm: function(data) {
			var errorLen = $("#myModal").find(".formError:visible").length;
			var inputTxt = $("#myModal").find("input[type=text]");
			var inputTxtVal;
			$.each(inputTxt, function() {    
				inputTxtVal= $(this).val();
				console.log(inputTxtVal!="");
			});
			
			if(errorLen == 0){
				var verUrl = data.url;
				var fileApk = $("#upAddition").val();
				if(verUrl) {
					data.url;
				}else if(fileApk) {
	                fileApk
				}else if(verUrl&&fileApk) {
					data.url;
				}
				return data;
			}else{
				return;
			}
			
//			
//			console.log(data);
//			return data;
		},
		processResponse: function(res) {
			if (res.response.code == 200) {
				hzh.alert("添加成功!");
				queryVerList(0,10);
				
				$("#myModal").modal("hide");
				$("#myModal").find("input").val("");
			} else {
				hzh.alert(res.response.data);
			}
		}
	};


	function queryVerList(start,num) {
		
		var appName = $("#AppName").val();
		var versionNum = $("#versionNum").val();
		var starTime = $("#releaseBegin").val();
		var endTime = $("#releaseEnd").val();
		hzh.api.request(0x03020111 , "selectAllVersion", {
			versionNum: versionNum,
			releaseBegin: starTime,
			releaseEnd: endTime,
			appName:appName,
			limitStart:start,
			limitEnd:num
		}, function(res) {
			if (res.response.code == 200) {
				hzh.RefreshTable("#versionListTable", res.response.data.result);
				var inputcheckbox = $("#versionListTable").find("input[type='checkbox']").val();
				if(inputcheckbox == 1){
					$("#versionListTable").find("input[type='checkbox']").prop('checked',true);
				}else {
					$("#versionListTable").find("input[type='checkbox']").prop('checked',false);
				}
				var totalPage = res.response.data.pageCount;
				verPages.refresh(totalPage,start / num);
				$(".pagination a[aria-label='上一页']，a[aria-label='下一页']").attr("class","");
				var dataCount = res.response.data.dataCount;
				var li = $(".pagination").find("li.active").nextAll();
				if(li.length == 1){
					$("#versionListTable_info").html("从 "+(start+1)+" 到 " +dataCount+ " 条 / 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#versionListTable_info").html("从 "+(0)+" 到 " +0+ " 条 / 共 "+(0)+" 条数据");
				}else{
					$("#versionListTable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条 / 共 "+(dataCount)+" 条数据");
				}
			} 
//			else {
//				hzh.alert("查找失败！");
//			}
		});
	};

	function versionListTable() {
		$('#versionListTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					//					"sFirst": "首页",
					//					"sPrevious": "前一页",
					//					"sNext": "后一页",
					//					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				className:"text-center hide",
				"sWidth":"4em"
			},{
				data:"appName",
				render:function(data,type,row) {
					if(data == undefined){
						return "";
					}else{
						return "<a href="+row.url+">"+data+"</a>"
					}
				},
				className:"text-center",
				"sWidth":"5em"
			}, {
				data: "versionNum",
				className:"text-center",
				render:function(data,type,row) {
					if(data == undefined){
						return "";
					}
					return data;
				},
				"sWidth":"4em"
			}, {
				data: "releaseTime",
				className:"text-center",
				"sWidth":"13em"				
			}, {
				
				data: "versionSize",
				className:"text-center hide",
				render:function(data,type,row) {
					if(data == undefined){
						return "";
					}
					return data;
				}
			}, {
				data: "developer",
				className:"text-center",
				render:function(data,type,row) {
					if(data == undefined){
						return "";
					}
					return data;
				},
				"sWidth":"4em"
			}, {
				data: "url",
				className:"text-center hide",
				"sWidth":"5em"
			}, {
				data: "environment",
				className:"text-center",
				render:function(data,type,row) {
					if(data == undefined){
						return "";
					}
					return data;
				},
				"sWidth":"5em"
			}, {
				data: "remark",
				className:"text-center hide",
				render:function(data,type,row) {
					if(data == undefined){
						return "";
					}
					return data;
				},
				"sWidth":"3em"
			}, {
				data:"type",
				render:function(data,type,row) {
					if(data == undefined){
						return "";
					}
					return data;
				},
				className:"text-center",
				"sWidth":"5em"
			},{
				data:"versionCode",
				render:function(data,type,row) {
					if(data == undefined){
						return "";
					}
					return data;
				},
				className:"text-center",
				"sWidth":"6em"
			},{
				data:"important",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" value="' + data + '" data-row="' + data + '">';
					}
					return data;
				},
				className:"importantCol text-center",
				"sWidth":"7em"
			}]

		});
	};
	
	function findApp(){
		hzh.api.request("0x03040125", "selectApp", {},function(res){
			if(res.response.code==200){
				var data = res.response.data.result;
				console.log(data);
				$(data).each(function(i,e){
					$("<option value="+e.id+">"+e.appName+"</option>").appendTo($("#appname"));
				});
			}
		})
	};
	

})(window, $);