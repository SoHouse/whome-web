(function(window, $) {
	var visitorPage;
	var visitorId;
	var commName;
	hzh.pages.js["visitors"] = {
		init: function() {
			initVisitorsListTable();
			queryVisitors(0,10);
			$("#queryVisitorBtn").bind("click",function(){
				queryVisitors(0,10);
			});
			$('.formular').validationEngine();
			visitorPage = new hzh.Pagination($("#visitorPage"), 10, function(page) {
				queryVisitors(page * 10, 10);
			});
			
			$("#visitorsListTable").delegate(".updateVisitor", "click", function(){
				visitorId = $(this).parents("tr").find(".idCol").html();
				updateVisitors();
			});
			
			
			$("#beback").bind("click",function(){
				$("#home").find("input[type=text]").val("");
				queryVisitors(0, 10);
			});			
			
//			$("#buildingListTable").delegate(".updateBuild", "click", function(){
//				var  tr= $(this).parents("tr");
//				buildId = $(tr).find("td.idCol").html();
//				var contents = $(tr).find("td.editCol");
//				var td = $("#updateBuildModal").find("input[type=text]");
//				for(var i=0;i<contents.length;i++){
//					var tdVal = $(contents).eq(i).html();
//					$(td).eq(i).val(tdVal);
//				}
//				commName = $(tr).find("td.communityNameCol").html();
//				$("#updateCommunityId").combogrid("setValue",commName);
//				$("#updateId").val($(tr).find("td.communityIdCol").html());
//			});
			
			
		},
		destroy: function() {
			
		},
		processForm:function(data){
			var errorlength = $('#myModal').find(".form-group>.formError:visible").length;
			if(data.telephone.length==11 && errorlength==0){
				return data;
			}else{
				return;
//				hzh.alert("您输入的信息有错！");
			}
			
		},
		processResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("添加成功！");
				$("#myModal").modal("hide");
				$("#myModal").find("input").val("");;
				queryVisitors(0,10);
			}
		},
		updateForm:function(data){
			data["id"] = buildId;
			var newName = $("#updateCommunitydiv").find("span input.textbox-value").val();
			if(newName != commName){
				return data;
			}else{
				data.communityId = $("#updateId").val();
				return data;
			}
		},
		updateResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("修改成功！");
				$("#updateBuildModal").modal("hide");
				queryVisitors(0,10);
			}else{
				hzh.alert(res.response.data);
			}
		}
		
	};
	
	function queryVisitors(start,num){
		var guestName = $("#guestName").val();
		hzh.api.request(0x03070103,"queryGuest",{
			guestName:guestName,
			limitStart: start,
			limitEnd: num
		},function(res){
			if(res.response.code == 200){
				hzh.RefreshTable("#visitorsListTable", res.response.data.result);
				var totalPage = res.response.data.pageCount;
				visitorPage.refresh(totalPage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var dataCount = res.response.data.dataCount;
				var li = $("#visitorPage").find("li.active").nextAll();
				if(li.length == 1){
					$("#visitorsListTable_info").html("从 "+(start+1)+" 到 " +dataCount+ " 条 / 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#visitorsListTable_info").html("从 "+(0)+" 到 " +(0)+ " 条 / 共 "+(0)+" 条数据");
				}else{
					$("#visitorsListTable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条 / 共 "+(dataCount)+" 条数据");
				}
			}
		});
	};
	
	function updateVisitors(){
		var r = confirm("确定离开？");
		if(r){
			hzh.api.request(0x03070104,"updateGuest",{id:visitorId},function(res){
				if(res.response.code == 200){
					hzh.alert("修改成功！");
					queryVisitors(0,10);
				}else{
					hzh.alert(res.response.data);
				}
			});
		}
		
	};
	
	function initVisitorsListTable() {
		$('#visitorsListTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bPaginate: false,
			responsive: true,
			bFilter: false,
			//			AllowSorting:false,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",

				"oPaginate": {
					//				"sFirst": "首页",
					//				"sPrevious": "前一页",
					//				"sNext": "后一页",
					//				"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				className: "text-center idCol hide",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			},{
				data: "guestName",
				className: "text-center editCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			},{
				data: "sex",
				className: "text-center communityIdCol",
				render:function(data,type,row){
					if(data == "0"){
						return "女";
					}else{
						return "男";
					}
				},
			},{
				data: "telephone",
				className: "text-center communityIdCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			},{
				data: "meetName",
				className: "text-center communityIdCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			},{
				data: "address",
				className: "text-center hide communityIdCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			},{
				data: "createTime",
				className: "text-center editCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			}, {
				data: "leaveTime",
				className: "text-center hide communityNameCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			},{
				data: "creator",
				className: "text-center communityIdCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			},{
				data: "remark",
				className: "text-center hide communityIdCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			},{
				data: "item",
				className: "text-center hide editCol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				},
			}, {
				data: "isLeave",
				className: "text-center communityIdCol",
				render:function(data,type,row){
					if(data == "true"){
						return "是";
					}else{
						return "否";
					}
				},
			},{
				render: function(data, type, row) {
					if(row.isLeave == "true"){
						return "<a href='#' data-toggle='modal' data-target='#updateBuildModal' class='btn btn-default updateVisitor' disabled>确认离开</a>";
					}else {
						return "<a href='#' data-toggle='modal' data-target='#updateBuildModal' class='btn btn-default updateVisitor'>确认离开</a>";
					}
				},
				className: "dt-body-center text-center"
			}]
		});
	};
	

})(window, $);