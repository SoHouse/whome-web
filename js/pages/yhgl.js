(function(window, $) {

	var userPage;
	var userResourceId;
	var userid;
	var rolePage;
	var comId;
	var houseName;
	var houseNameTd;
	hzh.pages.js["yhgl"] = {
		init: function() {
//			$('#dataTables-example').dataTable();
			queryUser(0, 10);
			bindhousehold(0,10);
			searchhousehold(0,10);
			initDatable();
			initRoleTable();
			currentPage = 0;
			$("#userbtn").bind("click", function() {
				queryUser(0, 10); //初始化用户/列表
			});
			$("#addUserBtn").bind("click", addUser);
			//返回
			$("#beback").bind("click",function(){
				var queryterms=$("#home").find("input[type=text]").val("");
				queryUser(0, 10);
			});
			$("#dataTables-example").delegate(".delbtn", "click", delUser); //删除用户
			$("#dataTables-example").delegate(".telUpdateBtn", "click", updateUser); //修改用户
			$("#dataTables-example").delegate(".ensurebtn", "click", ensure);
			$("#dataTables-example").delegate(".canclebtn", "click", cancle); //取消
			

			$("#ucheckBox").bind("click", uselectAll); //选中所有
			$("#checkAllBtn").bind("click", allUserDel); //删除所有

			
			
			
			$("#dataTables-example").delegate(".queryAdminBtn", "click", queryAdminCommunity);

			userPage = new hzh.Pagination($("#userPage"), 10, function(page) {
				var limitStart = page * 10;
				queryUser(limitStart, 10);
			});
			var selectIds=[];
			var roleid = [];
			rolePage = new hzh.Pagination($("#rolePage"), 10, function(page) {
				selectUserRole(page * 10, 10);
				
				var selectId=[];
				var tr = $("#userRoleBox").find("tbody tr");
				$(tr).find("td.roleIdCol").each(function(j,d){
					roleid.push($(d).html());
				});
				$("#userRoleBox").find("input:checkbox[name='checkbox']:checked").each(function(i, e) { //each()方法遍历选中的checked
					selectId.push($(e).val());
					for(var j=0;j<roleid.length;j++){
						if($(e).val()==roleid[j]){
							$(e).prop("checked", true);
						}
					}
				});				
			});
			
			
			$("#dataTables-example").delegate(".userRoleSetBtn", "click", function(){
				var uTr = $(this).parents("tr"); //找到this的祖先节点
				userResourceId = uTr.find("td.userIdColumn").html(); //取到该行的值
				var userRoleName = uTr.find("td.roleColumn");
				$("#saveUserRole").data("roleName", userRoleName); //.data()取数据
				$("#userId").val(userResourceId);
				selectUserRole(0,10);
			});
			
			$("#saveUserRole").bind("click",updateUserRole);
			$("#updateAdminCom").bind("click", updateAdminCommunity);
			
			//加载treeTable
//			$.getScript('../treeTable/jquery.treeTable.js', function() {
//				console.log("treeTable loaded")
//			});
			
			$('#adduser').validationEngine();
			
			$("#addhouseBtn").bind("click",function(){
				var addVal = $("#addhouseName").val();
				console.log(addVal);
				hzh.api.request(0x03020101, "queryHousehold", {
					headOfHousehold: addVal,
					userId:hzh.api.user.userId,
					limitStart: 0,
					limitEnd: 10
				}, function(res) {
					var data = res.response.data.result;
		            $("#householdName").combogrid("grid").datagrid("loadData", data);
				});
			});
			$("#updateHouseBtn").bind("click",function(){
				var updateval =$("#updatehouseName").val();
				console.log(updateval);
				hzh.api.request(0x03020101, "queryHousehold", {
					headOfHousehold: updateval,
					userId:hzh.api.user.userId,
					limitStart: 0,
					limitEnd: 10
				}, function(res) {
					var data = res.response.data.result;
		            $("#updateHousehold").combogrid("grid").datagrid("loadData", data);//
				});
					
			});
			//定义列表
			
			$('#householdName').combogrid({
				toolbar:"#tb",
				panelWidth: 700,
				panelHeight:425,
				//fit : true,// datagrid自适应宽高
            	//fitColumns : true,
				textField:"headOfHousehold",
			    idField:'id',
			    communityIdField:'communityId',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
//              pageList: [10,20],//可以设置每页记录条数的列表 
				columns:[[
					{field: 'id',title: 'id',width: 70}, 
					{field: 'communityId',title: '小区ID',width: 50},
					{field: 'communityName',title: '小区名称',width: 80},
					{field: 'buildingNum',title: '楼号',width: 50},
					{field: 'unitNum',title: '单元',width: 50},
					{field: 'roomNum',title: '房号',width: 50},
//					{field: 'familyCount',title: '人数',width: 80},
//					{field: 'familys',title: '家庭成员',width: 80},
//					{field: 'buildingArea',title: '建筑面积',width: 80},
					{field: 'headOfHousehold',title: '户主',width: 90},
					{field: 'telephone',title: '户主电话',width: 120},
					{field: 'unitConstractionArea',title: '套内面积',width: 90},
					
//					{field: 'sharedPublicArea',title: '公摊面积',width: 120}
				]],	
			});
			$('#updateHousehold').combogrid({
				toolbar:"#searchhousehold",
				panelWidth: 680,
				panelHeight:425,
				textField:"headOfHousehold",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'id',title: 'id',width: 70}, 
					{field: 'communityName',title: '小区名称',width: 80},
					{field: 'buildingNum',title: '楼号',width: 50},
					{field: 'unitNum',title: '单元',width: 50},
					{field: 'roomNum',title: '房号',width: 50},
					{field: 'headOfHousehold',title: '户主',width: 90},
					{field: 'telephone',title: '户主电话',width: 120},
					{field: 'unitConstractionArea',title: '套内面积',width: 90},
				]],	
			});
			
			$(".datagrid-view").click(getSelected);
			
			$("#dataTables-example").delegate(".updatebtn","click",function(){
				var aduserTr = $(this).parents("tr");
				houseNameTd = aduserTr.find(".houseNameCol");
				
				$("#savehousenameBtn").data("houseNameTd", houseNameTd);
				userid = aduserTr.find("td.idCol").html();
			});
		},
		destroy: function() {
			$('.combo-p').remove();
		},
		processForm:function(data){
			data["userId"] = userid;
			houseName = $("#updateUserModal").find(".textbox-text").val();
			return data;
		},
		processResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("添加成功！");
				$("#updateUserModal").modal("hide");
				$(houseNameTd).html(houseName);
				$("#updateUserModal").find(".textbox-text").val("");
			}else{
				hzh.alert(res.response.data);
			}
		}
	};

	//查询用户
	function queryUser(limitStart, limitEnd) {
		var un = $("#userLoginName").val(); //获取查询条件中登录名的值
		var tel = $("#userTel").val();//获取查询条件中电话的值
		console.log(tel);
		var roomnum = $("#useremail").val();//获取查询条件中邮件的值
		hzh.api.request(0x03020123, "queryUser", { //请求接口
//			hzh.api.request(0x03010101, "queryUser", { //请求接口
			loginName: un,
			telephone: tel,
			email: roomnum,
			limitStart: limitStart,
			limitEnd: limitEnd
		}, function(res) {
			if (res.response.code == 200) {
				hzh.RefreshTable("#dataTables-example", res.response.data.result); //刷新table表格，并将数据显示到页面上
				var totalPage = res.response.data.pageCount;
				userPage.refresh(totalPage, limitStart / limitEnd);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var li = $("#userPage").find("li.active").nextAll();
				if(li.length == 1){
					$("#dataTables-example_info").html("从 "+(limitStart+1)+" 到 " +(res.response.data.dataCount)+ " 条/ 共 "+(res.response.data.dataCount)+" 条数据");
				}else if(li.length==0){
					$("#dataTables-example_info").html("从 "+(0)+" 到 " +(0)+ " 条/ 共 "+(0)+" 条数据");
				}else{
					$("#dataTables-example_info").html("从 "+(limitStart+1)+" 到 " +(limitStart+limitEnd)+ " 条/ 共 "+(res.response.data.dataCount)+" 条数据");
				}
				  
				//$("#dataTables-example_info").html("第 "+(limitStart/10+1)+" 页 / 共 "+totalPage+" 页");  
			} 
//			else {
//				hzh.alert("未找到该用户！");
//			}
		});
	};
	//绑定户	
	function bindhousehold(start,num) {
//		var updateval = $("#updatehouseName").val();
		hzh.api.request(0x03020101, "queryHousehold", {
//			headOfHousehold: updateval,
            userId:hzh.api.user.userId,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			var data = res.response.data.result;
            $("#updateHousehold").combogrid("grid").datagrid("loadData", data);
            var updatepage=$("#updateHousehold").combogrid("grid").datagrid("getPager");//
            updatepage.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03020101, "queryHousehold", {
                		userId:hzh.api.user.userId,
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#updateHousehold").combogrid("grid").datagrid("loadData", res.response.data.result);
						updatepage.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
            
		});
	};
	
	//添加用户时绑定户
	function searchhousehold(start,num) {
//		var addVal = $("#addhouseName").val();
		hzh.api.request(0x03020101, "queryHousehold", {
//			headOfHousehold: addVal,
			userId:hzh.api.user.userId,
			limitStart: start,
			limitEnd: num			
		}, function(res) {
			var data = res.response.data.result;
            $("#householdName").combogrid("grid").datagrid("loadData", data);//加载内容
			var pager = $("#householdName").combogrid("grid").datagrid("getPager");//获取pager属性
            pager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03020101, "queryHousehold", {
                		userId:hzh.api.user.userId,
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#householdName").combogrid("grid").datagrid("loadData", res.response.data.result);
						pager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				},
            });
            
		});
	};
	
	
	function getSelected(){
		var row = $("#householdName").combogrid("grid").datagrid("getSelected");
		if (row){
			comId=row.communityId;
		}
	};
	
	
	//添加用户
	function addUser() {
		var addname = $("#addLoginName").val();
		var tel = $("#addTel").val();
		var addpwd = $("#addpwd").val();
		var addremark = $("#addRemark").val();
		var addmail = $("#addEmail").val();
		var bindhousehold = $("#householdId").find("input.textbox-value").val();
		var errorlength = $('#adduser').find(".form-group>.formError:visible").length;
		if(errorlength==0 && tel.length==11){
			hzh.api.request(0x03020104, "addUser", {
				loginName: addname,
				telephone: tel,
				pwd: hex_md5(addpwd),
				remark: addremark,
				email: addmail,
				householdId:bindhousehold,
				communityId:comId,
				
				isAdmin:"0"			
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("添加成功");
					queryUser(0, 10);
					$("#profile").find("input").each(function(i, e) {
						$(e).val("");
					});
				} else if (res.response.code == 525) {
					hzh.alert("用户已存在！");
				} else {
					alert("添加失败！");
				}
			});
		}else{
			hzh.alert("信息输入有误！");
		}
//      $("#adduser").find
		
	};

	//删除用户
	function delUser() {
		var uTr = $(this).parents("tr");
		var delId = uTr.find('td.userIdColumn').html();
		var r = confirm("确认删除？");
		if(r == true){
			hzh.api.request(0x03020106, "delUser", {
				id: delId
			}, function(res) {
				if (res.response.code == 200) {
					uTr.remove();
					hzh.alert("删除成功！");
					queryUser(0, 10);
				} else {
					alert("删除失败！");
				}
			});
		}		
	};

	//选中所有
	function uselectAll() {
		if (this.checked == true) {
			$("#dataTables-example").find("input[type='checkbox']").prop('checked', true);
		} else {
			$("#dataTables-example").find("input[type='checkbox']").prop('checked', false);
		}
	};

	//删除所有
	function allUserDel() {
		var userDelId = "";
		var userIds = [];
		$("#dataTables-example").find(".row-cbx:checked").each(function(i, e) { //each()方法遍历选中的checked
			userIds.push($(e).data("row")); //将所有checked选中的userid放入数组中
		});

		if (userIds.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		userDelId = userIds.join(","); //join()把数组中的元素用，隔开放入一个字符串
		
		var r = confirm("确认删除？"); //确定删除
		if (r == true) {
			hzh.api.request(0x03020106, "delUser", {
				id: userDelId
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功");
					$("#ucheckBox").prop('checked', false);
					queryUser(0, 10);
				}else{
					hzh.alert("删除失败！");
				}
			});
		}

	};

	function updateUser() {
		//alert("aa");
		var uTr = $(this).parents("tr"); //找到该节点的祖先节点
		var trSibNode = uTr.siblings(); //找到uTr的所有兄弟节点
		$(trSibNode).find(".ensurebtn:visible").parents("tr").each(function(i, e) {
			change(0, $(e));
		});
		change(1, uTr);
	};

	function ensure() {
		var uTr = $(this).parents("tr");
		var upId = uTr.find("td.userIdColumn").html();
		var telVal = $("#telval").val();
		var phone = /^(1\d{10})|(0\d{2,3}-?\d{7,8})$/;
		if(phone.test(telVal)&&telVal.length==11){
			hzh.api.request(0x03020105, "updateUser", {
				id: upId,
				telephone: telVal
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("修改成功！");
					change(0, uTr);
					userPage = new hzh.Pagination($(".pagination"), 10, function(page) {
						queryUser(page * 10, 10);
					});
				} 
//				else {
//					alert("修改失败！");
//				}
			});
		}else{
			hzh.alert("您输入的电话有误！");
		}
		
	};

	function change(status, tr) { //status为0，1，1表示按钮为显示的状态，0表示按钮为隐藏的状态；tr表示选中的行

		$(".ensurebtn", tr).toggle(status == 1); //确定按钮显示
		$(".telUpdateBtn", tr).toggle(status == 0); //修改按钮隐藏
		$(".delbtn", tr).toggle(status == 0); //删除按钮隐藏
		$(".canclebtn", tr).toggle(status == 1); //取消按钮显示

		$(tr).find(".telColumn").each(function(i, e) { //对修改的电话字段遍历
			var td = $(e);
			if (status) {
				td.html("<input class='telColumn' id='telval' type='text' value='" + td.html() + "' />"); //如果该行处于编辑状态，则将输入的值赋给该列
			} else {
				td.html(td.find("input").val()); //如果不是处于编辑状态，则将其字段原来的值赋他本身
			}

		});
	};

	function cancle() {
		change(0, $(this).parents("tr"));
		queryUser(0, 10);
	};

	//查询用户角色
	function selectUserRole(start,num) {
		userResourceId = $("#userId").val(); //取到该行的值
		hzh.api.request(0x03020310, "selectUserRole", {
			userId: userResourceId,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			if (res.response.code == 200) {
				hzh.RefreshTable("#userRoleBox", res.response.data.result);
				var total = res.response.data.pageCount;
				rolePage.refresh(total, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				
				
			} 
//			else {
//				alert("查找失败");
//			}
		});
	};
	//修改用户角色
	function updateUserRole() {
		var uid = $("#userRoleModal").find("input[type=hidden]").val(); 
		var uroleName = $(this).data("roleName"); //.data()取回数据
		var uRcheck = "";
		var userRoleIds = [];
		var roleString = "";
		var roleNameVal = [];

		$("#userRoleBox").find("input:checkbox[name='checkbox']:checked").each(function(i, e) { //each()方法遍历选中的checked
			userRoleIds.push($(e).val()); //将已选中的checkbox的值放入数组中
			roleNameVal.push($(e).parents("tr").find("td.uRoleCol").html()); //将角色名称取出并放入roleNameVal数组中
		});

		uRcheckIds = userRoleIds.join(","); //将数组用逗号给开并传给字符串
		roleString = roleNameVal.join(",");
		var r = confirm("确认修改");
		if (r = true) {
			hzh.api.request(0x03020408, "updateUserRole", {
				userId: uid,
				roleIds: uRcheckIds
			}, function(res) {
				if (res.response.code == 200) {
					$('#userRoleModal').modal('hide'); //隐藏弹窗
					$(uroleName).html(roleString); //将取得的角色名赋给用户列表中的角色列中
					hzh.alert("修改成功！");
				}
				else {
					hzh.alert("修改失败！");
				}
			});
		}
	};

	function treeSelector() {
		var that = this;
		var thisId = $(this).parents("tr").attr("id");
		checkTree(thisId, that.checked);
	};

	function checkTree(pid, checked) {
		$("#queryAdminTable").find("tr[pid='" + pid + "']").each(function() {
			$(this).find("input").prop("checked", checked);
			checkTree($(this).attr("id"), checked);
		})
	};

	function loadTreeDatatable(html) {
		$('#queryAdminTable').empty();
		var tableHtml = $('#queryAdminTable')[0].outerHTML;
		var treeTable = $(tableHtml);
		$('#queryAdminTable').replaceWith(treeTable);
		treeTable.html(html);
		var option = {
			theme: 'vsStyle',
			expandLevel: 3,
			beforeExpand: function($treeTable, id) {
				//判断id是否已经有了孩子节点，如果有了就不再加载，这样就可以起到缓存的作用
				if ($('.' + id, $treeTable).length) {
					return;
				}
			},
			nSelect: function($treeTable, id) {
				window.console && console.log('onSelect:' + id);
			}
		};

		treeTable.treeTable(option);
		$("#queryAdminTable").find("input").click(treeSelector);
	};

	function queryAdminCommunity() {
		var uid = $(this).parents("tr").find(".userIdColumn").html();
		$("#updateAdminCom").data("uid", uid);
		hzh.api.request(0x03020411, "queryAdminCommunity", {
			userId: uid
		}, function(res) {
			console.log(res.response.data);
			var data = res.response.data.regionArray;
			var comData = res.response.data.communityArray;
			var ptree = buildPermissionTree(data, comData, 0);
			var html = '<tr hasChild="true" id="-1" controller="true"><td>地区</td><td>选择</td></tr>';
			html += buildPermissionTable(ptree);
			loadTreeDatatable(html);
		});
	};

	function updateAdminCommunity() {
		//		$("#updateAdminCom").bind("click", function() {
		var uid = $(this).data("uid");
		console.log(uid);
		var cId = "";
		$("#queryAdminTable").find("input").each(function() {
			if (this.checked == true) {
				var checkenVal = $(this).val();
				if (checkenVal != "undefined") {
					cId = cId + checkenVal + ",";
				}
			}
		})
		cId = cId.substring(0, cId.length - 1);

		hzh.api.request(0x03020307, "updateAdminCommunity", {
				userId: uid,
				communityIds: cId
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("修改成功");
					$('#queryCellModal').modal('hide');
				} 
//				else {
//					hzh.alert("修改失败");
//				}
		});
	}


	function buildPermissionTable(list) {
		var html = "";
		for (var i = 0; i < list.length; i++) {
			var data = list[i];
			var pid = data.parentId;
			var ckd = data.selected == 1 ? "checked='checked'" : "";
			html = html + '<tr id="' + data.id + '" pId="' + (pid == 0 ? "-1" : pid) + '"><td>' + data.cityName + '</td><td><input type="checkbox" name="communityId" value=' + data.communityId + ' data-tid=' + data.id + ' ' + ckd + '></td></tr>';
			if (data.children && data.children.length > 0)
				html = html + buildPermissionTable(data.children);

		}
		return html;
	};

	function buildPermissionTree(regions, communities, parentId) {
		var ret = [];
		$.each(regions, function(i, e) {
			if (e.parentId == parentId) {
				var cld = buildPermissionTree(regions, communities, e.id);

				if (cld.length > 0)
					e.children = cld;
				else { //try finding communities in this region
					e.children = buildCommunityTree(communities, e.id);
				}
				ret.push(e);
			}
		});
		return ret;
	};

	function buildCommunityTree(communities, regionId) {
			var ret = [];
			$.each(communities, function(i, e) {
				//			if (e.cityId == regionId) {
				//				console.log(e.communityName);
				e.parentId = regionId;
				e.id = Math.random();
				e.cityName = e.communityName;
				ret.push(e);
				//			}
			});
			return ret;
		}
		//用户查询列表

	function initDatable() {
		$('#dataTables-example').DataTable({
			ordering: false,
			bAutoWidth: false,
			bPaginate: false,
			responsive: true,
			bFilter: false,
			//			AllowSorting:false,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_/共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",

				"oPaginate": {
					//				"sFirst": "首页",
					//				"sPrevious": "前一页",
					//				"sNext": "后一页",
					//				"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "userId",
				//              sClass: "text-center",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" data-row="' + data + '">';
					}
					return data;
				},
				className: "dt-body-center text-center",
//				sWidth: "3em"
			}, 
			{
				data: "userId",
				className: "userIdColumn idCol text-center hide"

			}, 
			{
				data: "loginName",
				className: "text-center"
			}, 
			{
				data: "telephone",
				className: "telColumn text-center"
			},
			{
				data: "headOfHousehold",
				className: "text-center houseNameCol"
			}, 
			{
				data: "email",
				className: "text-center hide"
			},  
			{
				data: "identification",
				className: "text-center",
				render: function(data, type, row) {
					if (data === undefined) {
						return "";
					}
					return data;
				}
			}, {
				data: "roleNames",
				className: "roleColumn text-center"
			},{
				data: "sourceType",
				className: "text-center hide",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			}, {
				render: function(data, type, row) {
					if (data === undefined) {//<a href='#' data-toggle='modal' style='margin-right: 10px;' data-target='#userRoleModal' class='btn btn-default userRoleSetBtn'>角色设置</a>
						return "<a href='javascript://' style='margin-right: 10px;'  class='btn btn-default telUpdateBtn'>修改</a><a href='javascript://' style='display:none;margin-right: 10px;'  class='btn btn-default ensurebtn'>确定</a><a href='javascript://' style='display:none;margin-right: 10px;'  class='btn btn-default canclebtn'>取消</a><a href='javascript://' style='margin-right: 10px;' class='btn btn-default delbtn'>删除</a><a href='#' data-toggle='modal' data-target='#updateUserModal' class='btn btn-default updatebtn'>绑定户</a>";

					}
					return data;
				},
				className: "dt-body-center text-center",
				sWidth: "13em"
			}]
		});
	};

	//查询角色列表
	function initRoleTable() {
		$('#userRoleBox').DataTable({
			ordering: false,
			bAutoWidth: false,
			bProcessing: false,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			bLengthChange: false,
			//			bAutoWidth: true,
			//			bPaginate: false,
			//			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",

				"oPaginate": {
					"sFirst": "首页",
					"sPrevious": "前一页",
					"sNext": "后一页",
					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "roleId",
				className: "text-center roleIdCol"
			}, {
				data: "roleName",
				className: "uRoleCol text-center"
			}, {
				data: "selected",
				className: "text-center",
				render: function(data, type, row) {
					if (type === 'display') {
						var checkedString = data == 1 ? 'checked = "checked" ' : "";
						return '<input type="checkbox" name="checkbox" ' + checkedString + '  class="editor-active row-cbx" value="' + row.roleId + '" data-row="' + data + '">';
					}
					return data;
				}
			}]
		});
	};
	
// function setSelectWidth(width) {
//      $(".ddl").css("width", document.body.clientWidth *width);
// };

})(window, $);