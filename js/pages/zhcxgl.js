(function(window, $) {
	var householdId;
	var housePage;
	var utilitiesPage;
	var householdData;
	var edit
	hzh.pages.js["zhcxgl"] = {
		init: function() {
			findHouseHold(0, 10);
			householdtable();
			utilitiesTable();
			
			$("#householdbtn").bind("click", function() {
				findHouseHold(0, 10);
				
			});
			$("#beback").bind("click",function(){
				var queryterms=$("#home").find("input[type=text]").val("");
				findHouseHold(0, 10);
			});
			housePage = new hzh.Pagination($("#page2"), 10, function(page) {
				findHouseHold(page * 10, 10);
			});
			$('.formular').validationEngine();
			$("#householdtable").delegate(".hupdatebtn", "click", function(){
				var hTr = $(this).parents("tr");
				
				var id = hTr.find("td.hidcol input").val();
				$("#householdId").val(id);
				edit = hTr.find("td.hzheditable");
				var modal = $("#updateHousehold").find("input[type=text]");
				$(edit).each(function(i,e){
					modal.eq(i).val($(e).html());
				});
			});
//			$("#householdtable").delegate(".hensurebtn", "click", ensure);
//			$("#householdtable").delegate(".hcanclebtn", "click", cancle);
			$("#householdtable").delegate(".hdelbtn", "click", delHouseHold);
			
			 
			$("#utilitiesTable").delegate(".updateUtilitiesBtn","click",updateUtilities);
			$("#utilitiesTable").delegate(".ensureUtilitiesBtn","click",ensureUtilities);
			$("#utilitiesTable").delegate(".cancleUtilitiesBtn","click",utilitiesCancle);       
			
			$("#housecheckBox").bind("click",selectAll);
			$("#delAllBtn").bind("click",delAllhouse);
			//住户查询分页
			
			
			//自动切换小区
			$("#communityContent").click(function(){
				findHouseHold(0, 10);
			});
			//缴费单号分页
			utilitiesPage = new hzh.Pagination($("#page1"), 10, function(page) {
				queryUtility(page * 10, 10);
			});
			$("#householdtable").delegate(".utilitiesCodeBtn","click",function(){
				householdId = $(this).parents("tr").find("td.hidcol input").val();
				//console.log(householdId);
				$("#householdIDInput").val(householdId);
				queryUtility(0,10);
			});
			/*$("#householdtable").keydown(function(e) {
				if (e.keyCode == 13) {
					var hTr = $("#householdtable").find(".hensurebtn:visible").parents("tr");
					var hId = hTr.find("td.hidcol input").val();
					var data = {id: hId};
					$(hTr).find(".hzheditable").each(function(i, e) {
						var td = $(e); //对有类hzheditable的列遍历
						var txtInput = td.find("input"); //对有类hzheditable的列找有input元素
						var id = txtInput.attr("id"); //获取id的属性值，并把取得的属性值赋给id
						data[id] = txtInput.val(); //取得txtInput中的值，放入data中
					});
					var phone = /^(1\d{10})|(0\d{2,3}-?\d{7,8})$/;
					var phonetest = phone.test(data.telephone);
					if(phonetest&&data.telephone.length==11){
						hzh.api.request(0x03020110, "updateHousehold", data, function(res) {
							if (res.response.code == 200) {
								hzh.alert("修改成功！");
								change(0, hTr);
							} else {
								hzh.alert("修改失败！");
							}
						});
					}else{
						hzh.alert("您输入的信息有误！");
					}
					
				}
			});*/
		},
		destroy: function() {

		},
		processForm:function(data){
			data["id"] = $("#householdId").val();
			var phone = /^(1\d{10})|(0\d{2,3}-?\d{7,8})$/;
			var phonetest = phone.test(data.telephone);
			if(phonetest&&data.telephone.length==11){
				newVal=$("#updateHousehold").find("input.newVal");
			 	return data;
			 }else{
			 	hzh.alert("您输入的电话有误！");
			 }
			
		},
		processResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("修改成功！");
				
				//findHouseHold(page*10,10);
				$(newVal).each(function(i,e){
					$(edit[i]).html($(e).val());
				});
				
				$("#updateHousehold").modal("hide");
			}
		}
	};


	function findHouseHold(start, num) {
		
		var comId = $("#communityName").val();
		var buildName = $("#buildingNum").val();
		var unitName = $("#unitNum").val();
		var hName = $("#headHouseholdName").val();
		var hTel = $("#telephone").val();
		
		hzh.api.request(0x03020101, "queryHousehold", {
			communityName: comId,
			buildingNum: buildName,
			unitNum: unitName,
			headOfHousehold: hName,
			telephone: hTel,
			userId:hzh.api.user.userId,
			communityId:hzh.api.user.communityId,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			if (res.response.code == 200) {
				var householdData = res.response.data.result;
				hzh.RefreshTable("#householdtable", householdData);
				var totalPage = res.response.data.pageCount;
				var totalData = res.response.data.dataCount;
				housePage.refresh(totalPage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var li = $("#page2").find("li.active").nextAll();
				if(li.length == 1){
					$("#householdtable_info").html("从 "+(start+1)+" 到 " +(totalData)+ " 条/ 共 "+(totalData)+" 条数据");
				}else if(li.length == 0){
					$("#householdtable_info").html("从 "+(0)+" 到 " +(0)+ " 条/ 共 "+(0)+" 条数据");
				}else{
					$("#householdtable_info").html("从 "+(start+1)+" 到 " +(start+num)+ " 条/ 共 "+(totalData)+" 条数据");
				}
			} 
//			else {
//				hzh.alert("未找到该用户！");
//			}
		});
	};


	function delHouseHold() {
		var hid = $(this).parents("tr").find(".hidcol input").val();
		console.log(hid);
		var r = confirm("确认删除？");
		if(r==true){
			hzh.api.request(0x03020119,"delHouseHold",{id:hid},function(res){
				if(res.response.code == 200){
					console.log(res);
					hzh.alert("删除成功！");
					findHouseHold(0,10);
				}else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}
		
	};
	
	function selectAll() {
		if(this.checked == true){
			$('#householdtable').find("input[type='checkbox']").prop("checked",true);
		}else{
			$('#householdtable').find("input[type='checkbox']").prop("checked",false);
		}
	};
	
	function delAllhouse() {
		var ids=[];
		var idString="";
		$('#householdtable').find("input[type='checkbox']:checked").each(function(i,e){
			ids.push($(e).val());
		});
		if (ids.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		idString=ids.join(",");
		var r = confirm("确定删除？");
		if(r==true){
			hzh.api.request(0x03020119,"delHouseHold",{id:idString},function(res){
				if(res.response.code == 200){
					hzh.alert("删除成功！");
					$("#housecheckBox").prop("checked",false);
					findHouseHold(0,10);
				}else{
					hzh.alert("删除失败！");
				}
		    });
		}
		
	};

//	queryUtilitiesCode 0x03060010 查询用户的缴费单号
//	updateUtilitiesCode 0x03060011 更新用户的缴费单号
	function queryUtility(start,num){
		
			householdId = $("#householdIDInput").val();
			hzh.api.request(0x03060010,"queryUtilitiesCode",{
				householdId:householdId,
				communityId:hzh.api.user.communityId,
				limitStart: start,
				limitEnd: num
			},function(res){
				if(res.response.code==200){
					hzh.RefreshTable("#utilitiesTable",res.response.data.result);
					
					var total = res.response.data.pageCount;
					utilitiesPage.refresh(total, start / num);
					$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				}
			});
//		});
		
	};

	function householdtable() {
		$('#householdtable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					//					"sFirst": "首页",
					//					"sPrevious": "前一页",
					//					"sNext": "后一页",
					//					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" value="' + data + '">';
					}
					return data;
				},
				sWidth: "3em",
				className: "hidcol text-center"
			}, {
				data: "id",
//				sWidth: "3em",
				className: "text-center"
			}, {
				data: "communityName",
				className: "buildcol text-center hide",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			}, {
				data: "buildingNum",
				className: "buildcol text-center",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			}, {
				data: "unitNum",
				className: "unitcol text-center",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			}, {
				data: "roomNum",
				className: "roomcol text-center",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			}, {
				data: "familyCount",
				className: "hfamcountcol hzheditable text-center"
			}, {
				data: "headOfHousehold",
				className: "headcol hzheditable text-center"
			}, {
				data: "familys",
				className: "familyscol hzheditable text-center hide"
			}, {
				data: "telephone",
				className: "telcol hzheditable text-center"
			},{
				data: "buildingArea",
				className: "buildareacol hzheditable hide text-center"
			},  {
				data: "unitConstractionArea",
				className: "conareacol hzheditable hide text-center"
			}, {
				data: "sharedPublicArea",
				className: "pubareacol hzheditable hide text-center"
			}, {
				render: function(data, type, row) {
					if (data === undefined) {//<a href='javascript://' style='display:none;margin-right:10px;' class='btn btn-default hensurebtn'>确定</a><a href='javascript://' style='display:none;margin-right:10px;' class='btn btn-default hcanclebtn'>取消</a>
						return "<a href='#' data-toggle='modal' data-target='#updateHousehold' style='margin-right:10px;' class='btn btn-default hupdatebtn'>修改</a><a href='javascript://' style='margin-right:10px;' class='btn btn-default hdelbtn'>删除</a><a href='#' data-toggle='modal' data-target='#myModal' class='btn btn-default utilitiesCodeBtn'>缴费单号</a>";

					}
					return data;
				},
				className: "text-center",
				sWidth: "15em"
			}]

		});
	};



	
	function updateUtilities() {
		var utd = $(this).parents("tr");
		var trSibNode = utd.siblings(); //找打该行的所有兄弟行
		$(trSibNode).find(".ensureUtilitiesBtn:visible").parents("tr").each(function(i, e) { //对兄弟行有处于编辑的进行遍历
			changeUtilities(0, $(e)); //取消这些行的编辑状态
		});
		changeUtilities(1, utd); //将该行处于编辑状态
	};

	function ensureUtilities() {
		var cusTr = $(this).parents("tr");
		var utilitiesId = cusTr.find("td.utilitiesIdCol").html();
		var uId = cusTr.find("td.uIdcol").html();
		var hid = $('#myModal').find("input[type=hidden]").val();
		var data = {id:uId,utilitiesTypeId: utilitiesId,householdId:hid};
		
		$(cusTr).find(".editcol").each(function(i, e) {
			var td = $(e);
			var txtInput = td.find("input");
			var id = txtInput.attr("id");
			data[id] = txtInput.val();
		});	
		hzh.api.request(0x03060011, "updateADCustomer", data, function(res) {
			if (res.response.code == 200) {
				hzh.alert("修改成功！");
				changeUtilities(0,cusTr);
			}else {
				hzh.alert("修改失败，请稍后再试！");
			}
		});
	};
	
	function changeUtilities(status, tr) {
		$(".updateUtilitiesBtn", tr).toggle(status == 0);
		$(".ensureUtilitiesBtn", tr).toggle(status == 1);
		$(".cancleUtilitiesBtn", tr).toggle(status == 1);  
		
		var table = $("#utilitiesTable").DataTable();
		$(tr).find(".editcol").each(function(i, e) {
			var td = $(e); //找到有.editcol的列
			var idx = table.cell(td).index().column; //找到要修改的列
			var id = table.column(idx).dataSrc(); //要修改列的属性名
			if (status) {
				td.html('<input style="width:100%" class="htableinput" id="' + id + '" type="text" value="' + td.html() + '" />'); //如果该行处于编辑状态，则将输入的值赋给该列
			} else {
				td.html(td.find("input").val()); //如果不是处于编辑状态，则将其字段原来的值赋他本身
			}
		});
	};

	function utilitiesCancle() {
		changeUtilities(0, $(this).parents("tr"));
		queryUtility(0,10);
	};
	
	function utilitiesTable(){
		$('#utilitiesTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bProcessing: false,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			bLengthChange: false,
			
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					"sFirst": "首页",
					"sPrevious": "前一页",
					"sNext": "后一页",
					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "utilitiesTypeId",
				render: function(data, type, row) {
					if (type === undefined) {
						return '';
					}
					return data;
				},
				sWidth: "3em",
				className: "text-center utilitiesIdCol"
			}, {
				data: "utilitiesName",
				className: "buildcol text-center",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			}, {
				data: "utilitiesCode",
				className: "editcol text-center",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			},{
				data: "id",
				className: "text-center hide uIdcol",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			},{
				render: function(data, type, row) {
					if (data === undefined) {
						return "<a href='javascript://' style='margin-right:10px;' class='btn btn-default updateUtilitiesBtn'>修改</a><a href='javascript://' style='display:none;margin-right:10px;' class='btn btn-default ensureUtilitiesBtn'>确定</a><a href='javascript://' style='display:none;' class='btn btn-default cancleUtilitiesBtn'>取消</a>";
					}
					return data;
				},
				className: "text-center",
				sWidth: "10em"
			}]

		});
	};

})(window, $);