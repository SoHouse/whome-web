(function(window, $) {

	var zonePage;
	var page;
	var zId;
	var adPage;
	var pageName;
	var pages;
	hzh.pages.js["zone"] = {
		init: function() {
			zoneListTable();
			adZoneTable();
			queryZone(0, 10);
			$("#zoneListBtn").bind("click", function() {
				queryZone(0, 10);
			});
			$("#zoneListTable").delegate(".zoneDelBtn", "click", delZone);
			zonePage = new hzh.Pagination($("#zonePage"), 10, function(page) {
				queryZone(page * 10, 10);
			});
			
			adPage = new hzh.Pagination($("#adPage"), 10, function(page) {
				queryZone(page * 10, 10);
			});
			addpageforzone();
			//版位批量删除
			$("#zonecheckBox").bind("click",selectAll);
			$("#delAllzoneBtn").bind("click",delAllzone);
			
			$("#updateAdZone").bind("click", updateAdZone);
			$('#addzoneForm').validationEngine({
				onValidationComplete:function (form,valid){
					if(valid){
						$(form).hzhsubmit();
					}else{
						hzh.alert("请将信息填写完整！");
					}
				}
			});
			
			//返回
			$("#beback").bind("click",function(){
				$("#home").find("input[type=text]").val("");
				queryZone(0,10);
			});
			
			$("#zoneListTable").delegate(".queryAdZoneBtn", "click", function(){
				var tr = $(this).parents("tr");
				var zoneid = tr.find(".idCol input").val();
				var adname = tr.find(".adcol");
				$("#adIdInput").val(zoneid);
				$("#updateAdZone").data("adname",adname);
				$("#updateAdZone").data("zoneid", zoneid);
				queryAdZone(0,10);
			});
			
			$("#zoneListTable").delegate(".zoneUpdateBtn", "click", function(){
				updatecampaign(0,10);
			});
			$("#zoneListTable").delegate(".zoneUpdateBtn", "click", function(){
				var zoneTr = $(this).parents("tr");
				zId = zoneTr.find("td.idCol input").val();
				var td = $(zoneTr).find(".exitcol");
				var table = $("#updateModal").find("input");
				for(var i=0;i<td.length;i++){
					var tdval = $(td).eq(i).html();
					$(table).eq(i).val(tdval);
				}
				$("#inputId").val($(zoneTr).find(".campaignIdCol").html());
				$("#updatePageId").val($(zoneTr).find(".pageIdCol").html());
				pageName = $(zoneTr).find(".pageNamecol").html();
				$("#pageid").combogrid("setValue",pageName);
				$("#campaignId").combogrid("setValue",$(zoneTr).find(".campaignNamecol").html());
				
			});
			
			$("#pageId").combogrid({
//				toolbar:"#updateTb",
				panelWidth: 500,
				panelHeight:400,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'name',title: '展示页名称',width: 90},
					{field: 'linkman',title: '联系人',width: 80},
					{field: 'id',title: 'ID',width: 250}
				]],	
			});
			
			 $("#pageid").combogrid({
//				toolbar:"#updateTb",
				panelWidth: 500,
				panelHeight:400,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'name',title: '展示页名称',width: 90},
					{field: 'linkman',title: '联系人',width: 80},
					{field: 'id',title: 'ID',width: 250}
				]],	
			});
			$("#campaignId").combogrid({
//				toolbar:"#updateTb",
				panelWidth: 500,
				panelHeight:400,
				textField:"name",
			    idField:'id',
		        pagination: true,           //是否分页  
		        rownumbers: true,           //序号  
		        collapsible: true,         //是否可折叠的
		        pageSize: 10,//每页显示的记录条数，默认为10  
				columns:[[
					{field: 'name',title: '项目名称',width: 90},
					{field: 'contact',title: '项目联系人',width: 80},
					{field: 'id',title: 'ID',width: 250}
				]],	
			});
			
			var textkey = $("#campaigndiv").find(".textbox-text");
			$(textkey).keydown(function(event){
				var content = $(this).val();
				var idval = $("#inputId").val();
				if(event.keyCode == 8){
					var arr=content.split(",");
					var newVal=arr.slice(0,arr.length-1).join(",");
					$(this).val(newVal);
					var arrid = idval.split(",");
					var newids = arrid.slice(0,arrid.length-1).join(",");
					console.log(newids);
					$("#inputId").val(newids);
				}
				return false;				
			});
			
		},
		destroy: function() {
			$('.combo-p').remove();
		},
		processForm: function(data) {
			/*var errorlength = $('#addzoneForm').find(".form-group>.formError:visible").length;
			console.log(errorlength);
			if(errorlength == 0){*/
				return data;
			/*}else{
				return ;
			}*/
		},
		processResponse: function(res) {
			if (res.response.code == 200) {
				hzh.alert("添加成功！");
				queryZone(0, 10);
				$("#addzoneForm").find("input").each(function(i, e) {
					$(e).val("");
				});
				$("#myModal").modal("hide");
			} else {
				hzh.alert(res.response.data);
			}
		},
		
		updateForm:function(data){
			data["zoneId"] = zId;
			var id = $("#campaigndiv").find("input[type=hidden]");
			var ids = [];
			$(id).each(function(i,e){
				ids.push($(e).val());
			});
			var idString = '';
			idString = ids.join(",");
			data.campaignIds = idString;
			
			var inputName = $("#updatePageDiv").find("span input.textbox-value").val();
			if(pageName != inputName){
				return data;
			}else {
				data.pageId = $("#updatePageId").val();
				return data;
			}
			return data;
		},
		updateResponse:function(res){
			if(res.response.code == 200){
				hzh.alert("修改成功！");
				queryZone(pages, 10);
				$("#updateModal").modal("hide");
			}else {
				hzh.alert("修改失败！");
			}
		}
	};

	function queryZone(start, num) {
		//展示页、版位、广告、广告客户、广告项目
		var page = $("#page").val();
		var zone = $("#zone").val();
		var adName = $("#adName").val();
		var adcustomer = $("#adcustomer").val();
		var adcampaign = $("#adcampaign").val();
		hzh.api.request(0x03040119, "selectADZone", {
			name: zone,
			pageName: page,
			campaignName: adcampaign,
			customerName: adcustomer,
			resourceName: adName,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			if (res.response.code == 200) {
				var data = res.response.data.result;
				hzh.RefreshTable("#zoneListTable", data);
				var totalpage = res.response.data.pageCount;
				pages=start;
				
				zonePage.refresh(totalpage, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
				var dataCount = res.response.data.dataCount;
				var li = $("#zonePage").find("li.active").nextAll();
				if(li.length == 1){
					$("#zoneListTable_info").html("从 "+(start+1)+" 到 " +(dataCount)+ " 条/ 共 "+(dataCount)+" 条数据");
				}else if(li.length == 0){
					$("#zoneListTable_info").html("从 "+(0)+" 到 " +(0)+ " 条/ 共 "+(0)+" 条数据");
				}else{
					$("#zoneListTable_info").html("从 "+(start+1)+" 到 " +(start+10)+ " 条/ 共 "+(dataCount)+" 条数据");
				}
			}
		});
	};
	
	function updatecampaign(start,num){
		hzh.api.request(0x03040110, "selectADCampaign", {
			limitStart: start,
			limitEnd: num
		},function(res){
			var data = res.response.data.result;
			$("#campaignId").combogrid("grid").datagrid("loadData", data);
				
			var campPager = $("#campaignId").combogrid("grid").datagrid("getPager");
            campPager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03040110, "selectADCampaign", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#campaignId").combogrid("grid").datagrid("loadData", res.response.data.result);
						campPager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
		});
	};
	
	function addpageforzone(start,num) {
		hzh.api.request(0x03040115, "selectADPage", {
			limitStart: start,
			limitEnd: num
		}, function(res) {
			var data = res.response.data.result;
			$("#pageId").combogrid("grid").datagrid("loadData", data);
				
			var pageIdPager = $("#pageId").combogrid("grid").datagrid("getPager");
            pageIdPager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03040115, "selectADPage", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#pageId").combogrid("grid").datagrid("loadData", res.response.data.result);
						pageIdPager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
            
            $("#pageid").combogrid("grid").datagrid("loadData", data);
			var pageidPager = $("#pageid").combogrid("grid").datagrid("getPager");
            pageidPager.pagination({ 
                total:res.response.data.dataCount,
                onSelectPage:function (pageNo, pageSize) {
                	var st = (pageNo - 1) * pageSize;
                	hzh.api.request(0x03040115, "selectADPage", {
						limitStart: st,
						limitEnd: pageSize
					}, function(res) {
						$("#pageid").combogrid("grid").datagrid("loadData", res.response.data.result);
						pageidPager.pagination('refresh', {  
	                        total:res.response.data.dataCount,  
	                        pageNumber:pageNo  
	                    }); 
					});
				}
            });
//			var html="";
//			$(data).each(function(i,e){
//				var adpagename = e.name;
//				var adpageid = e.id;
//				html+="$(<option value="+adpageid+">"+adpagename+"</option>)";
//			});
			
			
		});
	};
		
	
	function delZone() {

		var zoneid = $(this).parents("tr").find(".idCol input").val();
		var r = confirm("确认删除？");
		if(r==true){
			hzh.api.request(0x03040118, "deleADZone", {
				id: zoneid
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功！");
					queryZone(page * 10, 10);
				} else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}
		
	};
	
	function selectAll(){
		if(this.checked == true){
			$('#zoneListTable').find("input[type='checkbox']").prop("checked",true);
		}else{
			$("#zoneListTable").find("input[type='checkbox']").prop("checked",false);
		}
	};
	
	function delAllzone(){
		var ids=[];
		$('#zoneListTable').find("input[type='checkbox']:checked").each(function(i,e){
//			console.log($(e).val());
			ids.push($(e).val());
		});
		if (ids.length == 0) {
			hzh.alert("请选择至少一项");
			return;
		}
		var idString=ids.toString();
		var r = confirm("确定删除？");
		if(r==true){
			hzh.api.request(0x03040118, "deleADZone", {
				id: idString
			}, function(res) {
				if (res.response.code == 200) {
					hzh.alert("删除成功！");
					queryZone(page * 10, 10);
				} else {
					hzh.alert("删除失败，请稍后再试！");
				}
			});
		}		
	};
	
	function queryAdZone(start,num) {
		
		var zoneid = $("#adIdInput").val();
		hzh.api.request(0x03040122, "selectADResourceZone", {
			id: zoneid,
			limitStart: start,
			limitEnd: num
		}, function(res) {
			if (res.response.code == 200) {
				hzh.RefreshTable("#adZoneTable", res.response.data.result);
				
				var total = res.response.data.pageCount;
				adPage.refresh(total, start / num);
				$(".pagination a[aria-label='上一页'],a[aria-label='下一页']").attr("class", "");
			}
		});
	};

	function updateAdZone() {
		var uid = $(this).data("zoneid");
		var adname = $(this).data("adname");
		var resourceIds = [];
		var resourceNames = [];
		var resourceIdString = '';
		var resouceNameString = '';
		var checkedbox = $("#adZoneTable").find("input:checkbox[name='checkbox']:checked");
		checkedbox.each(function(i, e) {
			var idval = $(e).parents("tr").find("td.resIdCol").html();
			var nameval = $(e).parents("tr").find("td.adnamecol").html();
			resourceIds.push(idval);
			resourceNames.push(nameval);
		});
		resourceIdString = resourceIds.join(",");
		resouceNameString = resourceNames.join(',');
		hzh.api.request(0x03040123, "updateADResourceZone", {
			zoneId: uid,
			resourceIds: resourceIdString
		}, function(res) {
			if (res.response.code == 200) {
				hzh.alert("修改成功！");
				$("#queryAdZoneBtn").modal('hide');
				$(adname).html(resouceNameString);
			} else {
				hzh.alert("修改失败，请稍后再试！");
			}
		});
	};

	function adZoneTable() {
		$('#adZoneTable').DataTable({
			"bLengthChange": false,
			ordering: false,
			bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				//				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					"sFirst": "首页",
					"sPrevious": "前一页",
					"sNext": "后一页",
					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "resourceId",
				className: "resIdCol text-center"
			}, {
				data: "resourceName",
				className: "text-center adnamecol",
				render:function(data,type,row){
					if(data==undefined){
						return "";
					}
					return data;
				}
			}, {
				data: "selected",
				className: "text-center",
				render: function(data, type, row) {
					if (type === 'display') {
						var checkedString = data == 1 ? 'checked = "checked" ' : "";
						return '<input type="checkbox" name="checkbox" ' + checkedString + '  class="editor-active row-cbx" value="' + data + '" data-row="' + data + '">';
					}
					return data;
				}
			}]
		});
	};

	function zoneListTable() {
		$('#zoneListTable').DataTable({
			ordering: false,
			bAutoWidth: false,
			bProcessing: true,
			bFilter: false,
			bPaginate: false,
			responsive: true,
			"oLanguage": {
				"sLengthMenu": "每页显示 _MENU_ 条记录",
				"sZeroRecords": "抱歉， 没有找到",
				"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				"sInfoEmpty": "没有数据",
				"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				"oPaginate": {
					//					"sFirst": "首页",
					//					"sPrevious": "前一页",
					//					"sNext": "后一页",
					//					"sLast": "尾页"
				},
				"sZeroRecords": "没有检索到数据",
				"sProcessing": "<img src='./loading.gif' />",
				"sSearch": "搜索"
			},
			columns: [{
				data: "id",
				className: "idCol text-center",
				render: function(data, type, row) {
					if (type === 'display') {
						return '<input type="checkbox" name="subbox" class="editor-active row-cbx" value="' + data + '">';
					}
					return data;
				},
				sWidth: "3em"
			},{
				data: "campaignIds",
				className: "campaignIdCol hide",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			},{
				data: "pageId",
				className: "pageIdCol hide",
				render:function(data,type,row){
					if(data == undefined){
						return "";
					}
					return data;
				}
			},{
				data: "name",
				className: "namecol exitcol text-center"
			}, {
				data: "remark",
				className: "remarkcol exitcol hide text-center"
			}, {
				data: "width",
				className: "widthcol exitcol text-center"
			}, {
				data: "height",
				className: "heightcol exitcol text-center"
			}, {
				data: "campaignName",
				className: "campaignNamecol hide text-center"
			}, {
				data: "pageName",
				className: "pageNamecol text-center"
			},{
				data: "resourceName",
				className: "adcol text-center"
			},  {
				data: "customerName",
				className: "text-center hide"
			}, {
				render: function(data, type, row) {
					if (data === undefined) {
						return "<a href='#' data-toggle='modal' data-target='#updateModal' style='margin-right:10px' class='btn btn-default zoneUpdateBtn'>修改</a><a href='javascript://' style='margin-right:10px'  class='btn btn-default zoneDelBtn'>删除</a><a href='#' data-toggle='modal' data-target='#queryAdZoneBtn' class='btn btn-default queryAdZoneBtn'>修改广告</a>";
					}
					return data;
				},
				sWidth: "15em",
				className: "text-center"
			}]
		});
	};
		
	
})(window, $)