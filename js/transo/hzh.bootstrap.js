/**
 *	hzh.bootstrap.js by Stone Chow ,
 *   2015-03-09
 *	the core lib of transo js
 *
 */
 "use strict";
(function(window, $) {
	/*		var api=hzh.api;
			if(!api){
				throw "hzh.api must be instantiated before loading this script";
			}*/
			window.hzh.log=function(o){
				if(console && console.log){
					console.log(o);
				}
			};
	var pages={
		onload:null,
		onunload:null,
		currentUrl:null,
		currentPage:null,
		js:{},
		jsfailed:{},
		registerCallback:function(load,unload){
			this.onload=load;
			this.onunload=unload;
		},
		load:function(url,skipstate,page){
			var $this=this;
			if(this.currentUrl == url){
				return;
			}
			if(!skipstate&&this.currentUrl){
				console.log("pushingstate,"+this.currentUrl);
				window.history.pushState({url:this.currentUrl,page:this.currentPage},null,document.URL);
			}
			if(this.onunload){//call unload callback
				try{
					this.onunload();
				}
				catch(e){
					console.log(e);
				}
			}
			var cjs=this.js[this.currentPage];
			if(cjs){//call to destroy the current page;
				try{
					cjs.destroy();
				}
				catch(e){
					console.log(e);
				}
			}
			this.currentUrl=url;
			this.currentPage=page;
			hzh.showLoading();
			this.registerCallback(null,null);
			function callPageInit(page,obj){
				if(obj){
					try{
						obj.init();
					}
					catch(e){
						console.log(e);
					}
				}
				else{
					var js=$this.js[page];
					if(js){
						callPageInit(page,js);
					}
				}
			};
			$("#page-wrapper").load(url, function(result) {
				var njs=$this.js[page];
				if(njs){
					callPageInit(page,njs);
				}
				else if($this.jsfailed[page]){
					//do nothing;
				}
				else{
				 $.getScript("../js/pages/"+page+".js",function(a,b,c){
 					callPageInit(page);
				}).fail(function(){$this.jsfailed[page]=true;});;	
				}
				 init();
				 if($this.onload){
					 try{
						$this.onload();
					}
					catch(e){
						console.log(e);
					}
				 	
				 }
				 
				 
			});
		}
	};
	hzh.pages=pages;
	hzh. initMenuLink=function(){
		$("#side-menu a").click(function() {
			var url = $(this).attr("href");
			if (!url || url == "#") {
				return false;
			}
			var page=$(this).data("page");
			if(!page){
				page=getPageName(url);
			}
			pages.load(url,false,page);
			return false;
		});
		function getPageName(url){
			var lindex=url.lastIndexOf(".");
			var findex=url.lastIndexOf("/");

			return url.substring(findex,lindex);
		};
	};
	$(function() {
		var reg=/([^\.\/]+)\.{1}/gi;
		hzh.initMenuLink();
		
		init();
	});
	
	

	function init() {
		hzh.hideLoading();
		$("form.hzhform").hzhForm();
	};
/*window.addEventListener("popstate", function(e) {
	if(e.state){
		console.log("popstate,"+e.state.url);
		pages.load(e.state.url,true);
}
});*/
window.onpopstate = function(e) { 
  if (!e.state || !e.state.url) return; // none of my business
 		console.log("popstate,"+e.state.url);
		pages.load(e.state.url,true,e.state.page);
}

window.onload = function(e) {
  history.replaceState(null, null, document.URL);
};
function getFunction(name){
	var ret=window[name];
	if(ret){
		return ret;
	}
	try{
			return eval(name);
	}
	catch(e){
		hzh.log(e);
	}
	return null;
};
function submitform(form){
	var obj = $(form).serializeArray();
			var callback = null;
			var eventType = $(form).data("event");
			var requestType = $(form).data("request");
			if (!eventType) {
				return true;
			}
			var bsf = null;
			var bsfName = $(form).data("bs");
			if (bsfName) {
				bsf=getFunction(bsfName);
			}
			var data = {};
			$.each(obj, function(i, e) {
				data[e.name] = e.value;
			});
			var cb = $(form).data("cb");
			if(cb){
				callback=getFunction(cb);
			}
			var fileInputs = $('input[type=file]:enabled', form).filter(function(i,e) {
				//console.log(e);
				return $(e).val() !== '';
			});
			var files = [];
			$.each(fileInputs, function(i, e) {
				$.each(e.files, function(i, e) {
					files.push(e);
				});
			});
			if (bsf) {
				data = bsf(data);
				if(typeof data === "undefined" ){
					return false;
				}
			}
			if (files.length > 0) {
				//  alert(files.length);
				data.filesize = files.length;
				var fr = new hzh.FileReader();
				fr.read(files, function(rs) {
					//	alert(rs[0].result);
					for (var i = 0; i < rs.length; i++) {
						var d = rs[i];
						//console.log(d.result);
						//var arr=new Uint8Array(d.result);
						//console.log(arr);
						data["file" + i] = new hzh.models.HZHByteArray(d.result);
						data["filename" + i] = d.file.name;
						//api.request()
					}
					hzh.api.request(parseInt(eventType, 16), requestType, data, callback);
				});
			} else {
				hzh.api.request(parseInt(eventType, 16), requestType, data, callback);
			}
			return false;
};
	function hzhform() {
		if ($(this).data("hzhform") == "1") {
			return;
		}
		$(this).data("hzhform", "1");
		$(this).submit(function() {
 			return submitform(this);
			//return false;
		});

	};
	hzh.showLoading=function(){
		$("body").removeClass("loaded");
	};
	hzh.hideLoading=function(){
		$("body").addClass("loaded");
	};
	$.fn.extend({
		hzhForm: hzhform
	});
	$.fn.extend({
		hzhsubmit:function(){
			submitform(this);
		}
	});
	hzh.alert=window.alert;
	if(window.bootbox){
		hzh.alert=bootbox.alert;
	}
	
})(window,$);