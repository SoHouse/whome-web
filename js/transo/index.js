﻿/**
 *	index.js by Stone Chow ,
 *   2015-05-06
 *	the javascript file for index.html
 *
 */

(function(window, $) {
	
	initApi();
	var newsDetailId;
	function initApi() {
//				hzh.api = hzh.openApi("ws://192.168.1.114:5992", {
//		hzh.api = hzh.openApi("ws://192.168.1.55:5992", {
//			hzh.api = hzh.openApi("ws://192.168.1.99:5992", {
				hzh.api = hzh.openApi("ws://112.124.114.224:5992", {
			onConnected: function() {
				//                hzh.api.login("a","b");
			},
			onAuthurizeNeeded: function(code, data, sender) {
				 alert("authurizeneeded:"+code);
				//login
				//				alert('11');
				hzh.log("on Authurize Needed:"+code);
				location.href = "login.html";
			},
			onAuthurized: function(data, sender) {
				 console.log("logged in:"+json2str(data));
				//location.href = "index.html";
			},
			onError: function(err, sender) {
				//				alert('22');
				hzh.log("on Error:"+code);
				location.href = "login.html";
			},
			onDisconnected: function(sender) {
				setTimeout(function(){initApi();},3000);
				//initApi();
			},
			onEvent: function(eve, sender) {
				
				console.log('onEvent!!!!!!!!!!!!!!!!');
				console.log(eve.data.message);
			}

		});
		
		if (!hzh.api.isLogin()) {
			location.href = "login.html";
			return;
		}
		var token = hzh.api.user.token;
		var uid = hzh.api.user.userId;
		hzh.api.request(0x03020121, "queryAdminCommunity", {//0x03020412
//			limitStart: 0,
//			limitEnd: 10,
			userId: uid			
		}, function(res) {
			if (res.response.code == 200) {
				var arr = res.response.data.result;
				hzh.api.user.communities = arr;
				var selectval = $("#communityContent");
				var comName;
				var comId;
				var html="";
				$(arr).each(function(i,e){
					comName = e.name;
					comId = e.id;
//					$('<label class="checkbox-inline"><input type="checkbox" name="scopeId" value="'+e.communityId+'">'+e.communityName+'</label>').appendTo(selectval);
					html+="$(<option value="+comId+">"+comName+"</option>)";
				});
				$("#communityContent").html(html);
				var selcomId = $('#communityContent').find("option:selected").val();
				hzh.api.user.communityId = selcomId;
			}
		});
		var housePage;
		//菜单
//		menulist();
//		userBindCommunity();
		$("#communityContent").scrollTop(20);
		$("#communityContent").change(function(){
			var communityid = $('#communityContent').find("option:selected").val();
			console.log(communityid);
			hzh.api.user.communityId = communityid;
		});
		//首页
//		showUnread();
//		showAmount();
		showUser();
		showNews();
		//newsDetail();
		//$("#btn").bind("click",aa);
		$("#rightContent a[role='button']").click(goTo);//页面跳转
		
		$(".unread").delegate("a", "click", function() {
			var thisId = $(this).attr("for");
			unreadDetail(thisId, $(this));
		});
		
		//新闻详情		
		$("#todayList").delegate("a", "click", function() {
			newsDetailId = $(this).attr("for");
			//console.log(newsDetailId);
			newsDetail();
		});
		/*hzh.api.request(0x03070201, "getUploadToken", {},function(res){
			if(res.response.code == 200){
				var uploadtoken=res.response.data.result;
				hzh.api.user.uploadtoken=uploadtoken;
			}
		});*/
		 
//		$("#chatBtn").bind("click",chat);
	};
	
	
	$(function() {
		$(".logout-btn").click(function() {
			hzh.api.logout();
			return true;
		});
		$("#changePasswordBtn").click(function() {
			var modal = $(this).parentsUntil(".modal");
			var oldPwd = $("#oldpwd").val();
			var newPwd = $("#newpwd").val();
			var confirmPwd = $("#confirmpwd").val();
			if (!oldPwd || !newPwd || !confirmPwd) {
				hzh.alert("请输入旧密码及需要修改的新密码");
			}
			//	alert(hzh.api.user.loginName);
			if (newPwd != confirmPwd) {
				hzh.alert("输入密码不匹配");
			}
			hzh.showLoading();
			hzh.api.request(0x03010105, "changePwd", {
				loginName: hzh.api.user.loginName,
				password: hex_md5(oldPwd),
				newPwd: hex_md5(newPwd)
			}, function(data) {
				hzh.hideLoading();
				if (data.response.code == 200) {
					modal.find(".close").click();
					hzh.alert("修改成功");
				} else if (data.response.code == 512) {
					hzh.alert("旧密码错误");
				} else {
					hzh.alert("服务器发生错误，请稍后重试");
				}
			});
		});

	});
	hzh.RefreshTable = function(tableId, data) {

		table = $(tableId).dataTable();
		oSettings = table.fnSettings();

		table.fnClearTable(this);

		for (var i = 0; i < data.length; i++) {
			table.oApi._fnAddData(oSettings, data[i]);
		}
		oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
		table.fnDraw();

	};
	(function(window, $) {
		function refreshPageDiv(pageDiv, totalPage, currentPage, cb) {
			//var pageDiv = $(".pagination");
			pageDiv.empty();
			var pages = getShowingPages(totalPage, currentPage, 10);
			var previousPageLi = createPageLi(currentPage - 1, "上一页", currentPage == 0) //$('<li><a href="#" page= aria-label="Previous"><span aria-hidden="true">上一页</span></a></li>');
			previousPageLi.appendTo(pageDiv);
			//currpag, totalpage, , previouspage
			$.each(pages, function(i, e) {
				var li = createPageLi(e, e + 1, currentPage == e, currentPage == e);
				//遍历所有li
				li.appendTo(pageDiv); //将所有li放到pageDiv中

			});
			var nextPageLi = createPageLi(currentPage + 1, "下一页", currentPage >= totalPage - 1)
			nextPageLi.appendTo(pageDiv);
			pageDiv.find("li a").click(cb);
		};

		function onPageLinkClick(a, cb) {
			var alink = $(a);
			if (alink.parent().hasClass("disabled")) {
				return false;
			}
			var page = alink.attr("page");

			if (cb) {
				cb(page);
			}
			return false;
		};

		function getShowingPages(totalPage, currentPage, showingCount) {
			var ret = [];
			var half = showingCount / 2;
			if (totalPage < showingCount) {
				for (var i = 0; i < totalPage; i++) {
					ret.push(i);
				}
				return ret;
			}
			var precount = currentPage > half ? half - 1 : currentPage;
			precount = Math.max(precount, showingCount - totalPage + currentPage)
			for (var i = currentPage - precount; i < currentPage; i++) {
				ret.push(i);
			}
			for (var i = currentPage; i < currentPage + showingCount - precount; i++) {
				ret.push(i);
			}
			return ret;
		};

		function createPageLi(page, label, disabled, active) {
			var li = $('<li><a href="#" page="' + page + '" aria-label="' + label + '" class="pageLi"><span aria-hidden="true">' + label + '</span></a></li>');
			if (disabled && !active) {
				li.addClass("disabled");
			}
			if (active) {
				li.addClass("active");
			}
			return li;
		};
		var HzhPage = function(pageDiv, count, cb) {
			this.init(pageDiv, count, cb);
			return this;
		};

		hzh.copy(HzhPage.prototype, {
			clickCallback: null,
			pageDiv: null,
			showingCount: null,
			init: function(pageDiv, count, cb) {
				this.clickCallback = cb;
				this.pageDiv = pageDiv;
				this.showingCount = count;
			},
			refresh: function(totalPage, currentPage) {
				var that = this;
				refreshPageDiv(this.pageDiv, totalPage, currentPage, function() {
					onPageLinkClick(this, that.clickCallback);
					return false;
				});
			}

		});
		window.hzh.Pagination = HzhPage;
	}(window, $));
	
	// 菜单接口
	function menulist() {	
		var uid = hzh.api.user.userId;
		hzh.api.request(0x03010106, "menulist", {
			userId: uid
		}, function(res) {
			var data = res.response.data.result;
			console.log(data);
			var html = '';
			var ptree = buildMenuTree(data, 0);
			html += buildMenuList(ptree);
			$("#side-menu").html(html);
			$('#side-menu').metisMenu();  // 菜单层级展开
			hzh.initMenuLink();  //页面跳转初始化
		});
	};


	function buildMenuList(list) {
		var html = '';
		for (var i = 0; i < list.length; i++) {
			var data = list[i];
			var hasChildren = data.children && data.children.length > 0;
			if (hasChildren) {
				html += '<li ><a href="#"><i class="fa fa-wrench fa-fw"></i>' + data.name + ' <span class="fa arrow"></span></a>';
				html += '<ul class="nav nav-second-level collapse">';
				html += buildMenuList(data.children);
				html += '</ul>';
			} else {
				html += '<li ><a href="' + data.url + '">' + data.name + '</a></li>';
			}
			html += '</li>';
		}
		return html;
	};

	function buildMenuTree(content, parentId) {
		var ret = [];
		$.each(content, function(i, e) {
			if (e.parentId == parentId) {

				var cld = buildMenuTree(content, e.id);
				if (cld.length > 0)
					e.children = cld;
				ret.push(e);
			}
		});
		return ret;
	};

	//首页
	function CurentDate() {
		var now = new Date();
		var year = now.getFullYear(); //年
		var month = now.getMonth() + 1; //月
		var day = now.getDate(); //日
		var clock = year + "-";
		if (month < 10)
			clock += "0";
		clock += month + "-";
		if (day < 10)
			clock += "0";
		clock += day;
		return (clock);
	};


	function isRead(data, type) {
		var unRead = [];
		var unReadNum = 0;
		if (data.length) {
			for (var i = 0; i < data.length; i++) {
				if (data[i].isRead == "0") {
					unRead.push(i);
					unReadNum++;
				}
			};
		}
		if (type == Array) {
			return unRead;
		} else if (type == Number) {
			return unReadNum;
		};
	};

//	function showUnread() {
//		hzh.api.request(0x03010304, "queryInformation", {
//			loginName: hzh.api.user.loginName,
//			type: "notice",
//			limitEnd: 10000
//		}, function(res) {
//			var unreadNum = isRead(res.response.data.result, Array);
//			if (unreadNum.length == 0) {
//				$(".no-notice").append('<img src="../images/no-notice.png" alt="">');
//				return;
//			}else {
//				$(".unread").removeClass("unread").addClass("show-unread");
//				for (var i = 0; i < Math.min(10, unreadNum.length); i++) {
//					var resultVal = res.response.data.result[unreadNum[i]];
//					$(".show-unread strong").append('<a href="#" style="font-size:18px;" data-toggle="modal" data-target="#noticeModal" for="' + resultVal.id + '">' + resultVal.title + '</a><br>');
//				}
//			}
//			
//		});
//	};

//	function showAmount() {
//		hzh.api.request(0x03010201, "queryPaymentStatus", {
//			loginName: hzh.api.user.loginName,
//			limitEnd: 10000
//		}, function(res) {
//			var amount = 0;
//			var result = res.response.data;
//			console.log(result);
//			if(result==null){
//				$(".owe>p.owep").append('您没有欠费!');
//			}else if (result.水费.balance || result.电费.balance) {
//				$(".owe>p.waterp").append('水'+("&nbsp;&nbsp;&nbsp;")+'费: ' + result.水费.balance + '元');
//				$(".owe>p.electricityp").append('电'+("&nbsp;&nbsp;&nbsp;")+'费: ' + result.电费.balance + '元');
//				$(".owe>p.wuguanp").append('物管费: ' + result.electricity.balance + '元');
//			} else{
//				$(".owe>p.owep").append('您没有欠费!');
//			}
//		});
//	};

	function showUser() {
		var name = hzh.api.user.headOfHousehold;
//		console.log(hzh.api.user);
		$(".userName").html(name);
	};

	function goTo() {
		var oUrl = $(this).attr("for");
		console.log(oUrl);
		hzh.pages.load(oUrl + ".html", false, oUrl);
	};

	function showNews() {
		hzh.api.request(0x03010307, "queryInformation", { //0x03010304
			loginName: hzh.api.user.loginName,
			limitStart: 0,
			limitEnd: 10000,
			type: "article"
		}, function(res) {
			
			var data = res.response.data.result;
			var html = "";
			for (var i = 0; i < Math.min(6, data.length); i++) {
				var time = data[i].createTime.split(" ").slice(0,1);
				data[i].createTime = time;
				html+="<a style='display: block;margin-bottom:20px;' target='_blank' href='http://112.124.114.224:90/"+data[i].fileName+"'><span style='color: #818181;'>"+data[i].title+"</span><span style='display: inline-block;color:#818181;font-size:18px;float: right;'>"+time+"</span></a>";
//				html+="<a style='display: block;margin-bottom:20px;' target='_blank' href='http://112.124.114.224:90/"+data[i].fileName+"'><span style='color: #818181;'>"+data[i].title+"</span><span style='display: inline-block;color:#818181;font-size:18px;float: right;margin-right: 80px;'>"+data[i].createTime+"</span></a>";
				
				$("#news").html(html);
			}
		});
	};

	function newsDetail() {
//			var thisId = $(this).attr("for");
//			console.log(thisId);
			hzh.api.request(0x03010302, "", {
				id: newsDetailId,
				loginName: hzh.api.user.loginName
			}, function(res) {
				if (res.response.code == 200) {
					var dataObject = res.response.data;
					var contentval = hzh.BASE64.decoder(dataObject.content);
					$("#page-wrapper").html('<div class="row"><div class="col-lg-12"><a href="index.html">返回</a><h1 class="page-header text-center">' + dataObject.title + '</h1><h5 class="text-right">' + dataObject.createAuthor + '<br>' + dataObject.createTime + '</h5></div></div>');
					$("#page-wrapper").append('<div class="panel-body text-center">' + contentval + '</div>')
				}
			});
	};

	function unreadDetail(id, ev) {
		var newsId = id;
		var newsTitle = ev.text();
		hzh.api.request(0x03010302, "", {
			id: newsId,
			loginName: hzh.api.user.loginName
		}, function(res) {
			$("#noticeModal .modal-title").text(newsTitle);
			var result = res.response.data;
			var resultcontent = hzh.BASE64.decoder(result.content);
			$("#noticeModal .modal-body").html(resultcontent);
			$("#noticeModal .modal-body img").css({
				"max-width": "800px"
			});
		});
	};
	
//	function userBindCommunity() {
//		var uid = hzh.api.user.userId;
//		hzh.api.request(0x03020412, "queryAdminCommunity", {
//			userId: uid
//		}, function(res) {
//			if (res.response.code == 200) {
//				var arr = res.response.data.communityArray;
//				hzh.api.user.communities = arr;
//				var selectval = $("#communityContent");
//				var comName;
//				var comId;
//				var html="";
//				$(arr).each(function(i,e){
//					comName = e.communityName;
//					comId = e.communityId;
////					$('<label class="checkbox-inline"><input type="checkbox" name="scopeId" value="'+e.communityId+'">'+e.communityName+'</label>').appendTo(selectval);
//					html+="$(<option value="+comId+">"+comName+"</option>)";
//				});
//				$("#communityContent").html(html);
//				var selcomId = $('#communityContent').find("option:selected").val();
//				console.log(selcomId);
//				hzh.api.user.communityId = selcomId;
//			}			
//		});
//	};
	
//	function chat(){
//		var pushScope = $("#scope").val();
//		var scopeId = $("#scopeId").val();
//		var chatmessage = $("#chatmessage").val();
//		hzh.api.request(0x03000004, "queryAdminCommunity", {
//			scope:pushScope,
//			scopeId:scopeId,
//			message:chatmessage
//		}, function(res) {
//			console.log(res);
//		});
//	};
	
})(window, $);