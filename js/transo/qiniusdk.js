(function(window, $) {
	var FileManager =function(page) {
		this.init(page);
	};
	$.extend(FileManager.prototype, {
		dialog: null,
		init: function(elfinderpage) {
			var dialog = $('<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">' +
				'<div class="modal-dialog modal-lg">' +
				' <div class="modal-content">' +
				'<div class="modal-header">' +
				' <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
				'  <h4 class="modal-title" id="gridSystemModalLabel">文档资源管理</h4>' +
				' </div>' +
				'<div class="modal-body">' + 
				'<iframe id="aiframe" src="'+elfinderpage+'" width="100%" height="420" frameborder="0"></iframe>' +
				' </div>' +
				'<div class="modal-footer"><button type="button" class="btn btn-default"  data-dismiss="modal">关闭</button><button type="button" class="btn btn-primary confirmBtn">确定</button></div> </div>' +
				'</div>' +
				'</div>');
				var iframe=dialog.find("#aiframe");
				dialog.find(".confirmBtn").click(function(){
					iframe[0].contentWindow.selectFile();//按确定按钮时调用和打开按钮一样的click
				});
				this.dialog=dialog;
		},
		show:function(){
			this.dialog.modal('show');
		},
		hide:function(){
			this.dialog.modal('hide');
		},
		callback:null,
	});
	var container=window.hzh||window;
	container.openFileManager=function(cb){
		var fm=container.fileManager;//FileManager
		if(!fm){
			fm=new FileManager("http://127.0.0.1/whome-web/elfinder-2.x/elfinder.html");
//			fm=new FileManager("http://112.124.114.224:90/whome-web/elfinder-2.x/elfinder.html");
			//console.log(fm.dialog[0].innerHTML);
			container.fileManager=fm;
		}
		fm.callback=cb;
		fm.show();
	};
	function onFileSelected(files) {
		var fm=container.fileManager;
		if(fm && fm.callback){
			if(fm.callback(files)){
				fm.hide();
				//window.hzh.alert("添加成功！");
			}
		}
	};
	window.onFileSelected=onFileSelected;
}(window, $))