/**
 *	transo.js by Stone Chow ,
 *   2015-03-09
 *	the core lib of transo js
 *
 */
/*************jStorage start*********************/
"use strict";
(function() {
	function C() {
		var a = "{}";
		if ("userDataBehavior" == f) {
			g.load("jStorage");
			try {
				a = g.getAttribute("jStorage")
			} catch (b) {}
			try {
				r = g.getAttribute("jStorage_update")
			} catch (c) {}
			h.jStorage = a
		}
		D();
		x();
		E()
	}

	function u() {
		var a;
		clearTimeout(F);
		F = setTimeout(function() {
			if ("localStorage" == f || "globalStorage" == f) a = h.jStorage_update;
			else if ("userDataBehavior" == f) {
				g.load("jStorage");
				try {
					a = g.getAttribute("jStorage_update")
				} catch (b) {}
			}
			if (a && a != r) {
				r = a;
				var l = p.parse(p.stringify(c.__jstorage_meta.CRC32)),
					k;
				C();
				k = p.parse(p.stringify(c.__jstorage_meta.CRC32));
				var d, n = [],
					e = [];
				for (d in l) l.hasOwnProperty(d) && (k[d] ? l[d] != k[d] && "2." == String(l[d]).substr(0, 2) && n.push(d) : e.push(d));
				for (d in k) k.hasOwnProperty(d) && (l[d] || n.push(d));
				s(n, "updated");
				s(e, "deleted")
			}
		}, 25)
	}

	function s(a, b) {
		a = [].concat(a || []);
		var c, k, d, n;
		if ("flushed" == b) {
			a = [];
			for (c in m) m.hasOwnProperty(c) && a.push(c);
			b = "deleted"
		}
		c = 0;
		for (d = a.length; c < d; c++) {
			if (m[a[c]])
				for (k = 0, n = m[a[c]].length; k < n; k++) m[a[c]][k](a[c], b);
			if (m["*"])
				for (k = 0, n = m["*"].length; k < n; k++) m["*"][k](a[c], b)
		}
	}

	function v() {
		var a = (+new Date).toString();
		if ("localStorage" == f || "globalStorage" == f) try {
			h.jStorage_update = a
		} catch (b) {
			f = !1
		} else "userDataBehavior" == f && (g.setAttribute("jStorage_update", a), g.save("jStorage"));
		u()
	}

	function D() {
		if (h.jStorage) try {
			c = p.parse(String(h.jStorage))
		} catch (a) {
			h.jStorage = "{}"
		} else h.jStorage = "{}";
		z = h.jStorage ? String(h.jStorage).length : 0;
		c.__jstorage_meta || (c.__jstorage_meta = {});
		c.__jstorage_meta.CRC32 || (c.__jstorage_meta.CRC32 = {})
	}

	function w() {
		if (c.__jstorage_meta.PubSub) {
			for (var a = +new Date - 2E3, b = 0, l = c.__jstorage_meta.PubSub.length; b < l; b++)
				if (c.__jstorage_meta.PubSub[b][0] <= a) {
					c.__jstorage_meta.PubSub.splice(b, c.__jstorage_meta.PubSub.length - b);
					break
				}
			c.__jstorage_meta.PubSub.length || delete c.__jstorage_meta.PubSub
		}
		try {
			h.jStorage = p.stringify(c), g && (g.setAttribute("jStorage", h.jStorage), g.save("jStorage")), z = h.jStorage ? String(h.jStorage).length : 0
		} catch (k) {}
	}

	function q(a) {
		if ("string" != typeof a && "number" != typeof a) throw new TypeError("Key name must be string or numeric");
		if ("__jstorage_meta" == a) throw new TypeError("Reserved key name");
		return !0
	}

	function x() {
		var a, b, l, k, d = Infinity,
			n = !1,
			e = [];
		clearTimeout(G);
		if (c.__jstorage_meta && "object" == typeof c.__jstorage_meta.TTL) {
			a = +new Date;
			l = c.__jstorage_meta.TTL;
			k = c.__jstorage_meta.CRC32;
			for (b in l) l.hasOwnProperty(b) && (l[b] <= a ? (delete l[b], delete k[b], delete c[b], n = !0, e.push(b)) : l[b] < d && (d = l[b]));
			Infinity != d && (G = setTimeout(x, Math.min(d - a, 2147483647)));
			n && (w(), v(), s(e, "deleted"))
		}
	}

	function E() {
		var a;
		if (c.__jstorage_meta.PubSub) {
			var b, l = A,
				k = [];
			for (a = c.__jstorage_meta.PubSub.length - 1; 0 <= a; a--) b =
				c.__jstorage_meta.PubSub[a], b[0] > A && (l = b[0], k.unshift(b));
			for (a = k.length - 1; 0 <= a; a--) {
				b = k[a][1];
				var d = k[a][2];
				if (t[b])
					for (var n = 0, e = t[b].length; n < e; n++) try {
						t[b][n](b, p.parse(p.stringify(d)))
					} catch (g) {}
			}
			A = l
		}
	}
	var y = window.jQuery || window.$ || (window.$ = {}),
		p = {
			parse: window.JSON && (window.JSON.parse || window.JSON.decode) || String.prototype.evalJSON && function(a) {
				return String(a).evalJSON()
			} || y.parseJSON || y.evalJSON,
			stringify: Object.toJSON || window.JSON && (window.JSON.stringify || window.JSON.encode) || y.toJSON
		};
	if ("function" !==
		typeof p.parse || "function" !== typeof p.stringify) throw Error("No JSON support found, include //cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.js to page");
	var c = {
			__jstorage_meta: {
				CRC32: {}
			}
		},
		h = {
			jStorage: "{}"
		},
		g = null,
		z = 0,
		f = !1,
		m = {},
		F = !1,
		r = 0,
		t = {},
		A = +new Date,
		G, B = {
			isXML: function(a) {
				return (a = (a ? a.ownerDocument || a : 0).documentElement) ? "HTML" !== a.nodeName : !1
			},
			encode: function(a) {
				if (!this.isXML(a)) return !1;
				try {
					return (new XMLSerializer).serializeToString(a)
				} catch (b) {
					try {
						return a.xml
					} catch (c) {}
				}
				return !1
			},
			decode: function(a) {
				var b = "DOMParser" in window && (new DOMParser).parseFromString || window.ActiveXObject && function(a) {
					var b = new ActiveXObject("Microsoft.XMLDOM");
					b.async = "false";
					b.loadXML(a);
					return b
				};
				if (!b) return !1;
				a = b.call("DOMParser" in window && new DOMParser || window, a, "text/xml");
				return this.isXML(a) ? a : !1
			}
		};
	y.jStorage = {
		version: "0.4.12",
		set: function(a, b, l) {
			q(a);
			l = l || {};
			if ("undefined" == typeof b) return this.deleteKey(a), b;
			if (B.isXML(b)) b = {
				_is_xml: !0,
				xml: B.encode(b)
			};
			else {
				if ("function" == typeof b) return;
				b && "object" == typeof b && (b = p.parse(p.stringify(b)))
			}
			c[a] = b;
			for (var k = c.__jstorage_meta.CRC32, d = p.stringify(b), g = d.length, e = 2538058380 ^ g, h = 0, f; 4 <= g;) f = d.charCodeAt(h) & 255 | (d.charCodeAt(++h) & 255) << 8 | (d.charCodeAt(++h) & 255) << 16 | (d.charCodeAt(++h) & 255) << 24, f = 1540483477 * (f & 65535) + ((1540483477 * (f >>> 16) & 65535) << 16), f ^= f >>> 24, f = 1540483477 * (f & 65535) + ((1540483477 * (f >>> 16) & 65535) << 16), e = 1540483477 * (e & 65535) + ((1540483477 * (e >>> 16) & 65535) << 16) ^ f, g -= 4, ++h;
			switch (g) {
				case 3:
					e ^= (d.charCodeAt(h + 2) & 255) << 16;
				case 2:
					e ^= (d.charCodeAt(h + 1) & 255) << 8;
				case 1:
					e ^= d.charCodeAt(h) & 255, e = 1540483477 * (e & 65535) + ((1540483477 * (e >>> 16) & 65535) << 16)
			}
			e ^= e >>> 13;
			e = 1540483477 * (e & 65535) + ((1540483477 * (e >>> 16) & 65535) << 16);
			k[a] = "2." + ((e ^ e >>> 15) >>> 0);
			this.setTTL(a, l.TTL || 0);
			s(a, "updated");
			return b
		},
		get: function(a, b) {
			q(a);
			return a in c ? c[a] && "object" == typeof c[a] && c[a]._is_xml ? B.decode(c[a].xml) : c[a] : "undefined" == typeof b ? null : b
		},
		deleteKey: function(a) {
			q(a);
			return a in c ? (delete c[a], "object" == typeof c.__jstorage_meta.TTL && a in c.__jstorage_meta.TTL &&
				delete c.__jstorage_meta.TTL[a], delete c.__jstorage_meta.CRC32[a], w(), v(), s(a, "deleted"), !0) : !1
		},
		setTTL: function(a, b) {
			var l = +new Date;
			q(a);
			b = Number(b) || 0;
			return a in c ? (c.__jstorage_meta.TTL || (c.__jstorage_meta.TTL = {}), 0 < b ? c.__jstorage_meta.TTL[a] = l + b : delete c.__jstorage_meta.TTL[a], w(), x(), v(), !0) : !1
		},
		getTTL: function(a) {
			var b = +new Date;
			q(a);
			return a in c && c.__jstorage_meta.TTL && c.__jstorage_meta.TTL[a] ? (a = c.__jstorage_meta.TTL[a] - b) || 0 : 0
		},
		flush: function() {
			c = {
				__jstorage_meta: {
					CRC32: {}
				}
			};
			w();
			v();
			s(null,
				"flushed");
			return !0
		},
		storageObj: function() {
			function a() {}
			a.prototype = c;
			return new a
		},
		index: function() {
			var a = [],
				b;
			for (b in c) c.hasOwnProperty(b) && "__jstorage_meta" != b && a.push(b);
			return a
		},
		storageSize: function() {
			return z
		},
		currentBackend: function() {
			return f
		},
		storageAvailable: function() {
			return !!f
		},
		listenKeyChange: function(a, b) {
			q(a);
			m[a] || (m[a] = []);
			m[a].push(b)
		},
		stopListening: function(a, b) {
			q(a);
			if (m[a])
				if (b)
					for (var c = m[a].length - 1; 0 <= c; c--) m[a][c] == b && m[a].splice(c, 1);
				else delete m[a]
		},
		subscribe: function(a,b) {
			a = (a || "").toString();
			if (!a) throw new TypeError("Channel not defined");
			t[a] || (t[a] = []);
			t[a].push(b)
		},
		publish: function(a, b) {
			a = (a || "").toString();
			if (!a) throw new TypeError("Channel not defined");
			c.__jstorage_meta || (c.__jstorage_meta = {});
			c.__jstorage_meta.PubSub || (c.__jstorage_meta.PubSub = []);
			c.__jstorage_meta.PubSub.unshift([+new Date, a, b]);
			w();
			v()
		},
		reInit: function() {
			C()
		},
		noConflict: function(a) {
			delete window.$.jStorage;
			a && (window.jStorage = this);
			return this
		}
	};
	(function() {
		var a = !1;
		if ("localStorage" in
			window) try {
			window.localStorage.setItem("_tmptest", "tmpval"), a = !0, window.localStorage.removeItem("_tmptest")
		} catch (b) {}
		if (a) try {
			window.localStorage && (h = window.localStorage, f = "localStorage", r = h.jStorage_update)
		} catch (c) {} else if ("globalStorage" in window) try {
				window.globalStorage && (h = "localhost" == window.location.hostname ? window.globalStorage["localhost.localdomain"] : window.globalStorage[window.location.hostname], f = "globalStorage", r = h.jStorage_update)
			} catch (k) {} else if (g = document.createElement("link"),
				g.addBehavior) {
				g.style.behavior = "url(#default#userData)";
				document.getElementsByTagName("head")[0].appendChild(g);
				try {
					g.load("jStorage")
				} catch (d) {
					g.setAttribute("jStorage", "{}"), g.save("jStorage"), g.load("jStorage")
				}
				a = "{}";
				try {
					a = g.getAttribute("jStorage")
				} catch (m) {}
				try {
					r = g.getAttribute("jStorage_update")
				} catch (e) {}
				h.jStorage = a;
				f = "userDataBehavior"
			} else {
				g = null;
				return
			}
		D();
		x();
		"localStorage" == f || "globalStorage" == f ? "addEventListener" in window ? window.addEventListener("storage", u, !1) : document.attachEvent("onstorage",
			u) : "userDataBehavior" == f && setInterval(u, 1E3);
		E();
		"addEventListener" in window && window.addEventListener("pageshow", function(a) {
			a.persisted && u()
		}, !1)
	})()
})();
/*************jStorate end**********************/
(function(window, $) {
	window.ISDEBUG = true;
	$.jStorage.reInit();
	var consts = {
		CODE_TIME_OUT: 509,
		CODE_OK: 200,
		CODE_LOGINNEEDED: 510,
		CODE_ACCESSDENIED: 511,
		EVENT_RESPONSE_LOGIN: 0x04010001,
		KEY_HZH_TOKEN: "hzh.token",
		KEY_HZH_SIGN: "hzh.sign",
		TOKEN_EXPIRATION: 1000 * 60 * 20,
		KEY_HZH_USERDATA: "hzh.userdata",
		KEY_HZH_PEERID: "hzh.peer.id",
		VERSION: "1.0a"
	};
    var base64DecodeChars = new Array(
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
        52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
        -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
        15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
        -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
        41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1);
	var IDGenerator = function() {
		this.current = 0;
		return this;
	};
	IDGenerator.prototype.generate = function() {
		//this.current=this.current+1;
		if (this.current >= 1000) {
			this.current = 0;
		}
		return new Date().getTime() * 1000 + this.current++;
		//return ret;
	};

	function json2str(o) {
		var arr = [];
		var fmt = function(s) {
			if (typeof s == 'object' && s != null) return json2str(s);
			return /^(string|number)$/.test(typeof s) ? "\"" + s + "\"" : s;
		}
		for (var i in o) arr.push("\"" + i + "\":" + fmt(o[i]));
		return '{' + arr.join(',') + '}';
	};

	function string2json(str) {
		return eval("(" + str + ")");
	};

	function deepCopy(source, dest) {
		var result = dest || {};
		for (var key in source) {
			result[key] = typeof source[key] === 'object' ? deepCopy(source[key]) : source[key];
		}
		return result;
	};
	var HZH = {
		/*
		 * 构造函数的继承
		 */
		extend: function(child, parent, proto) {//继承
			var F = function() {};//F对象作为中介，当修改child时也不会影响parent的prototype对象
			F.prototype = parent.prototype;
			child.prototype = new F();//继承父级
			child.prototype.constructor = child;//修改指向
			child.uber = parent.prototype;
		if (proto) {
				this.copy(child.prototype, proto);
			}
			return child;
		},
		/*
		 * 非构造函数的继承 (深拷贝)
		 */
		copy: function(dest, src, deep) { //copy all properties from src to dest,
			var dest = dest || {};
			for (var i in src) {
				if (deep && typeof src[i] === 'object') {
					dest[i] = (src[i].constructor === Array) ? [] : {};
					deepCopy(src[i], dest[i]);
				} else {
					dest[i] = src[i];
				}
			}
			return dest;
		},
		idGenerator: new IDGenerator()
	};
	var HZHCoder = function() {
		return this;
	};
	HZH.copy(HZHCoder.prototype, {
		_type: -1,
		write: function(output) {
			throw new Error("not implemented");
		},
		read: function(input) {
			throw new Error("not implemented");
		}
	});
	var HZHWrapper = HZH.extend(function(value) {
		this.value = value;
		return this;
	}, HZHCoder, {
		value: null
	});
	var MODEL = {
		HZHCoder: HZHCoder,
		HZHEvent: HZH.extend(function(type, data, token) {
			this._type = 1000;
			this.id = HZH.idGenerator.generate();
			this.type = type;
			this.data = data;
			if (token)
				this.token = token;
			this.source = "";
			this.timestamp = 0;
			return this;
		}, HZHCoder, {
			id: 0,
			type: 0,
			data: null,
			token: null,
			source: null,
			timestamp: null,
			write: function(output) {
				output.writeInt(this._type);
				output.writeLong(this.id);
				output.writeInt(this.type);
				output.writeString(this.source);
				output.writeObject(this.data);
				output.writeString("");
				output.writeString("");
				output.writeString("");
				output.writeString(this.token);
				output.writeLong(this.timestamp);
			},
			read: function(input) {
				this.id = input.readLong(input);
				this.type = input.readInt(input);
				this.source = input.readString(input);
				this.data = input.readObject(input);
				this.hash = input.readString(input);
				this.route = input.readString(input);
				this.dest = input.readString(input);
				this.token = input.readString(input);
				this.timestamp = input.readLong(input);
			}
        }),
		HZHPeer: HZH.extend(function(id, os, version, timeout) {
			this._type = 1001;
			this.id = id;
			this.os = os;
			this.version = version;
			this.timeout = timeout;
			this.timestamp = new Date().getTime();
			return this;
		}, HZHCoder, {
			id: 0,
			os: "",
			version: "",
			timeout: 0,
			timestamp: 0,
			write: function(output) {
				output.writeInt(this._type);
				output.writeString(this.id);
				output.writeString(this.os);
				output.writeString(this.version);
				output.writeLong(this.timestamp);
				output.writeInt(this.timeout);
			},
			read: function(input) {
				this.id = input.readString(input);
				this.os = input.readString(input);
				this.version = input.readString(input);
				this.timestamp = input.readLong(input);
				this.timeout = input.readInt(input);
			}
		}),
		HZHString: HZH.extend(function(value) {
			this._type = 2001;
			this.value = value;
			return this;
		}, HZHWrapper, {
			value: null,
			write: function(output) {
				output.writeInt(this._type);
				output.writeString(this.value);
			},
			read: function(input) {
				this.value = input.readString(input);
			}
		}),

		HZHInt: HZH.extend(function(value) {
			this._type = 2002;
			this.value = value;
			return this;
		}, HZHWrapper, {
			value: null,
			write: function(output) {
				output.writeInt(this._type);
				output.writeInt(this.value);
			},
			read: function(input) {
				this.value = input.readInt(input);
			}
		}),
		HZHLong: HZH.extend(function(value) {
			this._type = 2003;
			this.value = value;
			return this;
		}, HZHWrapper, {
			value: null,
			write: function(output) {
				output.writeInt(this._type);
				output.writeLong(this.value);
			},
			read: function(input) {
				this.value = input.readLong(input);
			}
		}),
		HZHFloat: HZH.extend(function(value) {
			this._type = 2004;
			this.value = value;
			return this;
		}, HZHWrapper, {
			value: null,
			write: function(output) {
				output.writeInt(this._type);
				output.writeFloat(this.value);
			},
			read: function(input) {
				this.value = input.readFloat(input);
			}
		}),
		HZHDouble: HZH.extend(function(value) {
			this._type = 2005;
			this.value = value;
			return this;
		}, HZHWrapper, {
			value: null,
			write: function(output) {
				output.writeInt(this._type);
				output.writeDouble(this.value);
			},
			read: function(input) {
				this.value = input.readDouble(input);
			}
		}),
		HZHByte: HZH.extend(function(value) {
			this._type = 2006;
			this.value = value;
			return this;
		}, HZHWrapper, {
			value: null,
			write: function(output) {
				output.writeInt(this._type);
				output.writeByte(this.value);
			},
			read: function(input) {
				this.value = input.readByte(input);
			}
		}),
		HZHArray: HZH.extend(function(value) {
			this._type = 2007;
			this.value = value;
			return this;
		}, HZHWrapper, {
			value: null,
			write: function(output) {
				output.writeInt(this._type);
				var val = this.value;
				if (val && val.length > 0) {
					output.writeInt(val.length);
					for (var i in val) {
						//val[i].write(output);
						output.writeObject(val[i]);
					}
				} else {
					output.writeInt(0);
				}
			},
			read: function(input) {
				var size = input.readInt();
				var arr = [];
				while (size > 0) {
					arr.push(input.readObject());
					size--;
				}
				this.value = arr;
			}
		}),
		HZHMap: HZH.extend(function(value) {
			this._type = 2008;
			this.value = value;
			return this;
		}, HZHWrapper, {
			value: null,
			write: function(output) {
				output.writeInt(this._type);
				var keys = [];
				for (var key in this.value) {
					if (key != "_type") {
						keys.push(key);
					}
				}
				output.writeInt(keys.length);
				for (var i in keys) {
					var key = keys[i];
					output.writeString(key);
					//this.value[i].write(output);
					output.writeObject(this.value[key]);
				}
			},
			read: function(input) {
				var size = input.readInt();
				var val = {};
				while (size > 0) {
					val[input.readString()] = input.readObject();
					size--;
				}
				this.value = val;
			}
		}),
		HZHByteArray: HZH.extend(function(value) {
			this._type = 2009;
			this.value = value;
			return this;
		}, HZHWrapper, {
			value: null,
			write: function(output) {
				output.writeInt(this._type);
				output.writeBytes(this.value);
			},
			read: function(input) {
				this.value = input.readBytes(input);
			}
		}),
		HZHRequest: HZH.extend(function(value, requestType) {
			this._type = 3001;
			this.id = HZH.idGenerator.generate();
			this.type = requestType;
			this.data = value;
			this.tag = null;
			return this;
		}, HZHCoder, {
			id: 0,
			type: null,
			data: null,
			tag: null,
			write: function(output) {
				output.writeInt(this._type);
				output.writeLong(this.id);
				output.writeString(this.type);
				output.writeObject(this.data);
				output.writeObject(this.tag);
			},
			read: function(input) {
				this.id = input.readLong(input);
				this.type = input.readString(input);
				this.data = input.readObject(input);
				this.tag = input.readObject(input);
			}
		}),
		HZHResponse: HZH.extend(function(data, code, requestId) {
			this._type = 3002;
			this.data = data;
			this.code = code;
			this.requestId = requestId;
			return this;
		}, HZHCoder, {
			id: 0,
			data: null,
			tag: null,
			code: 0,
			requestId: 0,
			write: function(output) {
				output.writeInt(this._type);
				output.writeLong(this.id);
				output.writeLong(this.requestId);
				output.writeInt(this.code);
				output.writeObject(this.data);
				output.writeObject(this.tag);
			},
			read: function(input) {
				this.id = input.readLong(input);
				this.requestId = input.readLong(input);
				this.code = input.readInt(input);
				this.data = input.readObject(input);
				this.tag = input.readObject(input);
			}
		}),
		HZHNotification: HZH.extend(function(value) {
			this._type = 3003;
			this.data = value;
			return this;
		}, HZHCoder, {
			id: 0,
			data: null,
			tag: null,
			write: function(output) {
				output.writeInt(this._type);
				output.writeLong(this.id);
				output.writeObject(this.data);
				output.writeObject(this.tag);
			},
			read: function(input) {
				this.id = input.readLong(input);
				this.data = input.readObject(input);
				this.tag = input.readObject(input);
			}
		})


	};
	/***********************************************/

	/**************HZH Model Wrapper*********************************/
	(function(hzh) {
		var hzhmodelwrapper = function() {
			return this;
		};

		function wrapObject(obj) {
			if(!obj)
				return obj;
			
			if (obj._type) { //already a hzh model
				/*				for(var i in obj){
									if(i != "_type"){
										var val=obj[i];
										if(val instanceof Function == false){
											obj[i]=wrapObject(val);
										}
									}
								}*/
				if(obj._type == 2008 && obj.value){//if it's an HZHMap ,try check it's all fields 
					var val=obj.value;
					for (var key in val) {
						if(typeof val[key] == "object")
							val[key] = wrapObject(val[key]);
					}
				}
				return obj;
			}
			if (obj instanceof Array) {
				return wrapArray(obj);
			} else if (obj instanceof Object || typeof obj == "object") {
				return wrapHZHMap(obj);
			} else {
				return new hzh.models.HZHString(obj.toString());
			}

		};

		function wrapHZHMap(obj) {
			var val = {};
			for (var key in obj) {
				val[key] = wrapObject(obj[key]);
			}
			return new hzh.models.HZHMap(val);
		};

		function wrapArray(obj) {
			var arr = [];
			for (var i in obj) {
				arr.push(wrapObject(obj[i]));
			}
			return new hzh.models.HZHArray(arr);
		};

		function unwrapHZHModel(model) {
			if (model && model._type) {
				return unwrapObject(model);
			}
			return model
		};

		function isOmitProperty(val) {
			return val instanceof Function;
		}

		function unwrapObject(model) {
			if (model instanceof MODEL.HZHMap) {
				var ret = {};
				for (var key in model.value) {
					var val = model.value[key];
					if (isOmitProperty(val) == false)
						ret[key] = unwrapHZHModel(val);
				}
				return ret;
			} else if (model instanceof MODEL.HZHArray) {
				var ret = [];
				for (var i in model.value) {
					ret.push(unwrapHZHModel(model.value[i]));
				}
				return ret;
			} else if (model instanceof HZHWrapper) {
				return model.value;
			}
			var ret = {};
			for (var key in model) {
				var val = model[key];
				if (isOmitProperty(val) == false)
					ret[key] = unwrapHZHModel(val);
			}
			return ret;

		};
		hzh.copy(hzhmodelwrapper.prototype, {
			wrap: function(obj) {
				return wrapObject(obj);
			},
			unwrap: function(model) {
				return unwrapHZHModel(model);
			},
		});
		hzh.HZHModelWrapper = new hzhmodelwrapper();
	})(HZH);

	/**************HZH Model Wrapper end********************************/

	/**************file reader*******************/
	function BuildFileReader() {
		/*		var reader;
				var files;
				var index = 0;
				var callback = null;
				var results = [];
		*/
		function init() {
			//reader=new FileReader();
			//reader.onload=readeronload();
		};

		function readeronload(result, freader, file) {
			if (!result)
				return;
			var data = {
				result: processResult(result),
				file: file
			};
			freader.results.push(data);
			//freader.index++;
			if (freader.hasNext() == false) {
				freader.doCallback();
				//results=[];
				//reader=null;
			} else {
				doRead(freader);
			}
		}

		function processResult(input) {
			if (input) {
				/*return input.split("base64,")[1];*/
				return new Uint8Array(input);
			}
			return input;
		}

		function doRead(freader) {
			var reader = new FileReader();
			var f = freader.nextFile();
			reader.onload = function() {
				readeronload(this.result, freader, f);
			};
			reader.readAsArrayBuffer(f);

		}
		var fileReader = function() {
			init();
			return this;
		};
		HZH.copy(fileReader.prototype, {
			files: null,
			index: 0,
			results: [],
			callback: null,
			read: function(fls, cb) {
				this.files = fls;
				this.index = 0;
				this.results = [];
				this.callback = cb;
				doRead(this);
			},
			nextFile: function() {
				var f = this.files[this.index];
				this.index++;
				return f;
			},
			hasNext: function() {
				return this.files.length > this.index;
			},
			doCallback: function() {
				if (this.callback) {
					this.callback(this.results);
				}
			}

		});
		return fileReader;
	}
	HZH.FileReader = BuildFileReader();

	/**************file reader end*****************************/
	/**************BytesHelper********************************/

	(function(hzh) {
		function decodeFloat(bytes, signBits, exponentBits, fractionBits, eMin, eMax, littleEndian) {
			var totalBits = (signBits + exponentBits + fractionBits);

			var binary = "";
			for (var i = 0, l = bytes.length; i < l; i++) {
				var bits = bytes[i].toString(2);
				while (bits.length < 8)
					bits = "0" + bits;

				if (littleEndian)
					binary = bits + binary;
				else
					binary += bits;
			}

			var sign = (binary.charAt(0) == '1') ? -1 : 1;
			var exponent = parseInt(binary.substr(signBits, exponentBits), 2) - eMax;
			var significandBase = binary.substr(signBits + exponentBits, fractionBits);
			var significandBin = '1' + significandBase;
			var i = 0;
			var val = 1;
			var significand = 0;

			if (exponent == -eMax) {
				if (significandBase.indexOf('1') == -1)
					return 0;
				else {
					exponent = eMin;
					significandBin = '0' + significandBase;
				}
			}

			while (i < significandBin.length) {
				significand += val * parseInt(significandBin.charAt(i));
				val = val / 2;
				i++;
			}

			return sign * significand * Math.pow(2, exponent);
		};

		function int2bytes(value, bytes) {
			var ret = [];
			bytes = bytes || 4;
			for (var k = 0; k < bytes; k++) {
				ret[bytes - 1 - k] = value & (255);
				value = value / 256;
			}
			return ret;
		};

		function bytes2int(bytes) {
			var l = bytes.length;
			var ret = 0;
			for (var k = 0; k < l; k++) {
				ret *= 256;
				ret += bytes[k];
			}
			return ret;
		}

		function toFloat64(value) {

			var hiWord = 0,
				loWord = 0;
			switch (value) {
				case Number.POSITIVE_INFINITY:
					hiWord = 0x7FF00000;
					break;
				case Number.NEGATIVE_INFINITY:
					hiWord = 0xFFF00000;
					break;
				case +0.0:
					hiWord = 0x40000000;
					break;
				case -0.0:
					hiWord = 0xC0000000;
					break;
				default:
					if (Number.isNaN(value)) {
						hiWord = 0x7FF80000;
						break;
					}

					if (value <= -0.0) {
						hiWord = 0x80000000;
						value = -value;
					}

					var exponent = Math.floor(Math.log(value) / Math.log(2));
					var significand = Math.floor((value / Math.pow(2, exponent)) * Math.pow(2, 52));

					loWord = significand & 0xFFFFFFFF;
					significand /= Math.pow(2, 32);

					exponent += 1023;
					if (exponent >= 0x7FF) {
						exponent = 0x7FF;
						significand = 0;
					} else if (exponent < 0) exponent = 0;

					hiWord = hiWord | (exponent << 20);
					hiWord = hiWord | (significand & ~(-1 << 20));
					break;
			}

			return int2bytes(hiWord).concat(int2bytes(loWord));
		};

		function toFloat32(value) {
			var bytes = 0;
			switch (value) {
				case Number.POSITIVE_INFINITY:
					bytes = 0x7F800000;
					break;
				case Number.NEGATIVE_INFINITY:
					bytes = 0xFF800000;
					break;
				case +0.0:
					bytes = 0x40000000;
					break;
				case -0.0:
					bytes = 0xC0000000;
					break;
				default:
					if (Number.isNaN(value)) {
						bytes = 0x7FC00000;
						break;
					}

					if (value <= -0.0) {
						bytes = 0x80000000;
						value = -value;
					}

					var exponent = Math.floor(Math.log(value) / Math.log(2));
					var significand = ((value / Math.pow(2, exponent)) * 0x00800000) | 0;

					exponent += 127;
					if (exponent >= 0xFF) {
						exponent = 0xFF;
						significand = 0;
					} else if (exponent < 0) exponent = 0;

					bytes = bytes | (exponent << 23);
					bytes = bytes | (significand & ~(-1 << 23));
					break;
			}
			return int2bytes(bytes);
		};

		function arraySlice(arr, start, end) {
			if (arr.slice) {
				return arr.slice(start, end);
			} else {
				return arr.subarray(start, end);
			}
		};
		var binaryParser = function(littleEndian) {
			this.bigEndian = !littleEndian;
			return this;
		};
		hzh.copy(binaryParser.prototype, {
			bigEndian: true,
			toInt: function(bytes) {
				return bytes2int(this.bigEndian ? bytes : bytes.reverse());
			},
			fromInt: function(number) {
				var ret = int2bytes(number);
				return this.bigEndian ? ret : ret.reverse();
			},
			toLong: function(bytes) {
				return bytes2int(this.bigEndian ? bytes : bytes.reverse());
			},
			fromLong: function(number) {
				var ret = int2bytes(number, 8);
				return this.bigEndian ? ret : ret.reverse();
			},
			toFloat: function(bytes) {
				return decodeFloat(bytes, 1, 8, 23, -126, 127, !this.bigEndian);
			},
			fromFloat: function(number) {
				var ret = toFloat32(number);
				return this.bigEndian ? ret : ret.reverse();
			},
			toDouble: function(bytes) {
				return decodeFloat(bytes, 1, 11, 52, -1022, 1023, !this.bigEndian);
			},
			fromDouble: function(number) {
				var ret = toFloat64(number);
				return this.bigEndian ? ret : ret.reverse();
			},
			fromString: function(str) {
				var bytes = [],
					char;
				str = encodeURI(str);

				while (str.length) {
					char = str.slice(0, 1);
					str = str.slice(1);

					if ('%' !== char) {
						bytes.push(char.charCodeAt(0));
					} else {
						char = str.slice(0, 2);
						str = str.slice(2);

						bytes.push(parseInt(char, 16));
					}
				}

				return bytes;
			},
			toString: function(bytes) {
				var data;
				if (bytes instanceof Uint8Array) {
					data = bytes;
				} else {
					data = new Uint8Array(bytes);
				}
				var result = "";
				var i = 0;
				var c = 0;
				var c3 = 0;
				var c2 = 0;


				// If we have a BOM skip it
				if (data.length >= 3 && data[0] === 0xef && data[1] === 0xbb && data[2] === 0xbf) {
					i = 3;
				}

				while (i < data.length) {
					c = data[i];

					if (c < 128) {
						result += String.fromCharCode(c);
						i++;
					} else if (c > 191 && c < 224) {
						if (i + 1 >= data.length) {
							throw "UTF-8 Decode failed. Two byte character was truncated.";
						}
						c2 = data[i + 1];
						result += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
						i += 2;
					} else {
						if (i + 2 >= data.length) {
							throw "UTF-8 Decode failed. Multi byte character was truncated.";
						}
						c2 = data[i + 1];
						c3 = data[i + 2];
						result += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
						i += 3;
					}
				}
				return result;

			}
		});
		hzh.BinaryParser = binaryParser;
	})(HZH);
	var binaryParser = new HZH.BinaryParser();
	/**************BytesHelper end********************************/
	/**************HZHInput*********************************/
	function createCoder(type) {
		switch (type) {
			case 1000:
				return new MODEL.HZHEvent();
			case 1001:
				return new MODEL.HZHPeer();
			case 2001:
				return new MODEL.HZHString();
			case 2002:
				return new MODEL.HZHInt();
			case 2003:
				return new MODEL.HZHLong();
			case 2004:
				return new MODEL.HZHFloat();
			case 2005:
				return new MODEL.HZHDouble();
			case 2006:
				return new MODEL.HZHByte();
			case 2007:
				return new MODEL.HZHArray();
			case 2008:
				return new MODEL.HZHMap();
			case 2009:
				return new MODEL.HZHByteArray();
			case 3001:
				return new MODEL.HZHRequest();
			case 3002:
				return new MODEL.HZHResponse();
			case 3003:
				return new MODEL.HZHNotification();
		}
		throw new Error("unsupportted type:" + type)
	};
	var hzhinput = function() {
		return this;
	};
	HZH.copy(hzhinput.prototype, {
		buffer: null,
		position: 0,
		reset: function(uint8array) {
			this.buffer = new Uint8Array(uint8array);
			this.position = 0;
		},
		readBytes: function(length) {
			var buf = this.buffer.subarray(this.position, this.position + length);
			this.position = this.position + length;
			return buf;
		},
		readInt: function() {
			return binaryParser.toInt(this.readBytes(4));
		},
		readLong: function() {
			return binaryParser.toLong(this.readBytes(8));
		},
		readFloat: function() {
			return binaryParser.toFloat(this.readBytes(4));
		},
		readDouble: function() {
			return binaryParser.toDouble(this.readBytes(8));
		},
		readByte: function() {
			return this.readBytes(1)[0];
		},
		readBoolean: function() {
			return this.readByte() == 1;
		},
		readString: function() {
			var size = this.readInt();
			var bytes = this.readBytes(size);
			return binaryParser.toString(bytes);
		},
		readObject: function() {
			var type = this.readInt();
			if (type == 0)
				return null;
			var obj = createCoder(type);
			obj.read(this);
			return obj;
		}
	});
	/**************HZHInput end*********************************/
	/**************HZHOutput*********************************/

	var hzhoutput = function() {
		return this;
	};
	HZH.copy(hzhoutput.prototype, {
		array: [],
		writeInt: function(value) {
			this.writePureBytes(binaryParser.fromInt(value));
		},
		writeLong: function(value) {
			this.writePureBytes(binaryParser.fromLong(value));
		},
		writeFloat: function(value) {
			this.writePureBytes(binaryParser.fromFloat(value));
		},
		writeDouble: function(value) {
			this.writePureBytes(binaryParser.fromDouble(value));
		},
		writePureBytes: function(bytes) {
			if (bytes instanceof Uint8Array) {
				for (var i in bytes) {
					this.array.push(bytes[i]);
				}
			} else {
				this.array = this.array.concat(bytes);
			}
		},
		writeByte: function(byte) {
			this.writePureBytes([byte && 0xFF]);
		},
		writeBytes: function(bytes) {
			if (bytes && bytes.length > 0) {
				this.writeInt(bytes.length);
				this.writePureBytes(bytes);
			} else {
				this.writeInt(0);
			}
		},
		writeString: function(value) {
			if (value) {
				this.writeBytes(binaryParser.fromString(value));

			} else {
				this.writeBytes(null);
			}
		},
		writeObject: function(value) {
            //console.log(value);
			if ( value ) {
				value = hzh.HZHModelWrapper.wrap(value);
				value.write(this);
                //console.log("aaa");
			} else {
                this.writeInt(0);
                //console.log("bbb");
            }
		},
		writeBoolean: function(value) {
			this.writeByte(value ? 1 : 0);
		},
		flush: function() {
			this.array = [];
		}

	});
	HZH.Output = hzhoutput;
	/**************HZHOutput end*********************************/
	/**************Connector***********************************************/
	function buildConnector() {
			var Connector = function(args) {
				if (args.peer) {
					this.localPeer = new hzh.models.HZHPeer(peer.id, peer.os, peer.version, peer.timeout);
				} else {
					this.localPeer = new hzh.models.HZHPeer(getPeerId(), navigator.userAgent, consts.VERSION, 10);
				}
				if (args.listener) {
					HZH.copy(this.listener, args.listener, true);
				}
				this.connect(args.uri);
				return this;
			};

			function getPeerId() {
				var id = $.jStorage.get(consts.KEY_HZH_PEERID);
				if (!id) {
					id = uuid(16, 16);
					$.jStorage.set(consts.KEY_HZH_PEERID, id);
				}
				return id;
			};

			function uuid(len, radix) {
				var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
				var uuid = [],
					i;
				radix = radix || chars.length;

				if (len) {
					// Compact form
					for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random() * radix];
				} else {
					// rfc4122, version 4 form
					var r;

					// rfc4122 requires these characters
					uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
					uuid[14] = '4';

					// Fill in random data.  At i==19 set the high bits of clock sequence as
					// per rfc4122, sec. 4.1.5
					for (i = 0; i < 36; i++) {
						if (!uuid[i]) {
							r = 0 | Math.random() * 16;
							uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
						}
					}
				}

				return uuid.join('');
			};

			function stringToUint(string, block) {
				//var string = btoa(unescape(encodeURIComponent(string))),
				//	var string=unescape(encodeURIComponent(string));
				var charList = string.split(''),
					uintArray = [];
				var ret = [];
				var idx = 0;
				var length = charList.length;
				var blockSize = block > length ? length : block;
				while (idx < length) {
					var i = 0;
					for (; i < blockSize; i++) {
						uintArray.push(charList[idx + i].charCodeAt(0));
					}
					ret.push(new Uint8Array(uintArray));
					uintArray = [];
					idx += i;
					var left = length - idx;
					blockSize = block > left ? left : block;
				}
				return ret;
			};

			function int2bytes(val) {
				var ret = [];
				for (var i = 0; i < 4; i++) {
					ret[3 - i] = val >> (i * 8) & 0xFF;
				}
				return ret;
			};

			function uintToString(uintArray) {
				var encodedString = String.fromCharCode.apply(null, uintArray),
					decodedString = decodeURIComponent(escape(atob(encodedString)));
				return decodedString;
			};

			function sliceData(bytes, block) {

				var ret = [],
					len = bytes.length,
					pos = 0;
				while ((pos + block) < len) {
					ret.push(new Uint8Array(bytes.slice(pos, pos + block)));
					pos += block;
				}
				ret.push(new Uint8Array(bytes.slice(pos, len)));
				return ret;
			};

			function sendEvent(evt, socket) {
				/*				var string = unescape(encodeURIComponent(string));
								var str = json2str(evt);
								var string = unescape(encodeURIComponent(str));
								if (string.length < this.sliceSize) {

									socket.send(str);
									return;
								}
								//console.log(str);
								var arr = stringToUint(string, 10240);
								var l = string.length;*/
				//evt.data=HZH.HZHModelWrapper.wrap(evt.data);
				var op = new hzh.Output();
				evt.write(op);
				var arr = sliceData(op.array, 2048);
				var l = op.array.length;
				var sent = 0;
				socket.send(new Uint8Array([0xAA, 0xAA, 0xAA, 0xAA].concat(int2bytes(l))));
				var ic = 0;
				for (var i = 0; i < arr.length; i++) {
					sent = sent + arr[i].length;
					socket.send(arr[i]);
					ic++
				}
				console.log("sent:" + sent + ",ic:" + ic);
				socket.send(new Uint8Array([0xAA, 0xAA, 0xAA, 0xAA]));
			};

			function extendConnector() {
				HZH.copy(Connector.prototype, {
					socket: null,
					localPeer: null,
					remotePeer: null,
					opened: false,
					sliceSize: 10240,
					inputReader: null,
					listener: {
						onopen: function() {},
						onclose: function() {},
						onevent: function() {},
						onerror: function() {}
					},
					connect: function(uri) {
						this.socket = new WebSocket(uri);
						this.socket.binaryType = 'arraybuffer';//
						this.init(this.socket);
					},
					init: function(ws) {
						var $this = this;
						this.inputReader = new hzhinput();
						ws.onopen = function() {
							console.log("WebSocket opened!");
							$this.send(new hzh.models.HZHEvent(0x01000001, $this.localPeer));

							//ws.send(str);
						};
						ws.onmessage = function(evt) {
							//console.log(evt.data);
							var reader = $this.inputReader;
							reader.reset(new Uint8Array(evt.data));
							/*							var blob = new Blob([reader.buffer], {type: "application/octet-stream"}),
							               				 url = window.URL.createObjectURL(blob);
							               				var a=$("<a/>");a.appendTo($(window.document.body));
							               				a=a[0];
							            				a.href = url;
							            			a.download = "a.bin";
							            			a.click();*/
							/*var data = string2json(evt.data);*/
							var data = reader.readObject();
							if (data && data._type == 1000) {
								$this.onEvent(data);
							}
						};
						ws.onclose = function(evt) {
							console.log("WebSocketClosed!");
							$this.opened = false;
							$this.clearHeartBeat();
							$this.listener.onclose($this);
						};
						ws.onerror = function(evt) {
							console.log("WebSocketError!");
							$this.listener.onerror($this, evt);
						};
					},
					send: function(evt) {
						evt.source = this.localPeer.id;
						evt.timestamp = new Date().getTime();
						sendEvent(evt, this.socket);
					},
					close: function() {
						this.socket.close();
						this.opened = false;
					},
					onEvent: function(evt) {
						var $this = this;
						if (handleInner(evt)) {
							return;
						}
						$this.listener.onevent(evt, $this);
						function handleInner(e) {
							switch (e.type) {
								case 0x01000002:
									onOpen($this, e);
									return true;
								case 0x01000003:
									$this.close();
									return true;
								case 0x01000004:
									$this.close();
									return true;
								case 0x02000005:
									$this.onEvent(e.data);
									return true;
							}
							return false;
						};
					},
					heatBeatTicket: 0,
					scheduleHeartBeat: function() {
						var $this = this;
						this.clearHeartBeat();
						this.heatBeatTicket = setInterval(function() {
							console.log("sending heart beat event");
							$this.send(new hzh.models.HZHEvent(0x01000005, $this.localPeer));
						}, this.remotePeer.timeout * 1000);
					},
					clearHeartBeat: function() {
						clearInterval(this.heatBeatTicket);
					}
				});

				function onOpen(con, evt) {
					con.opened = true;
					con.remotePeer = evt.data;
					con.listener.onopen(con);
					con.scheduleHeartBeat();
					//alert(con.remotePeer.id);
				};
			};
			extendConnector();
			return Connector;
		}
		/*****************Connector end*****************************************/

	/*****************HZH API***********************************************/
	function openHZHAPI(conUri, globalCallback) {
		var requestMap = {};
		var requestList = [];
		var interval = 3000;
		var checkInterval = -1;
		var timeout = 10000;
		var callbackMap = {};
		var instance = null;
		var token = null;
        var sign = null;
		var user = null;
		var subscribeMap = {};
		var requestQueue = [];
		var connected = false;
		var connector = null;
		var callback = {
			onEvent: function(eve, sender) {
				console.log(eve);
			},
			onResponse: function(res, sender) {},
			onException: function(error, sender) {},
			onAuthurized: function(data, sender) {},
			onConnected: function(sender) {},
			onDisconnected: function(sender) {},
			onAuthurizeNeeded: function(code, data, sender) {},
			onError: function(error, sender) {}
		};

		function checkTimeout() {
			checkInterval = -1;
			var nd = new Date().getTime();
			var rmlist = [];
			for (var i = 0; i < requestList.length; i++) {
				var rid = requestList[i];
				var rd = requestMap[rid];
				if (!rd) {
					rmlist.push(rid);
				} else {
					if ((nd - rd.time) >= timeout) { //the request is timed out
						delete requestMap[rid];
						rmlist.push(rid);
						onTimeout(rd);
					}
				}
			}
			for (var i = 0; i < rmlist.length; i++) {

				removeFromArray(requestList, rmlist[i]);
			}
			scheduleTimeoutChecking();
		};

		function removeFromArray(arr, item) {
			arr.splice($.inArray(item, arr), 1)
		};
		/**
		 * * shedule a timeout checking task
		 */
		function scheduleTimeoutChecking() {
			//has on-going request and 
			if (requestList.length > 0 && checkInterval < 0) {
				checkInterval = setTimeout(checkTimeout, interval); //schedule next check
			}
		};

		function onTimeout(rd) {
			onResponse(createResponse(consts.CODE_TIME_OUT, rd.request, null));
		};

		function onResponse(res) {
			var rid = res.request.id;
			var cb = callbackMap[rid];
			if (cb) {
				cb(res);
				delete callbackMap[rid];
			} else {
				callback.onResponse(res, instance);
			}
		};

		function createResponse(code, request, data) {
			return {
				response: new MODEL.HZHResponse(data, code, request.id),
				request: request
			};
		};

		function onConnectorOpen(con) {
			connector = con;
			connected = true;
			checkRequestQueue();
			callback.onConnected(instance);
		};

		function onConnectorError(con, err) {
			callback.onError(err, instance);
		};

		function onConnectorClose(con) {
			connector = null;
			connected = false;
			callback.onDisconnected(instance);
		};

		function onConnectorResponse(res, eve) {
			if (handleResponseInternally(res, eve)) {
				return;
			}
			var reqId = res.requestId;
			if (!reqId)
				return; //not a valid response;
			var req = requestMap[reqId];
			if (!req) {
				return; //the request is probably timed out,ignore it for now
			}
			//requestList.remove(reqId);
			removeFromArray(requestList, reqId);
			delete requestMap[reqId];
			onResponse({
				request: req.request,
				response: res
			});

		};

		function handleResponseInternally(res, eve) {
			if (eve.type == consts.EVENT_RESPONSE_LOGIN) {
				handleLoginResponse(res, eve);
				return true;
			}
			if (res.code == consts.CODE_LOGINNEEDED || res.code == consts.CODE_ACCESSDENIED) {
				callback.onAuthurizeNeeded(res.code, res.data, instance);
				return true;
			}
			return false;
		};

		function handleLoginResponse(res, eve) {
			if (res.code == consts.CODE_OK) { //logined in
				token = res.data.token;
                sign = res.data.sign;
				$.jStorage.set(consts.KEY_HZH_TOKEN, token);
                $.jStorage.set(consts.KEY_HZH_SIGN, sign);
				$.jStorage.set(consts.KEY_HZH_USERDATA, res.data);
				resetTokenTTL();
				callback.onAuthurized(res.data, instance);
			} else {
				callback.onAuthurizeNeeded(res.code, res.data, instance);
			}
		};

		function onConnectorEvent(eve, con) {
			if (!eve) {
				return;
			}
			eve = HZH.HZHModelWrapper.unwrap(eve);
			if (eve.data && eve.data._type == 3002) { //it's a response
				onConnectorResponse(eve.data, eve);
			} else {
				if (subscribeMap[eve.type]) {

					if (subscribeMap[eve.type](eve, instance) == true) {
						return;
					}
				}
				callback.onEvent(eve, instance);
			}
		};

		function resetTokenTTL() {
			$.jStorage.setTTL(consts.KEY_HZH_TOKEN, consts.TOKEN_EXPIRATION);
			$.jStorage.setTTL(consts.KEY_HZH_USERDATA, consts.TOKEN_EXPIRATION);
		};

		function sendRequest(e) {
			if (connected) {
                doSendRequest(e);
			} else {
				requestQueue.push(e);
			}
		};

		function doSendRequest(e) {
			//var e = new MODEL.HZHEvent(eventType, req,token);
			var req = e.data;
			requestList.push(req.id);
			requestMap[req.id] = {
				request: req,
				time: new Date().getTime()
			};
			connector.send(e);
			scheduleTimeoutChecking();
		};

		function checkRequestQueue() {
			if (connected && requestQueue.length > 0) {
				var queue = requestQueue;
				for (var i = 0; i < queue.length; i++) {
					doSendRequest(queue[i]);
				}
			}
		};
		var api = function(uri, cb) {
			this.connector = new HZH.Connector({
				uri: uri,
				listener: {
					onopen: onConnectorOpen,
					onclose: onConnectorClose,
					onevent: onConnectorEvent,
					onerror: onConnectorError
				}
			});
			if (cb) {
				this.setCallback(cb);
			}
			if ($.jStorage.getTTL(consts.KEY_HZH_TOKEN) > 0) {
				token = $.jStorage.get(consts.KEY_HZH_TOKEN);
				this.user = $.jStorage.get(consts.KEY_HZH_USERDATA);
			} else {
				logout();
			}
			return this;
		};

		function logout() {
			token = null;
			user = null;
			$.jStorage.set(consts.KEY_HZH_TOKEN, null);
			$.jStorage.set(consts.KEY_HZH_USERDATA, null);
		};

		HZH.copy(api.prototype, {
			connected: false,
			connector: null,
			user: null,
			request: function(eventType, requestType, data, cb) {
                data['token']=token;
                data['sign'] = sign;
				var req = new MODEL.HZHRequest(new MODEL.HZHMap(data), requestType);
				var e = new MODEL.HZHEvent(eventType, req, token)//var c = new MODEL.HZHEvent(eventType, req, sign);
				if (cb) {
					callbackMap[req.id] = cb;
				}
				sendRequest(e);
			},
			subscribe: function(eventType, cb) {
				subscribeMap[eventType] = cb;
			},
			unsubscribe: function(eventType) {
				delete subscribeMap[eventType];
			},
			setToken:function(newtoken){
				token=newtoken;
			},
			getToken:function(){
				return token;
			},
			/**
			 * * login
			 */
			login: function(username, password) {
				this.request(0x03010001, "login", {
					username: username,
					password: password
				});
			},
			logout: function() {
				logout();
			},
			isLogin: function() {
				token = $.jStorage.get(consts.KEY_HZH_TOKEN);
                sign = $.jStorage.get(consts.KEY_HZH_SIGN);
				if (token)
					return true;
				return false;
			},
			setCallback: function(cb) {
				HZH.copy(callback, cb, false);
			}
		});
		instance = new api(conUri, globalCallback);
		return instance;
	};
	HZH.openApi = openHZHAPI;
	/*****************HZH API end*******************************************/

	/**********************BASE64 start**************************************************/
	/**
	 *create by 2012-08-25 pm 17:48
	 *@author hexinglun@gmail.com
	 *BASE64 Encode and Decode By UTF-8 unicode
	 *可以和java的BASE64编码和解码互相转化
	 */
	(function(hzh) {
		var BASE64_MAPPING = [
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
			'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
			'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
			'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
			'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
			'w', 'x', 'y', 'z', '0', '1', '2', '3',
			'4', '5', '6', '7', '8', '9', '+', '/'
		];

		/**
		 *ascii convert to binary
		 */
		var _toBinary = function(ascii) {
			var binary = new Array();
			while (ascii > 0) {
				var b = ascii % 2;
				ascii = Math.floor(ascii / 2);
				binary.push(b);
			}
			/*
			var len = binary.length;
			if(6-len > 0){
				for(var i = 6-len ; i > 0 ; --i){
					binary.push(0);
				}
			}*/
			binary.reverse();
			return binary;
		};

		/**
		 *binary convert to decimal
		 */
		var _toDecimal = function(binary) {
			var dec = 0;
			var p = 0;
			for (var i = binary.length - 1; i >= 0; --i) {
				var b = binary[i];
				if (b == 1) {
					dec += Math.pow(2, p);
				}
				++p;
			}
			return dec;
		};

		/**
		 *unicode convert to utf-8
		 */
		var _toUTF8Binary = function(c, binaryArray) {
			var mustLen = (8 - (c + 1)) + ((c - 1) * 6);
			var fatLen = binaryArray.length;
			var diff = mustLen - fatLen;
			while (--diff >= 0) {
				binaryArray.unshift(0);
			}
			var binary = [];
			var _c = c;
			while (--_c >= 0) {
				binary.push(1);
			}
			binary.push(0);
			var i = 0,
				len = 8 - (c + 1);
			for (; i < len; ++i) {
				binary.push(binaryArray[i]);
			}

			for (var j = 0; j < c - 1; ++j) {
				binary.push(1);
				binary.push(0);
				var sum = 6;
				while (--sum >= 0) {
					binary.push(binaryArray[i++]);
				}
			}
			return binary;
		};

		var __BASE64 = {
			/**
			 *BASE64 Encode
			 */
			encoder: function(str) {
				var base64_Index = [];
				var binaryArray = [];
				for (var i = 0, len = str.length; i < len; ++i) {
					var unicode = str.charCodeAt(i);
					var _tmpBinary = _toBinary(unicode);
					if (unicode < 0x80) {
						var _tmpdiff = 8 - _tmpBinary.length;
						while (--_tmpdiff >= 0) {
							_tmpBinary.unshift(0);
						}
						binaryArray = binaryArray.concat(_tmpBinary);
					} else if (unicode >= 0x80 && unicode <= 0x7FF) {
						binaryArray = binaryArray.concat(_toUTF8Binary(2, _tmpBinary));
					} else if (unicode >= 0x800 && unicode <= 0xFFFF) { //UTF-8 3byte
						binaryArray = binaryArray.concat(_toUTF8Binary(3, _tmpBinary));
					} else if (unicode >= 0x10000 && unicode <= 0x1FFFFF) { //UTF-8 4byte
						binaryArray = binaryArray.concat(_toUTF8Binary(4, _tmpBinary));
					} else if (unicode >= 0x200000 && unicode <= 0x3FFFFFF) { //UTF-8 5byte
						binaryArray = binaryArray.concat(_toUTF8Binary(5, _tmpBinary));
					} else if (unicode >= 4000000 && unicode <= 0x7FFFFFFF) { //UTF-8 6byte
						binaryArray = binaryArray.concat(_toUTF8Binary(6, _tmpBinary));
					}
				}

				var extra_Zero_Count = 0;
				for (var i = 0, len = binaryArray.length; i < len; i += 6) {
					var diff = (i + 6) - len;
					if (diff == 2) {
						extra_Zero_Count = 2;
					} else if (diff == 4) {
						extra_Zero_Count = 4;
					}
					//if(extra_Zero_Count > 0){
					//	len += extra_Zero_Count+1;
					//}
					var _tmpExtra_Zero_Count = extra_Zero_Count;
					while (--_tmpExtra_Zero_Count >= 0) {
						binaryArray.push(0);
					}
					base64_Index.push(_toDecimal(binaryArray.slice(i, i + 6)));
				}

				var base64 = '';
				for (var i = 0, len = base64_Index.length; i < len; ++i) {
					base64 += BASE64_MAPPING[base64_Index[i]];
				}

				for (var i = 0, len = extra_Zero_Count / 2; i < len; ++i) {
					base64 += '=';
				}
				return base64;
			},
			/**
			 *BASE64  Decode for UTF-8
			 */
			decoder: function(_base64Str) {
                //var _len = _base64Str.length;
                //var extra_Zero_Count = 0;
                ///**
                //*计算在进行BASE64编码的时候，补了几个0
                //*/
                //if (_base64Str.charAt(_len - 1) == '=') {
					////alert(_base64Str.charAt(_len-1));
					////alert(_base64Str.charAt(_len-2));
					//if (_base64Str.charAt(_len - 2) == '=') { //两个等号说明补了4个0
					//	extra_Zero_Count = 4;
					//	_base64Str = _base64Str.substring(0, _len - 2);
					//} else { //一个等号说明补了2个0
					//	extra_Zero_Count = 2;
					//	_base64Str = _base64Str.substring(0, _len - 1);
					//}
                //}
                //
                //var binaryArray = [];
                //for (var i = 0, len = _base64Str.length; i < len; ++i) {
					//var c = _base64Str.charAt(i);
					//for (var j = 0, size = BASE64_MAPPING.length; j < size; ++j) {
					//	if (c == BASE64_MAPPING[j]) {
					//		var _tmp = _toBinary(j);
					//		/*不足6位的补0*/
					//		var _tmpLen = _tmp.length;
					//		if (6 - _tmpLen > 0) {
					//			for (var k = 6 - _tmpLen; k > 0; --k) {
					//				_tmp.unshift(0);
					//			}
					//		}
					//		binaryArray = binaryArray.concat(_tmp);
					//		break;
					//	}
					//}
                //}
                //
                //if (extra_Zero_Count > 0) {
					//binaryArray = binaryArray.slice(0, binaryArray.length - extra_Zero_Count);
                //}
                //
                //var unicode = [];
                //var unicodeBinary = [];
                //for (var i = 0, len = binaryArray.length; i < len;) {
					//if (binaryArray[i] == 0) {
					//	unicode = unicode.concat(_toDecimal(binaryArray.slice(i, i + 8)));
					//	i += 8;
					//} else {
					//	var sum = 0;
					//	while (i < len) {
					//		if (binaryArray[i] == 1) {
					//			++sum;
					//		} else {
					//			break;
					//		}
					//		++i;
					//	}
					//	unicodeBinary = unicodeBinary.concat(binaryArray.slice(i + 1, i + 8 - sum));
					//	i += 8 - sum;
					//	while (sum > 1) {
					//		unicodeBinary = unicodeBinary.concat(binaryArray.slice(i + 2, i + 8));
					//		i += 8;
					//		--sum;
					//	}
					//	unicode = unicode.concat(_toDecimal(unicodeBinary));
					//	unicodeBinary = [];
					//}
                //}
                //return unicode;

                //*****************************重写************************************

        var base64Str = base64decode(_base64Str);
        return utf8to16(base64Str)
			}
		};

		hzh.BASE64 = __BASE64;
	})(HZH);

	/**********************BASE64 end**************************************************/
	if (ISDEBUG) {
		$.ajaxSetup({
			crossDomain: true
		});
	}

	HZH.models = MODEL;
	HZH.Connector = buildConnector();
	window.hzh = HZH;

function utf8to16(str) {
    var out, i, len, c;
    var char2, char3;

    out = "";
    len = str.length;
    i = 0;
    while(i < len) {
        c = str.charCodeAt(i++);
        switch(c >> 4)
        {
            case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
            // 0xxxxxxx
            out += str.charAt(i-1);
            break;
            case 12: case 13:
            // 110x xxxx   10xx xxxx
            char2 = str.charCodeAt(i++);
            out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
            break;
            case 14:
                // 1110 xxxx  10xx xxxx  10xx xxxx
                char2 = str.charCodeAt(i++);
                char3 = str.charCodeAt(i++);
                out += String.fromCharCode(((c & 0x0F) << 12) |
                ((char2 & 0x3F) << 6) |
                ((char3 & 0x3F) << 0));
                break;
        }
    }

    return out;
}

function base64decode(str) {
    var c1, c2, c3, c4;
    var i, len, out;

    len = str.length;
    i = 0;
    out = "";
    while(i < len) {
        /* c1 */
        do {
            c1 = base64DecodeChars[str.charCodeAt(i++) & 0xff];
        } while(i < len && c1 == -1);
        if(c1 == -1)
            break;

        /* c2 */
        do {
            c2 = base64DecodeChars[str.charCodeAt(i++) & 0xff];
        } while(i < len && c2 == -1);
        if(c2 == -1)
            break;

        out += String.fromCharCode((c1 << 2) | ((c2 & 0x30) >> 4));

        /* c3 */
        do {
            c3 = str.charCodeAt(i++) & 0xff;
            if(c3 == 61)
                return out;
            c3 = base64DecodeChars[c3];
        } while(i < len && c3 == -1);
        if(c3 == -1)
            break;

        out += String.fromCharCode(((c2 & 0XF) << 4) | ((c3 & 0x3C) >> 2));

        /* c4 */
        do {
            c4 = str.charCodeAt(i++) & 0xff;
            if(c4 == 61)
                return out;
            c4 = base64DecodeChars[c4];
        } while(i < len && c4 == -1);
        if(c4 == -1)
            break;
        out += String.fromCharCode(((c3 & 0x03) << 6) | c4);
    }
    return out;
}

})(window, $);
